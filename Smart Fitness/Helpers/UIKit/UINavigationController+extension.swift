//
//  NavigationController+extension.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 18.11.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

extension UINavigationController {
    
    func setFullTranslucent() {
        self.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.isTranslucent = true
        self.view.backgroundColor = .clear
        self.navigationBar.barStyle = .black
        self.navigationBar.removeGradient()
    }
    
    func setTitleFont() {
        
        var size: CGFloat = 18
        switch SizeManager.device {
        case .iPhone:
            break
        case .iPad:
            size = 30
        }
        let attributesNormal = [NSAttributedString.Key.font: Constants.returnFontName(style: .semibold, size: size), NSAttributedString.Key.foregroundColor: UIColor.white]
        
        navigationBar.titleTextAttributes = attributesNormal
    }
}
