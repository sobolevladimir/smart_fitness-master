//
//  NSLayoutConstraint+extension.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 13.07.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

extension NSLayoutConstraint {
    
    @IBInspectable
    var proConstant: CGFloat {
        get {
            constant
        }
        set {
            if SizeManager.isPro {
                constant = newValue
            }
        }
    }
}
