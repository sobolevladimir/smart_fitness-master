//
//  UIImageView+Kingfisher.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 17.01.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import Kingfisher
import SwiftyGif

extension UIImageView {
    
    public func downloadImage(from urlString: String?, placeholder: UIImage? = nil, isResizing: Bool = true) {
        guard var urlString = urlString,
            let url = URL(string: urlString) else {
                if placeholder == nil {
                    image = UIImage(named: "placeholder")
                } else {
                    image = placeholder
                }
                return
        }
        
        if let separator = urlString.range(of: "?") {
            urlString.removeSubrange(separator.lowerBound..<urlString.endIndex)
        }
        
        ImageCache.default.memoryStorage.config.totalCostLimit = 1
        let resource = ImageResource(downloadURL: url, cacheKey: urlString)
               
        self.kf.indicatorType = .activity
        var options: KingfisherOptionsInfo? = nil
        
        if isResizing {
            let resizingProcessor = ResizingImageProcessor(referenceSize: .init(width: self.bounds.size.width * UIScreen.main.scale, height: self.bounds.size.height * UIScreen.main.scale) )
            options = [ .processor(resizingProcessor)]
        }
        self.kf.setImage(with: resource, placeholder: placeholder, options: options, progressBlock: nil) { [weak self] (result) in
            self?.removeAnimation()
            switch result {
                
            case .success(_):
               break
            case .failure:
                if placeholder == nil {
                    if let image = UIImage(named: urlString) {
                        self?.image = image
                    } else {
                    }
                    
                } else {
                    self?.image = placeholder
                }
            }
        }
    }
    
    func removeAnimation() {
        self.animationManager?.deleteImageView(self)
    }
    
    
    public func downloadImageFromInstagram(from urlString: String) {
        guard let url = URL(string: urlString) else {
            image = UIImage(named: "placeholder")
            return
        }
        
        var key = urlString
        if let separator = urlString.range(of: "?") {
            key.removeSubrange(separator.lowerBound..<urlString.endIndex)
        }
        
        let resource = ImageResource(downloadURL: url, cacheKey: urlString)
        self.kf.indicatorType = .activity
        self.kf.setImage(with: resource, options: [
            .cacheOriginalImage
        ], progressBlock: nil) {  (result) in
            switch result {
            case .success(_):
                break
            case .failure:
                break
            }
        }
    }
}

extension UIImage {
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    
    
    func jpeg(_ jpegQuality: JPEGQuality) -> Data? {
        return jpegData(compressionQuality: jpegQuality.rawValue)
    }
}

extension UIImage {
    func resize(width: CGFloat) -> UIImage {
        let height = (width/self.size.width)*self.size.height
        return self.resize(size: CGSize(width: width, height: height))
    }
    
    func resize(height: CGFloat) -> UIImage {
        let width = (height/self.size.height)*self.size.width
        return self.resize(size: CGSize(width: width, height: height))
    }
    
    func resize(size: CGSize) -> UIImage {
        let widthRatio = size.width / self.size.width
        let heightRatio = size.height / self.size.height
        var updateSize = size
        if(widthRatio > heightRatio) {
            updateSize = CGSize(width: self.size.width * heightRatio, height: self.size.height * heightRatio)
        } else if heightRatio > widthRatio {
            updateSize = CGSize(width:self.size.width * widthRatio,  height:self.size.height * widthRatio)
        }
        UIGraphicsBeginImageContextWithOptions(updateSize, false, UIScreen.main.scale)
        self.draw(in: CGRect(origin: .zero, size: updateSize))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
}
