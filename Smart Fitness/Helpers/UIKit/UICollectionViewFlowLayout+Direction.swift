//
//  UICollectionViewFlowLayout+Direction.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 09.12.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

extension UICollectionViewFlowLayout {

    override open var flipsHorizontallyInOppositeLayoutDirection: Bool {
        switch LocalizationManager.Language.language {
            case .english:
               return false
            case .hebrew:
              return  true
        }
    }
}
