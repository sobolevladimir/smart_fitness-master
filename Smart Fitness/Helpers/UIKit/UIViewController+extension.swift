//
//  UIViewController+extension.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 21.11.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit
import Toast_Swift

extension UIViewController {
    func showAlert(text: String, isCenter: Bool = false) {
        var style = ToastStyle()
        var position: ToastPosition = .bottom
        position = isCenter ? .center : .bottom
        switch LocalizationManager.Language.language {
            
        case .english:
             style.messageAlignment = .left
        case .hebrew:
             style.messageAlignment = .right
        }
        self.view.makeToast(text, duration: 3.0, position: position, style: style)
    }
}

extension UIViewController {
    func fixDirection() {
        switch LocalizationManager.Language.language {
            case .english:
               navigationItem.leftBarButtonItem?.image = UIImage(named: "backArrowEn")
            case .hebrew:
                navigationItem.leftBarButtonItem?.image = UIImage(named: "backArrowHe")
        }
    }
}

extension UIViewController {
    func setBarItemsFont() {
        
        var size: CGFloat = 14
        switch SizeManager.device {
        case .iPhone:
            break
        case .iPad:
            size = 28
        }
        let attributesNormal = [NSAttributedString.Key.font: Constants.returnFontName(style: .semibold, size: size), NSAttributedString.Key.foregroundColor: UIColor.white]
        
        let attributesDisabled = [NSAttributedString.Key.font: Constants.returnFontName(style: .semibold, size: size), NSAttributedString.Key.foregroundColor: UIColor(white: 1, alpha: 0.7)]
        
        navigationItem.leftBarButtonItem?.setTitleTextAttributes(attributesNormal, for: .normal)
        navigationItem.leftBarButtonItem?.setTitleTextAttributes(attributesDisabled, for: .disabled)
        
        navigationItem.rightBarButtonItem?.setTitleTextAttributes(attributesNormal, for: .normal)
        navigationItem.rightBarButtonItem?.setTitleTextAttributes(attributesDisabled, for: .disabled)
    }
}

