//
//  String+localized.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 11.11.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

extension String {
    func localized() -> String {
        let language = LocalizationManager.Language.language.rawValue
        let path = Bundle.main.path(forResource: language, ofType: "lproj")
        let bundle = Bundle(path: path!)
        return bundle?.localizedString(forKey: self, value: nil, table: nil) ?? self
    }
}

extension String {
    func image() -> UIImage? {
       UIImage(named: self)
    }
}


extension String {
    func toDouble() -> Double? {
        let numberFormatter = NumberFormatter()
        numberFormatter.locale = Locale(identifier: "en_US_POSIX")
        numberFormatter.minimumFractionDigits = 2
        return numberFormatter.number(from: self)?.doubleValue
    }
}
