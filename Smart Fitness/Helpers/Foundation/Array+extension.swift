//
//  Array+extension.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 03.12.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import Foundation

extension Array {
    mutating func mutateEach(_ body: (inout Element) -> Void) {
        for index in self.indices {
            body( &self[index] )
        }
    }
}
