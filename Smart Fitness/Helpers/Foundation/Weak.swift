//
//  Weak.swift
//  FitStar
//
//  Created by Ruslan Khamskyi on 8/5/19.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import Foundation

struct Weak<Object: AnyObject> {
    weak var value: Object?
}
