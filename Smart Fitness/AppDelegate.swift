//
//  AppDelegate.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 13/03/19.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import FBSDKLoginKit
import GoogleSignIn
import TwitterKit
import Firebase
//import NotificationCenter
import AppLovinSDK
import Purchases
import Messages

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var orientationLock = UIInterfaceOrientationMask.portrait
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        
        LocalizationManager.setup(usePreferredLanguage: true)
        
        initFacebookSDK(application, launchOptions: launchOptions)
        
        initGoogleSDK()
        
        initTwitter()
        
        ALSdk.initializeSdk()
        
        Purchases.debugLogsEnabled = true
        Purchases.configure(withAPIKey: "qYILGqCPsnAWfqSdyOKngcvHTxNwkvTQ")
        
        RedirectManager.shared.subscriptionManager = SmartFitnessProducts()
        
        if launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] != nil {
            UserDefaults.standard.set("push", forKey: "push")
        }
        
        configureFirebase()
        
        configurePushNotification()
        
        
        RedirectManager.shared.presentRootController()
        
        return true
    }
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return self.orientationLock
    }
    
    //get error here
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error:
        Error) {
     
    }
    
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) {
            self.handleIncomingDynamicLink(dynamicLink)
            return true
        }
        return ApplicationDelegate.shared.application(app, open: url, options: options) || GIDSignIn.sharedInstance().handle(url) || TWTRTwitter.sharedInstance().application(app, open: url, options: options) || application(app, open: url,
            sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,annotation: "")
    }
    
    func handleIncomingDynamicLink(_ dynamicLink: DynamicLink) {
        guard let url = dynamicLink.url else { return }
        guard let components = URLComponents(url: url, resolvingAgainstBaseURL: false),
            let queryItems = components.queryItems else { return }
        for item in queryItems {
            UserDefaults.standard.set(item.value, forKey: Constants.deepLink)
        }
        RedirectManager.shared.presentRootController()
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return false
    }
    
    func applicationWillResignActive(_ application: UIApplication) { }
    
    func applicationDidEnterBackground(_ application: UIApplication) { }
    
    func applicationWillEnterForeground(_ application: UIApplication) { }
    
    func applicationDidBecomeActive(_ application: UIApplication) { }
    
    func applicationWillTerminate(_ application: UIApplication) { }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity,
                     restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        if let incomingURL = userActivity.webpageURL {
            let linkHandled = DynamicLinks.dynamicLinks().handleUniversalLink(incomingURL, completion: { (dynamicLink, error) in
                if let _ = error {
                } else if let dynamicLink = dynamicLink {
                    self.handleIncomingDynamicLink(dynamicLink)
                }
            })
            return linkHandled
        }
        return false
    }
}

extension AppDelegate {
    
    private func initFacebookSDK(_ application: UIApplication, launchOptions: [UIApplication.LaunchOptionsKey: Any]?) {
        Settings.isAutoLogAppEventsEnabled = true
        Settings.isAdvertiserIDCollectionEnabled = true
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    private func initTwitter() {
        TWTRTwitter.sharedInstance().start(withConsumerKey: Constants.twitterConsumerKey, consumerSecret: Constants.twitterConsumerSecret)
    }
    
    private func initGoogleSDK() {
        GIDSignIn.sharedInstance().clientID = Constants.googleClientId
    }
    
    static var shared: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    
    func configureFirebase() {
        FirebaseApp.configure()
    }
    
    func configurePushNotification() {
        UNUserNotificationCenter.current().delegate = self
        
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        
        UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { (granted, error) in
            
            if granted == true {
                UNUserNotificationCenter.current().delegate = self
                Messaging.messaging().delegate = self
                
                DispatchQueue.main.async {
                    let application = UIApplication.shared
                    application.registerForRemoteNotifications()
                    application.applicationIconBadgeNumber = 0
                }
                
            } else {
               
            }
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: () -> Void) {
        
        UserDefaults.standard.set("push", forKey: "push")
        RedirectManager.shared.presentRootController()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: (UNNotificationPresentationOptions) -> Void) {
        let application = UIApplication.shared
        application.applicationIconBadgeNumber = 0
        completionHandler([.alert, .badge, .sound])
        
    }
}

extension AppDelegate: MessagingDelegate {
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
      
    }
}

