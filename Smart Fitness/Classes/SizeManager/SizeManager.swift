//
//  SizeManager.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 14.01.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

class SizeManager {
    
    enum Device {
        case iPhone, iPad
    }
    
    static private(set) var device: Device = .iPhone
    
    static var isPro: Bool {
           if UIScreen.main.bounds.size.height == 1366 {
               return true
           } else {
               return false
           }
       }
    
    class func setDeviceType(_ type: Device) {
        self.device = type
    }
    

    
}
