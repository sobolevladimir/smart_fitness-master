//
//  CardioDataManager.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 28.11.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import Foundation

final class CardioDataCreator {
    
    // MARK: - Public Properties
    var timeData: [[String]] {
        var data = [makeData(minimumValue: 00, maximumValue: 60, step: 1, addZero: true)]
        data += [[":"]]
        data += [makeData(minimumValue: 00, maximumValue: 60, step: 1, addZero: true)]
        return data
    }
    
    var distanceData: [[String]] {
        var data = [makeData(minimumValue: 00, maximumValue: 100, step: 1)]
        if state == .rowingMachine || state == .swimming {
            data = [makeData(minimumValue: 0, maximumValue: 31, step: 1)]
        }
        data += [["."]]
        var step = 10
        if state == .swimming {
            step = 1
        }
        data += [makeData(minimumValue: 00, maximumValue: 100, step: step, addZero: false)]
        if state == .stairClimbing {
            data = [makeData(minimumValue: 0, maximumValue: 26, step: 1)]
        }
        if state == .jumpingRope {
            data = [makeData(minimumValue: 1, maximumValue: 1000, step: 1)]
        }
        return data
    }
    
    var speedData: [[String]] {
        var data = [makeData(minimumValue: 00, maximumValue: 26, step: 1)]
        data += [["."]]
        data += [makeData(minimumValue: 00, maximumValue: 10, step: 1)]
        if state == .cycling || state == .elliptical {
            data = [makeData(minimumValue: 1, maximumValue: 26, step: 1)]
        }
        if state == .rowingMachine {
            data = [[Constants.Cardio().mediumString,
                     Constants.Cardio().easyString,
                     Constants.Cardio().hardString,
                     Constants.Cardio().racePaceString,
                     Constants.Cardio().warmUpString,
                     Constants.Cardio().coolDownString,
                     Constants.Cardio().restString]]
        }
        if state == .swimming {
            data = [[Constants.Cardio().frontCrowlString,
                     Constants.Cardio().breaststrokeString,
                     Constants.Cardio().backStrokeString,
                     Constants.Cardio().butterflyStrokeString]]
        }
        if state == .stairClimbing {
            data = [makeData(minimumValue: 0, maximumValue: 1000, step: 1)]
        }
        if state == .jumpingRope {
            data = [[Constants.Circuit().normal, Constants.Circuit().fast, Constants.Circuit().slow]]
        }
        return data
    }
    
    var inclineData: [[String]] {
        var data = [makeData(minimumValue: 00, maximumValue: 21, step: 1)]
        data += [["."]]
        data += [makeData(minimumValue: 00, maximumValue: 10, step: 5)]
        if state == .cycling {
            data = [makeData(minimumValue: 10, maximumValue: 202, step: 2)]
            data[0].insert("0", at: 0)
            data += [[Constants.defaultRPMandSPMSeparator]]
            data += [makeData(minimumValue: 20, maximumValue: 222, step: 2)]
            data[2].insert("0", at: 0)
        }
        if state == .rowingMachine {
            data = [makeData(minimumValue: 0, maximumValue: 10, step: 1, addZero: true)]
            data += [[":"]]
            data += [makeData(minimumValue: 0, maximumValue: 60, step: 1, addZero: true)]
        }
        if state == .stairClimbing {
            data = [makeData(minimumValue: 0, maximumValue: 91, step: 1)]
            data += [[Constants.defaultRPMandSPMSeparator]]
            data += [makeData(minimumValue: 0, maximumValue: 91, step: 1)]
        }
        if state == .swimming {
            data = [[Constants.Cardio().normalString,
                     Constants.Cardio().sprintString,
                     Constants.Cardio().noLegsString,
                     Constants.Cardio().noHandsString,
                     Constants.Cardio().breathHoldString]]
        }
        return data
    }
    
    var paceData: [[String]] {
        var data = [makeData(minimumValue: 00, maximumValue: 60, step: 1)]
        data += [["."]]
        data += [makeData(minimumValue: 00, maximumValue: 60, step: 10)]
        if state == .elliptical {
            data = [makeData(minimumValue: 10, maximumValue: 222, step: 2)]
            data[0].insert("0", at: 0)
            data += [[Constants.defaultRPMandSPMSeparator]]
            data += [makeData(minimumValue: 20, maximumValue: 232, step: 2)]
            data[2].insert("0", at: 0)
        }
        if state == .rowingMachine {
            data = [makeData(minimumValue: 0, maximumValue: 91, step: 1)]
            data += [[Constants.defaultRPMandSPMSeparator]]
            data += [makeData(minimumValue: 0, maximumValue: 81, step: 1)]
        }
        if state == .swimming {
            data = [[Constants.Cardio().mainSetString,
                     Constants.Cardio().warmUpString,
                     Constants.Cardio().coolDownString,
                     Constants.Cardio().restString]]
        }
        return data
    }
    
    // MARK: - Private properties
    private var state: CardioActivity
    
    // MARK: - Init
    init(state: CardioActivity) {
        self.state = state
    }
    
    // MARK: - Public Methods
    func set(state: CardioActivity) {
        self.state = state
    }
    
    // MARK: - Private
    private func makeData(minimumValue: Int, maximumValue: Int, step: Int, addZero: Bool = false) -> [String] {
        var data = [String]()
        for step in stride(from: minimumValue, to: maximumValue, by: step) {
            if addZero {
                if "\(step)".count == 1 {
                    data.append("0\(step)")
                } else {
                    data.append("\(step)")
                }
            } else {
                
                data.append("\(step)")
            }
        }
        return data
    }
}
