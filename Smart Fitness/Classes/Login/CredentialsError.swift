//
//  CredencialError.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 28.10.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import Foundation

enum CredentialsError: Error {
    case usernameEmpty
    case usernameHasSpace
    case userAlreadyExists(message: String)
    case passwordEmpty
    case emailIsEmpty
    case emailIsNotValid
    case emailIsNotValidLength
    case emailAlreadyExists(message: String)
    case userIsNotFound(message: String)
    case wrongPassword(message: String)
    case wrongEmailUserName
    case userNameAndPassword
    case other(message: String)
}

extension CredentialsError: LocalizedError {
    var localizedDescription: String {
        switch self {
        case .passwordEmpty: return Constants.LoginFlow.LocalizedDescription().passwordEmpty
        case .usernameEmpty: return Constants.LoginFlow.LocalizedDescription().usernameEmpty
        case .emailIsEmpty: return Constants.LoginFlow.LocalizedDescription().emailIsEmpty
        case .emailIsNotValid: return Constants.LoginFlow.LocalizedDescription().emailIsNotValid
        case .emailIsNotValidLength: return Constants.LoginFlow.LocalizedDescription().emailIsNotValidLength
        case .usernameHasSpace: return Constants.LoginFlow.LocalizedDescription().usernameHasSpace
        case .userIsNotFound(let msg): return msg
        case .wrongPassword(let msg): return msg
        case .userAlreadyExists(let message): return message
        case .emailAlreadyExists(let message): return message
        case .wrongEmailUserName: return Constants.LoginFlow.LocalizedDescription().emailTaken
        case .userNameAndPassword: return Constants.LoginFlow.LocalizedDescription().passwordEmpty
        case .other(message: let message):
            return message
        }
    }
    
    var failureReason: String? {
        switch self {
        case .passwordEmpty: return Constants.LoginFlow.FailureReason().passwordEmpty
        case .usernameEmpty: return Constants.LoginFlow.FailureReason().usernameEmpty
        case .emailIsEmpty: return Constants.LoginFlow.FailureReason().emailIsEmpty
        case .emailIsNotValid: return Constants.LoginFlow.FailureReason().emailIsNotValid
        case .emailIsNotValidLength: return Constants.LoginFlow.FailureReason().emailIsNotValidLength
        case .usernameHasSpace: return Constants.LoginFlow.FailureReason().usernameHasSpace
        case .userIsNotFound: return ""
        case .wrongPassword: return ""
        case .userAlreadyExists: return ""
        case .emailAlreadyExists: return ""
        case .wrongEmailUserName: return Constants.LoginFlow.LocalizedDescription().usernameTaken
        case .userNameAndPassword: return Constants.LoginFlow.LocalizedDescription().usernameEmpty
        case .other:
            return ""
        }
    }
}
