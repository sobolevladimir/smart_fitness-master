//
//  FacebookLogin.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 24.10.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import FBSDKLoginKit

class FacebookLogin: NSObject {
    static let shared = FacebookLogin()
    private let facebookLoginManager = LoginManager()
    private let facebookPermissions = ["user_photos", "public_profile", "email"]
    
    func loginWithFacebook(from: UIViewController, _ completion: @escaping (Result<String, Error>) -> Void) {
        facebookLoginManager.logIn(permissions: facebookPermissions, from: from) { (result, error) in
            if let error = error {
                completion(.failure(error))
            }
            if let result = result {
                guard let token = result.token?.tokenString else { return }
                completion(.success(token))
            }
        }
    }
}
