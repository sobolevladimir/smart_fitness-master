//
//  AppleLogin.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 03.02.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import Foundation
import AuthenticationServices

typealias AppleCompletionHandler = (_ token: String?, _ error: Error?) -> Void

class AppleLogin: NSObject {
    
    // MARK: - Properties
    static let shared = AppleLogin()
    var handler: AppleCompletionHandler?
    
    // MARK: - Methods
    
    func loginWithApple(from: UIViewController, _ completion: @escaping AppleCompletionHandler) {
        
        if #available(iOS 13.0, *) {
            
            let request = ASAuthorizationAppleIDProvider().createRequest()
            request.requestedScopes = [.fullName, .email]
            
            let controller = ASAuthorizationController(authorizationRequests: [request])
            controller.delegate = self
            controller.performRequests()
            handler = completion
        }
    }
}

// MARK: - ASAuthorizationControllerDelegate

extension AppleLogin: ASAuthorizationControllerDelegate {
    
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
       guard let authorizationCode = appleIDCredential.authorizationCode,
            let authCode = String(data: authorizationCode, encoding: .utf8) else { return }
             handler?(authCode, nil)
        }
    }
    
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        return
    }
    
}
