//
//  GoogleLogin.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 24.10.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import GoogleSignIn

typealias GoogleCompletionHandler = (_ token: String?, _ error: Error?) -> Void

class GoogleLogin: NSObject {
    
    // MARK: - Properties
    static let shared = GoogleLogin()
    var handler: GoogleCompletionHandler?
    
    // MARK: - Methods
    func loginWithGoogle(from: UIViewController, _ completion: @escaping GoogleCompletionHandler) {
        handler = completion
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().presentingViewController = from
        GIDSignIn.sharedInstance().signIn()
    }
}

// MARK: - GIDSignInDelegate
extension GoogleLogin: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            handler?(nil, error)
            handler = nil
            return
        }
        guard let auth = user.authentication else {
            handler?(nil, error)
            handler = nil
            return
        }
        handler?(auth.accessToken, nil)
        handler = nil
    }
}
