//
//  LoginManager.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 24.10.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import TwitterKit

class SocialLoginManager {
    
    typealias LoginCompletion = (Result<Token, Error>) -> Void
    
    func apple(from vc: BaseViewController, _ completion: @escaping LoginCompletion) {
        AppleLogin.shared.loginWithApple(from: vc) { (code, error) in
            if let code = code {
                vc.startAnimation()
                UserProvider.loginWithApple(code: code) { (result) in
                    vc.stopAnimation()
                    switch result {
                    case .success(let userToken):
                        completion(.success(userToken))
                    case .failure(let error):
                        completion(.failure(error))
                    }
                }
                
            } else if let error = error {
                vc.stopAnimation()
                completion(.failure(error))
            }
        }
    }
    
    func facebook(from: BaseViewController, _ completion: @escaping LoginCompletion) {
        FacebookLogin.shared.loginWithFacebook(from: from) { (result) in
            switch result {
            case .success(let token):
                 from.startAnimation()
                UserProvider.loginWithFacebook(token: token) { (response) in
                    from.stopAnimation()
                    switch response {
                        
                    case .success(let userToken):
                        completion(.success(userToken))
                    case .failure(let error):
                        completion(.failure(error))
                    }
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func google(from: BaseViewController, _ completion: @escaping LoginCompletion) {
        GoogleLogin.shared.loginWithGoogle(from: from) { (token, error) in
            if let error = error {
                completion(.failure(error))
            } else if let token = token {
                 from.startAnimation()
                UserProvider.loginWithGoogle(token: token) { (response) in
                     from.stopAnimation()
                    switch response {
                        
                    case .success(let userToken):
                        completion(.success(userToken))
                    case .failure(let error):
                        completion(.failure(error))
                    }
                }
            }
        }
    }
    
    func twitter(from: BaseViewController, _ completion: @escaping LoginCompletion) {
        TWTRTwitter.sharedInstance().logIn(completion: { (session, error) in
            if let error = error {
                completion(.failure(error))
            } else if let session = session {
                from.startAnimation()
                UserProvider.loginTwitter(accessToken: session.authToken, accessTokenSecret: session.authTokenSecret) { (response) in
                    from.stopAnimation()
                    switch response {
                    case .success(let userToken):
                        completion(.success(userToken))
                    case .failure(let error):
                        completion(.failure(error))
                    }
                }
            }
        })
    }
    
    func instagram(from: UIViewController, _ completion: @escaping ((InstagramTestUser) -> Void)) {
        let instagram = InstagramWebLoginViewController()
        instagram.completion = { code in
            completion(code)
        }
        from.present(UINavigationController(rootViewController: instagram), animated: true, completion: nil)
    }
    
    func loginWithEmail(username: String, password: String, _ completion: @escaping (Result<Token, NetworkError>) -> Void) {
        UserProvider.loginUser(username: username, password: password) { (response) in
            switch response {
            case .success(let userToken):
                completion(.success(userToken))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func createNewUser(username: String, email: String, password: String, _ completion: @escaping (Result<Token, NetworkError>) -> Void) {
        UserProvider.registerNewUser(username: username, email: email, password: password) { (response) in
            switch response {
            case .success(let userToken):
                completion(.success(userToken))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
