//
//  InstagramLoginViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 24.10.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit
import WebKit

enum InstagramKeys { // TODO: relocate to plsit
    static let authURI = "https://api.instagram.com/oauth/authorize"
    static let appID = "2154573461510903"
  //  static let appSecret = "738778d8c4467cde4168546c6e06d27a"
    static let redirectURI = "https://github.com/yanaico/Smart-Fitness"
    static let accessToken = "access_token"
    static let instagramScope = "user_profile,user_media"
}

class InstagramWebLoginViewController: UIViewController {
    
    // MARK: - Views
    var webView = WKWebView()
    var instagramApi = InstagramApi()
    var testUserData: InstagramTestUser?
    
    // MARK: - Properties
    var completion: ((InstagramTestUser) -> Void)?
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(dismissVC))
        webView.frame = view.frame
        view.addSubview(webView)
        webView.navigationDelegate = self
        load()
        
    }
    @objc func dismissVC() {
        dismiss(animated: true, completion: nil)
    }
    
    func load() {
        instagramApi.authorizeApp { (url) in
                   DispatchQueue.main.async {
                    self.webView.load(URLRequest(url: url!))
                   }
               }
    }
    
    func checkRequestForCallbackURL(request: URLRequest) -> Bool {
        let requestURLString = (request.url?.absoluteString)! as String
        if requestURLString.hasPrefix(InstagramKeys.redirectURI) {
            let range: Range<String.Index> = requestURLString.range(of: "code=")!
            let codeFull = String(requestURLString[range.upperBound...])
            let rangeEnd: Range<String.Index> = codeFull.range(of: "#")!
            let code = String(codeFull[..<rangeEnd.lowerBound])
           // completion?(code)
            dismissVC()
            return false
        }
        return true
    }
}

extension InstagramWebLoginViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
               let request = navigationAction.request
        self.instagramApi.getTestUserIDAndToken(request: request) { (instagramTestUser) in
            self.testUserData = instagramTestUser
            self.completion?(instagramTestUser)
            DispatchQueue.main.async {
                  self.dismissVC()
            }
          
                  // self.parent.presentAuth = false
               }
               decisionHandler(WKNavigationActionPolicy.allow)
           }
}

class InstagramApi {
    
    //MARK:- Member variables
    static let shared = InstagramApi()
    
    private let instagramAppID = "2154573461510903"
    
 //   private let redirectURIURLEncoded = "https%3A%2F%2Fwww.hotcocoasoftware.com%2F"
    
    private let redirectURI = "https://github.com/yanaico/Smart-Fitness"
    
    private let app_secret = "738778d8c4467cde4168546c6e06d27a"
    
    private let boundary = "boundary=\(NSUUID().uuidString)"
    
    //MARK:- Enums
    private enum BaseURL: String {
        case displayApi = "https://api.instagram.com/"
        case graphApi = "https://graph.instagram.com/"
    }
    
    private enum Method: String {
        case authorize = "oauth/authorize"
        case access_token = "oauth/access_token"
    }
    
    //MARK:- Constructor
    init() {}
    
    //MARK:- Private Methods
    private func getFormBody(_ parameters: [[String : String]], _ boundary: String) -> Data {
        var body = ""
        let error: NSError? = nil
        for param in parameters {
            let paramName = param["name"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if let filename = param["fileName"] {
                let contentType = param["content-type"]!
                var fileContent: String = ""
                do { fileContent = try String(contentsOfFile: filename, encoding: String.Encoding.utf8)}
                catch {
                    print(error)
                }
                if (error != nil) {
                    print(error!)
                }
                body += "; filename=\"\(filename)\"\r\n"
                body += "Content-Type: \(contentType)\r\n\r\n"
                body += fileContent
            } else if let paramValue = param["value"] {
                body += "\r\n\r\n\(paramValue)"
            }
        }
        return body.data(using: .utf8)!
    }
    
    private func getTokenFromCallbackURL(request: URLRequest) -> String? {
        let requestURLString = (request.url?.absoluteString)! as String
        if requestURLString.starts(with: "\(redirectURI)?code=") {
            
            print("Response uri:",requestURLString)
            if let range = requestURLString.range(of: "\(redirectURI)?code=") {
                return String(requestURLString[range.upperBound...].dropLast(2))
            }
        }
        return nil
    }
    
    private func getMediaData(testUserData: InstagramTestUser, completion: @escaping (Feed) -> Void) {
        let urlString = "\(BaseURL.graphApi.rawValue)me/media?fields=id,caption&access_token=\(testUserData.access_token)"
        
        let request = URLRequest(url: URL(string: urlString)!)
        
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error in
            if let response = response {
                print(response)
            }
            do { let jsonData = try JSONDecoder().decode(Feed.self, from: data!)
                print(jsonData)
                completion(jsonData)
            }
            catch let error as NSError {
                print(error)
            }
        })
        task.resume()
    }
    
    //MARK:- Public Methods
    func authorizeApp(completion: @escaping (_ url: URL?) -> Void ) {
         let authURL = String(format: "%@?app_id=%@&redirect_uri=%@&response_type=code&scope=%@", arguments: [InstagramKeys.authURI,InstagramKeys.appID,InstagramKeys.redirectURI, InstagramKeys.instagramScope])
        
        let request = URLRequest(url: URL(string: authURL)!)
        
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error in
            if let response = response {
                print(response)
                completion(response.url)
            }
        })
        task.resume()
    }
    
    func getTestUserIDAndToken(request: URLRequest, completion: @escaping (InstagramTestUser) -> Void){
        
        guard let authToken = getTokenFromCallbackURL(request: request) else {
            return
        }
        
        let headers = [
            "content-type": "multipart/form-data; boundary=\(boundary)"
        ]
        let parameters = [
            [
                "name": "app_id",
                "value": instagramAppID
            ],
            [
                "name": "app_secret",
                "value": app_secret
            ],
            [
                "name": "grant_type",
                "value": "authorization_code"
            ],
            [
                "name": "redirect_uri",
                "value": redirectURI
            ],
            [
                "name": "code",
                "value": authToken
            ]
        ]
        
        var request = URLRequest(url: URL(string: BaseURL.displayApi.rawValue + Method.access_token.rawValue)!)
        
        let postData = getFormBody(parameters, boundary)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request, completionHandler: {(data, response, error) in
            if (error != nil) {
                print(error!)
            } else {
                do { let jsonData = try JSONDecoder().decode(InstagramTestUser.self, from: data!)
                    print(jsonData)
                    completion(jsonData)
                }
                catch let error as NSError {
                    print(error)
                }
                
            }
        })
        dataTask.resume()
    }

    
    func getInstagramUser(testUserData: InstagramTestUser, completion: @escaping (InstagramUser) -> Void) {
        let urlString = "\(BaseURL.graphApi.rawValue)\(testUserData.user_id)?fields=id,username&access_token=\(testUserData.access_token)"
        let request = URLRequest(url: URL(string: urlString)!)
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request, completionHandler: {(data, response, error) in
            if (error != nil) {
                print(error!)
            } else {
                let httpResponse = response as? HTTPURLResponse
                print(httpResponse!)
            }
            do { let jsonData = try JSONDecoder().decode(InstagramUser.self, from: data!)
                completion(jsonData)
            }
            catch let error as NSError {
                print(error)
            }
        })
        dataTask.resume()
    }
    
    func getMedia(testUserData: InstagramTestUser, completion: @escaping (InstagramMedia) -> Void) {
        getMediaData(testUserData: testUserData) { (mediaFeed) in
            
            for index in 0..<mediaFeed.data.count {
                let urlString = "\(BaseURL.graphApi.rawValue + mediaFeed.data[index].id)?fields=id,media_type,media_url,username,timestamp&access_token=\(testUserData.access_token)"
                    let request = URLRequest(url: URL(string: urlString)!)
                    
                    let session = URLSession.shared
                    let task = session.dataTask(with: request, completionHandler: { data, response, error in
                        if let response = response {
                            print(response)
                        }
                        do { let jsonData = try JSONDecoder().decode(InstagramMedia.self, from: data!)
                            print(jsonData)
                            completion(jsonData)
                        }
                        catch let error as NSError {
                            print(error)
                        }
                    })
                    task.resume()
            }
    
        }
    }
    
    func fetchImage(urlString: String, completion: @escaping (Data?) -> Void) {
        let request = URLRequest(url: URL(string: urlString)!)
        
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error in
            if let response = response {
                print(response)
            }
            completion(data)
        })
        task.resume()
    }
   
}

//MARK:- Instagram Users
struct InstagramTestUser: Codable {
    var access_token: String
    var user_id: Int
}

struct InstagramUser: Codable {
    var id: String
    var username: String
}

//MARK:- Instagram Feed
struct Feed: Codable {
    var data: [MediaData]
    var paging : PagingData
}

struct MediaData: Codable {
    var id: String
    var caption: String?
}

struct PagingData: Codable {
    var cursors: CursorData
    var next: String?
}

struct CursorData: Codable {
    var before: String
    var after: String
}

struct InstagramMedia: Codable {
      var id: String
      var media_type: MediaType
      var media_url: String
      var username: String
      var timestamp: String
}

enum MediaType: String,Codable {
    case IMAGE
    case VIDEO
    case CAROUSEL_ALBUM
}
