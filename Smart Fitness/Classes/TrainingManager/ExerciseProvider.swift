//
//  ExerciseProvider.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 15.01.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import Foundation

class ExerciseProvider {
    class func getExercisesForMuscleGroup(_ group: String, completion: @escaping (Result<CategoryJSON, NetworkError>) -> Void) {
        NetworkRouter.performRequest(request: .getAllSubcategories(muscleGroup: group)) { (response) in
            switch response {
            case .success(let successResponse):
                switch Parser.parseResponseData(successResponse.data, for: CategoryJSON.self) {
                case .success(let data):
                    completion(.success(data))
                case .failure(let error):
                    completion(.failure(NetworkError(error: error)))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    class func addExerciseToFavourite(id: Int, completion: @escaping (Result<Bool, NetworkError>) -> Void) {
        NetworkRouter.performRequest(request: .addExerciseToFavourite(id: id)) { (response) in
            switch response {
            case .success:
                completion(.success(true))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    class func removeExerciseFromFavourite(id: Int, completion: @escaping (Result<Bool, NetworkError>) -> Void) {
        NetworkRouter.performRequest(request: .removeExerciseFromFavourite(id: id)) { (response) in
            switch response {
            case .success:
                completion(.success(true))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}

