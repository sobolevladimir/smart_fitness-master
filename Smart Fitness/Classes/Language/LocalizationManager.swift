//
//  LocalizationSystem.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 28.10.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

public class LocalizationManager: NSObject {

    enum Language: String {
        case english = "en"
        case hebrew = "he"
        
        static let userLanguageSavedKey = "UserLanguageSavedKey"
        static var language: Language {
            get {
                guard let language = Language(rawValue: currentLanguage) else { return .english}
                return language
            }
        }
    }
    
    private static var usePreferredLanguage: Bool!

    public static func setup(usePreferredLanguage: Bool = false) {
        LocalizationManager.usePreferredLanguage = usePreferredLanguage
        Bundle.update(language: currentLanguage)
    }

    public static var availableLanguages: [String] {
        return Bundle.main.localizations.filter { $0 != "Base" }
    }

     static var currentLanguage: String {
        get {
            if let currentLanguage = UserDefaults.standard.string(forKey: Language.userLanguageSavedKey) {
                return currentLanguage
            } else if usePreferredLanguage ?? true,
                let preferredLanguage = Bundle.main.preferredLocalizations.first,
                availableLanguages.contains("he") {
                return preferredLanguage
            } else {
                return "Base"
            }
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "Language")
            UserDefaults.standard.setValue(newValue, forKey: Language.userLanguageSavedKey)
            UserDefaults.standard.synchronize()
            Bundle.update(language: newValue)
        }
    }

    public static var isRTL: Bool {
        return Locale.characterDirection(forLanguage: currentLanguage) == .rightToLeft
    }

    public static func setHebrew() {
        LocalizationManager.currentLanguage = Language.hebrew.rawValue
    }
    
    public static func setEnglish() {
        LocalizationManager.currentLanguage = Language.english.rawValue
    }

}

// MARK: Bundle Handling
var bundleKey = 0

public class BundleEx: Bundle {

    public override func localizedString(forKey key: String,
                                         value: String?,
                                         table tableName: String?) -> String {
        guard let bundle = objc_getAssociatedObject(self, &bundleKey) as? Bundle else {
            return super.localizedString(forKey: key, value: value, table: tableName)
        }

        return bundle.localizedString(forKey: key, value: value, table: tableName)
    }
}

extension Bundle {

    static func update(language: String) {
        DispatchQueue.once(sender: self) {
            object_setClass(main, BundleEx.self)
        }
        updateLocalizationAttributes()
        let path = Bundle(path: Bundle.main.path(forResource: language, ofType: "lproj") ?? Bundle.main.path(forResource: "Base", ofType: "lproj")!)
        let bundlePath = path
        objc_setAssociatedObject(Bundle.main, &bundleKey, bundlePath,
                                 objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }
    
    static func updateLocalizationAttributes() {
        let isRTL = LocalizationManager.isRTL
        let semanticContentAttribute: UISemanticContentAttribute = isRTL ? .forceRightToLeft : .forceLeftToRight
        UIView.appearance().semanticContentAttribute = semanticContentAttribute
        UINavigationBar.appearance().semanticContentAttribute = semanticContentAttribute
     
        UserDefaults.standard.set(isRTL, forKey: "AppleTextDirection")
        UserDefaults.standard.set(isRTL, forKey: "NSForceRightToLeftWritingDirection")
        UserDefaults.standard.synchronize()
    }
}

public extension DispatchQueue {

    private static var _onceTracker = [String]()

    static func once(sender: AnyObject, _ block: () -> Void) {
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        func address<T: AnyObject>(object: T) -> String {
            let addr = unsafeBitCast(object, to: Int.self)
            return NSString(format: "%p", addr) as String
        }
        let senderPointer = address(object: sender)
        if _onceTracker.contains(senderPointer) {
            return
        }
        _onceTracker.append(senderPointer)
        block()
    }
}
