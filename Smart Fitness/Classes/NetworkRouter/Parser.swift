//
//  Parser.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 23.12.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import Foundation

class Parser: NSObject {
    static func parseResponseData<T: Decodable>(_ data: Any?, for type: T.Type) -> Result<T, Error> {
        guard let data = data as? Data else { return .failure(NetworkError(code: 0, message: "data is missing")) }
        do {
            let decoder = JSONDecoder()
            let object = try decoder.decode(type, from: data)
            return .success(object)
        } catch let error {
            return .failure(error)
        }
    }
}
