//
//  ProgramProvider.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 07.02.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import Foundation

final class ProgramProvider {
    class func createProgram(program: Program, completion: @escaping (Result<Program, NetworkError>) -> Void) {
        NetworkRouter.performRequest(request: .createProgram(program: program)) { (response) in
            switch response {
            case .success(let successesResponse):
                
                switch Parser.parseResponseData(successesResponse.data, for: Program.self) {
                case .success(let data):
                    completion(.success(data))
                case .failure(let error):
                    completion(.failure(NetworkError(error: error)))
                }
                
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    class func allPrograms(completion: @escaping (Result<Programs, NetworkError>) -> Void) {
        NetworkRouter.performRequest(request: .getAllPrograms) { (response) in
            switch response {
                
            case .success(let successesResponse):
                
                switch Parser.parseResponseData(successesResponse.data, for: Programs.self) {
                case .success(let data):
                    completion(.success(data))
                case .failure(let error):
                    completion(.failure(NetworkError(error: error)))
                }
                
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    class func deleteProgram(id: Int, completion: @escaping (Result<Bool, NetworkError>) -> Void) {
        NetworkRouter.performRequest(request: .deleteProgram(id: id)) { (response) in
            switch response {
            case .success:
                completion(.success(true))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    class func editProgram(id: Int, program: Program, completion: @escaping (Result<Program, NetworkError>) -> Void) {
        NetworkRouter.performRequest(request: .editProgram(id: id, program: program)) { (response) in
            switch response {
            case .success(let successesResponse):
                switch Parser.parseResponseData(successesResponse.data, for: Program.self) {
                case .success(let data):
                    completion(.success(data))
                case .failure(let error):
                    completion(.failure(NetworkError(error: error)))
                }
                
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
