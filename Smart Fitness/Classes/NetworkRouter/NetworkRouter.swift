//
//  NetworkRouter.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 23.12.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import Moya

enum NetworkRouter {
    
    // MARK: - Login
    case loginGoogle(token: String)
    case loginInstagram(code: String)
    case loginFacebook(token: String)
    case loginTwitter(accessToken: String, accessTokenSecret: String)
    case loginEmail(username: String, password: String)
    case appleSignIn(code: String)
    
    // MARK: - Logout
    case logout
    
    // MARK: - Password
    case recoverPassword(email: String)
    
    // MARK: - Create User
    case createUser(username: String, email: String, password: String)
    
    // MARK: - Current User
    case getCurrentUser
    case me
    case editUser(gender: String, level: String, height: Int, weight: Double, birthDay: Double)
    case uploadUserImage(data: Data)
    
    // MARK: - Exercises
    case getAllSubcategories(muscleGroup: String)
    case addExerciseToFavourite(id: Int)
    case removeExerciseFromFavourite(id: Int)
    
    // MARK: - Program
    case createProgram(program: Program)
    case getAllPrograms
    case deleteProgram(id: Int)
    case editProgram(id: Int, program: Program)
    
    // MARK: - Share Program
    case searchUsers(filter: String)
    case sendProgram(userId: Int, programId: Int)
    case getSharedPrograms
    case saveAndDeleteSharedProgram(programId: Int, accept: Bool)
    case deleteSharedProgram(programId: Int)
    case pinProgram(id: Int)
    case linkProgram(programId: Int)
    case fcmToken(userId: Int, deviceType: String, fcmToken: String)
    
    // MARK: - Stories
    case saveStory(data: Data)
    case deleteStory(id: Int)
    case getAllStories
}

// https://api.smartfitnessapp.com
// http://apidev.smartfitnessapp.com
extension NetworkRouter: TargetType {
    var baseURL:  URL { return URL(string: "https://api.smartfitnessapp.com")! }
    
    var path: String {
        switch self {
            
        case .loginGoogle:
            return "/users/auth/google"
        case .loginInstagram:
            return "/users/auth/instagram"
        case .loginFacebook:
            return "/users/auth/facebook"
        case .loginTwitter:
            return "/users/auth/twitter"
        case .loginEmail:
            return "/users/auth"
        case .createUser:
            return "/users"
        case .getCurrentUser:
            return "/users/me"
        case .logout:
            return "/users/logout"
        case .recoverPassword:
            return "users/reset"
        case .getAllSubcategories:
            return "/subcategories"
        case .addExerciseToFavourite(let id), .removeExerciseFromFavourite(let id):
            return "exercises/favourite/\(id)"
        case .me:
            return "/users/me"
        case .editUser:
            return "/users"
        case .uploadUserImage:
            return "/users/image"
        case .createProgram:
            return "/programs"
        case .getAllPrograms:
            return "/programs"
        case .deleteProgram(let id):
            return "/programs/\(id)"
        case .searchUsers:
            return "/users/sharing"
        case .sendProgram(let userId, let programId):
            return "/share/\(userId)/program/\(programId)"
        case .getSharedPrograms:
            return "/share/programs"
        case .saveAndDeleteSharedProgram(let programId, _):
            return "/share/program/\(programId)"
        case .editProgram(let id, _):
            return "/programs/\(id)"
        case .pinProgram(let id):
            return "/share/pin-it/program/\(id)"
        case .appleSignIn:
            return "/users/auth/apple"
        case .linkProgram(let id):
            return "/share/link/program/\(id)"
        case .deleteSharedProgram(let programId):
            return "/share/programs/\(programId)"
        case .fcmToken:
            return "/notification"
        case .saveStory:
            return "/users/story"
        case .deleteStory(let id):
            return "/users/story/\(id)"
        case .getAllStories:
            return "/users/story"
        }
    }
    
    var method: Method {
        switch self {
        case .loginEmail, .loginGoogle, .loginFacebook, .loginInstagram, .loginTwitter, .createUser, .logout, .uploadUserImage:
            return .post
        case .getCurrentUser, .me:
            return .get
        case .recoverPassword:
            return .post
        case .getAllSubcategories:
            return .get
        case .addExerciseToFavourite:
            return .post
        case .removeExerciseFromFavourite:
            return .delete
        case .editUser:
            return .patch
        case .createProgram:
            return .post
        case .getAllPrograms:
            return .get
        case .deleteProgram:
            return .delete
        case .searchUsers, .getSharedPrograms:
            return .get
        case .sendProgram:
            return .post
        case .saveAndDeleteSharedProgram:
            return .post
        case .editProgram:
            return .put
        case .pinProgram:
            return .post
        case .appleSignIn:
            return .post
        case .linkProgram:
            return .post
        case .deleteSharedProgram:
            return .delete
        case .fcmToken:
            return .post
        case .saveStory:
            return .post
        case .deleteStory:
            return .delete
        case .getAllStories:
            return .get
        }
    }
    
    var sampleData: Data {
        switch self {
        default:
            return "server error - sample data".data(using: .utf8)!
        }
    }
    
    var task: Task {
        switch self {
            
        case .loginGoogle(let token):
            return .requestParameters(parameters: ["token": token], encoding: URLEncoding.queryString)
            
        case .loginInstagram(let code):
            return .requestParameters(parameters: ["code": code], encoding: URLEncoding.queryString)
            
        case .loginFacebook(let token):
            return .requestParameters(parameters: ["token": token], encoding: URLEncoding.queryString)
            
        case .loginTwitter(let accessToken, let accessTokenSecret):
            return .requestParameters(parameters: ["accessToken": accessToken, "accessTokenSecret": accessTokenSecret], encoding: JSONEncoding.default)
            
        case .loginEmail(let username, let password):
            return .requestParameters(parameters: ["username": username, "password": password], encoding: JSONEncoding.default)
            
        case .createUser(let username, let email, let password):
            return .requestParameters(parameters: ["username": username, "email": email, "password": password], encoding: JSONEncoding.default)
            
        case .getCurrentUser:
            return .requestPlain
            
        case .logout:
            return .requestPlain
            
        case .recoverPassword(let email):
            return .requestParameters(parameters: ["email": email ], encoding: JSONEncoding.default)
            
        case .getAllSubcategories(let muscleGroup):
            return .requestParameters(parameters: ["muscleGroup": muscleGroup], encoding: URLEncoding.queryString)
            
        case .addExerciseToFavourite, .removeExerciseFromFavourite, .me:
            return .requestPlain
        case .uploadUserImage(let data):
            let formData: [Moya.MultipartFormData] = [Moya.MultipartFormData(provider: .data(data), name: "file", fileName: "user.jpeg", mimeType: "image/jpeg")]
            return .uploadMultipart(formData)
        case .saveStory(let data):
            let formData: [Moya.MultipartFormData] = [Moya.MultipartFormData(provider: .data(data), name: "file", fileName: "user.jpeg", mimeType: "image/jpeg")]
            return .uploadMultipart(formData)
        case .editUser(let gender, let level, let height, let weight, let birthDay):
            let parameters = ["gender": gender, "level": level ,"height": height ,"weight": weight, "birthDay": birthDay] as [String : Any]
            return .requestParameters(parameters: parameters, encoding: JSONEncoding.default)
        case .createProgram(let program):
            return .requestJSONEncodable(program)
        case .getAllPrograms:
            return .requestPlain
        case .deleteProgram:
            return .requestPlain
        case .searchUsers(let filter):
            return .requestParameters(parameters: ["filter": filter], encoding: URLEncoding.queryString)
        case .sendProgram, .getSharedPrograms:
            return .requestPlain
            
        case .saveAndDeleteSharedProgram(_, let accept):
            return .requestParameters(parameters: ["accept": accept], encoding: URLEncoding.queryString)
        case .editProgram(_ , let program):
            return .requestJSONEncodable(program)
        case .pinProgram:
            return .requestPlain
        case .appleSignIn(let code):
            return .requestParameters(parameters: ["code": code], encoding: URLEncoding.queryString)
        case .linkProgram:
            return .requestPlain
        case .deleteSharedProgram:
            return .requestPlain
        case .fcmToken(userId: let userId, deviceType: let deviceType, fcmToken: let fcmToken):
            return .requestParameters(parameters: ["userId": userId, "deviceType": deviceType, "token": fcmToken], encoding: URLEncoding.queryString)
        case .deleteStory:
            return .requestPlain
        case .getAllStories:
            return .requestPlain
        }
    }
    
    var headers: [String : String]? {
        let contentTypeJSON: [String: String] = ["Content-type": "application/json"]
        let accessToken: [String: String] =  ["Authorization": UserAuthManager.token ?? ""]
        var language = "us"
        switch LocalizationManager.Language.language {
        case .english:
            break
        case .hebrew:
            language = "iw"
        }
        let acceptLanguage: [String : String] = ["Accept-Language": language]
        let contentTypeWithToken = contentTypeJSON.merging(accessToken) { $1 }
        let acceptLanguageWithAccessToken = acceptLanguage.merging(accessToken) { $1 }
        let contentTypeWithTokenAndLanguage = contentTypeWithToken.merging(acceptLanguage) { $1 }
        
        switch self {
        case .loginEmail, .loginGoogle, .loginFacebook, .loginInstagram, .loginTwitter, .createUser, .appleSignIn:
            return acceptLanguage
        case .getCurrentUser, .me, .editUser:
            return acceptLanguageWithAccessToken
        case .logout, .uploadUserImage:
            return accessToken
        case .recoverPassword:
            return contentTypeWithTokenAndLanguage
        case .getAllSubcategories:
            return contentTypeWithTokenAndLanguage
        case .addExerciseToFavourite, .removeExerciseFromFavourite:
            return accessToken
        case .createProgram:
            return acceptLanguageWithAccessToken
        case .getAllPrograms, .getSharedPrograms:
            return acceptLanguageWithAccessToken
        case .deleteProgram:
            return acceptLanguageWithAccessToken
        case .searchUsers:
            return accessToken
        case .sendProgram:
            return acceptLanguageWithAccessToken
        case .saveAndDeleteSharedProgram:
            return accessToken
        case .editProgram:
            return acceptLanguageWithAccessToken
        case .pinProgram:
            return accessToken
        case .linkProgram:
            return accessToken
        case .deleteSharedProgram:
            return acceptLanguageWithAccessToken
        case .fcmToken:
            return acceptLanguageWithAccessToken
        case .saveStory:
             return acceptLanguageWithAccessToken
        case .deleteStory:
             return acceptLanguageWithAccessToken
        case .getAllStories:
             return acceptLanguageWithAccessToken
        }
    }
}

extension NetworkRouter {
    static let provider = MoyaProvider<NetworkRouter>()
    
    static func performRequest(request: NetworkRouter, completion: @escaping (Result<Response, NetworkError>) -> Void) {
        provider.request(request) { (result) in
            switch result {
            case .success(let response):
               // print(response.data.toString())
                if response.statusCode == 400 || response.statusCode == 404 || response.statusCode == 1000 || response.statusCode == 1001 || response.statusCode == 1002 || response.statusCode == 1003 || response.statusCode == 500 || response.statusCode == 401 {
                    switch Parser.parseResponseData(response.data, for: ErrorModel.self) {
                    case .success(let error):
                        completion(.failure(NetworkError(code: error.code, message: error.message )))
                    case .failure:
                        completion(.failure(.unknown(message: response.data.toString())))
                    }
                    return
                }
                completion(.success(response))
            case .failure(let error):
                completion(.failure(NetworkError(errorM: error)))
            }
        }
    }
}

struct ErrorModel: Codable {
    var code: Int
    var message: String
}

extension Data {
    func toString() -> String {
        String(data: self, encoding: String.Encoding.utf8) ?? "Data could not be printed"
    }
}
