//
//  UserProvider.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 23.12.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import Foundation

class UserProvider {
    
    class func loginWithFacebook(token: String, completion: @escaping (Result<Token, NetworkError>) -> Void) {
        NetworkRouter.performRequest(request: .loginFacebook(token: token)) { (response) in
            switch response {
                
            case .success(let successResponse):
                switch Parser.parseResponseData(successResponse.data, for: Token.self) {
                case .success(let data):
                    completion(.success(data))
                case .failure(let error):
                    completion(.failure(NetworkError(error: error)))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    class func loginWithGoogle(token: String, completion: @escaping (Result<Token, NetworkError>) -> Void) {
        NetworkRouter.performRequest(request: .loginGoogle(token: token)) { (response) in
            switch response {
                
            case .success(let successResponse):
                
                switch Parser.parseResponseData(successResponse.data, for: Token.self) {
                case .success(let data):
                    completion(.success(data))
                case .failure(let error):
                    completion(.failure(NetworkError(error: error)))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    class func loginInstagram(code: String, completion: @escaping (Result<Token, NetworkError>) -> Void) {
        NetworkRouter.performRequest(request: .loginInstagram(code: code)) { (response) in
            switch response {
            case .success(let successResponse):
                switch Parser.parseResponseData(successResponse.data, for: Token.self) {
                case .success(let data):
                    completion(.success(data))
                case .failure(let error):
                    completion(.failure(NetworkError(error: error)))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    class func loginTwitter(accessToken: String, accessTokenSecret: String, completion: @escaping (Result<Token, NetworkError>) -> Void) {
        NetworkRouter.performRequest(request: .loginTwitter(accessToken: accessToken, accessTokenSecret: accessTokenSecret)) { (response) in
            switch response {
            case .success(let successResponse):
                switch Parser.parseResponseData(successResponse.data, for: Token.self) {
                case .success(let data):
                    completion(.success(data))
                case .failure(let error):
                    completion(.failure(NetworkError(error: error)))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    class func loginWithApple(code: String, completion: @escaping (Result<Token, NetworkError>) -> Void) {
        NetworkRouter.performRequest(request: .appleSignIn(code: code)) { (result) in
            switch result {
            case .success(let successResponse):
                switch Parser.parseResponseData(successResponse.data, for: Token.self) {
                case .success(let data):
                    completion(.success(data))
                case .failure(let error):
                    completion(.failure(NetworkError(error: error)))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
        
    }
    
    class func loginUser(username: String, password: String, completion: @escaping (Result<Token, NetworkError>) -> Void) {
        NetworkRouter.performRequest(request: .loginEmail(username: username, password: password)) { (response) in
            switch response {
            case .success(let successResponse):
                switch Parser.parseResponseData(successResponse.data, for: Token.self) {
                case .success(let data):
                    completion(.success(data))
                case .failure(let error):
                    completion(.failure(NetworkError(error: error)))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    class func registerNewUser(username: String, email: String, password: String, completion: @escaping (Result<Token, NetworkError>) -> Void) {
        NetworkRouter.performRequest(request: .createUser(username: username, email: email, password: password)) { (response) in
            switch response {
            case .success(let successResponse):
                switch Parser.parseResponseData(successResponse.data, for: Token.self) {
                case .success(let data):
                    completion(.success(data))
                case .failure(let error):
                    completion(.failure(NetworkError(error: error)))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    class func getCurrentUser(completion: @escaping (Result<User, NetworkError>) -> Void) {
        NetworkRouter.performRequest(request: .getCurrentUser) { (response) in
            switch response {
            case .success(let successResponse):
                
                switch Parser.parseResponseData(successResponse.data, for: User.self) {
                case .success(let data):
                    completion(.success(data))
                case .failure(let error):
                    completion(.failure(NetworkError(error: error)))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    class func logout(completion: @escaping (Result<Bool, NetworkError>) -> Void) {
        NetworkRouter.performRequest(request: .logout) { (response) in
            switch response {
            case .success:
                completion(.success(true))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    class func recoverPassword(email: String, completion: @escaping (Result<Bool, NetworkError>) -> Void) {
        NetworkRouter.performRequest(request: .recoverPassword(email: email)) { (response) in
            switch response {
            case .success:
                completion(.success(true))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    class func getMe(completion: @escaping (Result<User, NetworkError>) -> Void) {
        NetworkRouter.performRequest(request: .me) { (response) in
            switch response {
            case .success(let successResponse):
                
                switch Parser.parseResponseData(successResponse.data, for: User.self) {
                case .success(let data):
                    completion(.success(data))
                case .failure(let error):
                    completion(.failure(NetworkError(error: error)))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    class func editMe(gender: String, level: String, height: Int, weight: Double, birthDay: Double, completion: @escaping (Result<User, NetworkError>) -> Void) {
        NetworkRouter.performRequest(request: .editUser(gender: gender, level: level, height: height, weight: weight, birthDay: birthDay)) { (response) in
            switch response {
            case .success(let successResponse):
                switch Parser.parseResponseData(successResponse.data, for: User.self) {
                case .success(let data):
                    completion(.success(data))
                case .failure(let error):
                    completion(.failure(NetworkError(error: error)))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    class func uploadUserImage(data: Data, completion: @escaping (Result<User, NetworkError>) -> Void) {
        NetworkRouter.performRequest(request: .uploadUserImage(data: data)) { (response) in
            switch response {
            case .success(let successResponse):
                switch Parser.parseResponseData(successResponse.data, for: User.self) {
                case .success(let data):
                    completion(.success(data))
                case .failure(let error):
                    completion(.failure(NetworkError(error: error)))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    class func searchUser(filter: String, completion: @escaping  (Result<Users, NetworkError>) -> Void) {
        NetworkRouter.performRequest(request: .searchUsers(filter: filter)) { (response) in
            switch response {
            case .success(let successResponse):
                switch Parser.parseResponseData(successResponse.data, for: Users.self) {
                case .success(let data):
                    completion(.success(data))
                case .failure(let error):
                    completion(.failure(NetworkError(error: error)))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
}
