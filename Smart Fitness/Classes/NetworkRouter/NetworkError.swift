//
//  NetworkError.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 23.12.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import Moya

enum NetworkError: Error {
    
    case badRequest(message: String?)
    case unauthorized
    case notFound(message: String?)
    case internalServer
    case unknown(message: String?)
    case parsing(message: String?)
    case invalidEmail(message: String?)
    case invalidUser(message: String?)
    case invalidPassword(message: String?)
    case noInternet
    case userAndEmailIsTaken
    
    var code: Int {
        switch self {
        case .badRequest:
            return 400
        case .unauthorized:
            return 401
        case .notFound:
            return 404
        case .internalServer:
            return 500
        case .unknown:
            return 401
        case .parsing:
            return 401
        case .invalidEmail:
            return 1000
        case .invalidUser:
            return 1001
        case .invalidPassword:
            return 1002
        case .userAndEmailIsTaken: return 1003
        case .noInternet:
            return 6
        }
    }
    
    init(code: Int, message: String?) {
        switch code {
        case 400:
            self = .badRequest(message: message)
        case 401:
            self = .unauthorized
        case 404:
            self = .notFound(message: message)
        case 500:
            self = .internalServer
        case 1000:
            self = .invalidEmail(message: message)
        case 1001:
            self = .invalidUser(message: message)
        case 1002:
            self = .invalidPassword(message: message)
        case 1003: self = .userAndEmailIsTaken
        case 6:
            self = .noInternet
        default:
            self = .unknown(message: message ?? "Unknown error occured during the process.")
        }
    }
    
    init(errorM: MoyaError) {
        self.init(error: errorM)
        if errorM.errorCode == 6 {
            self = .noInternet
        }
    }
    
    var message: String? {
        switch self {
            
        case .badRequest(let message):
            return message
        case .unauthorized:
            return nil
        case .notFound(let message):
            return message
        case .internalServer:
            return nil
        case .unknown(let message):
            return message
        case .parsing(let message):
            return message
        case .invalidEmail(let message):
            return message
        case .invalidUser(let message):
            return message
        case .invalidPassword(let message):
            return message
        case .userAndEmailIsTaken: return nil
        case .noInternet:
            return nil
        }
    }
    
    init(error: NSError) {
        self = .unknown(message: error.localizedDescription)
    }
    
    init(error: Swift.Error) {
        self = .unknown(message: error.localizedDescription)
    }
}
