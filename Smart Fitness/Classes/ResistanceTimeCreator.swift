//
//  ResistanceTimeCreator.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 05.12.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import Foundation

final class ResistanceTimeCreator {
    
    // MARK: - Public Properties
    var minutesData: [String] {
        makeData(minimumValue: 0, maximumValue: 60, step: 1, addZero: true)
    }
    
    var secondsData: [String] {
        makeData(minimumValue: 0, maximumValue: 60, step: 1, addZero: true)
    }
    
    // MARK: - Private
    private func makeData(minimumValue: Int, maximumValue: Int, step: Int, addZero: Bool = false) -> [String] {
        var data = [String]()
        for step in stride(from: minimumValue, to: maximumValue, by: step) {
            if addZero {
                if "\(step)".count == 1 {
                    data.append("0\(step)")
                } else {
                    data.append("\(step)")
                }
            } else {
                
                data.append("\(step)")
            }
        }
        return data
    }
}
