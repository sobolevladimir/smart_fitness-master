//
//  SmartFitnessProducts.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 22.02.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import Purchases

final class SmartFitnessProducts {
    
    enum Products: String, CodingKey {
        case month = "smartFitness.month"
        case oneYear = "smartFitness.year"
    }
    
    // MARK: - Properties
    var products: [Purchases.Package] = []
    
    
    // MARK: - Init
    init() {
        getProducts(completion: { packages in
            self.products = packages
        })
    }
    
    func getProducts(completion: @escaping ([Purchases.Package]) -> Void) {
        Purchases.shared.offerings { [weak self] (offerings, error) in
            if let offerings = offerings {
                
                if let packages = offerings.current?.availablePackages {
                    self?.products = packages
                    
                    completion(packages)
                } else {
                    completion([])
                }
            } else {
                completion([])
            }
        }
    }
    
    
    func buyMonth(completion: @escaping (Result<Bool, Error>) -> Void) {
        guard let package = products.first(where: { $0.product.productIdentifier == Products.month.rawValue }) else { return }
        Purchases.shared.purchasePackage(package) { (transaction, purchaserInfo, error, userCancelled) in
            if let error = error {
                completion(.failure(error))
            } else if purchaserInfo?.entitlements.all["Pro"]?.isActive == true {
                UserDefaults.standard.set(true, forKey: Constants.isSubscribedKey)
                completion(.success(true))
            } else {
                completion(.success(false))
            }
        }
    }
    
    func buyYear(completion: @escaping (Result<Bool, Error>) -> Void) {
        guard let package = products.first(where: { $0.product.productIdentifier == Products.oneYear.rawValue }) else { return }
        Purchases.shared.purchasePackage(package) { (transaction, purchaserInfo, error, userCancelled) in
            if let error = error {
                completion(.failure(error))
            } else if purchaserInfo?.entitlements.all["Pro"]?.isActive == true {
                UserDefaults.standard.set(true, forKey: Constants.isSubscribedKey)
                completion(.success(true))
            } else {
                completion(.success(false))
            }
        }
    }
    
    func restore(completion: @escaping (Result<Bool, Error>) -> Void) {
        Purchases.shared.restoreTransactions { (purchaserInfo, error) in
            if let error = error {
                completion(.failure(error))
            } else if purchaserInfo?.entitlements.all["Pro"]?.isActive == true {
                UserDefaults.standard.set(true, forKey: Constants.isSubscribedKey)
                completion(.success(true))
            } else {
                completion(.success(false))
            }
        }
    }
    
    func isActive() {
        Purchases.shared.purchaserInfo { (purchaserInfo, error) in
            if let _ = error {
                UserDefaults.standard.set(false, forKey: Constants.isSubscribedKey)
            } else if purchaserInfo?.entitlements.all["Pro"]?.isActive == true {
                UserDefaults.standard.set(true, forKey: Constants.isSubscribedKey)
            } else {
                if UserDefaults.standard.bool(forKey: Constants.userBackEndSubscriptionKey) {
                    UserDefaults.standard.set(true, forKey: Constants.isSubscribedKey)
                } else {
                    UserDefaults.standard.set(false, forKey: Constants.isSubscribedKey)
                }
            }
        }
    }
}
