//
//  RedirectMenager.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 10.05.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit
import Firebase

final class RedirectManager {
    
    static var shared = RedirectManager()
    
    private var isFirst = true
    
    var subscriptionManager: SmartFitnessProducts?
    
    private  var isLogged: Bool {
        return  UserAuthManager.keychainRead()
    }
    
    
    private func checkLogin(animationVC: SplashScreenVC? = nil) {
        if isLogged {
            UserProvider.getCurrentUser { (result) in
                switch result {
                case .success(let user):
                    UserAuthManager.setUser(user)
                    InstanceID.instanceID().instanceID { (result, error) in
                        if let error = error {
                            print("Error fetching remote instance ID: \(error)")
                        } else if let result = result {
                            guard let id = user.id else { fatalError() }
                            NetworkRouter.performRequest(request: .fcmToken(userId: id, deviceType: "iOS", fcmToken: result.token)) { (result) in
                                switch result {
                                case .success(let response):
                                    print(response)
                                case .failure(let error):
                                    print(error)
                                }
                            }
                        }
                    }
                case .failure:
                    UserAuthManager.keychainRemove()
                }
                animationVC?.gifImageView.stopAnimatingGif()
                self.choosingController()
                return
            }
        } else {
            self.choosingController()
        }
    }
    
    
    func presentRootController() {
        AppDelegate.shared.window?.frame = UIScreen.main.bounds
        if isFirst {
            let splashVC = SplashScreenVC.instantiate()
            AppDelegate.shared.window?.rootViewController = splashVC
            AppDelegate.shared.window?.makeKeyAndVisible()
            let gif = try! UIImage(gifName: Constants.GifImages.splashGifName, levelOfIntegrity: 1)
            splashVC.gifImageView.setGifImage(gif)
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
                guard let self = self else { return }
                self.checkLogin(animationVC: splashVC)
                self.isFirst.toggle()
            }
        } else {
            self.checkLogin()
        }
    }
    
    private func choosingController() {
        subscriptionManager?.isActive()
        guard let window = UIApplication.shared.keyWindow else { fatalError() }
        let vc = self.isLogged ? MainViewController.instantiate() : UINavigationController(rootViewController: LoginViewController.instantiate())
        if vc is MainViewController {
            window.rootViewController = vc
            let options: UIView.AnimationOptions = .transitionCrossDissolve
            let duration: TimeInterval = 0.3
            UIView.transition(with: window, duration: duration, options: options, animations: {})
        } else {
            window.rootViewController = vc
            let options: UIView.AnimationOptions = .transitionCrossDissolve
            let duration: TimeInterval = 0.3
            UIView.transition(with: window, duration: duration, options: options, animations: {})
        }
        
    }
}
