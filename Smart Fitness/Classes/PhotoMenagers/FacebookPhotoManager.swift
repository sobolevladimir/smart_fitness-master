//
//  FacebookPhotoManager.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 21.05.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import FBSDKCoreKit

final class FacebookPhotoManager {
    
    // MARK: - Singleton
    static let shared = FacebookPhotoManager()
    
    // MARK: - Var
    
    private var albumList: [FacebookAlbum] = []
    
    private var pictureUrl = "https://graph.facebook.com/%@/picture?type=small&access_token=%@"
    
    static let idTaggedPhotosAlbum = "idPhotosOfYouTagged"
    
    private var profilePictureUrl: String?
    
    /// Boolean to check if we have already added the tagged album, prevent multiple addition when fetching next cursor
    private var alreadyAddTagged: Bool = false
    
    // MARK: - Retrieve Facebook's Albums
    func fetchFacebookAlbums(after: String? = nil,
                             completion: ((Result<[FacebookAlbum], Error>) -> Void)? = nil) {
        var  path = "me/albums?fields=id,name,count,cover_photo"
        if let afterPath = after {
            path = path.appendingFormat("&after=%@", afterPath)
        }
  
        let graphRequest = GraphRequest(graphPath: path, parameters: [:])
        
        graphRequest.start { [weak self] conection, result, error in
            guard let self = self else { return }
            
            if let error = error {
                completion?(.failure(error))
                return
            } else {
                if let fbResult = result as? [String: AnyObject] {
                    self.parseFbAlbumResult(fbResult: fbResult)
                    if let paging = fbResult["paging"] as? [String: AnyObject],
                        paging["next"] != nil,
                        let cursors = paging["cursors"] as? [String: AnyObject],
                        let after = cursors["after"] as? String {
                        self.fetchFacebookAlbums(after: after, completion: completion)
                    } else {
                        completion?(.success(self.albumList))
                    }
                    conection?.cancel()
                }
            }
        }
    }
   
    private func parseFbAlbumResult(fbResult: [String: AnyObject]) {
        if let albumArray = fbResult["data"] as? [AnyObject] {
            for album in albumArray {
                if let albumDic = album as? [String: AnyObject],
                    let albumName = albumDic["name"] as? String,
                    let albumId = albumDic["id"] as? String,
                    let albumCount = albumDic["count"] as? Int {
                    let token = AccessToken.current?.tokenString ?? ""
                    let albumUrlPath = String(format: self.pictureUrl, albumId, token)
                    if let coverUrl = URL(string: albumUrlPath) {
                        let albm = FacebookAlbum(name: albumName,
                                                 count: albumCount,
                                                 coverUrl: coverUrl,
                                                 albmId: albumId)
                        self.albumList.append(albm)
                    }
                }
            }
        }
    }
    
    // MARK: - Retrieve Facebook's Picture
    func fbAlbumsPictureRequest(after: String? = nil,
                                album: FacebookAlbum,
                                completion: ((Result<FacebookAlbum, Error>) -> Void)? = nil) {
        guard let identifier = album.albumId else {
            return
        }
        var path = identifier == FacebookPhotoManager.idTaggedPhotosAlbum
            ? "/me/photos?fields=picture,source,id"
            : "/\(identifier)/photos?fields=picture,source,id"
        if let afterPath = after {
            path = path.appendingFormat("&after=%@", afterPath)
        }
        let graphRequest = GraphRequest(graphPath: path, parameters: [:])
        _ = graphRequest.start { [weak self] _, result, error in
            guard let strongSelf = self else { return }
            
            if let error = error {
                completion?(.failure(error))
                return
            } else {
                if let fbResult = result as? [String: AnyObject] {
                    strongSelf.parseFbPicture(fbResult: fbResult,
                                              album: album)
                    if let paging = fbResult["paging"] as? [String: AnyObject],
                        paging["next"] != nil,
                        let cursors = paging["cursors"] as? [String: AnyObject],
                        let after = cursors["after"] as? String {
                        strongSelf.fbAlbumsPictureRequest(after: after, album: album, completion: completion)
                    } else {
                        completion?(.success(album))
                    }
                }
            }
        }
    }
    
    private func parseFbPicture(fbResult: [String: AnyObject],
                                album: FacebookAlbum) {
        if let photosResult = fbResult["data"] as? [AnyObject] {
            for photo in photosResult {
                if let photoDic = photo as? [String: AnyObject],
                    let identifier = photoDic["id"] as? String,
                    let picture = photoDic["picture"] as? String,
                    let source = photoDic["source"] as? String {
                    let photoObject = FacebookImage(picture: picture,
                                                    imgId: identifier,
                                                    source: source)
                    album.photos.append(photoObject)
                }
            }
        }
    }

}


class FacebookAlbum {
    var name: String?
    var count: Int?
    var coverUrl: URL?
    var albumId: String?
    var photos: [FacebookImage] = []

    // MARK: - Init
    init(name: String,
         count: Int? = nil,
         coverUrl: URL? = nil,
         albmId: String) {
        self.name = name
        self.albumId = albmId
        self.coverUrl = coverUrl
        self.count = count
    }
}

public enum ImageSize {
    case normal
    case full
}

public class FacebookImage: Equatable, Hashable {
    public static func == (lhs: FacebookImage, rhs: FacebookImage) -> Bool {
        lhs.imageId == rhs.imageId
    }
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(self.imageId)
        hasher.combine(self.normalSizeUrl)
    }
    
    
    public var image: UIImage?
    public var normalSizeUrl: String?
    public var fullSizeUrl: String?
    public var imageId: String?
    
    // MARK: - Init
    init(picture: String, imgId: String, source: String) {
        self.imageId = imgId
        self.normalSizeUrl = picture
        self.fullSizeUrl = source
    }
}
