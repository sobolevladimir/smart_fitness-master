//
//  UserAuthManager.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 23.12.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import Foundation
import SwiftKeychainWrapper

class UserAuthManager: NSObject {
    
    static private(set) var token: String?
    
    static private(set) var user: User?
    
    static func setUser(_ user: User) {
        UserAuthManager.user = user
        UserDefaults.standard.set( user.hasSubscription ?? false, forKey: Constants.userBackEndSubscriptionKey)
    }
    
    static func setToken(_ token: String) {
        UserAuthManager.token = token
        KeychainWrapper.standard.set(token, forKey: "token")
    }
    
   @discardableResult static func keychainRead() -> Bool {
    guard let token = KeychainWrapper.standard.string(forKey: "token") else {
            return false
        }

        UserAuthManager.setToken(token)
        return true
    }

    static func keychainRemove() {
        KeychainWrapper.standard.removeObject(forKey: "token")
    }
    
    static func logoutUser() {
        keychainRemove()
    }
}
