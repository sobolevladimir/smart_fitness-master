//
//  ResistanceDataManager.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 04.12.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import Foundation

final class ResistanceDataCreator {
    
    // MARK: - Public Properties
    var setsData: [[String]] {
        [makeData(minimumValue: 1, maximumValue: 100, step: 1)]
    }
    
    var repsData: [[String]] {
       var data = [makeData(minimumValue: 1, maximumValue: 100, step: 1)]
        data[0].insert(Constants.ResistanceExercises.Methods().maxString, at: 0)
        return data
    }
    
    var timeData: [[String]] {
        var data = [makeData(minimumValue: 00, maximumValue: 60, step: 1, addZero: true)]
        data += [[":"]]
        data += [makeData(minimumValue: 00, maximumValue: 60, step: 1, addZero: true)]
        return data
    }
    
    var kgData: [[String]] {
        var data = [makeData(minimumValue: 0, maximumValue: 301, step: 1)]
        data += [["."]]
        data += [makeData(minimumValue: 0, maximumValue: 100, step: 25)]
        return data
    }
    
    var percentData: [[String]] {
        [makeData(minimumValue: 5, maximumValue: 155, step: 5)]
    }
    
    var machineNumberData: [String] {
       var data =  makeData(minimumValue: 0, maximumValue: 100, step: 1)
        data.insert("--", at: 0)
        return data
    }
    
    // MARK: - Private
    private func makeData(minimumValue: Int, maximumValue: Int, step: Int, addZero: Bool = false) -> [String] {
        var data = [String]()
        for step in stride(from: minimumValue, to: maximumValue, by: step) {
            if addZero {
                if "\(step)".count == 1 {
                    data.append("0\(step)")
                } else {
                    data.append("\(step)")
                }
            } else {
                data.append("\(step)")
            }
        }
        return data
    }
}
