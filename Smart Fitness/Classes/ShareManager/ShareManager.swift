//
//  ShareManager.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 13.02.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import Foundation

final class ShareManager {
    class func shareProgram(userId: Int, programId: Int, completion: @escaping (Result<Bool, NetworkError>) -> Void) {
        NetworkRouter.performRequest(request: .sendProgram(userId: userId, programId: programId)) { (result) in
            switch result {
            case .success:
                completion(.success(true))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    class func shareProgramWithLink(programId: Int, completion: @escaping (Result<String, NetworkError>) -> Void) {
        NetworkRouter.performRequest(request: .linkProgram(programId: programId)) { (result) in
            switch result {
            case .success(let successResult):
                switch Parser.parseResponseData(successResult.data, for: ShortLink.self) {
                case .success(let data):
                     completion(.success(data.shortLink))
                case .failure(let error):
                    completion(.failure(NetworkError(error: error)))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    class func getSharedPrograms(completion: @escaping (Result<SharedPrograms, NetworkError>) -> Void) {
        NetworkRouter.performRequest(request: .getSharedPrograms) { (result) in
            switch result {
            case .success(let successResult):
                switch Parser.parseResponseData(successResult.data, for: SharedPrograms.self) {
                case .success(let data):
                    completion(.success(data))
                case .failure(let error):
                    completion(.failure(NetworkError(error: error)))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    class func rejectSharedProgram(programId: Int, completion: @escaping (Result<Bool, NetworkError>) -> Void) {
        NetworkRouter.performRequest(request: .saveAndDeleteSharedProgram(programId: programId, accept: false)) { (result) in
            switch result {
            case .success:
                completion(.success(true))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    class func deleteSharedProgram(programId: Int, completion: @escaping (Result<Bool, NetworkError>) -> Void) {
        NetworkRouter.performRequest(request: .deleteSharedProgram(programId: programId)) { (result) in
               switch result {
               case .success:
                   completion(.success(true))
               case .failure(let error):
                   completion(.failure(error))
               }
           }
       }
    
    class func acceptSharedProgram(programId: Int, completion: @escaping (Result<Bool, NetworkError>) -> Void) {
        NetworkRouter.performRequest(request: .saveAndDeleteSharedProgram(programId: programId, accept: true)) { (result) in
            switch result {
            case .success:
                completion(.success(true))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    class func pinSharedProgram(programId: Int, completion: @escaping (Result<Bool, NetworkError>) -> Void) {
        NetworkRouter.performRequest(request: .pinProgram(id: programId)) { (result) in
            switch result {
            case .success:
                completion(.success(true))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
