//
//  KeyboardHandler.swift
//  FirebaseChat
//
//  Created by Ruslan Khamskyi on 17.10.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

protocol KeyboardHandler {
  var topConstraint: NSLayoutConstraint! { get }
}

extension KeyboardHandler where Self: UIViewController {
  typealias CompletionObject<T> = (_ response: T) -> Void
    
  func addKeyboardObservers(_ completion: CompletionObject<Bool>?  = nil) {
    NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillShowNotification, object: nil, queue: nil) {[weak self] (notification) in
      self?.handleKeyboard(notification: notification)
      completion?(true)
    }
    
    NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillHideNotification, object: nil, queue: nil) {[weak self] (notification) in
      self?.handleKeyboard(notification: notification)
      completion?(false)
    }
  }
    
  private func handleKeyboard(notification: Notification) {
    guard let userInfo = notification.userInfo else { return }
    guard let keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
    topConstraint.constant = notification.name == UIResponder.keyboardWillHideNotification ? 0 : -keyboardFrame.height / 2
    UIView.animate(withDuration: 0.3) {
      self.view.layoutIfNeeded()
    }
  }
}
