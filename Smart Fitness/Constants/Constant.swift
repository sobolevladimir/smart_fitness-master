//
//  Constant.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 15/03/19.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

enum Constants {
    static let twitterConsumerKey = "bldwfbCmIjAjXmhPRDYdR1cOx"
    static let twitterConsumerSecret = "Af8hQvZAomZpbebjfbuqwAglMPyqiKbs6MFKLjZeBQgDxVEgsr"
    static let googleClientId = "371077871536-bb6qgm4u6m3p29lemvkp8fgdlrclg69p.apps.googleusercontent.com"
    static let deepLink = "deepLink"
    //static let gcmMessageIDKey = "4C2LHDFD6L"
    static let isSubscribedKey = "SmartFitnessSubscription"
    static let userBackEndSubscriptionKey = "user.hasSubscription"
    
    enum Colors {
        static let blueDark: UIColor = #colorLiteral(red: 0.1725490196, green: 0.6963843703, blue: 0.9218934178, alpha: 1)
        static let grayLight: UIColor = #colorLiteral(red: 0.8391267061, green: 0.8392683864, blue: 0.8391068578, alpha: 1)
        static let graySuperLight: UIColor = #colorLiteral(red: 0.9293137789, green: 0.9294697642, blue: 0.929291904, alpha: 1)
        static let orangeLight: UIColor = #colorLiteral(red: 0.9901713729, green: 0.8049211502, blue: 0.2465699315, alpha: 1)
        static let orangeDark: UIColor = #colorLiteral(red: 0.9869434237, green: 0.7311882973, blue: 0.3040594459, alpha: 1)
    }
    
    enum GifImages {
        static let splashGifName = "gif.gif"
    }
    
    enum FontString: String {
        case bold = "OpenSans-Bold"
        case semiboldItalic = "OpenSans-SemiboldItalic"
        case extraBoldItalic = "OpenSans-ExtraboldItalic"
        case boldItalic = "OpenSans-BoldItalic"
        case light = "OpenSans-Light"
        case semibold = "OpenSans-Semibold"
        case regular = "OpenSans"
        case italic = "OpenSans-Italic"
        case extraBold = "OpenSans-Extrabold"
       }
    
    static func returnFontName(style: FontString, size: CGFloat) -> UIFont {
        guard let font = UIFont(name: style.rawValue, size: size) else { return UIFont.systemFont(ofSize: size)}
        return font
    }
    
    struct ProgramInfo {
        var done: String { "Done".localized() }
        var programInfo: String { "Program Info".localized() }
        var wrongUploadDate: String { "You cannot set a date older than the date of building".localized() }
        
        var cardioProgram: String { "Cardio Program".localized() }
        var resistanceProgram: String { "Resistance Program".localized() }
        var circuitProgram: String { "Circuit Program".localized() }
    }
    
    struct Timer {
        var times: String { "Times".localized() }
        var pause: String { "PAUSE".localized() }
        var play: String { "PLAY".localized() }
        
        var rest: String { "Rest".localized() }
        var timer: String { "Timer".localized() }
        
        var setTime: String { "SET TIME".localized() }
        var setRest: String { "SET REST".localized() }
    }
    
    enum LoginFlow {
       
        
        struct LocalizedDescription {
            
            var passwordEmpty: String { "Password must be at least 6 characters, numbers and letters".localized() }
            var usernameEmpty: String { "Username must be 3 to 14 characters".localized() }
            var emailIsEmpty: String { "Invalid email address".localized() }
            var emailIsNotValid: String { "Invalid email address".localized() }
            var emailIsNotValidLength: String { "Email has to be less than 80 symbols".localized() }
            var usernameHasSpace: String { "Please use only letters and numbers without space".localized() }
            
            var emailTaken: String { "*Email is already taken".localized() }
            var usernameTaken: String { "*Username is already taken".localized() }            
        }
        
        struct FailureReason {
            var passwordEmpty: String { "Password".localized() }
            var usernameEmpty: String { "Username".localized() }
            var emailIsEmpty: String { "Email is Empty".localized() }
            var emailIsNotValid: String { "Invalid email address".localized() }
            var emailIsNotValidLength: String { "Invalid email address".localized() }
            var usernameHasSpace: String { "Bad username".localized() }
        }
        
        struct Decision {
            var ok: String { "OK".localized() }
        }
    }
    
    static var defaultRPMandSPMSeparator: String = " - "
    
    struct Cardio {
        var distanceString: String { "Distance".localized() }
        var timeString: String { "Time".localized() }
        var speedString: String { "Speed".localized() }
        var inclineString: String { "Incline".localized() }
        var paceString: String { "Pace".localized() }
        var resistanceString: String { "Resistance".localized() }
        var intensityString: String { "Intensity".localized() }
        var stepsString: String { "Steps".localized() }
        var styleString: String { "Style".localized() }
        var exerciseString: String { "Exercise".localized() }
        var rPMString: String { "RPM".localized() }
        var sPMString: String { "SPM".localized() }
        var sPMStairClimbingString: String { "SPM‎‏‏‎*".localized() }
        var resistanceLevelString: String { "Resistance(level)".localized() }
        var helpString: String { "Help".localized() }
        var runningString: String { "Running".localized() }
        var cyclingString: String { "Cycling".localized() }
        let ellipticalString = "Elliptical".localized()
        var rowingMachineString: String { "Rowing Machine".localized() }
        var stairClimbingString: String { "Stair Climbing".localized() }
        var swimmingString: String { "Swimming".localized() }
        var discardString: String { "DISCARD".localized() }
        var saveString: String { "SAVE".localized() }
        var totalTimeString: String { "Total Time:".localized() }
        var totalDistanceString: String { "Total Distance:".localized() }
        var mediumString: String { "Medium".localized() }
        var easyString: String { "Easy".localized() }
        var hardString: String { "Hard".localized() }
        var racePaceString: String { "Race Pace".localized() }
        var warmUpString: String { "Warm up".localized() }
        var coolDownString: String { "Cool Down".localized() }
        var restString: String { "Rest".localized() }
        var frontCrowlString: String { "Front crawl".localized() }
        var breaststrokeString: String { "Breaststroke".localized() }
        var backStrokeString: String { "Backstroke".localized() }
        var butterflyStrokeString: String { "Butterfly stroke".localized() }
        var normalString: String { "Normal".localized() }
        var sprintString: String { "Sprint".localized() }
        var noLegsString: String { "No Legs".localized() }
        var noHandsString: String { "No Hands".localized() }
        var breathHoldString: String { "Breath Hold".localized() }
        var mainSetString: String { "Main Set".localized() }
        var topText: String { "Note: This cardio program is a static program\rdesigned to define the trainee’s activity steps.\rAccording to times, pace and difficulty levels\rThis data is defined by the cardio machines\rBut you can perform the same activities without machines".localized() }
        var stepThreeText: String { "Step 3: After fill in the data, you can add a new\rrow and prepare a multi-data plan".localized() }
    }
    
    struct Circuit {
        var jumpingRope: String { "Jumping Rope".localized() }
        var level: String { "(level)".localized() }
        var normal: String { "Normal".localized() }
        var fast: String { "Fast".localized() }
        var slow: String { "Slow".localized() }
        var fromWhatMuscle: String { "From what muscle?".localized() }
        var skips: String { "Skips".localized() }
        var kg: String { "Kg".localized() }
        
        var roundDeleted: String { "Round deleted".localized() }
    }
    
    enum ResistanceExercises {
        
        struct Messages {
            let timeOfMessage: Double = 3
            var exerciseDelete: String { "Exercise deleted".localized() }
            var minimumIsOneSet: String { "The minimum is 1 set".localized() }
            var maximumIsSix: String { "The maximum is 6 drops".localized() }
            var minimumIsTwoDrops: String { "The minimum is 2 drops".localized() }
            var chooseAtLeastTwoEx: String { "Choose at least 2 exercises".localized() }
            var pyramidCreated: String { "Pyramid created".localized() }
            var percentageCreated: String { "Percentage created".localized() }
            var dropSetCreated: String { "Drop set created".localized() }
            var supersetIsCreated: String { "Superset is created".localized() }
            var theLimitIsSix: String { "The limit is 6 exercises".localized() }
            var exerciseAddedToList: String { "The exercise is added to the list".localized() }
            var yourExerciseAddedToList: String { "Your exercise is added to the list".localized() }
            var addedToFavorite: String { "Exercise added to favorites".localized() }
            var removedFromFavorite: String { "Exercise removed from favorites".localized() }
            var chooseAtLeastOne: String { "Choose at least 1 exercise".localized() }
            var addYourOwnExercise: String { "Add your own exercise".localized() }
            var writeYourExercise: String { "Please write your exercise\nin the search bar".localized() }
            
            var noExercises: String { "Current exercises were already defined with workout methods".localized() }
            var programDeleted: String { "Program deleted".localized() }
        }
        
        struct Category {
            var machinesString: String { "Machines".localized() }
            var dumbbellsString: String { "Dumbbells".localized() }
            var barbellsString: String { "Barbells".localized() }
            var pulleyString: String { "Pulley cable".localized() }
            var bodyweightString: String { "Bodyweight".localized() }
            var accessoriesString: String { "Accessories".localized() }
            var favoritesString: String { "Favorite".localized() }
        }
        
        struct Muscles {
            var chest: String { "Chest".localized() }
            var back: String { "Back".localized() }
            var legs: String { "Legs".localized() }
            var shoulders: String { "Shoulders".localized() }
            var biceps: String { "Biceps".localized() }
            var triceps: String { "Triceps".localized() }
            var abs: String { "Abdominals".localized() }
            var forearms: String { "Forearms".localized() }
            var lowerBack: String { "Lower Back".localized() }
            var cardio: String { "Cardio".localized() }
        }
        
        struct Methods {
          
            static let dropSetRepsDefault = [10, 10]
            static let dropSetKGDefault = [15.0, 10.0]
            static let dropSetAddDropRepsValue = 10
            static let dropSetAddDropKgValue = 10.0
            static let pyramidRepsDefaultValue = 8
            static let percentKgDefaultValue = 20.0
            static let percentRMDefaultValue = 6
            static let percentValue = 85
            static let supersetDefaultReps = 10
            static let supersetDefaultKg = 15.0
            static let restTimeDefault = "00:45"
            static let defaultAmountOfSets = 3
            static let defaultMachineNumber = 100
            
            var repsString: String { "Reps:".localized() }
            var repsWithNoPointsString: String { "Reps".localized() }
            var restString: String { "REST:".localized() }
            var maxString: String { "Max".localized() }
            var rmString: String { "RM".localized() }
            var percentOfRMSetNString: String { "Percent of RM - Set Number".localized() }
            var pyramidSetNString: String { "Pyramid - Set Number".localized() }
            var pyramidSetOneString: String { "Pyramid - Set Number 1".localized() }
            var supersetNumberOne: String { "Superset - Set Number 1".localized() }
            var supersetNumberString: String { "Superset - Set Number".localized() }
            var dropSetNumberString: String { "Dropset - Set Number".localized() }
            var supersetString: String { "Superset".localized() }
            var supersetDifferentString: String { "Superset2".localized() }
            var dropSetString: String { "Drop Set".localized() }
            var pyramidString: String { "Pyramid".localized() }
            var percentOfRMString: String { "Percentage".localized() }
            var methodString: String { "Method".localized() }
            var createString: String { "Create".localized() }
            var unlinkSupersetString: String { "Unlink Superset".localized() }
            var deleteString: String { "Delete".localized() }
            var exerciseString: String { "Exercise".localized() }
            var exercisesString: String { "Exercises".localized() }
            var selectString: String { "Select".localized() }
            var searchString: String { "Search".localized() }
            var deleteSplitTr: String { "Delete Split Training".localized() }
            var splitTrainingString: String { "Split Training".localized() }
            var timeString: String { "Time".localized() }
            
            var exWithPoint: String { "Ex.".localized() }
            var drop: String { "Drop".localized() }
            var other: String { "OTHER".localized() }
        }
    }
    
    struct SideMenu {
        var homeString: String { "Home".localized() }
        var programString: String { "Programs received".localized() }
        var languageString: String { "Languages".localized() }
        var activityString: String { "Track your activity".localized() }
        var gpsString: String { "GPS Running".localized() }
        var feedbackString: String { "Feedback".localized() }
        var shareString: String { "Share".localized() }
        var aboutString: String { "About".localized() }
        var logoutString: String { "Logout".localized() }
    }
    
    struct PopUps {
        var pinnedProgram: String { "The program was \nsuccessfully \npinned to the board".localized() }
        var fillOneValue: String { "Please fill at least \none value".localized() }
        var supersetCreated: String { "Superset is created".localized() }
        var chooseTwoExercises: String { "Choose at least \n2 exercises".localized() }
        var theLimitIs: String { "The limit is".localized() }
        var addCardio: String { "Would you like to add\nany cardio program?".localized() }
        var addString: String { "Add".localized() }
        var noFinishString: String { "No, Finish".localized() }
        var deleteProgram: String { "Are you sure you want to \ndelete this program ?".localized() }
        var deleteActivity: String { "Are you sure you want \nto delete this activity ?".localized() }
        var deleteSplitTraining: String { "Are you sure you \nwant to delete the split \ntraining ?".localized() }
        var unlinkExercises: String { "Are you sure you want \nto unlink the exercises ?".localized() }
        var leaveWithoutSaving: String { "Are you sure you want\nto leave this page\nwithout saving ?".localized() }
        var deleteRound: String { "Are you sure you want\nto delete this round ?".localized() }
        var deleteExercise: String { "Are you sure you want\nto delete this exercises ?".localized() }
       
    }
    
    struct Profile {
       
        var male: String { "Male".localized() }
        var female: String { "Female".localized() }
        var calculations: String { "BMI calculation needs it".localized() }
        var requiredPlan: String { "Required to customize a plan for you. \nIn progress...".localized()}
        
        var dateOfBirth: String { "Date of Birth".localized() }
        var weight: String { "Weight".localized() }
        var height: String { "Height".localized() }
        var gender: String { "Gender".localized() }
        var level: String { "Fitness Level".localized() }
    
        var beginner: String { "Beginner".localized() }
        var intermediate: String { "Intermediate".localized() }
        var expert: String { "Expert".localized() }
   
        var kg: String { "KG".localized() }
        var cm: String { "CM".localized() }
        
        var measurements: String { "Measurements".localized() }
        var water: String { "Water".localized() }
        var bmi: String { "BMI".localized() }
        var imageDeleted: String { "Image deleted".localized() }
        var failedToLoad: String {"Sorry something went wrong\n\nPlease try again later".localized()}
        var storyAdded: String { "You made it!\nAnother milestone on the way\nto your GOAL.".localized() }
        var profileFacebookStories: String { "Your Facebook Photos".localized() }
        var profileInstagramStories: String { "Your Instagram Photos".localized() }
    }
    
    struct Subscriptions{
        var perMonth: String { "perMonth".localized() }
    }
    
    struct Internet {
        var noInternet: String { "No internet connection".localized() }
        var saved: String { "Saved".localized() }
    }
    
    struct TabBarStrings {
        var home: String { "Home".localized() }
        var programs: String { "Programs".localized() }
        var timer: String { "Timer".localized() }
        var profile: String { "Profile".localized() }
    }
}
