//
//  ResistanceData.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 03.12.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import Foundation

struct GroupsData {
    var key: MuscleGroupKey = .chest
    var title: String  {
        key.localizedString
    }
    var imageUrl: String = ""
    var id: String = ""
  
    func getData() -> [GroupsData] {
        [
            .init(key: .chest, imageUrl: "Chest", id: "1"),
            .init(key: .back, imageUrl: "Back", id: "2"),
            .init(key: .legs, imageUrl: "Legs", id: "3"),
            .init(key: .shoulders, imageUrl: "Shoulders", id: "4"),
            .init(key: .biceps, imageUrl: "Biceps", id: "5"),
            .init(key: .triceps, imageUrl: "Triceps", id: "6"),
            .init(key: .abdominals, imageUrl: "Abdominals", id: "7"),
            .init(key: .forearms, imageUrl: "Forearms", id: "8"),
            .init(key: .lowerBack, imageUrl: "Lowerback", id: "9"),
            .init(key: .cardio, imageUrl: "Cardio", id: "10")
        ]
    }
    
    enum MuscleGroupKey: String, Codable {
        case chest = "Chest"
        case back = "Back"
        case legs = "Legs"
        case shoulders = "Shoulders"
        case biceps = "Biceps"
        case triceps = "Triceps"
        case abdominals = "Abdominals"
        case forearms = "Forearms"
        case lowerBack = "Lowerback"
        case cardio = "Cardio"
        
        var localizedString: String {
            switch self {
            case .chest: return Constants.ResistanceExercises.Muscles().chest
            case .back: return Constants.ResistanceExercises.Muscles().back
            case .legs: return Constants.ResistanceExercises.Muscles().legs
            case .shoulders: return Constants.ResistanceExercises.Muscles().shoulders
            case .biceps: return Constants.ResistanceExercises.Muscles().biceps
            case .triceps: return Constants.ResistanceExercises.Muscles().triceps
            case .abdominals: return Constants.ResistanceExercises.Muscles().abs
            case .forearms: return Constants.ResistanceExercises.Muscles().forearms
            case .lowerBack: return Constants.ResistanceExercises.Muscles().lowerBack
            case .cardio: return Constants.ResistanceExercises.Muscles().cardio
            }
        }
        
        var imageName: String {
            switch self {
            case .chest:
                return "Chest"
            case .back:
                return "Back"
            case .legs:
                return "Legs"
            case .shoulders:
                return "Shoulders"
            case .biceps:
                return "Biceps"
            case .triceps:
                return "Triceps"
            case .abdominals:
                return "Abdominals"
            case .forearms:
                return "Forearms"
            case .lowerBack:
                return "Lowerback"
            case .cardio:
                return "Cardio"
            }
        }
    }
}
