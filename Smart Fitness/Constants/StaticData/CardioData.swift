//
//  CardioData.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 09.01.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import Foundation

enum CardioActivity: String, Codable {
    case running, cycling, elliptical, rowingMachine, stairClimbing, swimming, jumpingRope
    
    var localizeString: String {
        switch self {
            
        case .running:
            return Constants.Cardio().runningString
        case .cycling:
            return Constants.Cardio().cyclingString
        case .elliptical:
            return Constants.Cardio().ellipticalString
        case .rowingMachine:
            return Constants.Cardio().rowingMachineString
        case .stairClimbing:
            return Constants.Cardio().stairClimbingString
        case .swimming:
            return Constants.Cardio().swimmingString
        case .jumpingRope:
            return Constants.Circuit().jumpingRope
        }
    }
    var imageName: String {
        switch self {
        case .running:
            return "cardioRunning"
        case .cycling:
            return "cardioCycling"
        case .elliptical:
            return "cardioElliptical"
        case .rowingMachine:
            return "cardioRowingMachine"
        case .stairClimbing:
            return "cardioStairClimbing"
        case .swimming:
            return "swimmingIcon"
        case .jumpingRope:
            return Constants.Circuit().jumpingRope
        }
    }
}
    
    struct CardioData {
        var title: String {
            return activityType.localizeString
        }
        var imageName: String = ""
        var bigImageName: String = ""
        var activityType: CardioActivity = .cycling
        
        static func getData() -> [CardioData] {
            [
                .init(imageName: "cardioRunning", bigImageName: "circuitRunning", activityType: CardioActivity.running),
                .init(imageName: "cardioCycling", bigImageName: "circuitCycling", activityType: CardioActivity.cycling),
                .init(imageName: "cardioElliptical", bigImageName: "circuitElliptical", activityType: CardioActivity.elliptical),
                .init(imageName: "cardioRowingMachine", bigImageName: "rowingMachineCircuit", activityType: CardioActivity.rowingMachine),
                .init(imageName: "cardioStairClimbing", bigImageName: "circuitStairs", activityType: CardioActivity.stairClimbing),
                .init(imageName: "swimmingIcon", activityType: CardioActivity.swimming)
            ]
        }
        
        static func createDefaultRow(to state: CardioActivity) -> CardioTableViewCell.Content {
            var item: CardioTableViewCell.Content = .init(imageName: "ic_clock",
                                                          timeValue: "00:00",
                                                          speedValue: "0.0",
                                                          inclineValue: "0.0",
                                                          paceValue: "00:00",
                                                          distanceValue: "0.0", activityType: state)
            switch state {
            case .running:
                break
            case .cycling:
                item = .init(imageName: "ic_clock",
                             timeValue: "00:00",
                             speedValue: "1",
                             inclineValue: "0\(Constants.defaultRPMandSPMSeparator)0",
                    paceValue: "",
                    distanceValue: "0.0", activityType: state)
            case .elliptical:
                item = .init(imageName: "ic_clock",
                             timeValue: "00:00",
                             speedValue: "1",
                             inclineValue: "0.0",
                             paceValue: "0\(Constants.defaultRPMandSPMSeparator)0",
                    distanceValue: "0.0", activityType: state)
            case .rowingMachine:
                item = .init(imageName: "ic_clock",
                             timeValue: "00:00",
                             speedValue: Constants.Cardio().mediumString,
                             inclineValue: "00:00",
                             paceValue: "0\(Constants.defaultRPMandSPMSeparator)0",
                    distanceValue: "0.0", activityType: state)
            case .stairClimbing:
                item = .init(imageName: "ic_clock",
                             timeValue: "00:00",
                             speedValue: "1",
                             inclineValue: "0",
                             paceValue: "0\(Constants.defaultRPMandSPMSeparator)0",
                    distanceValue: "", activityType: state)
            case .swimming:
                item = .init(imageName: "ic_clock",
                             timeValue: "00:00",
                             speedValue: Constants.Cardio().frontCrowlString,
                             inclineValue: Constants.Cardio().normalString,
                             paceValue: Constants.Cardio().mainSetString,
                             distanceValue: "0.0", activityType: state)
            case .jumpingRope:
                item = .init(imageName: "",
                             timeValue: "00:00",
                             speedValue: Constants.Circuit().normal,
                             inclineValue: "",
                             paceValue: "",
                             distanceValue: "1",
                             activityType: state)
            }
            return item
        }
}
