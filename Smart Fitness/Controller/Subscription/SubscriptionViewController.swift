//
//  SubscriptionViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 22.02.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit
import StoreKit

final class SubscriptionViewController: UIViewController, Storyboarded {
    
    // MARK: - Views
    @IBOutlet var labels: [UILabel]!
    @IBOutlet weak var monthPriceLabel: UILabel!
    @IBOutlet weak var yearPerMonthPriceLabel: UILabel!
    @IBOutlet weak var yearPriceLabel: UILabel!
    
    // MARK - Properties
    var subscription = SmartFitnessProducts()
    var windowFrame: CGRect = .zero
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        windowFrame = AppDelegate.shared.window?.frame ?? .zero
        AppDelegate.shared.window?.frame = UIScreen.main.bounds
        subscription.getProducts { (packages) in
            packages.forEach({ package in
                if package.product.productIdentifier == SmartFitnessProducts.Products.month.rawValue {
                    self.monthPriceLabel.text = package.product.localizedPrice
                }
                if package.product.productIdentifier == SmartFitnessProducts.Products.oneYear.rawValue {
                    self.yearPriceLabel.text = package.product.localizedPrice
                    self.yearPerMonthPriceLabel.text = package.product.pricePerMonth
                }
            })
        }
        let deviceName = UIDevice.modelName
        if deviceName.contains("iPhone SE") || deviceName.contains("iPhone 5s")  {
            labels.forEach({
                $0.font = Constants.returnFontName(style: .semibold, size: 12)
            })
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppDelegate.shared.window?.frame = UIScreen.main.bounds
        navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let deviceName = UIDevice.modelName
        labels.forEach {
            if deviceName.contains("iPhone SE") || deviceName.contains("iPhone 5s")  {
                $0.font = Constants.returnFontName(style: .semibold, size: 12)
            }
            $0.setContentCompressionResistancePriority(UILayoutPriority.required, for: .vertical)
        }
        view.layoutIfNeeded()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        AppDelegate.shared.window?.frame = windowFrame
        navigationController?.isNavigationBarHidden = false
    }
    
    
    // MARK: - Actions
    @IBAction func closeDidTap(_ sender: Any) {
        dismiss(animated: true)
    }
    
    @IBAction private func trialDidTap(_ sender: Any) {
        subscription.buyMonth(completion:  { [weak self] result in
            switch result {
                
            case .success(let success):
                if success {
                    RedirectManager.shared.presentRootController()
                    self?.dismiss(animated: false)
                }
            case .failure(let error):
                self?.showAlert(text: error.localizedDescription)
            }
        })
    }
    
    @IBAction private func subscribeDidTap(_ sender: Any) {
        subscription.buyYear(completion:  { [weak self] result in
            switch result {
                
            case .success(let success):
                if success {
                    RedirectManager.shared.presentRootController()
                    self?.dismiss(animated: false)
                }
            case .failure(let error):
                self?.showAlert(text: error.localizedDescription)
            }
        })
    }
    
    @IBAction func restoreDidTap(_ sender: Any) {
        subscription.restore(completion: { [weak self] result in
            switch result {
                
            case .success(let success):
                if success {
                    RedirectManager.shared.presentRootController()
                    self?.dismiss(animated: false)
                }
            case .failure(let error):
                self?.showAlert(text: error.localizedDescription)
            }
        })
    }
    
    @IBAction func termsAction(_ sender: Any) {
        let vc = TermsOfUseViewController.instantiate()
        vc.urlString = "https://www.smartfitnessapp.com/termsofservice/"
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func privacyAction(_ sender: Any) {
        let vc = TermsOfUseViewController.instantiate()
        vc.urlString = "https://www.smartfitnessapp.com/privacypolicy/"
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension SKProduct {
    
    var localizedPrice: String? {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = priceLocale
        return formatter.string(from: price)
    }
    
    var pricePerMonth: String? {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = priceLocale
        return formatter.string(from: price.dividing(by: 12))
    }
    
}


