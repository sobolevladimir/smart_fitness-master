//
//  ProgramsCollectionViewCell.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 18.07.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class ProgramsCollectionViewCell: UICollectionViewCell {

    // MARK: - Outlets
    @IBOutlet weak private var programImageView: UIImageView!
    @IBOutlet weak private var favouriteButton: UIButton!
    @IBOutlet weak private var programTitleLabel: UILabel!
    @IBOutlet weak private var crownImageView: UIImageView!
    @IBOutlet weak private var bicepsStackView: UIStackView!

    // MARK: - Properties

    // MARK: - Init
    func set(content: DefaultProgram) {
        bicepsStackView.subviews.enumerated().forEach({ biceps in
            guard let imView = biceps.element as? UIImageView else { return }
            imView.image = content.complicityImageName(for: biceps.offset).image()
        })
        crownImageView.isHidden = !(content.isCrown)
        favouriteButton.setImage(content.favouriteImageName.image(), for: .normal)
    }

    // MARK: - Actions
    @IBAction private func favouriteDidTap(_ sender: Any) {
    }

    // MARK: - Private

}
