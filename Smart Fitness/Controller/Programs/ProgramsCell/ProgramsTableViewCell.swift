//
//  ProgramsTableViewCell.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 16.07.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class ProgramsTableViewCell: UITableViewCell {

    // MARK: - Outlets
    @IBOutlet weak private var collectionView: UICollectionView!
    @IBOutlet weak private var typeProgramsLabel: UILabel!
    @IBOutlet weak private var countLabel: UILabel!

    // MARK: - Properties
    var dataSource: [DefaultProgram] = []

    // MARK: - Init
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.register(cellType: ProgramsCollectionViewCell.self)
        collectionView.dataSource = self
        collectionView.delegate = self
    }

    func set(content: DefaultPrograms) {
        typeProgramsLabel.text = content.title
        countLabel.text = content.programsCount

        dataSource = content.programs
        collectionView.reloadData()
    }
}

// MARK: - UICollectionViewDataSource
extension ProgramsTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        dataSource.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(with: ProgramsCollectionViewCell.self, for: indexPath)
        cell.set(content: dataSource[indexPath.row])
        return cell
    }

}

// MARK: - UICollectionViewDelegateFlowLayout
extension ProgramsTableViewCell: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: 139, height: 171)
    }
}
