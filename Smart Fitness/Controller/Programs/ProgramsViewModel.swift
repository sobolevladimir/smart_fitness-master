//
//  ProgramsViewModel.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 16.07.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import Foundation

struct ProgramsViewModel {
    var dataSource: [DefaultPrograms] = [.init(programs: [.init(programs: .init(id: 1, cardioProgram: [], resistanceProgram: [], circuitProgram: [], programInfo: .init(traineeName: nil, trainerName: nil, buildingDate: 111111111, updateDate: 22222222, programName: "hey", imageUrl: nil), programType: .cardio), isFavourite: true, complicityIndex: 0, imageUrl: ""), .init(programs: .init(id: 1, cardioProgram: [], resistanceProgram: [], circuitProgram: [], programInfo: .init(traineeName: nil, trainerName: nil, buildingDate: 111111111, updateDate: 22222222, programName: "hey", imageUrl: nil), programType: .cardio), isFavourite: false, complicityIndex: 1, imageUrl: "")], title: "Full body workouts"),
                                         .init(programs: [.init(programs: .init(id: 1, cardioProgram: [], resistanceProgram: [], circuitProgram: [], programInfo: .init(traineeName: nil, trainerName: nil, buildingDate: 111111111, updateDate: 22222222, programName: "hey", imageUrl: nil), programType: .cardio), isFavourite: true, complicityIndex: 5, imageUrl: "")], title: "Full body workouts")
    ]
}

struct DefaultPrograms: Codable {

    var programs: [DefaultProgram]
    var title: String
    var programsCount: String {
        return "\(programs.count) Programs"
    }
}

struct DefaultProgram: Codable {
    let programs: Program
    var isFavourite: Bool
    let complicityIndex: Int
    let imageUrl: String

    var favouriteImageName: String {
        return isFavourite ? "starBluePrograms" : "starBlueBorderPrograms"
    }

    var isCrown: Bool {
        complicityIndex > 3
    }

    func complicityImageName(for index: Int) -> String {
        if index <= complicityIndex {
            return "bicepsWhitePrograms"
        } else {
            return "bicepsWhiteBorderPrograms"
        }
    }
}
