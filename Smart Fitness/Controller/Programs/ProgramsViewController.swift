//
//  ProgramsViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 16.07.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class ProgramsViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak private var tableView: UITableView!
    
    // MARK: - Menagers
    var viewModel: ProgramsViewModel
    var router: ProgramsRouter
    
    // MARK: - Properties
    var didOpenMenu: (() -> Void)?
    
    // MARK: - Init
    init(viewModel: ProgramsViewModel) {
        self.viewModel = viewModel
        self.router = ProgramsRouter()
        super.init(nibName: nil, bundle: nil)
        self.router.viewController = self
    }

    required init?(coder: NSCoder) {
        fatalError("Not supported!")
    }

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    // MARK: - Setup Methods
    private func setupUI() {
        setupNavigationBar()
       
        tableView.register(cellType: ProgramsTableViewCell.self)
        tableView.contentInset.top = 17
    }
    
    // MARK: - Actions
    @objc func menuDidTap() { didOpenMenu?() }
    
    @objc func starDidTap() {  }
    
    // MARK: - Private
    private func setupNavigationBar() {
        title = Constants.TabBarStrings().programs
        navigationController?.navigationBar.setBlueGradient()
        navigationController?.setTitleFont()
        navigationItem.leftBarButtonItem = .init(image: #imageLiteral(resourceName: "ic_menu"), landscapeImagePhone: nil, style: .done, target: self, action: #selector(menuDidTap))
        navigationItem.leftBarButtonItem?.tintColor = .white
        
//        navigationItem.rightBarButtonItem = .init(image: #imageLiteral(resourceName: "starWhitePrograms"), landscapeImagePhone: nil, style: .done, target: self, action: #selector(starDidTap))
//        navigationItem.rightBarButtonItem?.tintColor = .white
        
    }
    
}

// MARK: - UITableViewDataSource
extension ProgramsViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.dataSource.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(with: ProgramsTableViewCell.self, for: indexPath)
        cell.set(content: viewModel.dataSource[indexPath.row])
        return cell
    }
}

// MARK: -
extension ProgramsViewController { }
