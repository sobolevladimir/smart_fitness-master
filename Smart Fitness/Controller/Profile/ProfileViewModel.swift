//
//  ProfileViewModel.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 24.05.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import Kingfisher
import FBSDKCoreKit
import FBSDKLoginKit

final class ProfileViewModel {
    
    // MARK: - Properties
    var facebookPhotoManager: FacebookPhotoManager?
    var user: InstagramTestUser?
    
    var dataSource: [ProfileViewController.Content] = []
    var currentIndex = 0
    var userNeedsUpdate = true
    
    var insertItemAt: ((Int) -> Void)?
    
    lazy var dateFormater: DateFormatter = {
        let df = DateFormatter()
        switch LocalizationManager.Language.language {
        case .english:
            df.locale = Locale(identifier: "en")
        case .hebrew:
            df.locale = Locale(identifier: "he_IL")
        }
        df.dateFormat = "dd MMM, yyyy"
        return df
    }()
    
    // MARK: - Methods
    func getCurrentUser(completion: @escaping ((Result<User, Error>) -> Void)) {
        if let user = UserAuthManager.user {
            completion(.success(user))
        }
        
        UserProvider.getMe {  (result) in
            
            switch result {
            case .success(let user):
                UserAuthManager.setUser(user)
                completion(.success(user))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func getStories(completion: @escaping ((Result<Bool, Error>) -> Void)) {
        NetworkRouter.performRequest(request: .getAllStories) { [weak self] (result) in
            guard let self = self else { return }
            switch result {
                
            case .success(let successResponse):
                switch Parser.parseResponseData(successResponse.data, for: Stories.self) {
                case .success(let data):
                    data.s3Urls.enumerated().forEach { [weak self] story in
                        guard let self = self else { return }
                        let ds: [ProfileViewController.Content] = data.s3Urls.map { .init(imageUrl: $0.s3Urls, image: nil, date: self.dateFormater.string(from: Date(milliseconds: $0.date)), timestamp: story.element.date, id: $0.id) }
                        self.dataSource = ds.sorted(by: { $0.timestamp > $1.timestamp })
                        self.downloadImages()
                        completion(.success(true))
                    }
                case .failure(let error):
                    completion(.failure(error))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    private func downloadImages() {
        dataSource.enumerated().forEach({ story in
            if story.element.image == nil {
                let cache = ImageCache.default
                var key = story.element.imageUrl
                if let separator = story.element.imageUrl.range(of: "?") {
                    key.removeSubrange(separator.lowerBound..<story.element.imageUrl.endIndex)
                }
                cache.retrieveImage(forKey: "\(key)original") { result in
                    switch result {
                    case .success(let value):
                        if value.image == nil {
                            let resource = ImageResource(downloadURL: URL(string: story.element.imageUrl)!, cacheKey: story.element.imageUrl)
                            KingfisherManager.shared.retrieveImage(with: resource, options: [
                                .cacheOriginalImage
                            ], progressBlock: nil, downloadTaskUpdated: nil) { (result) in
                                switch result {
                                case .success(let value):
                                    self.dataSource[story.offset].image = value.image
                                case .failure(let error):
                                    print(error)
                                }
                            }
                        } else {
                            self.dataSource[story.offset].image = value.image
                        }
                        
                    case .failure(let error):
                        print(error)
                    }
                }
            }
        })
    }
    
    func deleteStory(completion: @escaping ((Result<Bool, Error>) -> Void)) {
        NetworkRouter.performRequest(request: .deleteStory(id: dataSource[currentIndex].id)) { (result) in
            switch result {
            case .success:
                let cache = ImageCache.default
                var key = self.dataSource[self.currentIndex].imageUrl
                if let separator = self.dataSource[self.currentIndex].imageUrl.range(of: "?") {
                    key.removeSubrange(separator.lowerBound..<self.dataSource[self.currentIndex].imageUrl.endIndex)
                }
                cache.removeImage(forKey: "\(key)original")
                self.dataSource.remove(at: self.currentIndex)
                completion(.success(true))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func addStory(image: UIImage, completion: @escaping ((Result<Bool, Error>) -> Void)) {
        var resizedImage = image
        if image.size.height > 1900 || image.size.width > 1900 {
            resizedImage = image.resize(size: .init(width: 1920, height: 1920))
        }
        
        guard let data = resizedImage.jpeg(.low) else { return }
        NetworkRouter.performRequest(request: .saveStory(data: data)) { [weak self] (result) in
            guard let self = self else { return }
            switch result {
            case .success(let ok):
                switch Parser.parseResponseData(ok.data, for: Story.self) {
                    
                case .success(let story):
                    self.dataSource.insert(.init(imageUrl: story.s3Urls, image: resizedImage, date: self.dateFormater.string(from: Date(milliseconds: story.date)), timestamp: story.date, id: story.id), at: 0)
                    let cache = ImageCache.default
                    
                    var key = story.s3Urls
                    if let separator = story.s3Urls.range(of: "?") {
                        key.removeSubrange(separator.lowerBound..<story.s3Urls.endIndex)
                    }
                    
                    cache.store(resizedImage, forKey: "\(key)original")
                    self.insertItemAt?(1)
                    completion(.success(true))
                case .failure(let error):
                    completion(.failure(error))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    private func loginWithFacebook(from vc: UIViewController, completion: @escaping (Result<Bool, Error>) -> Void) {
        FacebookLogin().loginWithFacebook(from: vc) { [weak self] (result) in
            switch result {
            case .success(_):
                self?.getImageFromFacebook(rootVC: vc, completion: completion)
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func addPhotoFromFacebook(from vc: UIViewController, completion: @escaping (Result<Bool, Error>) -> Void) {
        if FBSDKLoginKit.AccessToken.isCurrentAccessTokenActive {
            if (FBSDKLoginKit.AccessToken.current?.hasGranted(permission: "user_photos"))! {
                getImageFromFacebook(rootVC: vc, completion: completion)
            } else {
                FBSDKLoginKit.LoginManager().logOut()
                loginWithFacebook(from: vc, completion: completion)
            }
            
        } else {
            loginWithFacebook(from: vc, completion: completion)
        }
        
    }
    
    private func getImageFromFacebook(rootVC: UIViewController, completion: @escaping (Result<Bool, Error>) -> Void) {
        facebookPhotoManager = FacebookPhotoManager()
        facebookPhotoManager?.fetchFacebookAlbums() { [weak self] result in
            switch result {
            case .success(let albums):
                let vc = FacebookAlbumsViewController(viewModel: FacebookAlbumsViewModel(dataSource: albums), title: Constants.Profile().profileFacebookStories)
                vc.didSelectImage = { [weak self] image in
                    guard let self = self else { return }
                    self.addStory(image: image, completion: completion)
                }
                let navBar = UINavigationController(rootViewController: vc)
                navBar.modalPresentationStyle = .fullScreen
                rootVC.present(navBar, animated: true, completion: nil)
                self?.facebookPhotoManager = nil
            case .failure(let error):
                completion(.failure(error))
                return
            }
        }
    }
    
    func addPhotoFromInstagram(from vc: UIViewController, completion: @escaping (Result<Bool, Error>) -> Void) {
        if let user = self.user {
            showInstagramPhotosVC(for: user, rootVc: vc, completion: completion)
        } else {
            SocialLoginManager().instagram(from: vc) { (user) in
                self.user = user
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: { [weak self] in
                    self?.showInstagramPhotosVC(for: user, rootVc: vc, completion: completion)
                })
            }
        }
    }
    
    private func showInstagramPhotosVC(for user: InstagramTestUser, rootVc: UIViewController, completion: @escaping (Result<Bool, Error>) -> Void) {
        let vc = ProfileSelectedImageViewController(viewModel: ProfileSelectedImageViewModel(dataSource: []), title: Constants.Profile().profileInstagramStories)
        vc.didSelectImage = { [weak self] image in
            guard let self = self else { return }
            self.addStory(image: image, completion: completion)
        }
        
        InstagramApi.shared.getMedia(testUserData: user) { (media) in
            if media.media_type != .VIDEO {
                DispatchQueue.main.async {
                    vc.viewModel.dataSource.append(media.media_url)
                }
            }
        }
        
        let navVC = UINavigationController(rootViewController: vc)
        navVC.modalPresentationStyle = .fullScreen
        rootVc.present(navVC, animated: true, completion: nil)
    }
    
    func uploadUserAvatar(image: UIImage, completion: @escaping ((Result<UIImage, Error>) -> Void)) {
        let resizedImage = image.resize(size: .init(width: 300, height: 300))
        guard let data = resizedImage.jpeg(.low) else { return }
        UserProvider.uploadUserImage(data: data) { (result) in
            switch result {
            case .success(let user):
                UserAuthManager.setUser(user)
                completion(.success(resizedImage))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}

extension ProfileViewController {
    struct Content {
        var imageUrl: String
        var image: UIImage?
        var date: String
        var timestamp: Int64 = 0
        var id: Int
    }
}
