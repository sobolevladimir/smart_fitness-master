//
//  TakePhotoProtocol.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 16.05.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

protocol TakePhotoProtocol: class {
    func close(with photoRouter: TakePhotoViewController.PhotoRouter)
   
}
