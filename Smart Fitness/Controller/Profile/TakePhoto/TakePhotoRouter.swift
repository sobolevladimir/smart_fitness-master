//
//  TakePhotoRouter.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 16.05.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit.UIViewController

final class TakePhotoRouter {
    
    weak var viewController: TakePhotoViewController?
    
    deinit {
        print("deinit TakePhotoRouter")
    }
}

extension TakePhotoRouter: TakePhotoProtocol {
    func close(with photoRouter: TakePhotoViewController.PhotoRouter) {
        viewController?.dismiss(animated: false, completion: { [weak self] in
            self?.viewController?.didSelectedRoute?(photoRouter)
        })
    }
}

extension TakePhotoRouter {}
