//
//  TakePhotoViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 16.05.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class TakePhotoViewController: UIViewController {
    
    enum PhotoRouter {
        case photo, camera, facebook, instagram, none
    }
    
    // MARK: - Outlets
    
    // MARK: - Menagers
    var viewModel: TakePhotoViewModel
    var router: TakePhotoRouter
    
    // MARK: - Properties
    var didSelectedRoute: ((PhotoRouter) -> Void)?
    
    // MARK: - Init
    init(viewModel: TakePhotoViewModel) {
        self.viewModel = viewModel
        self.router = TakePhotoRouter()
        super.init(nibName: nil, bundle: nil)
        self.router.viewController = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not supported!")
    }
    
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        UIView.animate(withDuration: 0.3) { [weak self] in
            self?.view.alpha = 1
        }
    }
    
    // MARK: - Setup Methods
    private func setupUI() {
        view.alpha = 0
    }
    
    // MARK: - Actions
    @IBAction func closeDidTap(_ sender: Any) {
        animateDismiss(with: .none)
    }
    
    @IBAction func galleryDidTap(_ sender: Any) {
        animateDismiss(with: .photo)
    }
    
    @IBAction func instagramDidTap(_ sender: Any) {
        animateDismiss(with: .instagram)
    }
    
    @IBAction func facebookDidTap(_ sender: Any) {
        animateDismiss(with: .facebook)
    }
    
    @IBAction func takePhotoDidTap(_ sender: Any) {
        animateDismiss(with: .camera)
    }
    
    
    // MARK: - Private
    private func animateDismiss(with photoRouter: PhotoRouter) {
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            self?.view.alpha = 0
        }) { [weak self] (c) in
            self?.router.close(with: photoRouter)
        }
    }
}

// MARK: -
extension TakePhotoViewController { }

// MARK: -
extension TakePhotoViewController { }
