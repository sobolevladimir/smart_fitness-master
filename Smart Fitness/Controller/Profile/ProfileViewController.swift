//
//  ProfileViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 05.02.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit
//import Kingfisher
import AXPhotoViewer

class ProfileViewController: BaseViewController {
    
    // MARK: - Views
    @IBOutlet weak var segmentedView: ProfileSegmentedControl!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var birthdayLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var yourProgressLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK: - Properties
    var photosViewController: AXPhotosViewController?
    let contentView = UIView()
    var didOpenMenu: (() -> Void)?
    var didUpdateAvatar: ((UIImage) -> Void)?
    
    lazy var avatarPickerController = UIImagePickerController()
    lazy var storyPhotoPickerController = UIImagePickerController()
    lazy var storyCameraPickerController = UIImagePickerController()
    
    // MARK: - Managers
    var viewModel = ProfileViewModel()
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.register(cellType: AddStoryCell.self)
        yourProgressLabel.isHidden = viewModel.dataSource.count > 0
        navigationController?.navigationBar.setBlueGradient()
        navigationController?.setTitleFont()
        viewModel.insertItemAt = { [weak self] index in
            self?.stopAnimation()
            self?.collectionView.insertItems(at: [IndexPath(item: index, section: 0)])
            self?.yourProgressLabel.isHidden = self!.viewModel.dataSource.count > 0
        }
        
        startAnimation()
        viewModel.getStories { [weak self] (result) in
            self?.stopAnimation()
            switch result {
            case .success:
                self?.collectionView.reloadData()
                self?.yourProgressLabel.isHidden = self!.viewModel.dataSource.count > 0
            case .failure(let error):
                self?.showAlert(text: error.localizedDescription)
            }
        }
        
        viewModel.getCurrentUser { [weak self] (result) in
             self?.stopAnimation()
            switch result {
            case .success(let user):
                self?.setUsersData(user: user)
            case .failure(let error):
                self?.showAlert(text: error.localizedDescription)
            }
        }
    }
    
    // MARK: - Actions
    @IBAction func backDidTap(_ sender: Any) {
        didOpenMenu?()
    }
    
    @IBAction func editDidTap(_ sender: Any) {
        let vc = EditProfileViewController.instantiate()
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)
        vc.didEditUser = { [weak self] user in self?.setUsersData(user: user)}
    }
    
    @IBAction func avatarDidTap(_ sender: Any) {
        avatarPickerController.delegate = self
        avatarPickerController.allowsEditing = true
        avatarPickerController.mediaTypes = ["public.image"]
        avatarPickerController.sourceType = .photoLibrary
        present(avatarPickerController, animated: true)
    }
    
    @IBAction func shareDidTap(_ sender: Any) {
        guard let photo = viewModel.dataSource[viewModel.currentIndex].image else { return }
        let activityVC = UIActivityViewController(activityItems: [photo], applicationActivities: nil)
        activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList, .saveToCameraRoll]
        if let popoverController = activityVC.popoverPresentationController {
            popoverController.sourceRect = CGRect(x: UIScreen.main.bounds.width / 2, y: UIScreen.main.bounds.height / 2, width: 0, height: 0)
            popoverController.sourceView = self.view
            popoverController.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
        }
        activityVC.popoverPresentationController?.sourceView = photosViewController?.view
        
        photosViewController?.present(activityVC, animated: true, completion: nil)
        
        UIView.animate(withDuration: 0.3) {
            self.contentView.alpha = 0
        }
    }
    
    @IBAction func deleteDidTap(_ sender: Any) {
        let popUpVC = LogoutViewController.instantiate()
        popUpVC.state = .deleteImage
        photosViewController?.present(popUpVC, animated: false)
        popUpVC.didDelete = { [weak self] in
            guard let self = self else { return }
            self.viewModel.deleteStory(completion: { [weak self] isDeleted in
                guard let self = self else { return }
                switch isDeleted {
                case .success:
                    self.collectionView.deleteItems(at: [IndexPath(item: self.viewModel.currentIndex + 1, section: 0)])
                    self.yourProgressLabel.isHidden = self.viewModel.dataSource.count > 0
                    self.photosViewController?.dismiss(animated: false) {
                        self.showAlert(text: Constants.Profile().imageDeleted)
                    }
                    UIView.animate(withDuration: 0.3) { [weak self] in
                        self?.contentView.alpha = 0
                    }
                case .failure(let error):
                    self.photosViewController?.dismiss(animated: false) {
                        self.handleCompletion(result: .failure(error))
                    }
                }
            })
        }
    }
    
    // MARK: - Private
    private func setUsersData(user: User) {
        titleLabel.text = user.username ?? "Unknown"
        if let weight = user.weight {
            weightLabel.text = "\(weight)"
        } else {
            weightLabel.text = "70.0"
        }
        
        levelLabel.text = user.level ?? Constants.Profile().beginner
        genderLabel.text = user.gender ?? Constants.Profile().male
        
        if let height = user.height {
            heightLabel.text = "\(height)"
        } else {
            heightLabel.text = "170"
        }
        
        profileImageView.downloadImage(from: user.imageUrl, placeholder: profileImageView.image)
        guard let timeInterval = user.birthDay else {
            let date = Date(timeIntervalSince1970: 631152000.0)
            birthdayLabel.text = viewModel.dateFormater.string(from: date)
            return
        }
        
        birthdayLabel.text = viewModel.dateFormater.string(from: Date(timeIntervalSince1970: timeInterval))
    }
    
    private func addPhotoFromPhone() {
        storyPhotoPickerController.delegate = self
        storyPhotoPickerController.allowsEditing = false
        storyPhotoPickerController.mediaTypes = ["public.image"]
        storyPhotoPickerController.sourceType = .photoLibrary
        present(storyPhotoPickerController, animated: true)
    }
    
    private func addPhotoFromCamera() {
        storyCameraPickerController.delegate = self
        storyCameraPickerController.allowsEditing = false
        storyCameraPickerController.mediaTypes = ["public.image"]
        storyCameraPickerController.sourceType = .camera
        present(storyCameraPickerController, animated: true)
    }
    
    private func addPhotoFromFacebook() {
        viewModel.addPhotoFromFacebook(from: self, completion: { [weak self] result in self?.handleCompletion(result: result) })
    }
    
    private func addPhotoFromInstagram() {
        viewModel.addPhotoFromInstagram(from: self, completion: { [weak self] result in self?.handleCompletion(result: result) })
    }
    
    private func handleCompletion(result: Result<Bool, Error>) {
        switch result {
        case .success:
            let popUpVC = ComingSoonViewController.instantiate()
            popUpVC.state = .addStory
            present(popUpVC, animated: false, completion: nil)
        case .failure(let error):
            showAlert(text: error.localizedDescription)
            let popUpVC = ComingSoonViewController.instantiate()
            popUpVC.state = .failedToUpload
            present(popUpVC, animated: false, completion: nil)
        }
    }
}

// MARK: - UIImagePickerControllerDelegate
extension ProfileViewController: UIImagePickerControllerDelegate {
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
    
    public func imagePickerController(_ picker: UIImagePickerController,
                                      didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        picker.delegate = nil
        viewModel.userNeedsUpdate = false
        
        picker.dismiss(animated: true) { [weak self] in
            guard let self = self else { return }
            self.startAnimation()
            if picker == self.avatarPickerController {
                guard let image = info[.editedImage] as? UIImage else { return }
                self.viewModel.uploadUserAvatar(image: image)  { [weak self] result in
                    self?.stopAnimation()
                    switch result {
                    case .success(let resizedImage):
                         self?.profileImageView.image = resizedImage
                         self?.didUpdateAvatar?(resizedImage)
                    case .failure(let error):
                        self?.showAlert(text: error.localizedDescription)
                        let popUpVC = ComingSoonViewController.instantiate()
                        popUpVC.state = .failedToUpload
                        self?.present(popUpVC, animated: false, completion: nil)
                    }
                }
            }
            guard let image = info[.originalImage] as? UIImage else { return }
            if picker == self.storyPhotoPickerController || picker == self.storyCameraPickerController {
                self.viewModel.addStory(image: image, completion: { [weak self] result in self?.handleCompletion(result: result) })
            }
        }
    }
}

// MARK: - UINavigationControllerDelegate
extension ProfileViewController: UINavigationControllerDelegate { }

// MARK: - UICollectionViewDataSource
extension ProfileViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.dataSource.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.item == 0 {
            let cell = collectionView.dequeueReusableCell(with: AddStoryCell.self, for: indexPath)
            return cell
        }
        let cell = collectionView.dequeueReusableCell(with: ProfileStoriesCell.self, for: indexPath)
        cell.set(content: viewModel.dataSource[indexPath.item - 1])
        return cell
    }
}

// MARk: - UICollectionViewDelegateFlowLayout
extension ProfileViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: false)
        if indexPath.item == 0 {
            let vc = TakePhotoViewController(viewModel: TakePhotoViewModel())
            vc.modalPresentationStyle = .overFullScreen
            present(vc, animated: false)
            vc.didSelectedRoute = { [weak self] type in
                switch type {
                case .photo:
                    self?.addPhotoFromPhone()
                case .camera:
                    self?.addPhotoFromCamera()
                case .facebook:
                    self?.addPhotoFromFacebook()
                case .instagram:
                    self?.addPhotoFromInstagram()
                case .none:
                    break
                }
            }
        } else {
            openImage(at: indexPath.item - 1, dataSource: viewModel.dataSource) { (index) in
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: 92, height: 107.5)
    }
}

extension ProfileViewController: AXPhotosViewControllerDelegate {
    func photosViewController(_ photosViewController: AXPhotosViewController, willUpdate overlayView: AXOverlayView, for photo: AXPhotoProtocol, at index: Int, totalNumberOfPhotos: Int) {
        photosViewController.overlayView.title = viewModel.dataSource[index].date
        viewModel.currentIndex = index
    }
    
    func photosViewController(_ photosViewController: AXPhotosViewController, overlayView: AXOverlayView, visibilityWillChange visible: Bool) {
        if contentView.alpha == 1 {
            UIView.animate(withDuration: 0.3) { [weak self] in
                self?.contentView.alpha = 0
            }
        }
    }
    
    func openImage(at index: Int, dataSource: [ProfileViewController.Content], completion: @escaping (Int) -> Void) {
        let dataSource = AXPhotosDataSource(photos:
            dataSource.map({
                AXPhoto(attributedTitle: NSAttributedString(string: ""),
                        image: $0.image)
            }),
                                            initialPhotoIndex: index,
                                            prefetchBehavior: .aggressive)
        viewModel.currentIndex = index
        let transitionInfo = AXTransitionInfo(interactiveDismissalEnabled: true, startingView: nil) {  (_, newIndex) -> UIImageView? in
            RotationManager.lockOrientation(.portrait, andRotateTo: .portrait)
            completion(newIndex)
            return nil
        }
        photosViewController = AXPhotosViewController(dataSource: dataSource, pagingConfig: nil, transitionInfo: transitionInfo)
        photosViewController?.view.backgroundColor = .white
        photosViewController?.overlayView.rightBarButtonItems = [.init(image: #imageLiteral(resourceName: "whiteDots"), style: .done, target: self, action: #selector(showMenu))]
        
        photosViewController?.overlayView.leftBarButtonItem = .init(image: UIImage(named: "closePhoto"), style: .done, target: self, action: #selector(closePhotoViewController))
        
        photosViewController?.delegate = self
        photosViewController?.modalPresentationStyle = .overFullScreen
        RotationManager.lockOrientation(.all)
        guard let photosViewController = photosViewController else { return }
        photosViewController.overlayView.title = self.viewModel.dataSource[index].date
        photosViewController.overlayView.titleTextAttributes = [.font: Constants.returnFontName(style: .regular, size: 23), NSAttributedString.Key.foregroundColor : UIColor.white]
        setupMenuView()
        self.present(photosViewController, animated: true)
    }
    
    private func setupMenuView() {
        contentView.backgroundColor = .white
        contentView.layer.cornerRadius = 4
        contentView.alpha = 0
        guard let photosViewController = photosViewController else { return }
        photosViewController.view.addSubview(contentView)
        let window = UIApplication.shared.keyWindow
        let topPadding = window?.safeAreaInsets.top ?? 0
        switch LocalizationManager.Language.language {
        case .english:
            contentView.frame = .init(x: photosViewController.view.frame.width - 141 - 3, y: topPadding + 4, width: 141, height: 113)
        case .hebrew:
            contentView.frame = .init(x: 3, y: topPadding + 4, width: 141, height: 113)
        }
        createButtons()
    }
    
    private func createButtons() {
        let font = Constants.returnFontName(style: .regular, size: 23)
        
        let button1 = UIButton()
        button1.setImage(#imageLiteral(resourceName: "imageShareIcon"), for: .normal)
        button1.setTitle("Share".localized(), for: .normal)
        button1.setTitleColor(.black, for: .normal)
        button1.titleLabel?.font = font
        
        button1.addTarget(self, action: #selector(shareDidTap(_:)), for: .touchUpInside)
        
        let button2 = UIButton()
        button2.setImage(#imageLiteral(resourceName: "imageDeleteIcon"), for: .normal)
        button2.setTitle("Delete".localized(), for: .normal)
        button2.setTitleColor(.black, for: .normal)
        button2.titleLabel?.font = font
        
        button2.addTarget(self, action: #selector(deleteDidTap(_:)), for: .touchUpInside)
        
        let stackView = UIStackView(arrangedSubviews: [button1, button2])
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        stackView.spacing = 8
        contentView.addSubview(stackView)
        stackView.frame = contentView.bounds
        
        switch LocalizationManager.Language.language {
            
        case .english:
            button1.imageEdgeInsets.left = -24
            button2.imageEdgeInsets.left = -20
        case .hebrew:
            button1.imageEdgeInsets.left = 24
            button2.imageEdgeInsets.left = 20
        }
    }
    
    @objc func showMenu() {
        photosViewController?.overlayView.layoutSubviews()
        photosViewController?.view.bringSubviewToFront(contentView)
        UIView.animate(withDuration: 0.3) {  [weak self] in
            self?.contentView.alpha = 1
        }
    }
    
    @objc func closePhotoViewController() {
        photosViewController?.dismiss(animated: false)
    }
}
