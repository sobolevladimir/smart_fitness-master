//
//  ProfileStoriesCell.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 17.05.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit.UICollectionViewCell

final class ProfileStoriesCell: UICollectionViewCell {
    
    // MARK: - Outlets
    @IBOutlet weak private var dateLabel: UILabel!
    @IBOutlet weak private var storyImageView: UIImageView!
    
    // MARK: - Init
    func set(content: ProfileViewController.Content) {
        if let image = content.image {
            storyImageView.image = image
        } else {
            storyImageView.downloadImage(from: content.imageUrl, isResizing: false)
            self.layoutIfNeeded()
        }
        dateLabel.text = content.date
    }
}
