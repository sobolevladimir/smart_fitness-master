//
//  ProfileSelectedImageProtocol.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 17.05.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

protocol ProfileSelectedImageProtocol: class {
    func close()
    func goBack()
}
