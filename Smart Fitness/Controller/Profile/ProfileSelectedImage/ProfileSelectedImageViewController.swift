//
//  ProfileSelectedImageViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 17.05.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class ProfileSelectedImageViewController: BaseViewController {
    
    // MARK: - Outlets
    @IBOutlet weak private var noPhotosLabel: UILabel!
    @IBOutlet weak private var collectionView: UICollectionView!
  
    // MARK: - Menagers
    var viewModel: ProfileSelectedImageViewModel
    var router: ProfileSelectedImageRouter
    
    // MARK: - Properties
    var isCloseButton = true
    var didSelectImage: ((UIImage) -> Void)?
    
    
    // MARK: - Init
    init(viewModel: ProfileSelectedImageViewModel, title: String) {
           self.viewModel = viewModel
           self.router = ProfileSelectedImageRouter()
           super.init(nibName: nil, bundle: nil)
           self.router.viewController = self
        self.title = title
       }

       required init?(coder: NSCoder) {
           fatalError("Not supported!")
       }

       
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        if self.viewModel.dataSource.count == 0 {
            startAnimation()
        }
      
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) { [weak self] in
            self?.stopAnimation()
            if self?.viewModel.dataSource.count == 0 {
                self?.noPhotosLabel.isHidden = false
            }
        }
    }
    
    // MARK: - Setup Methods
    private func setupUI() {
        collectionView.register(cellType: ImageStoryCell.self)
        viewModel.isAppended = { [weak self] in
            guard let self = self else { return }
            self.noPhotosLabel.isHidden = true
            self.stopAnimation()
            self.collectionView.insertItems(at: [IndexPath(item: self.viewModel.dataSource.count - 1, section: 0)])
        }
        
        let closeItem =  UIBarButtonItem(image: UIImage(named: "closePhoto"), style: .done, target: self, action: #selector(backDidTap))
        closeItem.tintColor = .white
        navigationItem.leftBarButtonItem = closeItem
        navigationController?.navigationBar.isTranslucent = false
        if !isCloseButton {
            self.fixDirection()
        } else {
            navigationController?.navigationBar.setBlueGradient()
        }
        navigationController?.setTitleFont()
    }
    
    // MARK: - Actions
    @IBAction func backDidTap(_ sender: Any) {
        if isCloseButton {
            router.close()
        } else {
            router.goBack()
        }
    }
    
    // MARK: - Private
  
}

// MARK: - UICollectionViewDataSource
extension ProfileSelectedImageViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(with: ImageStoryCell.self, for: indexPath)
        cell.set(imageURl: viewModel.dataSource[indexPath.item])
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension ProfileSelectedImageViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: collectionView.bounds.width / 3 - 20, height: collectionView.bounds.width / 3 - 20)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? ImageStoryCell,
        let image = cell.storyImageView.image else { return }
        collectionView.isUserInteractionEnabled = false
        collectionView.deselectItem(at: indexPath, animated: false)
        dismiss(animated: true, completion: nil)
        didSelectImage?(image)
       
    }
}

// MARK: - UIScrollViewDelegate
extension ProfileSelectedImageViewController: UIScrollViewDelegate {
  
}
