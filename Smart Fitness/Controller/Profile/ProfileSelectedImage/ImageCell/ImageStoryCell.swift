//
//  ImageStoryCell.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 17.05.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

class ImageStoryCell: UICollectionViewCell {

    @IBOutlet weak var storyImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func set(imageURl: String) {
        storyImageView.downloadImageFromInstagram(from: imageURl)
    }

}

