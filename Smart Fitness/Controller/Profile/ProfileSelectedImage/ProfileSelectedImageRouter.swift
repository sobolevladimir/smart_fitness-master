//
//  ProfileSelectedImageRouter.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 17.05.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit.UIViewController

final class ProfileSelectedImageRouter {
    
    weak var viewController: UIViewController?
    
    deinit {
        print("deinit ProfileSelectedImageRouter")
    }
}

extension ProfileSelectedImageRouter: ProfileSelectedImageProtocol {
    func goBack() {
        viewController?.navigationController?.popViewController(animated: true)
    }
    
    func close() {
        viewController?.dismiss(animated: true, completion: nil)
    }
}

extension ProfileSelectedImageRouter {}
