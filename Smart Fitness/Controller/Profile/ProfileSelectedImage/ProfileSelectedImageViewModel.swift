//
//  ProfileSelectedImageViewModel.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 17.05.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import Foundation

struct ProfileSelectedImageViewModel {
    var dataSource: [String] {
        didSet {
            isAppended?()
        }
    }
    
    var isAppended: (() -> Void)?
}

