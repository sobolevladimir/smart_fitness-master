//
//  FacebookAlbumsProtocol.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 24.05.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

protocol FacebookAlbumsProtocol: class {
    func dismis()
    func showSelectedAlbum(album: FacebookAlbum, title: String, completion: ((UIImage) -> Void)?)
}
