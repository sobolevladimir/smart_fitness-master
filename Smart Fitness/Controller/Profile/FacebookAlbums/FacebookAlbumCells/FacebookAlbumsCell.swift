//
//  FacebookAlbumsCell.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 24.05.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class FacebookAlbumsCell: UITableViewCell {
    
    // MARK: - Outlets
    @IBOutlet weak private var albumLabel: UILabel!
    @IBOutlet weak private var albumImageView: UIImageView!
    
    // MARK: - Init
    func set(imageUrl: String, name: String) {
        albumLabel.text = name
        albumImageView.downloadImageFromInstagram(from: imageUrl)
    }
}
