//
//  FacebookAlbumsRouter.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 24.05.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit.UIViewController

final class FacebookAlbumsRouter {
    
    weak var viewController: UIViewController?
    
    deinit {
        print("deinit FacebookAlbumsRouter")
    }
}

extension FacebookAlbumsRouter: FacebookAlbumsProtocol {
    
    func dismis() {
        viewController?.dismiss(animated: true, completion: nil)
    }
    
    func showSelectedAlbum(album: FacebookAlbum, title: String, completion: ((UIImage) -> Void)?) {
        FacebookPhotoManager.shared.fbAlbumsPictureRequest(album: album) { result in
            switch result {
                
            case .success(let ok):
                let originalPhotos = Array(Set(ok.photos))
                guard let photosUrl: [String] = originalPhotos.map({ $0.fullSizeUrl }) as? [String] else { return }
                
                DispatchQueue.main.async {
                    let vc = ProfileSelectedImageViewController(viewModel: ProfileSelectedImageViewModel(dataSource: photosUrl), title: title)
                    vc.isCloseButton = false
                    vc.didSelectImage = { image in
                         completion?(image)
                    }
                    self.viewController?.navigationController?.pushViewController(vc, animated: true)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
}

extension FacebookAlbumsRouter {}
