//
//  FacebookAlbumsViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 24.05.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class FacebookAlbumsViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Menagers
    var viewModel: FacebookAlbumsViewModel
    var router: FacebookAlbumsRouter
    
    // MARK: - Properties
    var didSelectImage: ((UIImage) -> Void)?
    
    // MARK: - Init
    init(viewModel: FacebookAlbumsViewModel, title: String) {
        self.viewModel = viewModel
        self.router = FacebookAlbumsRouter()
        super.init(nibName: nil, bundle: nil)
        self.router.viewController = self
        self.title = title
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not supported!")
    }
    
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    // MARK: - Setup Methods
    private func setupUI() {
        tableView.register(cellType: FacebookAlbumsCell.self)
        tableView.tableFooterView = UIView()
        navigationController?.navigationBar.setBlueGradient()
        let closeItem =  UIBarButtonItem(image: UIImage(named: "closePhoto"), style: .done, target: self, action: #selector(close))
        closeItem.tintColor = .white
        navigationItem.leftBarButtonItem = closeItem
        navigationController?.setTitleFont()
    }
    
    // MARK: - Private
    @objc private func close() {
        dismiss(animated: true, completion: nil)
    }
}

// MARK: - UITableViewDataSource
extension FacebookAlbumsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(with: FacebookAlbumsCell.self, for: indexPath)
        cell.set(imageUrl: viewModel.dataSource[indexPath.row].coverUrl?.absoluteString ?? "", name: viewModel.dataSource[indexPath.row].name ?? "")
        return cell
    }
    
    
}

// MARK: - UITableViewDelegate
extension FacebookAlbumsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        router.showSelectedAlbum(album: viewModel.dataSource[indexPath.row], title: title ?? "", completion: { [weak self] image in
            self?.didSelectImage?(image)
        })
    }
}
