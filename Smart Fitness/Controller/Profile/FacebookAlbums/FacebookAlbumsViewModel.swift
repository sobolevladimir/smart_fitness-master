//
//  FacebookAlbumsViewModel.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 24.05.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import Foundation

struct FacebookAlbumsViewModel {
    var dataSource: [FacebookAlbum]
}
