//
//  EditProfileCell.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 05.02.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class EditProfileCell: UITableViewCell {
    
    // MARK: - Views
    @IBOutlet weak private var editIconImageView: UIImageView!
    @IBOutlet weak private var titleLabel: UILabel!
    @IBOutlet weak private var subTitleLabel: UILabel!
    @IBOutlet weak private var valueLabel: UILabel!
    
    // MARK: - Properties
    lazy var dateFormater: DateFormatter = {
        let df = DateFormatter()
        switch LocalizationManager.Language.language {
        case .english:
            df.locale = Locale(identifier: "en")
        case .hebrew:
            df.locale = Locale(identifier: "he_IL")
        }
        df.dateFormat = "dd MMM, yyyy"
        return df
    }()
    
    // MARK: - Init Method
    func set(content: Content) {
        editIconImageView.image = content.icon.image()
        titleLabel.text = content.title
        subTitleLabel.text = content.subtitle
        if content.type == .dateOfBirth {
            guard let timeInterval = content.value as? Double else { return }
            let date = Date(timeIntervalSince1970: timeInterval)
            valueLabel.text = dateFormater.string(from: date)
        } else {
            valueLabel.text = content.value as? String
        }
        
    }
}

// MARK: - Content
extension EditProfileCell {
    struct Content {
        var icon: String
        var type: ProfileEditType
        var title: String {
            type.localizedString
        }
        var subtitle: String
        var value: Any
    }
}
