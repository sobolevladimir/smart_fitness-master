//
//  EditProfileViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 05.02.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

enum ProfileEditType: String {
    case dateOfBirth, weight, height, gender, level
    
    var localizedString: String {
        switch self {
        case .dateOfBirth:
            return Constants.Profile().dateOfBirth
        case .weight:
            return Constants.Profile().weight
        case .height:
            return Constants.Profile().height
        case .gender:
            return Constants.Profile().gender
        case .level:
            return Constants.Profile().level
        }
    }
}

final class EditProfileViewController: BaseViewController {
    
    // MARK: - Views
    @IBOutlet weak private var tableView: UITableView!
    
    // MARK: - Properties
    private var data: [EditProfileCell.Content] = [
        .init(icon: "profileCalendar", type: .dateOfBirth, subtitle: Constants.Profile().calculations, value: UserAuthManager.user?.birthDay ?? 631152000.0),
        .init(icon: "profileEditKg", type: .weight, subtitle: Constants.Profile().calculations, value: "\(UserAuthManager.user?.weight ?? 70.0) \(Constants.Profile().kg)"),
        .init(icon: "profileHeightIcon", type: .height, subtitle: Constants.Profile().calculations, value: "\(UserAuthManager.user?.height ?? 170) \(Constants.Profile().cm)"),
        .init(icon: "profileGender", type: .gender, subtitle: Constants.Profile().calculations, value: UserAuthManager.user?.gender ?? Constants.Profile().male),
        .init(icon: "profileLevel", type: .level, subtitle: Constants.Profile().requiredPlan, value: UserAuthManager.user?.level ?? Constants.Profile().beginner)
    ]
    
    var didEditUser: ((User) -> Void)?
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.contentInset.bottom = 100
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        fixDirection()
    }
    
    // MARK: - Actions
    @IBAction private func backDidTap(_ sender: Any) { navigationController?.popViewController(animated: true) }
    
    @IBAction private func doneDidTap(_ sender: Any) { saveEditedUser() }
    
    // MARK: - Private
    private func saveEditedUser() {
        getDataForUser { (user) in
            UserAuthManager.setUser(user)
            let gender: String
            if user.gender == Constants.Profile().male {
                gender = "Male"
            } else {
                gender = "Female"
            }
            let level: String
            if user.level == Constants.Profile().beginner {
                level = "Beginner"
            } else if user.level == Constants.Profile().intermediate {
                level = "Intermediate"
            } else {
                level = "Expert"
            }
            
            startAnimation()
            UserProvider.editMe(gender: gender,
                                level: level,
                                height: user.height ?? 0,
                                weight: user.weight ?? 0,
                                birthDay: user.birthDay ?? 0) { [weak self] result in
                                    self?.stopAnimation()
                                    switch result {
                                    case .success(let value):
                                        self?.didEditUser?(value)
                                        self?.navigationController?.popViewController(animated: true)
                                    case .failure:
                                        self?.navigationController?.popViewController(animated: true)
                                    }
            }
        }
    }
    
    private func getDataForUser(completion: (User) -> Void) {
        guard let user = UserAuthManager.user else { return }
        defer {
            completion(user)
        }
        data.forEach { item in
            switch item.type {
            case .dateOfBirth:
                user.birthDay = (item.value as? Double) ?? 0
            case .weight:
                if var weight = item.value as? String {
                    switch LocalizationManager.Language.language {
                    case .english:
                        weight.removeLast(3)
                    case .hebrew:
                        weight.removeLast(4)
                    }
                    
                    user.weight = weight.toDouble() ?? 0.0
                }
            case .height:
                if var height = item.value as? String {
                    switch LocalizationManager.Language.language {
                    case .english:
                        height.removeLast(3)
                    case .hebrew:
                        height.removeLast(4)
                    }
                    UserAuthManager.user?.height = Int(height) ?? 0
                }
            case .gender:
                UserAuthManager.user?.gender = item.value as? String
            case .level:
                UserAuthManager.user?.level = item.value as? String
            }
        }
    }
}

// MARK: - UITableViewDataSource
extension EditProfileViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(with: EditProfileCell.self, for: indexPath)
        cell.set(content: data[indexPath.row])
        return cell
    }
}

// MARK: - UITableViewDelegate
extension EditProfileViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch data[indexPath.row].type {
            
        case .dateOfBirth:
            let vc = EditBirthDateViewController.instantiate()
            if let timeInterval = data[indexPath.row].value as? Double {
                vc.set(date: timeInterval)
            }
            vc.saveDidTap = { [weak self] date in
                self?.data[indexPath.row].value = date
                tableView.reloadRows(at: [indexPath], with: .fade)
            }
            present(vc, animated: false)
            
        case .weight, .height:
            let vc = EditWeightViewController.instantiate()
            if let initialValue = data[indexPath.row].value as? String {
                vc.set(state: data[indexPath.row].type == .weight ? .weight : .height, initialValue: initialValue)
            }
            
            vc.saveDidTap = { [weak self] value in
                self?.data[indexPath.row].value = value
                
                tableView.reloadRows(at: [indexPath], with: .fade)
            }
            present(vc, animated: false)
            
        case .gender:
            let vc = EditGenderViewController.instantiate()
            if let index = GenderData.data.lastIndex(where: { $0.name == data[indexPath.row].value as? String}) {
                vc.set(imageNames: GenderData.data.map({ $0.imageName }), title: GenderData.male.title, index: index)
            } else {
                vc.set(imageNames: GenderData.data.map({ $0.imageName }), title: GenderData.male.title, index: 0)
            }
            
            vc.didSelectValueAt = { [weak self] index in
                self?.data[indexPath.row].value = GenderData.data[index].name
                
                tableView.reloadRows(at: [indexPath], with: .fade)
            }
            present(vc, animated: false)
            
        case .level:
            let vc = EditGenderViewController.instantiate()
            if let index = LevelData.data.lastIndex(where: { $0.name == data[indexPath.row].value as? String}) {
                vc.set(imageNames: LevelData.data.map({ $0.imageName }), title: LevelData.beginner.title, index: index)
            } else {
                vc.set(imageNames: LevelData.data.map({ $0.imageName }), title: LevelData.beginner.title, index: 0)
            }
            vc.didSelectValueAt = { [weak self] index in
                self?.data[indexPath.row].value = LevelData.data[index].name
                
                tableView.reloadRows(at: [indexPath], with: .fade)
            }
            present(vc, animated: false)
        }
    }
}

enum GenderData: String {
    case male, female
    
    var name: String {
        switch self {
        case .male: return Constants.Profile().male
        case .female: return Constants.Profile().female
        }
    }
    
    var imageName: String {
        switch LocalizationManager.Language.language {
        case .english:
            switch self {
            case .male: return "profileMale"
            case .female: return "profileFemale"
            }
        case .hebrew:
            switch self {
            case .male: return "profileMaleHebrew"
            case .female: return "profileFemaleHebrew"
            }
        }
    }
    
    var title: String {
        Constants.Profile().gender
    }
    
    static var data: [Self] = [.male, .female]
}

enum LevelData: String {
    case beginner, intermediate, expert
    
    var name: String {
        switch self {
        case .beginner: return Constants.Profile().beginner
        case .intermediate: return Constants.Profile().intermediate
        case .expert: return Constants.Profile().expert
        }
    }
    
    var imageName: String {
        switch LocalizationManager.Language.language {
        case .english:
            switch self {
                
            case .beginner: return "profileBeginner"
            case .intermediate: return "profileIntermediate"
            case .expert: return "profileExpert"
            }
        case .hebrew:
            switch self {
                
            case .beginner: return "profileBeginnerHebrew"
            case .intermediate: return "profileIntermediateHebrew"
            case .expert: return "profileExpertHebrew"
            }
        }
    }
    
    var title: String {
        Constants.Profile().level
    }
    
    static var data: [Self] = [.beginner, .intermediate, .expert]
}
