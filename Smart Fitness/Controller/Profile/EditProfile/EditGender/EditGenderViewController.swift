//
//  EditGenderViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 05.02.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

class EditGenderViewController: UIViewController, Storyboarded {
    
    // MARK: - Views
    @IBOutlet weak private var titleLabel: UILabel!
    @IBOutlet private var buttonsArray: [RadioCheckButton]!
    @IBOutlet private var imagesArray: [UIImageView]!
    
    // MARK: - Constraints
    @IBOutlet weak var containerViewWidthAnchor: NSLayoutConstraint!
    
    // MARK: - Properties
    private var imageNames: [String] = []
    private var editTile: String = ""
    private var index: Int = 0
    var didSelectValueAt: ((Int) -> Void)?
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        imageNames.enumerated().forEach({ index, imageName in
            buttonsArray[index].isHidden = false
            imagesArray[index].image = imageName.image()
            buttonsArray[index].addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        })
        self.titleLabel.text = editTile
        buttonsArray.forEach { $0.isChecked = false }
        buttonsArray[index].isChecked = true
       
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        UIView.animate(withDuration: 0.1) {  [weak self] in
            self?.view.alpha = 1
        }
        view.layoutIfNeeded()
    }
    
    // MARK: - Public Init
    func set(imageNames: [String], title: String, index: Int) {
        self.imageNames = imageNames
        self.editTile = title
        self.index = index
    }
    
    // MARK: - Actions
    @IBAction func closeTapped(_ sender: Any) {
        UIView.animate(withDuration: 0.1, animations: { [weak self] in
            self?.view.alpha = 0
        }) { [weak self] _ in
            self?.dismiss(animated: false, completion: nil)
        }
    }
    
    @objc func buttonAction(sender: RadioCheckButton) {
        buttonsArray.forEach { $0.isChecked = false }
        sender.isChecked = true
        buttonsArray.enumerated().forEach({ index, button in
            if sender == button {
                didSelectValueAt?(index)
            }
        })
        closeTapped(UIButton())
    }
    
    // MARK: - Private
}

extension EditGenderViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return touch.view == gestureRecognizer.view
    }
}

