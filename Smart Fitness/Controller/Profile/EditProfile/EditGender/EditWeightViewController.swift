//
//  EditWeightViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 06.02.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class EditWeightViewController: UIViewController, Storyboarded {
    
    enum State {
        case weight, height
    }
    
    // MARK: - Views
    @IBOutlet weak private var containerView: UIView!
    @IBOutlet weak private var titleLabel: UILabel!
    @IBOutlet weak private var valueLabel: UILabel!
    @IBOutlet weak private var pickerView: UIPickerView!
    
    // MARK: - Constraints
    @IBOutlet weak private var topConstraint: NSLayoutConstraint!
    
    // MARK: - Properties
    private lazy var dataHeight: [String] = (130...220).map({ "\($0)"})
    private lazy var dataWeight = ResistanceDataCreator().kgData
    private var state: State = .height
    private var initialValue: String = ""
    
    var saveDidTap: ((String) -> Void)?
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        pickerView.semanticContentAttribute = .forceLeftToRight
        setupUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        topConstraint.constant = containerView.frame.height
        UIView.animate(withDuration: 0.3) { [weak self] in
            self?.view.layoutSubviews()
        }
    }
    
    // MARK: - Init Method
    func set(state: State, initialValue: String) {
        self.state = state
        self.initialValue = initialValue
        self.initialValue.removeLast()
        self.initialValue.removeLast()
        self.initialValue.removeLast()
    }
    
    // MARK: - Actions
    @IBAction private func cancelDidTap(_ sender: Any) { dismissWithAnimation() }
    
    @IBAction private func saveDidTap(_ sender: Any) {
        var space = " "
        if LocalizationManager.Language.language == .hebrew {
            space = ""
        }
        switch state {
        case .weight:
            if !initialValue.contains(Constants.Profile().kg) {
                initialValue += "\(space)\(Constants.Profile().kg)"
            }
            
        case .height:
            if !initialValue.contains(Constants.Profile().cm) {
                initialValue += "\(space)\(Constants.Profile().cm)"
            }
        }
        saveDidTap?(initialValue)
        dismissWithAnimation()
    }
    
    // MARK: - Private
    private func dismissWithAnimation() {
        topConstraint.constant = 0
        UIView.animate(withDuration: 0.3, animations: { [weak self] in self?.view.layoutSubviews()
        }) { [weak self] (_) in self?.dismiss(animated: false, completion: nil) }
    }
    
    private func setupUI() {
        switch state {
        case .weight:
            setValues(from: initialValue, pickerView: pickerView, data: dataWeight, outputTemplate: &outputKG)
        case .height:
            setValues(from: initialValue, pickerView: pickerView, data: [dataHeight], outputTemplate: &outputKG)
            titleLabel.text = Constants.Profile().height
            valueLabel.text = Constants.Profile().cm
        }
    }
    
    private func setValues(from value: String, pickerView: UIPickerView, data: [[String]], outputTemplate: inout [String]) {
        guard value != "" else { return }
        var editedValue = value
        if editedValue.last == " " {
            editedValue.removeLast()
        }
        if data.count >= 2 {
            let separator = data[1][0]
            let lastComponentRange: Range<String.Index> = editedValue.range(of: separator)!
            var lastComponentString = String(editedValue[lastComponentRange.upperBound...])
            if separator == "." {
                if lastComponentString == "00" {
                    lastComponentString = "0"
                }
                if lastComponentString == "5" {
                    lastComponentString = "50"
                }
            }
            let firstComponentRange: Range<String.Index> = editedValue.range(of: separator)!
            let firstComponentString = String(editedValue[..<firstComponentRange.lowerBound])
            
            if let selectedIndexForFirstComponent = data.first?.lastIndex(where: { $0 == firstComponentString} ) {
                pickerView.selectRow(selectedIndexForFirstComponent, inComponent: 0, animated: false)
                
                if let selectedIndexForLastComponent = data.last?.lastIndex(where: {
                    return $0 == lastComponentString
                    
                } ) {
                    pickerView.selectRow(selectedIndexForLastComponent, inComponent: 2, animated: false)
                    outputTemplate = [data[0][selectedIndexForFirstComponent], separator, data[2][selectedIndexForLastComponent]]
                }
            }
        } else {
            if let index = data.last?.lastIndex(where: { $0 == editedValue } ) {
                pickerView.selectRow(index, inComponent: 0, animated: false)
            }
        }
    }
    
    var outputKG = ["", ".", ""]
}

// MARK: - UIPickerViewDataSource
extension EditWeightViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        switch state {
        case .weight:
            return dataWeight.count
        case .height:
            return 1
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch state {
        case .weight:
            return dataWeight[component].count
        case .height:
            return dataHeight.count
        }
    }
}

// MARK: - UIPickerViewDelegate
extension EditWeightViewController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch state {
        case .weight:
            outputKG[component] = dataWeight[component][row]
            initialValue = "\(outputKG.first!).\(outputKG.last!) \(Constants.Profile().kg)"
        case .height:
            initialValue = "\(dataHeight[row]) \(Constants.Profile().cm)"
        }
        pickerView.reloadAllComponents()
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var color: UIColor!
        var label = UILabel()
        if let v = view { label = v as! UILabel }
        label.font = Constants.returnFontName(style: .regular, size: 15)
        label.textAlignment = .center
        switch state {
        case .weight:
            label.text = dataWeight[component][row]
        case .height:
            label.text = dataHeight[row]
        }
        
        if pickerView.selectedRow(inComponent: component) == row {
            color = UIColor.orange
        } else {
            color = UIColor.black
        }
        label.textColor = color
        return label
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        41
    }
    
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        switch component {
        case 0:
            return  40
        case 1:
            return 2
        default: return 30
        }
    }
}
