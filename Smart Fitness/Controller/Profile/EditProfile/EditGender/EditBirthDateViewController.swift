//
//  EditBirthDateViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 06.02.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class EditBirthDateViewController: UIViewController, Storyboarded {

    // MARK: - Views
    @IBOutlet weak private var containerView: UIView!
    @IBOutlet weak private var datePickerView: UIDatePicker!
    
    // MARK: - Constraints
    @IBOutlet weak private var topConstraint: NSLayoutConstraint!
    
    // MARK: - Properties
    private var date: Double = 0
    var saveDidTap: ((Double) -> Void)?
    

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        switch LocalizationManager.Language.language {
        case .english:
            datePickerView.locale = Locale(identifier: "en")
        case .hebrew:
            datePickerView.locale = Locale(identifier: "he_IL")
        }
        datePickerView.setDate(Date(timeIntervalSince1970: date), animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        topConstraint.constant = containerView.frame.height
        UIView.animate(withDuration: 0.3) { [weak self] in
            self?.view.layoutSubviews()
        }
    }
    
    // MARK: - Init method
    func set(date: Double) {
        self.date = date
    }
    
    // MARK: - Actions
    @IBAction private func cancelDidTap(_ sender: Any) { dismissWithAnimation() }
      
    @IBAction private func saveDidTap(_ sender: Any) {
        saveDidTap?(date)
        dismissWithAnimation()
      }
    
    @IBAction func didDateChanged(_ sender: UIDatePicker) {
        date = sender.date.timeIntervalSince1970
    }
    
    
    // MARK: - Private
    private func dismissWithAnimation() {
        topConstraint.constant = 0
        UIView.animate(withDuration: 0.3, animations: { [weak self] in self?.view.layoutSubviews()
        }) { [weak self] (_) in self?.dismiss(animated: false, completion: nil) }
    }
  
}
