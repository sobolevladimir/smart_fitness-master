//
//  RootTabBarViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 18.07.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class RootTabBarViewController: UITabBarController {
    
    // MARK: - Properties
    var homeNavigationController: RootNavigationController?
    var timerNavigationController: RootNavigationController?
    var programsNavigationController: RootNavigationController?
    var profileNavigationController: RootNavigationController?
    
    //   let leftMenuNavigationController = SideMenuNavigationController(rootViewController: SideMenuViewController(viewModel: SideMenuViewModel()))
    
    var didSelectMenuType: (() -> Void)?
    var didUpdateAvatar: ((UIImage) -> Void)?
    var didSelectedTab: ((Int) -> Void)?
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        //  setupSideMenuViewController()
        setupViewControllers()
        tabBar.tintColor = #colorLiteral(red: 0.06274509804, green: 0.7019607843, blue: 0.9215686275, alpha: 1)
        self.delegate = self
    }
    
    // MARK: - Public Methods
    func updateAllViews(isAds: Bool) {
        if let vc = homeNavigationController?.viewControllers.first as? HomeViewController {
            vc.updateAllViews(isAds: isAds)
        }
    }
    
    func selectVC(at index: Int) {
        selectedIndex = index
    }
    
    // MARK: - Private
    private func setupViewControllers() {
        viewControllers = []
        
        let homeVC = HomeViewController.instantiate()
        homeVC.didOpenMenu = { [weak self] in self?.didSelectMenuType?() }
        homeNavigationController = RootNavigationController(rootViewController: homeVC, selectedImageName: "tabBarHome", imageName: "tabBarHome", title: Constants.TabBarStrings().home)
        guard let homeNavigationController = homeNavigationController else { return }
        viewControllers = [homeNavigationController]
        
        let timerVC = TimerViewController.instantiate()
        timerVC.navigationState = .menu
        timerVC.menuDidTapAction = { [weak self] in self?.didSelectMenuType?()}
        timerNavigationController = RootNavigationController(rootViewController: timerVC, selectedImageName: "tabBarTimer", imageName: "tabBarTimer", title: Constants.TabBarStrings().timer)
        guard let timerNavigationController = timerNavigationController else { return }
        viewControllers?.append(timerNavigationController)
        
        let programsVC = ProgramsViewController(viewModel: ProgramsViewModel())
        programsVC.didOpenMenu = { [weak self] in self?.didSelectMenuType?() }
        programsNavigationController = RootNavigationController(rootViewController: programsVC, selectedImageName: "tabBarPrograms", imageName: "tabBarPrograms", title: Constants.TabBarStrings().programs)
        guard let programsNavigationController = programsNavigationController else { return }
        viewControllers?.append(programsNavigationController)
        //
        let profileVC = ProfileViewController.instantiate()
        profileVC.didUpdateAvatar = { [weak self] image in self?.didUpdateAvatar?(image) }
        profileVC.didOpenMenu = { [weak self] in self?.didSelectMenuType?() }
        profileNavigationController = RootNavigationController(rootViewController: profileVC, selectedImageName: "tabBarProfile", imageName: "tabBarProfile", title: Constants.TabBarStrings().profile)
        guard let profileNavigationController = profileNavigationController else { return }
        viewControllers?.append(profileNavigationController)
    }
    
}

extension RootTabBarViewController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        let fromView: UIView = tabBarController.selectedViewController!.view
        let toView  : UIView = viewController.view
        
        if fromView == toView {
            return false
        }
        
        
        UIView.transition(from: fromView, to: toView, duration: 0.3, options: UIView.AnimationOptions.transitionCrossDissolve) { (finished: Bool) in
            
        }
        return true
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        print(selectedIndex)
        didSelectedTab?(selectedIndex)
    }
}
