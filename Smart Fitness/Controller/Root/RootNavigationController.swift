//
//  RootNavigationController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 18.07.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit.UINavigationController

final class RootNavigationController: UINavigationController {

    // MARK: - Properties
    var isMenuOpen = false

    public override var preferredStatusBarStyle: UIStatusBarStyle {
        if isMenuOpen {
            return UIStatusBarStyle.lightContent
        } else {
            return UIStatusBarStyle.default
        }
    }

    override var prefersStatusBarHidden: Bool {
        if isMenuOpen {
            return true
        } else {
            return false
        }
    }

    // MARK: - Init
    required init(rootViewController: UIViewController, selectedImageName: String, imageName: String, title: String) {
        super.init(nibName: "\(Self.self)", bundle: nil)
        self.viewControllers.append(rootViewController)
        // super.init(rootViewController: rootViewController)
        //   tabBarItem.imageInsets = UIEdgeInsets(top: 10, left: 0, bottom: -10, right: 0)
        tabBarItem.image = UIImage(named: imageName)
        tabBarItem.selectedImage = UIImage(named: selectedImageName)
        tabBarItem.title = title
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBar.setBlueGradient()
        setTitleFont()
        navigationBar.setBackgroundImage(UIImage(), for: .default)
    }
}
