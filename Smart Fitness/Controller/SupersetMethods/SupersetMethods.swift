//
//  SupersetMethods.swift
//  Smart Fitness
//
//  Created by MCA 2 on 07/06/19.
//  Copyright © 2019 stratecore. All rights reserved.
//

import UIKit

class SupersetMethods: UIViewController {
    
    // INIT VIEW CONTROLLER
    class func initViewController() -> SupersetMethods? {
        let viewController:SupersetMethods = STORYBOARD_MAIN.instantiateViewController(withIdentifier: "SupersetMethods") as! SupersetMethods
        return viewController
    }
    
    // DECLARE OUTLET
    @IBOutlet var imgViewBg: UIImageView!
    @IBOutlet var tblView: UITableView!
    @IBOutlet var btnCreateSuperSet: UIButton!
    @IBOutlet var btnFinish: UIButton!
    
    // DECLARE VARIABLE
    var arrExercise:[MODEL_Exercise] = [MODEL_Exercise]()
    var selected_muscle:Model_muscle?
    
    override func viewDidLoad(){
        super.viewDidLoad()
        self.imgViewBg.image = UIImage.init(named: (selected_muscle?.musclePlaceholderIcon)!)
        self.methodSetTitleNavigation()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.methodSetTitleNavigation()
    }
    
    /// This methods set the navigation of the view controller
    func methodSetTitleNavigation(){
        // SET NAVIGATION BAR
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.isNavigationBarHidden = false
        
        // SET FONTS
        let titleFont:UIFont = UIFont.init(name: FONT_Semibold, size: 17)!
        self.navigationController?.navigationBar.methodSetFonts(font: (appDelegate!.setTheFontSize(titleFont, valueFontSizeForX: 17)!), color: UIColor.white)
        
        // SET TITLE
        self.navigationItem.title = "Superset Method"
        //        selectedIndex
        
        // SET LEFT BUTTON
        let leftButton:UIBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "ic_back"), style: .done, target: self, action: #selector(buttonBackClicked))
        leftButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = leftButton
        
        
        // SET CANCEL BUTTON
        let cancelButton:UIBarButtonItem = UIBarButtonItem.init(title: "Cancel", style: .plain, target: self, action: #selector(buttonCancelClicked))
        cancelButton.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItem = cancelButton
        
    }
    
    /// this method will call when the back button user pressed
    @objc func buttonBackClicked() {
        self.navigationController?.popViewController(animated: true)
    }
    
    /// this method will call when the cancel button user pressed
    @objc func buttonCancelClicked() {
        self.navigationController?.popViewController(animated: true)
    }
}
extension SupersetMethods:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CreateSuperSetCell",for:indexPath) as! CreateSuperSetCell
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrExercise.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        // create cell
//        let cell = tableView.dequeueReusableCell(withIdentifier: "CreateSuperSetWithOtherMuscleCell",for:indexPath) as! CreateSuperSetWithOtherMuscleCell
//        cell.selectionStyle = .none
//        cell.labelText.text = arrExercise[indexPath.row].exerciseName
//        return cell
//    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // PASS TO NEXT VIEW CONTROLLER
        let controller:SelectExerciseForMuscle = SelectExerciseForMuscle.initViewController()!
        controller.selected_muscle = selected_muscle
       // controller.delegate = self
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
}
