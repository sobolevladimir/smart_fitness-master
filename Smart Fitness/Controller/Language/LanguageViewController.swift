//
//  LanguageViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 25.10.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class LanguageViewController: UIViewController, Storyboarded {
    
    // MARK: - Views
    @IBOutlet var buttonsArray: [RadioCheckButton]!
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setButtons()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        UIView.animate(withDuration: 0.1) {  [weak self] in
            self?.view.alpha = 1
        }
    }
    
    // MARK: - Actions
    @IBAction func closeTapped(_ sender: Any) {
        UIView.animate(withDuration: 0.1, animations: { [weak self] in
            self?.view.alpha = 0
        }) { [weak self] _ in
            self?.dismiss(animated: false, completion: nil)
        }
    }
    
    @IBAction func englishSelected(_ sender: Any) {
        dismiss(animated: false, completion: nil)
        buttonsArray.forEach { $0.isChecked = false }
        LocalizationManager.setEnglish()
        RedirectManager.shared.presentRootController()
      //  SubscriptionManager.bannerView = nil
        self.dismiss(animated: false)
    }
    
    @IBAction func hebrewSelected(_ sender: Any) {
        buttonsArray.forEach { $0.isChecked = false }
        LocalizationManager.setHebrew()
        RedirectManager.shared.presentRootController()
      //  SubscriptionManager.bannerView = nil
        self.dismiss(animated: false)
    }
    
    // MARK: - Private
    private func setButtons() {
        switch LocalizationManager.Language.language {
        case .english:
            buttonsArray.forEach { $0.isChecked = false }
            buttonsArray[0].isChecked = true
        case .hebrew:
            buttonsArray.forEach { $0.isChecked = false }
            buttonsArray[1].isChecked = true
        }
    }
}

extension LanguageViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return touch.view == gestureRecognizer.view
    }
}
