//
//  SplashScreenVC.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 13/03/19.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import SwiftyGif

final class SplashScreenVC: UIViewController, Storyboarded {
    
    // MARK: - Views
    @IBOutlet weak var gifImageView: UIImageView!
    
    // MARK: - Properties
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Init
    override func viewDidLoad() {
        super.viewDidLoad()
        if traitCollection.horizontalSizeClass == .compact {
            SizeManager.setDeviceType(.iPhone)
        } else {
            SizeManager.setDeviceType(.iPad)
        }
    }
}
