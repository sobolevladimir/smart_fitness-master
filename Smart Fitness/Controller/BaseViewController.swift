//
//  BaseViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 23.03.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import SwiftyGif
import SystemConfiguration
import Alamofire

class BaseViewController: UIViewController, Storyboarded {
    
    // MARK: - Properties
    private  let gifImageView = UIImageView()
    
    lazy private var animationView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.4)
        view.frame = self.view.bounds
        view.addSubview(gifImageView)
        gifImageView.frame = CGRect(center: view.center, size: .init(width: 50, height: 100))
        let gif = try! UIImage(gifName: Constants.GifImages.splashGifName, levelOfIntegrity: 1)
        gifImageView.contentMode = .scaleAspectFit
        gifImageView.setGifImage(gif)
        return view
    }()
    
    // MARK: - Lifecycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setBarItemsFont()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !isInternetAvailable() {
            self.showAlert(text: Constants.Internet().noInternet)
        }
    }
    
    // MARK: - Methods
    func startAnimation() {
        view.addSubview(animationView)
        gifImageView.startAnimating()
    }
    
    func stopAnimation() {
        gifImageView.stopAnimating()
        animationView.removeFromSuperview()
    }
    
    // MARK: - Internet connection
    func isInternetAvailable() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
}
