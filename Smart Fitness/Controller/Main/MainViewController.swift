//
//  MainViewController.swift
//  Smart Fitness
//
//  Created by Rusłan Chamski on 27/10/2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit
import MessageUI
import AppLovinSDK

final class MainViewController: BaseViewController {
    
    typealias State = DraggableHorizontalSideView.State
    
    // MARK: - Views
    @IBOutlet weak private var shadowView: UIView!
    @IBOutlet weak private var sideContainerView: DraggableHorizontalSideView!
    @IBOutlet weak var adsContainer: UIView!
    private var sideMenuVC: SideMenuViewController!
    private var mainContainer = RootTabBarViewController()
    private var aboutVC: AboutViewController = .instantiate()
    private var receivedVC: ProgramsReceivedViewController = .instantiate()
    private var adsVC: AdsViewController = .instantiate()
    @IBOutlet weak private var adsContainerViewHeightConstraint: NSLayoutConstraint!
    
    // MARK: - Constraints
    @IBOutlet weak private var menuLeadingConstraint: NSLayoutConstraint!
    
    // MARK: - Properties
    var isOpen = false
    let adView = ALAdView(size: .banner)
    private var currentState: State {
        return sideContainerView.currentState
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        menuLeadingConstraint.constant = -view.frame.width
        setupExpandingView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(false)
        sideMenuVC.closeAction = {  [weak self] in
            self?.toggleSideMenu()
        }
        
        sideMenuVC.didSelectMenuType = { [weak self] type in
            self?.presentControllerFor(type: type)
            self?.toggleSideMenu()
        }
        
        mainContainer.didSelectMenuType = { [weak self] in
            self?.toggleSideMenu()
        }
        
        aboutVC.didOpenMenu = { [weak self] in
            self?.toggleSideMenu()
        }
        
        receivedVC.didOpenMenu = { [weak self] in
            self?.toggleSideMenu()
        }
        
        receivedVC.didAddProgram = { [weak self] program in
            if let nav = self?.mainContainer.viewControllers?[0] as? RootNavigationController {
                if let vc = nav.viewControllers[0] as? HomeViewController {
                    vc.insertProgram(program: program)
                }
            }
        }
        
        mainContainer.didUpdateAvatar = { [weak self] image in
            self?.sideMenuVC.userImageView.image = image
        }
        
        mainContainer.didSelectedTab = { [weak self] index in
            switch index {
            case 0:
                self?.sideMenuVC.selectedCell = .home
            case 3:
                self?.sideMenuVC.selectedCell = .profile
            default:
                 self?.sideMenuVC.selectedCell = .none
            }
            self?.sideMenuVC.tableView.reloadData()
           
        }
        setAds()
        
        if let _ = UserDefaults.standard.value(forKey: "push") {
            UserDefaults.standard.removeObject(forKey: "push")
            presentControllerFor(type: .program)
        }
        guard let programId: String = UserDefaults.standard.value(forKey: Constants.deepLink) as? String else { return }
        guard let userId = UserAuthManager.user?.id else { return }
        guard let programIDInt = Int(programId) else { return }
        ShareManager.shareProgram(userId: userId, programId: programIDInt) { [weak self] (result) in
            switch result {
            case .success:
                UserDefaults.standard.removeObject(forKey: Constants.deepLink)
                self?.presentControllerFor(type: .program)
            case .failure:
                break
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? SideMenuViewController {
            sideMenuVC = vc
        }
        
        if let destAds = segue.destination as? AdsViewController {
            self.adsVC = destAds
        }
        
        if let destinationTabController = segue.destination as? RootTabBarViewController {
            mainContainer = destinationTabController
        }
    }
    
    // MARK: - Actions
    @IBAction func menuTapped(_ sender: Any) {
        toggleSideMenu()
    }
    
    // MARK: - Private
    private func toggleSideMenu() {
        isOpen.toggle()
        shadowView.isHidden = isOpen ? false : true
        shadowView.alpha = isOpen ? 1 : 0
        menuLeadingConstraint.constant = isOpen ? 0 : -view.frame.width
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            self?.view.layoutIfNeeded()
            }, completion: nil)
    }
    
    private func updateHome(isAds: Bool) {
        mainContainer.updateAllViews(isAds: isAds)
    }
    
    func presentControllerFor(type: MenuType) {
        switch type {
        case .about, .program, .home, .profile:
            if let navBar = mainContainer.viewControllers?[mainContainer.selectedIndex] as? RootNavigationController {
                navBar.popToRootViewController(animated: false)
            }
        default:
            break
        }
        
        switch type {
        case .program:
            if let navBar = mainContainer.viewControllers?[mainContainer.selectedIndex] as? RootNavigationController {
                receivedVC.hidesBottomBarWhenPushed = true
                navBar.pushViewController(receivedVC, animated: false)
            }
        case .language:
            present(LanguageViewController.instantiate(), animated: false, completion: nil)
        case .activity:
            present(ComingSoonViewController.instantiate(), animated: false, completion: nil)
        case .gps:
            present(ComingSoonViewController.instantiate(), animated: false, completion: nil)
        case .feedback:
            sendEmail()
        case .share:
            share()
        case .about:
            if let navBar = mainContainer.viewControllers?[mainContainer.selectedIndex] as? RootNavigationController {
                aboutVC.hidesBottomBarWhenPushed = true
                navBar.pushViewController(aboutVC, animated: false)
            }
            
        //    mainContainer = aboutVC
        case .logout:
            present(LogoutViewController.instantiate(), animated: false, completion: nil)
        case .home:
            mainContainer.selectVC(at: 0)
        case .profile:
            mainContainer.selectVC(at: 3)
        case .none:
            break
        }
    }
    
    private func share() {
        let textToShare = "Check out Smart Fitness app"
        let image: UIImage = #imageLiteral(resourceName: "big_logo")
        if let myWebsite = URL(string: "https://smartfitnessapp.page.link/2irR") {
            let objectsToShare = [textToShare, myWebsite, image] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
            if let popoverController = activityVC.popoverPresentationController {
                popoverController.sourceRect = CGRect(x: UIScreen.main.bounds.width / 2, y: UIScreen.main.bounds.height / 2, width: 0, height: 0)
                popoverController.sourceView = self.view
                popoverController.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
            }
            activityVC.popoverPresentationController?.sourceView = self.view
            self.present(activityVC, animated: true, completion: nil)
        }
    }
    
    private func setupExpandingView() {
        sideContainerView.transitionAnimation = { [weak self] state in
            guard let self = self else { return }
            self.menuLeadingConstraint.constant = state.popupOffset
            self.view.layoutIfNeeded()
            
        }
        
        sideContainerView.transitionCompletion = { [weak self] state in
            guard let self = self else { return }
            self.menuLeadingConstraint.constant = state.popupOffset
            
        }
        
        sideContainerView.didCloseView = {
            self.toggleSideMenu()
        }
        
        sideContainerView.fractionCompletion = { [weak self] fraction in
            self?.shadowView.alpha = 1 - (-fraction * 1.4)
        }
    }
    
    func sendEmail() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["smartfitnessfeedback@gmail.com"])
            mail.setSubject("Smart Fitness Feedback")
            let version = Bundle.main.infoDictionary!["CFBundleShortVersionString"] ?? "no version"
            mail.setMessageBody("System info App v\(version), model \(UIDevice.modelName)", isHTML: true)
            
            present(mail, animated: true)
        } else {
            // show failure alert
            showAlert(text: "Your email app is not set")
        }
    }
}

// MARK: - UIGestureRecognizerDelegate
extension MainViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return touch.view == gestureRecognizer.view
    }
}

// MARK: - Animation
private extension DraggableHorizontalSideView.State {
    var popupOffset: CGFloat {
        switch self {
        case .open: return 0
        case .closed: return -UIScreen.main.bounds.width
        }
    }
}

// MARK: - MFMailComposeViewControllerDelegate
extension MainViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}

// MARK: - ALAdLoadDelegate
extension MainViewController: ALAdLoadDelegate {
    func setAds() {
        let isSubscribed = UserDefaults.standard.bool(forKey: Constants.isSubscribedKey)
        if isSubscribed {
            self.updateHome(isAds: false)
            self.adsContainerViewHeightConstraint.constant = 0
        } else {
            if SizeManager.device == .iPhone {
                self.adsContainerViewHeightConstraint.constant = 50
                self.adView.frame = .init(x: 0, y: 0, width: adsVC.view.frame.width, height: adsVC.view.frame.height)
            } else {
                self.adsContainerViewHeightConstraint.constant = 90
                self.adView.frame = .init(x: 0, y: 0, width: adsVC.view.frame.width, height: 90)
            }
            
            self.adView.adEventDelegate = self
            self.adView.adLoadDelegate = self
            self.adView.adDisplayDelegate = self
            self.adView.loadNextAd()
            adsVC.view.addSubview(self.adView)
            
            self.updateHome(isAds: true)
            
        }
        view.layoutIfNeeded()
    }
    
    public func adService(_ adService: ALAdService, didLoad ad: ALAd) {
        //  SubscriptionManager.ad = ad
    }
    
    public func adService(_ adService: ALAdService, didFailToLoadAdWithError code: Int32) {
        print(code)
    }
}

extension MainViewController: ALAdViewEventDelegate { }

extension MainViewController: ALAdDisplayDelegate {
    public func ad(_ ad: ALAd, wasDisplayedIn view: UIView) { }
    
    public func ad(_ ad: ALAd, wasHiddenIn view: UIView) { }
    
    public func ad(_ ad: ALAd, wasClickedIn view: UIView) { }
    
}

