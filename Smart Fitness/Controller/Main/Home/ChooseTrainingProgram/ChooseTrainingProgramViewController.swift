//
//  ChooseTrainingProgramViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 30.10.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

enum TrainingType {
    case resistance, circuit, cardio
}

final class ChooseTrainingProgramViewController: UIViewController, Storyboarded {
    
    // MARK: - Constraint
    @IBOutlet weak private var exclamationMarkLeadingConstraint: NSLayoutConstraint!
    
    // MARK: - Properties
    var didSelectedTraining: ((TrainingType) -> Void)?
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.alpha = 0
        switch LocalizationManager.Language.language {
        case .english:
            break
        case .hebrew:
            exclamationMarkLeadingConstraint.constant = 8
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
           super.viewDidAppear(true)
           UIView.animate(withDuration: 0.1) {  [weak self] in
               self?.view.alpha = 1
           }
       }
   
    // MARK: - Actions
    @IBAction private func cencelTapped(_ sender: Any) {
        animatedDismiss()
       }
    
    @IBAction private func resistanceTapped(_ sender: UIButton) {
        didSelectedTraining?(.resistance)
        animatedDismiss()
       
    }
    
    @IBAction private func circuitTapped(_ sender: Any) {
        didSelectedTraining?(.circuit)
        animatedDismiss()
    }
    
    @IBAction private func cardioTapped(_ sender: Any) {
        didSelectedTraining?(.cardio)
        animatedDismiss()
    }
    
    // MARK: - Private
    private func animatedDismiss() {
        UIView.animate(withDuration: 0.1, animations: { [weak self] in
            self?.view.alpha = 0
        }) { [weak self] _ in
            self?.dismiss(animated: false, completion: nil)
        }
    }
}

// MARK: - UIGestureRecognizerDelegate
extension ChooseTrainingProgramViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return touch.view == gestureRecognizer.view
    }
}

