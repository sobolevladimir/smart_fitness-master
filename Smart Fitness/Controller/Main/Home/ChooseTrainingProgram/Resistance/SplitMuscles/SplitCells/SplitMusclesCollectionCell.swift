//
//  SplitMusclesCollectionCell.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 12.11.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class SplitMusclesCollectionCell: UICollectionViewCell {
    
    // MARK: - Views
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    // MARK: - Init
    override func prepareForReuse() {
        super.prepareForReuse()
        imageView.image = nil
        titleLabel.text = nil
    }
    
    // MARK: - Methods
    func set(content: Content) {
        imageView.image = UIImage(named: content.imageName)
        titleLabel.text = content.title
        contentView.translatesAutoresizingMaskIntoConstraints = false
        let leftConstraint = contentView.leftAnchor.constraint(equalTo: leftAnchor)
        let rightConstraint = contentView.rightAnchor.constraint(equalTo: rightAnchor)
        let topConstraint = contentView.topAnchor.constraint(equalTo: topAnchor)
        let bottomConstraint = contentView.bottomAnchor.constraint(equalTo: bottomAnchor)
        NSLayoutConstraint.activate([leftConstraint, rightConstraint, topConstraint, bottomConstraint])
    }
}

// MARK: - Content
extension SplitMusclesCollectionCell {
    struct Content: Equatable, Codable {
        let imageName: String
        var day: Int
        var id: String
        var title: String
        var order: Int
        var groupName: SplitDays {
            SplitDays.returnSplitSymbol(from: day)
        }
    }
}

enum SplitDays: String, Codable {
    case a, b, c, d, e, f, none
    
    var string: String {
        switch self {
        case .a: return "A"
        case .b: return "B"
        case .c: return "C"
        case .d: return "D"
        case .e: return "E"
        case .f: return "F"
        case .none: return ""
        }
    }
    
    var number: Int {
        switch self {
            
        case .a: return 1
        case .b: return 2
        case .c: return 3
        case .d: return 4
        case .e: return 5
        case .f: return 6
        case .none: return 7
            
        }
    }
    
  static  func returnSplitSymbol(from number: Int) -> Self {
        switch number {
        case 0: return .a
        case 1: return .b
        case 2: return .c
        case 3: return .d
        case 4: return .e
        case 5: return .f
        default: return .none
        }
    }
}
