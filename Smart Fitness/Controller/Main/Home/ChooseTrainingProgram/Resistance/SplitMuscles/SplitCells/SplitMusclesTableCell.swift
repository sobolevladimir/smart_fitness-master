//
//  SplitMusclesTableCell.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 12.11.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit
import MobileCoreServices

final class SplitMusclesTableCell: UITableViewCell {
    
    // MARK: - Views
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var groupLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var dragHereLabel: UILabel!
    
    // MARK: - Properties
    var groups: [SplitMusclesCollectionCell.Content] = []
    var removeItem: ((IndexPath) -> Void)?
    var dropFromTop: ((String) -> Void)?
    var dropFromCell: ((IndexPath, IndexPath) -> Void)?
    var index: Int = 0
    
    // MARK: - Init
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.dragInteractionEnabled = true
        collectionView.dragDelegate = self
        collectionView.dropDelegate = self
        collectionView.dataSource = self
        removeItem = { [weak self] indexPath in
            guard let self = self else { return }
            self.groups.remove(at: indexPath.row)
            self.dragHereLabel.isHidden = !(self.groups.count == 0)
            self.collectionView.deleteItems(at: [indexPath])
        }
        if let longPressRecognizer = collectionView.gestureRecognizers?.compactMap({ $0 as? UILongPressGestureRecognizer}).first {
            longPressRecognizer.minimumPressDuration = 0.1
        }
      
        collectionView.semanticContentAttribute = .forceLeftToRight
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.collectionView.collectionViewLayout.invalidateLayout()
    }
    
    // MARK: - Methods
    func set(content: Content) {
        groupLabel.text = content.group
        dayLabel.text = content.dayNumber
    }
    
    // MARK: - Private
    private func addMuscleGroup(id: String) {
        guard !groups.contains(where: { $0.id == id}) else { return }
        let itemIndex =  groups.count
        guard let item = GroupsData().getData().first(where: { $0.id == id }) else { return }
        groups.append(.init(imageName: item.imageUrl, day: index, id: id, title: item.title, order: itemIndex))
        collectionView.insertItems(at: [IndexPath(item: itemIndex, section: 0)])
        dragHereLabel.isHidden = !(groups.count == 0)
    }
}

// MARK: - UICollectionViewDataSource
extension SplitMusclesTableCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        groups.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(with: SplitMusclesCollectionCell.self, for: indexPath)
        cell.set(content: groups[indexPath.item])
        return cell
    }
}

// MARK: - UICollectionViewDragDelegate
extension SplitMusclesTableCell: UICollectionViewDragDelegate {
    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        let item = groups[indexPath.row]
        guard let data = item.id.data(using: .utf8) else { return [] }
        let itemProvider = NSItemProvider(item: data as NSData, typeIdentifier: kUTTypePlainText as String)
        session.localContext = (IndexPath(row: index, section: 0), indexPath)

        UISelectionFeedbackGenerator().selectionChanged()
        
        return [UIDragItem(itemProvider: itemProvider)]
    }
}

extension SplitMusclesTableCell: UICollectionViewDropDelegate {
    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
   
        coordinator.session.loadObjects(ofClass: NSString.self) { [weak self] items in
            guard let self = self,
             let id = items.first as? String else { return }
            self.addMuscleGroup(id: id)
            self.dropFromTop?(id)
            guard let localContext = coordinator.session.localDragSession?.localContext ,
                let (tableViewIndexPath, collectionViewIndexPath) = localContext as? (IndexPath, IndexPath) else { return }
            self.dropFromCell?(tableViewIndexPath, collectionViewIndexPath)
        }
    }
}

// MARK: - CellContent
extension SplitMusclesTableCell {
    struct Content {
        let group: String
        let dayNumber: String
    }
}
