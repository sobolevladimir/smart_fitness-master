//
//  SplitMusclesViewComtroller.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 12.11.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit
import MobileCoreServices

final class SplitMusclesViewController: BaseViewController {
    
    // MARK: - Views
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Properties
    var splitedTrainings: (([SplitMusclesCollectionCell.Content]) -> Void)?
    var collectionContentData: [SplitMusclesCollectionCell.Content] = []
    
    var tableContentData: [SplitMusclesTableCell.Content] = [
        .init(group: "A", dayNumber: "1"),
        .init(group: "B", dayNumber: "2"),
        .init(group: "C", dayNumber: "3"),
        .init(group: "D", dayNumber: "4"),
        .init(group: "E", dayNumber: "5"),
        .init(group: "F", dayNumber: "6")
    ]
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setTitleFont()
        collectionView.dragInteractionEnabled = true
        collectionContentData = GroupsData().getData().map({ SplitMusclesCollectionCell.Content(imageName: $0.imageUrl, day: 6, id: $0.id, title: $0.title, order: 0) })
        if let longPressRecognizer = collectionView.gestureRecognizers?.compactMap({ $0 as? UILongPressGestureRecognizer}).first {
            longPressRecognizer.minimumPressDuration = 0.1
        }
        tableView.semanticContentAttribute = .forceLeftToRight
        collectionView.semanticContentAttribute = .forceLeftToRight
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationItem.hidesBackButton = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(false)
        switch LocalizationManager.Language.language {
            case .english:
             UIView.appearance().semanticContentAttribute = .forceLeftToRight
            collectionView.semanticContentAttribute = .forceLeftToRight
            tableView.semanticContentAttribute = .forceLeftToRight
            case .hebrew:
             UIView.appearance().semanticContentAttribute = .forceRightToLeft
            collectionView.semanticContentAttribute = .forceRightToLeft
            tableView.semanticContentAttribute = .forceRightToLeft
        }
    }
    
    // MARK: - Actions
    @IBAction func cancelDidTap(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func finishDidTap(_ sender: Any) {
        finishSplittingTraining()
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Private
    private func finishSplittingTraining() {
        var splitedTrainingData: [SplitMusclesCollectionCell.Content] = []
        for row in 0...5 {
            if let cell = tableView.cellForRow(at: IndexPath(row: row, section: 0)) as? SplitMusclesTableCell {
                 splitedTrainingData += cell.groups
            }
        }
         splitedTrainings?(splitedTrainingData)
    }
}

// MARK: - UITableViewDataSource
extension SplitMusclesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableContentData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(with: SplitMusclesTableCell.self, for: indexPath)
        cell.set(content: tableContentData[indexPath.row])
        cell.index = indexPath.row
        cell.dropFromTop = { [weak self] id in
            guard let self = self else { return }
            guard let index = self.collectionContentData.firstIndex(where: { $0.id == id}) else { return }
            self.collectionContentData.remove(at: index)
            self.collectionView.deleteItems(at: [IndexPath(item: index, section: 0)])
        }
        cell.dropFromCell = { tableViewIndexPath, collectionViewIndexPath in
            (self.tableView.cellForRow(at: tableViewIndexPath) as! SplitMusclesTableCell).removeItem?(collectionViewIndexPath)
        }
        return cell
    }
}

// MARK: - UICollectionViewDataSource
extension SplitMusclesViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        collectionContentData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(with: SplitMusclesCollectionCell.self, for: indexPath)
        cell.set(content: collectionContentData[indexPath.row])
        return cell
    }
}

// MARK: - UICollectionViewDragDelegate
extension SplitMusclesViewController: UICollectionViewDragDelegate {
    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        let name = collectionContentData[indexPath.row].id
        guard let data = name.data(using: .utf8) else { return [] }
        let itemProvider = NSItemProvider(item: data as NSData, typeIdentifier: kUTTypePlainText as String)
        
        UISelectionFeedbackGenerator().selectionChanged()

        return [UIDragItem(itemProvider: itemProvider)]
    }
}

// MARK: - UICollectionViewDropDelegate
extension SplitMusclesViewController: UICollectionViewDropDelegate {
    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
        guard let localContext = coordinator.session.localDragSession?.localContext,
            let (tableViewIndexPath, collectionViewIndexPath) = localContext as? (IndexPath, IndexPath) else { return }
        let destinationIndexPath = coordinator.destinationIndexPath
        (tableView.cellForRow(at: tableViewIndexPath) as! SplitMusclesTableCell).removeItem?(collectionViewIndexPath)
        
        coordinator.session.loadObjects(ofClass: NSString.self) { [weak self] items in
            guard let self = self else { return }
            guard let id = items.first as? String else { return }
            guard let item = GroupsData().getData().first(where: {$0.id == id}) else { return }
            self.collectionContentData.insert(.init(imageName: item.title, day: 6, id: id, title: item.title, order: destinationIndexPath?.row ?? 0), at: destinationIndexPath?.row ?? 0)
            collectionView.insertItems(at: [(destinationIndexPath ?? IndexPath(item: 0, section: 0))])
        }
    }
}
