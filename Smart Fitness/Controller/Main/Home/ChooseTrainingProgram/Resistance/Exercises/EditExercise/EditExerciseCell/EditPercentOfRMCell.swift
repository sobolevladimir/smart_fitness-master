//
//  EditPercentOfRMCell.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 05.12.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class EditPercentOfRMCell: UICollectionViewCell {
    
    // MARK: - Views
    @IBOutlet weak private var repsLabel: UILabel!
    @IBOutlet weak private var repsButton: LoginButton!
    @IBOutlet weak private var kgButton: LoginButton!
    @IBOutlet weak private var percentButton: LoginButton!
    @IBOutlet weak private var repsPickerView: CardioPickerView!
    @IBOutlet weak private var kgPickerView: CardioPickerView!
    @IBOutlet weak private var percentPickerView: CardioPickerView!
    @IBOutlet weak private var percentStackView: UIStackView!
    
    @IBOutlet weak private var dropDownTimeButton: UIButton!
    @IBOutlet weak private var repsStackView: UIStackView!
    @IBOutlet weak private var timeButton: LoginButton!
    @IBOutlet weak private var timeStackView: UIStackView!
    @IBOutlet weak private var timePickerView: CardioPickerView!
    @IBOutlet weak private var arrowButton: UIButton!
    
    @IBOutlet weak var allValuesStackView: UIStackView!
    // MARK: - Constraints
    @IBOutlet weak private var repsHeightAnchor: NSLayoutConstraint!
    @IBOutlet weak private var kgHeightAnchor: NSLayoutConstraint!
    @IBOutlet weak private var percentHeightAnchor: NSLayoutConstraint!
    
    @IBOutlet var timePickerViewHeightAnchor: NSLayoutConstraint!
    @IBOutlet var repsDropDownHeightAnchor: NSLayoutConstraint!
    
    // MARK: - Properties
    var method: EditPercentOfRMViewController.Method = .percent
    var saveDidTap: ((ExercisePercentSetCell.Content) -> Void)?
    private var item: ExercisePercentSetCell.Content?
    private var dataManager = ResistanceDataCreator()
    private var outputKG: [String] = ["", "", ""]
    private var outputReps: [String] = [""]
    private var outputPercent: [String] = [""]
    
    private var outputTime: [String] = ["00", ":", "00"]
    private var isTimeMode = false
    private var isKgOnly: Bool = false
    
    // MARK: - Methods
    func set(content: ExercisePercentSetCell.Content, method: EditPercentOfRMViewController.Method, isKgOnly: Bool) {
        contentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(viewDidTap)))
        item = content
        
        if isKgOnly {
            allValuesStackView.spacing = 0
            percentStackView.isHidden = true
            timeStackView.isHidden = true
            repsStackView.isHidden = true
            arrowButton.isHidden = true
        } else {
            switch method {
            case .pyramid:
                repsLabel.text = Constants.ResistanceExercises.Methods().repsString
                percentPickerView.isHidden = true
                percentStackView.isHidden = true
            case .percent:
                setValues(from: "\(content.percent)" , pickerView: percentPickerView, button: percentButton, data: dataManager.percentData, outputTemplate: &outputPercent)
                percentButton.setTitle("\(content.percent)%", for: .normal)
                
            }
            
            if content.repsTime == "" {
                timeStackView.isHidden = true
                repsStackView.isHidden = false
            } else {
                repsStackView.isHidden = true
                timeStackView.isHidden = false
            }
            let buttonTitle = content.repsTime == "" ? Constants.Cardio().timeString : Constants.ResistanceExercises.Methods().repsWithNoPointsString
            dropDownTimeButton.setTitle(buttonTitle, for: .normal)
        }
       
        if isKgOnly && method == .percent {
            setValues(from: "\(content.percent)" , pickerView: percentPickerView, button: percentButton, data: dataManager.percentData, outputTemplate: &outputPercent)
            percentButton.setTitle("\(content.percent)%", for: .normal)
        }
        if content.rm == 1000 {
            setValues(from: Constants.ResistanceExercises.Methods().maxString , pickerView: repsPickerView, button: repsButton, data: dataManager.repsData, outputTemplate: &outputReps)
        } else if content.rm == 999 {
            setValues(from: "1" , pickerView: repsPickerView, button: repsButton, data: dataManager.repsData, outputTemplate: &outputKG)
        } else {
            setValues(from: "\(content.rm)" , pickerView: repsPickerView, button: repsButton, data: dataManager.repsData, outputTemplate: &outputReps)
        }
        
        var repsTime = ""
        if content.repsTime == "" {
            repsTime = "00:00"
        } else {
            repsTime = content.repsTime
        }
        setValues(from: repsTime, pickerView: timePickerView, button: timeButton, data: dataManager.timeData, outputTemplate: &outputTime)
        var kgValue = content.kg[content.indexOfKg]
        if kgValue == 301 {
            kgValue = 0.0
        }
        
        setValues(from: String(format: "%.2f", kgValue), pickerView: kgPickerView, button: kgButton, data: dataManager.kgData, outputTemplate: &outputKG)
    }
    
    // MARK: - Action
    @IBAction private func repsButtonAction(_ sender: Any) { expandPickerView(heightConstraint: repsHeightAnchor) }
    @IBAction private func kgButtonAction(_ sender: Any) { expandPickerView(heightConstraint: kgHeightAnchor) }
    @IBAction private func percentButtonAction(_ sender: Any) { expandPickerView(heightConstraint: percentHeightAnchor) }
    
    @IBAction func repsDropDownDidTap(_ sender: Any) {
           repsDropDownHeightAnchor.constant = (repsDropDownHeightAnchor.constant == 50) ? 0 : 50
           UIView.animate(withDuration: 0.3) {
               self.contentView.layoutIfNeeded()
           }
       }
       
       @IBAction func timeDidTap(_ sender: UIButton) {
           repsStackView.isHidden.toggle()
           timeStackView.isHidden.toggle()
           let buttonTitle = sender.titleLabel?.text == Constants.Cardio().timeString ? Constants.ResistanceExercises.Methods().repsWithNoPointsString : Constants.Cardio().timeString
           sender.setTitle(buttonTitle, for: .normal)
           viewDidTap(withAnimation: false)
       }
       
       @IBAction func timeButtonDidTap(_ sender: Any) {
           expandPickerView(heightConstraint: timePickerViewHeightAnchor)
       }
       
    // MARK: - Private
    @objc private func viewDidTap(withAnimation: Bool = true) {
        [repsHeightAnchor, kgHeightAnchor, percentHeightAnchor, timePickerViewHeightAnchor, repsDropDownHeightAnchor].forEach {
            if $0?.constant != 0 { $0?.constant = 0 }
             if withAnimation {
                UIView.animate(withDuration: 0.3) { [weak self] in self?.layoutIfNeeded() }
            }
        }
    }
    
    private func expandPickerView(heightConstraint: NSLayoutConstraint) {
        [repsHeightAnchor, kgHeightAnchor, percentHeightAnchor].forEach {
            if $0 == heightConstraint { return }
            if $0?.constant != 0 { $0?.constant = 0 }
        }
        heightConstraint.constant = (heightConstraint.constant == 0) ? 127 : 0
        UIView.animate(withDuration: 0.3) { [weak self] in self?.layoutIfNeeded() }
    }
    
    private func getText(from button: UIButton) -> String {
        guard let text = button.title(for: .normal) else { return ""}
        return text
    }
    
    private func setValues(from value: String, pickerView: UIPickerView, button: UIButton, data: [[String]], outputTemplate: inout [String]) {
        guard value != "" else { return }
        if data.count >= 2 {
            let separator = data[1][0]
            let lastComponentRange: Range<String.Index> = value.range(of: separator)!
            var lastComponentString = String(value[lastComponentRange.upperBound...])
            if separator == "." {
                if lastComponentString == "00" {
                    lastComponentString = "0"
                }
            }
            let firstComponentRange: Range<String.Index> = value.range(of: separator)!
            let firstComponentString = String(value[..<firstComponentRange.lowerBound])
            
            if let selectedIndexForFirstComponent = data.first?.lastIndex(where: { $0 == firstComponentString} ) {
                pickerView.selectRow(selectedIndexForFirstComponent, inComponent: 0, animated: false)
                
                if let selectedIndexForLastComponent = data.last?.lastIndex(where: { $0 == lastComponentString } ) {
                    pickerView.selectRow(selectedIndexForLastComponent, inComponent: 2, animated: false)
                    button.setTitle(data[0][selectedIndexForFirstComponent] + separator + data[2][selectedIndexForLastComponent], for: .normal)
                    outputTemplate = [data[0][selectedIndexForFirstComponent], separator, data[2][selectedIndexForLastComponent]]
                }
            }
        } else {
            if let index = data.last?.lastIndex(where: { $0 == value } ) {
                pickerView.selectRow(index, inComponent: 0, animated: false)
                button.setTitle(data[0][index], for: .normal)
                outputTemplate = [data[0][index]]
            }
        }
    }
}

// MARK: - UIPickerViewDataSource
extension EditPercentOfRMCell: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        switch pickerView {
        case repsPickerView: return dataManager.repsData.count
        case kgPickerView: return dataManager.kgData.count
        case percentPickerView: return dataManager.percentData.count
        case timePickerView:
            return dataManager.timeData.count
        default: return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView {
        case repsPickerView: return dataManager.repsData[component].count
        case kgPickerView: return dataManager.kgData[component].count
        case percentPickerView: return dataManager.percentData[component].count
        case timePickerView:
            return dataManager.timeData[component].count
        default: return 0
        }
    }
}

// MARK: - UIPickerViewDelegate
extension EditPercentOfRMCell: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerView {
        case repsPickerView:
            repsButton.setTitle(dataManager.repsData[component][row], for: .normal)
            isTimeMode = false
        case kgPickerView:
            outputKG[component] = dataManager.kgData[component][row]
            kgButton.setTitle("\(outputKG.first!).\(outputKG.last!)", for: .normal)
            if let value = Double("\(outputKG.first!).\(outputKG.last!)") {
                item?.kg[item!.indexOfKg] = value
            }
        case percentPickerView:
            percentButton.setTitle("\(dataManager.percentData[component][row])%", for: .normal)
            if let value = Int(dataManager.percentData[component][row]) {
                item?.percent = value
            }
        case timePickerView:
            outputTime[component] = dataManager.timeData[component][row]
            timeButton.setTitle("\(outputTime.first!):\(outputTime.last!)", for: .normal)
            isTimeMode = true
        default: break
        }
        pickerView.reloadAllComponents()
        guard var item = item else { return }
        if isTimeMode {
            item.rm = 999
            item.repsTime = getText(from: timeButton)
        } else {
            if getText(from: repsButton) == Constants.ResistanceExercises.Methods().maxString {
                item.rm = 1000
            } else {
                item.rm = Int(getText(from: repsButton)) ?? 0
            }
            item.repsTime = ""
        }
        
        saveDidTap?(item)
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var color: UIColor!
        var label = UILabel()
        if let v = view { label = v as! UILabel }
        label.font = Constants.returnFontName(style: .regular, size: 15)
        label.textAlignment = .center
        switch pickerView {
        case repsPickerView:
            label.text = dataManager.repsData[component][row]
        case kgPickerView:
            label.text = dataManager.kgData[component][row]
        case percentPickerView:
            label.text = "\(dataManager.percentData[component][row])%"
        case timePickerView:
            label.text = dataManager.timeData[component][row]
        default: break
        }
        
        if pickerView.selectedRow(inComponent: component) == row {
            color = UIColor.orange
        } else {
            color = UIColor.black
        }
        label.textColor = color
        return label
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        32
    }
    
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        if pickerView == kgPickerView {
            switch component {
            case 0:
                return  35
            case 1:
                return 2
            default: return 25
            }
        } else if pickerView == timePickerView {
            switch component {
            case 0:
                return  34
            case 1:
                return 2
            default: return 39
            }
        } else {
            return 65
        }
    }
}
