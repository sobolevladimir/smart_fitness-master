//
//  EditDropSetSingleExerciseCell.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 03.01.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class EditDropSetSingleExerciseCell: UICollectionViewCell {
    
    // MARK: - Views
    @IBOutlet weak private var repLabel: UILabel!
    @IBOutlet weak private var kgLabel: UILabel!
    
    @IBOutlet weak private var repButton: LoginButton!
    @IBOutlet weak private var kgButton: LoginButton!
    
    @IBOutlet weak private var repPickerView: CardioPickerView!
    @IBOutlet weak private var kgPickerView: CardioPickerView!
    
    @IBOutlet weak private var repsStackView: UIStackView!
    
     // MARK: - Constraints
     @IBOutlet weak private var repHeightConstraint: NSLayoutConstraint!
     @IBOutlet weak private var kgHeightConstraint: NSLayoutConstraint!
    
    // MARK: - Properties
    var saveDidTap: ((ExerciseSupersetSetExerciseCell.Content) -> Void)?
    private var dataManager = ResistanceDataCreator()
    private var outputKG: [String] = ["", "", ""]
    private var outputRep: [String] = [""]
    private var item: ExerciseSupersetSetExerciseCell.Content?
    
    // MARK: - Init
    override func awakeFromNib() {
        super.awakeFromNib()
        repPickerView.dataSource = self
        kgPickerView.dataSource = self
        repPickerView.delegate = self
        kgPickerView.delegate = self
    }
    // MARK: - Methods
    func set(content: ExerciseSupersetSetExerciseCell.Content, exNumber: Int, method: EditDropSetViewController.Method, isKgOnly: Bool) {
        if isKgOnly {
            repsStackView.isHidden = true
        }
        contentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(viewDidTap)))
        item = content
        switch method {
        case .superSet:
            repLabel.text  = "\(Constants.ResistanceExercises.Methods().exWithPoint)\(exNumber)"
            kgLabel.text = "\(Constants.ResistanceExercises.Methods().exWithPoint)\(exNumber)"
        case .dropSet:
            repLabel.text  = "\(Constants.ResistanceExercises.Methods().drop)\(exNumber)"
            kgLabel.text = "\(Constants.ResistanceExercises.Methods().drop)\(exNumber)"
        }
        if content.repsValue == 1000 {
            setValues(from: Constants.ResistanceExercises.Methods().maxString, pickerView: repPickerView, button: repButton, data: dataManager.repsData, outputTemplate: &outputRep)
        } else if content.repsValue == 999 {
            setValues(from: Constants.ResistanceExercises.Methods().timeString , pickerView: repPickerView, button: repButton, data: dataManager.repsData, outputTemplate: &outputKG)
        } else {
            setValues(from: "\(content.repsValue)", pickerView: repPickerView, button: repButton, data: dataManager.repsData, outputTemplate: &outputRep)
        }
        var kgValue = content.kgValue
        if kgValue == 301 {
            kgValue = 0.0
        }
        setValues(from: String(format: "%.2f", kgValue), pickerView: kgPickerView, button: kgButton, data: dataManager.kgData, outputTemplate: &outputKG)
    }
       
    // MARK: - Action
       
       @IBAction func dropButtonDidTap(_ sender: LoginButton) {
           switch sender {
           case repButton: expandPickerView(heightConstraint: repHeightConstraint)
          
           case kgButton: expandPickerView(heightConstraint: kgHeightConstraint)
          
           default: break
           }
       }
    
    // MARK: - Private
    @objc func viewDidTap() {
        [repHeightConstraint, kgHeightConstraint].forEach {
            if $0?.constant != 0 { $0?.constant = 0 }
            UIView.animate(withDuration: 0.3) { [weak self] in self?.layoutIfNeeded() }
        }
      }
    
    private func expandPickerView(heightConstraint: NSLayoutConstraint) {
        [repHeightConstraint, kgHeightConstraint].forEach {
            if $0 == heightConstraint { return }
            if $0?.constant != 0 { $0?.constant = 0 }
        }
        heightConstraint.constant = (heightConstraint.constant == 0) ? 127 : 0
        UIView.animate(withDuration: 0.3) { [weak self] in self?.layoutIfNeeded() }
    }
    
    private func getText(from button: UIButton) -> String {
        guard let text = button.title(for: .normal) else { return ""}
        return text
    }
    
    private func setValues(from value: String, pickerView: UIPickerView, button: UIButton, data: [[String]], outputTemplate: inout [String]) {
        guard value != "" else { return }
        if data.count >= 2 {
            let separator = data[1][0]
            let lastComponentRange: Range<String.Index> = value.range(of: separator)!
            var lastComponentString = String(value[lastComponentRange.upperBound...])
            if separator == "." {
                if lastComponentString == "00" {
                    lastComponentString = "0"
                }
            }
            let firstComponentRange: Range<String.Index> = value.range(of: separator)!
            let firstComponentString = String(value[..<firstComponentRange.lowerBound])
            
            if let selectedIndexForFirstComponent = data.first?.lastIndex(where: { $0 == firstComponentString} ) {
                pickerView.selectRow(selectedIndexForFirstComponent, inComponent: 0, animated: false)
                
                if let selectedIndexForLastComponent = data.last?.lastIndex(where: { $0 == lastComponentString } ) {
                    pickerView.selectRow(selectedIndexForLastComponent, inComponent: 2, animated: false)
                    button.setTitle(data[0][selectedIndexForFirstComponent] + separator + data[2][selectedIndexForLastComponent], for: .normal)
                    outputTemplate = [data[0][selectedIndexForFirstComponent], separator, data[2][selectedIndexForLastComponent]]
                }
            }
        } else {
            if let index = data.last?.lastIndex(where: { $0 == value } ) {
                pickerView.selectRow(index, inComponent: 0, animated: false)
                button.setTitle(data[0][index], for: .normal)
                outputTemplate = [data[0][index]]
            }
        }
    }
       
}

// MARK: - Content
extension EditDropSetSingleExerciseCell {
    struct Content {
        var reps: String
        var kg: String
    }
}

// MARK: - UIPickerViewDataSource
extension EditDropSetSingleExerciseCell: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        switch pickerView {
        case repPickerView:
            return dataManager.repsData.count
        case kgPickerView:
            return dataManager.kgData.count
        default: return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView {
        case repPickerView:
            return dataManager.repsData[component].count
        case kgPickerView:
            return dataManager.kgData[component].count
        default: return 0
        }
    }
}

// MARK: - UIPickerViewDelegate
extension EditDropSetSingleExerciseCell: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerView {
        case repPickerView:
            repButton.setTitle(dataManager.repsData[component][row], for: .normal)
            if getText(from: repButton) == Constants.ResistanceExercises.Methods().maxString {
                item?.repsValue = 1000
            } else if getText(from: repButton) == Constants.ResistanceExercises.Methods().timeString{
                item?.repsValue = 999
            } else {
                item?.repsValue = Int(dataManager.repsData[component][row]) ?? 0
            }
        case kgPickerView:
            outputKG[component] = dataManager.kgData[component][row]
            kgButton.setTitle("\(outputKG.first!).\(outputKG.last!)", for: .normal)
            item?.kgValue = Double("\(outputKG.first!).\(outputKG.last!)") ?? 0.0
        default: break
        }
        guard let item = item else { return }
        saveDidTap?(item)
        pickerView.reloadAllComponents()
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var color: UIColor!
        var label = UILabel()
        if let v = view { label = v as! UILabel }
        label.font = Constants.returnFontName(style: .regular, size: 15)
        label.textAlignment = .center
        switch pickerView {
        case repPickerView:
            label.text = dataManager.repsData[component][row]
        case kgPickerView:
            label.text = dataManager.kgData[component][row]
        default: break
        }
        if pickerView.selectedRow(inComponent: component) == row {
            color = UIColor.orange
        } else {
            color = UIColor.black
        }
        label.textColor = color
        return label
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        32
    }
    
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        switch pickerView {
        case kgPickerView:
            switch component {
            case 0:
                return  30
            case 1:
                return 4
            default: return 25
            }
        default: return 42
        }
    }
}
