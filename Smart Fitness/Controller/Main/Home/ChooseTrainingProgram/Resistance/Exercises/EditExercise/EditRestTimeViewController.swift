//
//  EditRestTimeViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 05.12.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class EditRestTimeViewController: UIViewController, Storyboarded {
   
    enum State {
        case resistance, timer, rest
    }
    
    // MARK: - Views
    @IBOutlet weak private var containerView: UIView!
    @IBOutlet weak private var minutesPickerView: UIPickerView!
    @IBOutlet weak private var secondsPickerView: UIPickerView!
    @IBOutlet weak private var mainStackView: UIStackView!
    @IBOutlet weak private var saveButton: UIButton!
    
    @IBOutlet weak var titleLabel: UILabel!
    // MARK: - Constraints
    @IBOutlet weak private var topConstraint: NSLayoutConstraint!
   
    // MARK: - Public Properties
    var saveDidTap: ((String) -> Void)?
    var time: String = ""
    var minutes = ""
    var seconds = ""
    var cancelDidTap: (() -> Void)?
    var state: State = .resistance
    
    // MARK: - Properties
    private var dataManager = ResistanceTimeCreator()
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setValues(from: time)
        mainStackView.semanticContentAttribute = .forceLeftToRight
        switch state {
            
        case .resistance: break
        case .timer:
            titleLabel.text = Constants.Timer().timer
            saveButton.setTitle(Constants.Timer().setTime, for: .normal)
        case .rest:
            titleLabel.text = Constants.Timer().rest
            saveButton.setTitle(Constants.Timer().setRest, for: .normal)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        topConstraint.constant = -containerView.frame.height
        UIView.animate(withDuration: 0.3) { [weak self] in
            self?.view.layoutSubviews()
        }
    }
    
    // MARK: - Actions
    @IBAction private func cancelDidTap(_ sender: Any) {
        cancelDidTap?()
        dismissWithAnimation() }
    
    @IBAction private func saveDidTap(_ sender: Any) {
        saveEditing()
        dismissWithAnimation()
    }
    
    // MARK: - Private
    private func dismissWithAnimation() {
        topConstraint.constant = 0
        UIView.animate(withDuration: 0.3, animations: { [weak self] in self?.view.layoutSubviews()
        }) { [weak self] (_) in self?.dismiss(animated: false, completion: nil) }
    }
    
    private func saveEditing() {
        let editedTime = "\(minutes):\(seconds)"
        saveDidTap?(editedTime)
    }
    
    private func setValues(from string: String) {
        let lastComponentRange: Range<String.Index> = string.range(of: ":")!
        let lastComponentString = String(string[lastComponentRange.upperBound...])
        
        let firstComponentRange: Range<String.Index> = string.range(of: ":")!
        let firstComponentString = String(string[..<firstComponentRange.lowerBound])
        
        if let selectedIndexForMinutes = dataManager.minutesData.lastIndex(where: { $0 == firstComponentString} ) {
            minutesPickerView.selectRow(selectedIndexForMinutes, inComponent: 0, animated: false)
            minutes = dataManager.minutesData[selectedIndexForMinutes]
            if let selectedIndexForSeconds = dataManager.secondsData.lastIndex(where: { $0 == lastComponentString } ) {
                secondsPickerView.selectRow(selectedIndexForSeconds, inComponent: 0, animated: false)
                seconds = dataManager.minutesData[selectedIndexForSeconds]
            }
        }
    }
}

// MARK: - UIPickerViewDataSource
extension EditRestTimeViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        pickerView.subviews.forEach({
            $0.isHidden = $0.frame.height < 1.0
        })
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
         switch pickerView {
               case minutesPickerView:
                   return dataManager.minutesData.count
               case secondsPickerView:
                   return dataManager.secondsData.count
               default: return 0
               }
    }
}


// MARK: - UIPickerViewDelegate
extension EditRestTimeViewController: UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
           switch pickerView {
                  case minutesPickerView:
                    minutes = dataManager.minutesData[row]
                  case secondsPickerView:
                    seconds = dataManager.secondsData[row]
                  default: break
                  }
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label = UILabel()
        if let v = view { label = v as! UILabel }
        label.font = Constants.returnFontName(style: .regular, size: 17)
        label.textAlignment = .center
         switch pickerView {
               case minutesPickerView:
                    label.text = dataManager.minutesData[row]
               case secondsPickerView:
                   label.text = dataManager.secondsData[row]
               default: break
               }
        return label
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        41
    }
}

