//
//  EditDropSetCell.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 05.12.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class EditDropSetCell: UICollectionViewCell {
    
    // MARK: - Views
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var repsTitleLabel: UILabel!
    
    @IBOutlet weak var kgTitleLabel: UILabel!
    
    // MARK: - Properties
    var saveDidTap: (([ExerciseSupersetSetExerciseCell.Content]) -> Void)?
    private var items: [ExerciseSupersetSetExerciseCell.Content] = []
    private var method: EditDropSetViewController.Method = .dropSet
    private var isKgOnly: Bool = false
 
    // MARK: - Init
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.dataSource = self
       collectionView.delegate = self
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        collectionView.reloadData()
    }
    
    // MARK: - Methods
    func set(content: [ExerciseSupersetSetExerciseCell.Content], method: EditDropSetViewController.Method, isKgOnly: Bool) {
        self.isKgOnly = isKgOnly
        if isKgOnly {
            kgTitleLabel.isHidden = true
            repsTitleLabel.text = Constants.Circuit().kg
        }
        contentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(viewDidTap)))
        items = content
        self.method = method
        switch method {
        case .superSet:
            repsTitleLabel.text = Constants.ResistanceExercises.Methods().repsString
        case .dropSet: break
        }
    }
    
    // MARK: - Private
    @objc private func viewDidTap() {
        items.enumerated().forEach { (index, _) in
            (collectionView.cellForItem(at: IndexPath(row: index, section: 0)) as? EditDropSetSingleExerciseCell)?.viewDidTap()
        }
    }
}

// MARK: - UICollectionViewDataSource
extension EditDropSetCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(with: EditDropSetSingleExerciseCell.self, for: indexPath)
        
        cell.set(content: items[indexPath.row], exNumber: indexPath.row + 1, method: method, isKgOnly: isKgOnly)
        cell.saveDidTap = { [weak self] item in
            guard let self = self else { return }
            self.items[indexPath.row] = item
            self.saveDidTap?(self.items)
        }
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension EditDropSetCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        .init(width: 50, height: 320)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {

        let totalCellWidth = 50 * items.count
        let totalSpacingWidth = 0

        let leftInset = (collectionView.frame.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
        let rightInset = leftInset

        return UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)
    }
}
