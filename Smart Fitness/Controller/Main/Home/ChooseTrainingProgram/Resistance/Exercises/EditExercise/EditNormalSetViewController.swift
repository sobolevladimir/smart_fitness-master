//
//  EditNormalSetViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 04.12.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class EditNormalSetViewController: UIViewController, Storyboarded {
    
    enum State {
        case resistance, circuit, onlyKg
    }
    
    typealias Item = Exercise
    
    
    // MARK: - Views
    @IBOutlet weak var setsStackView: UIStackView!
    
    @IBOutlet weak var kgsStackView: UIStackView!
    
    @IBOutlet weak private var containerView: UIView!
    @IBOutlet weak private var setsButton: LoginButton!
    @IBOutlet weak private var repsButton: LoginButton!
    @IBOutlet weak private var kgButton: LoginButton!
    @IBOutlet weak private var setsPickerView: UIPickerView!
    @IBOutlet weak private var repsPickerView: UIPickerView!
    @IBOutlet weak private var kgPickerView: UIPickerView!
    @IBOutlet weak private var valuesStackView: UIStackView!
        
    @IBOutlet weak private var dropDownTimeButton: UIButton!
    @IBOutlet weak private var repsStackView: UIStackView!
    @IBOutlet weak private var timeButton: LoginButton!
    @IBOutlet weak private var timeStackView: UIStackView!
    @IBOutlet weak private var timePickerView: CardioPickerView!
    
    @IBOutlet weak private var timeArrowButton: UIButton!
    @IBOutlet weak private var stackOfRepsAndTime: UIStackView!
    
    // MARK: - Constraints
    @IBOutlet weak private var topConstraint: NSLayoutConstraint!
    @IBOutlet weak private var setsPickerViewHeightAnchor: NSLayoutConstraint!
    @IBOutlet weak private var repsPickerViewHeightAnchor: NSLayoutConstraint!
    @IBOutlet weak private var kgPickerViewHeightAnchor: NSLayoutConstraint!
    
    @IBOutlet private var pickersArray: [NSLayoutConstraint]!
    
    @IBOutlet private var timePickerViewHeightAnchor: NSLayoutConstraint!
    @IBOutlet private var repsDropDownHeightAnchor: NSLayoutConstraint!
    
    // MARK: - Public Properties
    var saveDidTap: ((Item) -> Void)?
    var item: Item!
    var state: State = .resistance
    
    // MARK: - Properties
    private var dataManager = ResistanceDataCreator()
    
    private var outputKG: [String] = ["", "", ""]
    private var outputReps: [String] = [""]
    private var outputTime: [String] = ["00", ":", "00"]
    private var isTimeMode = false
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        isTimeMode = item.repsTime != ""
        setupUI()
        containerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(viewDidTap)))
        if state == .circuit {
            setsStackView.isHidden = true
            if item.category == .bodyweight || item.category == .accessories {
                kgsStackView.isHidden = true
            }
        }
        
        if item.repsTime == "" {
            timeStackView.isHidden = true
            repsStackView.isHidden = false
        } else {
            repsStackView.isHidden = true
            timeStackView.isHidden = false
        }
        
        if state == .onlyKg {
            stackOfRepsAndTime.isHidden = true
            setsStackView.isHidden = true
            timeArrowButton.isHidden = true
        }
        let buttonTitle = item.repsTime == "" ? Constants.Cardio().timeString : Constants.ResistanceExercises.Methods().repsWithNoPointsString
        dropDownTimeButton.setTitle(buttonTitle, for: .normal)
        
        setCellUIElements(by: item.category)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        topConstraint.constant = -containerView.frame.height
        UIView.animate(withDuration: 0.3) { [weak self] in
            self?.view.layoutSubviews()
        }
    }
  
    // MARK: - Actions
    ///////////////////////////
    @IBAction func repsDropDownDidTap(_ sender: Any) {
        repsDropDownHeightAnchor.constant = (repsDropDownHeightAnchor.constant == 50) ? 0 : 50
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func timeDidTap(_ sender: UIButton) {
        repsStackView.isHidden.toggle()
        timeStackView.isHidden.toggle()
        let buttonTitle = sender.titleLabel?.text == Constants.Cardio().timeString ? Constants.ResistanceExercises.Methods().repsWithNoPointsString : Constants.Cardio().timeString
        sender.setTitle(buttonTitle, for: .normal)
        viewDidTap(withAnimation: false)
    }
    
    @IBAction func timeButtonDidTap(_ sender: Any) {
        expandPickerView(heightConstraint: timePickerViewHeightAnchor)
    }
    
    
    @objc private func viewDidTap(withAnimation: Bool = true) {
        pickersArray.forEach {
            if $0.constant != 0 { $0.constant = 0 }
            if withAnimation {
                UIView.animate(withDuration: 0.3) { [weak self] in self?.view.layoutIfNeeded() }
            }
        }
    }
    
    @IBAction private func setsDidTap(_ sender: Any) { expandPickerView(heightConstraint: setsPickerViewHeightAnchor)}
    
    @IBAction private func repsDidTap(_ sender: Any) { expandPickerView(heightConstraint: repsPickerViewHeightAnchor)}
    
    @IBAction private func kgDidTap(_ sender: Any) { expandPickerView(heightConstraint: kgPickerViewHeightAnchor)}
    
    @IBAction private func cancelDidTap(_ sender: Any) { dismissWithAnimation() }
    
    @IBAction private func saveDidTap(_ sender: Any) {
        saveEditing()
        dismissWithAnimation()
    }
    
    // MARK: - Private
    private func dismissWithAnimation() {
        topConstraint.constant = 0
        UIView.animate(withDuration: 0.3, animations: { [weak self] in self?.view.layoutSubviews()
        }) { [weak self] (_) in self?.dismiss(animated: false, completion: nil) }
    }
    
    private func expandPickerView(heightConstraint: NSLayoutConstraint) {
        pickersArray.forEach {
            if $0 == heightConstraint { return }
            if $0.constant != 0 { $0.constant = 0 }
        }
        heightConstraint.constant = (heightConstraint.constant == 0) ? 127 : 0
        UIView.animate(withDuration: 0.3) { [weak self] in self?.view.layoutIfNeeded() }
    }
    
    private func setupUI() {
        valuesStackView.semanticContentAttribute = .forceLeftToRight
        setValues(from: "\(item.setsValue)" , pickerView: setsPickerView, button: setsButton, data: dataManager.setsData, outputTemplate: &outputReps)
        if item.repsValue == 1000 {
             setValues(from: Constants.ResistanceExercises.Methods().maxString , pickerView: repsPickerView, button: repsButton, data: dataManager.repsData, outputTemplate: &outputReps)
        } else if  item.repsValue == 999 {
           setValues(from: "1" , pickerView: repsPickerView, button: repsButton, data: dataManager.repsData, outputTemplate: &outputReps)
        } else {
             setValues(from: "\(item.repsValue)" , pickerView: repsPickerView, button: repsButton, data: dataManager.repsData, outputTemplate: &outputReps)
        }
        var kgValue = item.kgValue[item.indexOfKg]
        if kgValue == 301 {
            kgValue = 0.0
        }
        setValues(from: String(format: "%.2f", kgValue), pickerView: kgPickerView, button: kgButton, data: dataManager.kgData, outputTemplate: &outputKG)
        var repsTime = ""
        if item.repsTime == "" {
            repsTime = "00:00"
        } else {
            repsTime = item.repsTime
        }
        setValues(from: repsTime, pickerView: timePickerView, button: timeButton, data: dataManager.timeData, outputTemplate: &outputTime)
    }
    
    private func saveEditing() {
        guard var editedItem = item else { return }
        
        if isTimeMode {
            editedItem.repsValue = 999
            editedItem.repsTime = getText(from: timeButton)
        } else {
            if getText(from: repsButton) == Constants.ResistanceExercises.Methods().maxString {
                editedItem.repsValue = 1000
            } else {
                editedItem.repsValue = Int(getText(from: repsButton)) ?? 0
            }
            editedItem.repsTime = ""
        }
        
        editedItem.setsValue = Int(getText(from: setsButton)) ?? 0
        editedItem.kgValue[editedItem.indexOfKg] = Double(getText(from: kgButton)) ?? 0.0
        saveDidTap?(editedItem)
    }
    
    private func getText(from button: UIButton) -> String {
        guard let text = button.title(for: .normal) else { return ""}
        return text
    }
    
    private func setValues(from value: String, pickerView: UIPickerView, button: UIButton, data: [[String]], outputTemplate: inout [String]) {
        guard value != "" else { return }
        if data.count >= 2 {
            let separator = data[1][0]
            if let lastComponentRange: Range<String.Index> = value.range(of: separator) {
                var lastComponentString = String(value[lastComponentRange.upperBound...])
                if separator == "." {
                    if lastComponentString == "00" {
                        lastComponentString = "0"
                    }
                }
                if let firstComponentRange: Range<String.Index> = value.range(of: separator) {
                    let firstComponentString = String(value[..<firstComponentRange.lowerBound])
                    
                    if let selectedIndexForFirstComponent = data.first?.lastIndex(where: { $0 == firstComponentString} ) {
                        pickerView.selectRow(selectedIndexForFirstComponent, inComponent: 0, animated: false)
                        
                        if let selectedIndexForLastComponent = data.last?.lastIndex(where: { $0 == lastComponentString } ) {
                            pickerView.selectRow(selectedIndexForLastComponent, inComponent: 2, animated: false)
                            button.setTitle(data[0][selectedIndexForFirstComponent] + separator + data[2][selectedIndexForLastComponent], for: .normal)
                            outputTemplate = [data[0][selectedIndexForFirstComponent], separator, data[2][selectedIndexForLastComponent]]
                        }
                    }
                }
                
            }
        } else {
            if let index = data.last?.lastIndex(where: { $0 == value } ) {
                pickerView.selectRow(index, inComponent: 0, animated: false)
                button.setTitle(data[0][index], for: .normal)
                outputTemplate = [data[0][index]]
            }
        }
    }
    
    private func setCellUIElements(by category: Category) {
           switch category {
           case .machines:
               kgsStackView.isHidden = false
           case .dumbbells:
               kgsStackView.isHidden = false
           case .barbells:
               kgsStackView.isHidden = false
           case .pulley:
               kgsStackView.isHidden = false
           case .bodyweight:
               kgsStackView.isHidden = true
           case .accessories:
               kgsStackView.isHidden = false
           case .favourites: break
           }
       }
}

// MARK: - UIPickerViewDataSource
extension EditNormalSetViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        switch pickerView {
        case setsPickerView:
            return dataManager.setsData.count
        case repsPickerView:
            return dataManager.repsData.count
        case kgPickerView:
            return dataManager.kgData.count
        case timePickerView:
            return dataManager.timeData.count
        default: return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView {
        case setsPickerView:
            return dataManager.setsData[component].count
        case repsPickerView:
            return dataManager.repsData[component].count
        case kgPickerView:
            return dataManager.kgData[component].count
        case timePickerView:
            return dataManager.timeData[component].count
        default: return 0
        }
    }
}


// MARK: - UIPickerViewDelegate
extension EditNormalSetViewController: UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
           switch pickerView {
             case setsPickerView:
                setsButton.setTitle(dataManager.setsData[component][row], for: .normal)
             case repsPickerView:
                repsButton.setTitle(dataManager.repsData[component][row], for: .normal)
                isTimeMode = false
             case kgPickerView:
                outputKG[component] =  dataManager.kgData[component][row]
                kgButton.setTitle("\(outputKG.first!).\(outputKG.last!)", for: .normal)
            case timePickerView:
                 outputTime[component] = dataManager.timeData[component][row]
                 timeButton.setTitle("\(outputTime.first!):\(outputTime.last!)", for: .normal)
                 isTimeMode = true
             default: break
             }
        pickerView.reloadAllComponents()
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var color: UIColor!
        var label = UILabel()
        if let v = view { label = v as! UILabel }
        label.font = Constants.returnFontName(style: .regular, size: 15)
        label.textAlignment = .center
        switch pickerView {
        case setsPickerView:
            label.text = dataManager.setsData[component][row]
        case repsPickerView:
            label.text = dataManager.repsData[component][row]
        case kgPickerView:
            label.text = dataManager.kgData[component][row]
        case timePickerView:
            label.text = dataManager.timeData[component][row]
        default: break
        }
        
        if pickerView.selectedRow(inComponent: component) == row {
            color = UIColor.orange
        } else {
            color = UIColor.black
        }
        label.textColor = color
        return label
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        34
    }
    
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        if pickerView == kgPickerView{
            switch component {
            case 0:
                return  35
            case 1:
                return 2
            default: return 25
            }
        } else if pickerView == timePickerView {
            switch component {
            case 0:
                return  34
            case 1:
                return 2
            default: return 39
            }
        } else {
            return 65
        }
    }
}
