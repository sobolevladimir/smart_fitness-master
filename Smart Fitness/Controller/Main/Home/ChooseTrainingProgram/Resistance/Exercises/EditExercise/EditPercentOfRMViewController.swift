//
//  EditPercentOfRMViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 05.12.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class EditPercentOfRMViewController: UIViewController, Storyboarded {
    
    enum Method {
        case pyramid, percent
    }
    
    // MARK: - Views
    @IBOutlet weak private var collectionView: UICollectionView!
    @IBOutlet weak private var setNumberLabel: UILabel!
    @IBOutlet weak private var containerView: RoundedView!
    @IBOutlet weak private var previousButton: UIButton!
    @IBOutlet weak private var nextButton: UIButton!
    
    // MARK: - Constraints
    @IBOutlet weak private var topConstraint: NSLayoutConstraint!
    
    // MARK: - Properties
    var method: Method = .percent
    var data: [ExercisePercentSetCell.Content] = []
    var isKgOnly: Bool = false
    
    var saveDidTap: (([ExercisePercentSetCell.Content]) -> Void)?
    private var currentIndexPath: IndexPath = .init(item: 0, section: 0)
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        previousButton.isEnabled = currentIndexPath.item != 0
        nextButton.isEnabled = currentIndexPath.item != data.count - 1
        if method == .pyramid {
            setNumberLabel.text = Constants.ResistanceExercises.Methods().pyramidSetOneString
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        topConstraint.constant = -containerView.frame.height
        UIView.animate(withDuration: 0.3) { [weak self] in
            self?.view.layoutSubviews()
        }
        collectionView.scrollToItem(at: currentIndexPath, at: .centeredHorizontally, animated: false)
    }
   
    // MARK: - Actions
    
    @IBAction func saveButtonAction(_ sender: Any) {
        saveEditing()
        dismissWithAnimation()
    }
    
    @IBAction private func cancelAction(_ sender: Any) { dismissWithAnimation() }
    @IBAction private func nextButtonAction(_ sender: Any) { scrollToPage(isNext: true) }
    @IBAction private func previousButtonAction(_ sender: Any) { scrollToPage(isNext: false) }
    
    // MARK: - Private
    private func dismissWithAnimation() {
        topConstraint.constant = 0
        UIView.animate(withDuration: 0.3, animations: { [weak self] in self?.view.layoutSubviews()
        }) { [weak self] (_) in self?.dismiss(animated: false, completion: nil) }
    }
    
    private func saveEditing() {
        saveDidTap?(data)
    }
    
    private func scrollToPage(isNext: Bool) {
        if !isNext {
            guard currentIndexPath.item != 0 else { return}
            currentIndexPath.item -= 1
        } else {
            guard currentIndexPath.item != data.count - 1 else { return }
            currentIndexPath.item += 1
        }
        collectionView.scrollToItem(at: currentIndexPath, at: .centeredHorizontally, animated: true)
    }
}

// MARK: - UICollectionViewDataSource
extension EditPercentOfRMViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(with: EditPercentOfRMCell.self, for: indexPath)
        cell.set(content: data[indexPath.item], method: method, isKgOnly: isKgOnly )
        cell.saveDidTap = { [weak self] item in
            guard let self = self else { return }
            self.data[indexPath.item] = item
        }
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension EditPercentOfRMViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width
        let height = collectionView.frame.height
        return .init(width: width, height: height)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageWidth = scrollView.frame.size.width
        let item = floor((scrollView.contentOffset.x * 2.0 + pageWidth) / (pageWidth * 2.0))
        currentIndexPath.item = Int(item)
        setNumberLabel.text = "\(Constants.ResistanceExercises.Methods().percentOfRMSetNString) \(currentIndexPath.item + 1)"
        if method == .pyramid {
            setNumberLabel.text = "\(Constants.ResistanceExercises.Methods().pyramidSetNString) \(currentIndexPath.item + 1)"
        }
        previousButton.isEnabled = currentIndexPath.item != 0
        nextButton.isEnabled = currentIndexPath.item != data.count - 1
        collectionView.collectionViewLayout.invalidateLayout()
    }
}
