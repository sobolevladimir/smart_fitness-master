//
//  AddExercisesTableHeaderView.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 14.11.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class AddExercisesTableHeaderView: UITableViewHeaderFooterView {
    
    // MARK: - Views
    @IBOutlet weak private var mainImageView: UIImageView!
    @IBOutlet weak private var mainTitleLabel: UILabel!
    @IBOutlet weak private var arrowImageView: UIImageView!
    @IBOutlet weak private var tapButton: UIButton!
    
    // MARK: - Properties
    var didTap: (() -> Void)?
    private var isExpanded: Bool = false
    
    // MARK: - Init
    static let reuseIdentifier: String = String(describing: self)

    static var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: nil)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        arrowImageView.transform = .identity
        isExpanded = false
    }
    
    // MARK: - Actions
    @IBAction private func headerDidTap(_ sender: Any) {
        didTap?()
        tapHeader()
    }
    
    // MARK: - Methods
    func set(content: Content, isFavoriteCategory: Bool) {
        contentView.backgroundColor = content.isExpanded ? Constants.Colors.blueDark : .white
        arrowImageView.tintColor = content.isExpanded ? .white : Constants.Colors.blueDark
        mainTitleLabel.textColor = content.isExpanded ? .white : .black
       
        if isFavoriteCategory {
            mainImageView.image = UIImage(named: content.imageName)
            mainTitleLabel.text = Constants.ResistanceExercises.Category().favoritesString
            arrowImageView.isHidden = true
            contentView.backgroundColor = Constants.Colors.blueDark
        } else {
             arrowImageView.isHidden = false
             mainTitleLabel.text = content.title
            mainImageView.downloadImage(from: content.imageName)
        }
        
        content.isExpanded ? _ = (isExpanded.toggle(), rotateArrow(if: isExpanded)) : nil
    }
    
    func tapHeader() {
           isExpanded.toggle()
           rotateArrow(if: isExpanded)
           contentView.backgroundColor = isExpanded ? Constants.Colors.blueDark : .white
           arrowImageView.tintColor = isExpanded ?  .white : Constants.Colors.blueDark
           mainTitleLabel.textColor = isExpanded ?  .white : .black
       }
    
    // MARK: - Private
    private func rotateArrow(if expanded: Bool) {
        var angle: CGFloat = 0
        switch LocalizationManager.Language.language {
              case .english:
                  angle = 0.5 * .pi
              case .hebrew:
                  angle = -0.5 * .pi
              }
        let transform: CGAffineTransform = expanded ? .init(rotationAngle: angle) : .identity
        UIView.animate(withDuration: 0.2) { [weak self] in
            self?.arrowImageView.layer.setAffineTransform(transform)
        }
    }
}

// MARK: - Content
extension AddExercisesTableHeaderView {
    struct Content {
        var isFirst = false
        var imageName: String
        var title: String
        var isExpanded: Bool = false
    }
}
