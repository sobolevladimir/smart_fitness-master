//
//  AddExercisesCollectionCell.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 14.11.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class AddExercisesCollectionCell: UICollectionViewCell {
    
    // MARK: - Views
    @IBOutlet weak private var selectionView: RoundedView!
    @IBOutlet weak private var imageView: UIImageView!
    @IBOutlet weak private var titleLabel: UILabel!
    
    // MARK: - Properties
    override var isSelected: Bool {
        didSet {
            selectionView.layer.borderColor = !isSelected ? UIColor.white.cgColor : Constants.Colors.blueDark.cgColor
        }
    }
    
    // MARK: - Methods
    func set(content: Content) {
        imageView.image = UIImage(named: content.imageName)
        titleLabel.text = content.title
    }
}

// MARK: - Content
extension AddExercisesCollectionCell {
    struct Content {
        var imageName: String
        var category: Category
        var title: String {
            return category.localizedString
        }
    }
}
