//
//  SearchExercisesCell.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 15.11.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class SearchExercisesCell: UITableViewCell {
    
    // MARK: - Views
    @IBOutlet weak private var exerciseTitleLabel: UILabel!
    
    // MARK: - Properties
    var addExercise: ((String) -> Void)?
    
    // MARK: - Action
    @IBAction private func addButtonDidTap(_ sender: Any) {
        guard let exerciseTitle = exerciseTitleLabel.text else { return }
        addExercise?(exerciseTitle)
    }
    
    // MARK: - Methods
    func set(title: String, isFirst: Bool) {
        exerciseTitleLabel.textColor = isFirst ? .white : .black
        contentView.backgroundColor = isFirst ? #colorLiteral(red: 0.6077771783, green: 0.6078822613, blue: 0.6077624559, alpha: 1) : .white
        exerciseTitleLabel.text = title
    }
}
