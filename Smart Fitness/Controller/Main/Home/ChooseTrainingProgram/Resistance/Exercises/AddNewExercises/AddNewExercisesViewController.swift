//
//  AddNewExercisesViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 14.11.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit
import Moya
import Kingfisher

enum Category: String, Codable {
    
    case machines = "Machines"
    case dumbbells = "Dumbbells"
    case barbells = "Barbells"
    case pulley = "Pulley cable"
    case bodyweight = "Bodyweight"
    case accessories = "Accessories"
    case favourites = ""
    
    var localizedString: String {
        switch self {
        case .machines:
            return Constants.ResistanceExercises.Category().machinesString
        case .dumbbells:
            return Constants.ResistanceExercises.Category().dumbbellsString
        case .barbells:
            return Constants.ResistanceExercises.Category().barbellsString
        case .pulley:
            return Constants.ResistanceExercises.Category().pulleyString
        case .bodyweight:
            return Constants.ResistanceExercises.Category().bodyweightString
        case .accessories:
            return Constants.ResistanceExercises.Category().accessoriesString
        case .favourites:
            return Constants.ResistanceExercises.Category().favoritesString
        }
    }
}

struct AddExercisesCategory {
    var subcategories: [AddExercisesSubCategoryModel]
    var category: Category
}

struct AddExercisesSubCategoryModel {
    var exercises: [Exercise]
    var section: AddExercisesTableHeaderView.Content
}

final class AddNewExercisesViewController: BaseViewController {
    
    enum ExerciseScreenState {
        case addExercise, selectForSuperset, circuit
    }
    
    // MARK: - Views
    @IBOutlet weak private var collectionView: UICollectionView!
    @IBOutlet weak private var tableView: UITableView!
    @IBOutlet weak private var searchViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak private var searchView: UIView!
    @IBOutlet weak private var doneButton: UIView!
    
    private var searchItem: UIBarButtonItem?
    weak var searchViewController: SearchExercisesViewController?
    
    // MARK: - Properties
    var state: ExerciseScreenState = .addExercise
    var didAddExerciseFromSearch: ((Exercise) -> Void)?
    var didAddSelectedExercises: (([Exercise]) -> Void)?
    
    var groupKey: GroupsData.MuscleGroupKey!
    private var categoryIndex: Int = 0
    private var expandedSection: [Int] = []
    private var pendingRequestWorkItem: DispatchWorkItem?
    
    var selectedData: [Exercise] = []
    private var searchData: [Exercise] = []
    private var dataCollectionView: [AddExercisesCollectionCell.Content] = [
        .init(imageName: "Machines", category: .machines),
        .init(imageName: "Dumbbells", category: .dumbbells),
        .init(imageName: "Barbells", category: .barbells),
        .init(imageName: "Pulley cable", category: .pulley),
        .init(imageName: "Bodyweight", category: .bodyweight),
        .init(imageName: "Accessories", category: .accessories),
        .init(imageName: "ic_fav_top", category: .favourites)
    ]
    
    var categoriesData: [AddExercisesCategory] = []
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLocalazibleUI()
        if state == .addExercise {
             setupData()
        }
        setupTableView()
        setupCollectionView()
        addKeyboardObservers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        navigationController?.navigationBar.setBlueGradient()
        fixDirection()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if  state == .selectForSuperset || state == .circuit {
                  setupData()
              }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? SearchExercisesViewController {
            vc.state = state
            searchViewController = vc
            
            searchViewController?.didAddExercise = { [weak self] exercise in
                guard let self = self else { return }
                self.didAddExerciseFromSearch?(exercise)
            }
        }
    }
    
    deinit {
        print("Deinited")
    }
    
    // MARK: - Actions
    @IBAction private func addRemoveToFavourite(_ sender: UILongPressGestureRecognizer) {
        let point = sender.location(in: self.tableView)
        if sender.state == UIGestureRecognizer.State.began {
            
            guard let indexPath = self.tableView.indexPathForRow(at: point) else { return }
            guard let cell = tableView.cellForRow(at: indexPath) as? AddExercisesTableCell else { return }
            cell.favoriteButton.isHidden.toggle()
            var item = categoriesData[categoryIndex].subcategories[indexPath.section].exercises[indexPath.row]
            
            if !item.favourite {
                item.favourite.toggle()
                categoriesData[categoryIndex].subcategories[indexPath.section].exercises[indexPath.row].favourite.toggle()
                categoriesData[categoriesData.count - 1].subcategories[0].exercises.append(item)
                ExerciseProvider.addExerciseToFavourite(id: item.id) { [weak self] (result) in
                    switch result {
                    case .success(_):
                        self?.showAlert(text: Constants.ResistanceExercises.Messages().addedToFavorite)
                    case .failure(let error):
                        self?.showAlert(text: error.localizedDescription)
                    }
                }
                
            } else {
                guard let index = categoriesData[categoriesData.count - 1].subcategories[0].exercises.lastIndex(where: { $0.id == item.id }) else { return }
                categoriesData[categoriesData.count - 1].subcategories[0].exercises.remove(at: index)
                ExerciseProvider.removeExerciseFromFavourite(id: item.id) { [weak self] (result) in
                    switch result {
                    case .success(_):
                        self?.showAlert(text: Constants.ResistanceExercises.Messages().removedFromFavorite)
                    case .failure(let error):
                        self?.showAlert(text: error.localizedDescription)
                    }
                }
                if self.categoryIndex != self.categoriesData.count - 1 {
                    categoriesData[categoryIndex].subcategories[indexPath.section].exercises[indexPath.item].favourite.toggle()
                }
                if self.categoryIndex == self.categoriesData.count - 1 {
                    if categoriesData[categoryIndex].subcategories[indexPath.section].exercises.count == 0 {
                        tableView.reloadData()
                    } else {
                        tableView.deleteRows(at: [indexPath], with: .fade)
                    }
                    
                    categoriesData.enumerated().forEach { index, element in
                        element.subcategories.enumerated().forEach { subIndex, subElement in
                            guard let itemIndex = subElement.exercises.lastIndex(where: { $0.id == item.id }) else { return }
                            categoriesData[index].subcategories[subIndex].exercises[itemIndex].favourite.toggle()
                        }
                    }
                }
            }
        }
    }
    
    @IBAction private func backButtonDidTap(_ sender: Any) {
        guard navigationItem.titleView == nil else {
            searchViewController?.view.hideToast()
            navigationItem.titleView = nil
            navigationItem.rightBarButtonItem = searchItem
            searchViewHeightConstraint.constant = 0
            refreshSearchViewConstraint()
            searchViewController?.resultsData = []
            searchViewController?.searchText = Constants.ResistanceExercises.Messages().addYourOwnExercise
            searchViewController?.tableView.reloadData()
            return
        }
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction private func doneDidTap(_ sender: Any) {
        switch state {
        case .addExercise:
            didAddSelectedExercises?(selectedData)
            navigationController?.popViewController(animated: true)
        case .selectForSuperset:
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.didAddSelectedExercises?(self.selectedData)
            }
            navigationController?.popViewController(animated: true)
        default: break
        }
    }
    
    @IBAction private func searchButtonDidTap(_ sender: Any) {
        guard navigationItem.titleView == nil else { return }
        searchItem = navigationItem.rightBarButtonItem
        navigationItem.rightBarButtonItem = nil
        showSearchView()
    }
    
    // MARK: - Private
    private func setupTableView() {
        tableView.layer.shadowColor = UIColor.gray.cgColor
        tableView.layer.shadowOpacity = 0.3
        tableView.layer.shadowOffset = .init(width: 0, height: 1)
        tableView.layer.shadowRadius = 2
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 85, right: 0)
        self.tableView.register(AddExercisesTableHeaderView.nib, forHeaderFooterViewReuseIdentifier: AddExercisesTableHeaderView.reuseIdentifier)
    }
    
    private func expandFirstSection() {
        categoriesData.mutateEach {
            guard $0.subcategories.count > 0 else { return }
            $0.subcategories[0].section.isFirst  = true
            $0.subcategories[0].section.isExpanded = true
        }
    }
    
    private func expandSection(at section: Int) {
        var indexPaths = [IndexPath]()
        for row in categoriesData[categoryIndex].subcategories[section].exercises.indices {
            let indexPath = IndexPath(row: row, section: section)
            indexPaths.append(indexPath)
        }
        categoriesData[categoryIndex].subcategories[section].section.isExpanded.toggle()
        categoriesData[categoryIndex].subcategories[section].section.isExpanded ? tableView.insertRows(at: indexPaths, with: .fade) :  tableView.deleteRows(at: indexPaths, with: .fade)
    }
    
    private func addKeyboardObservers() {
        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillShowNotification, object: nil, queue: nil) {[weak self] (notification) in
            self?.handleKeyboard(notification: notification)
        }
        
        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillHideNotification, object: nil, queue: nil) {[weak self] (notification) in
            self?.handleKeyboard(notification: notification)
        }
    }
    
    private func handleKeyboard(notification: Notification) {
        guard let userInfo = notification.userInfo else { return }
        guard let keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        searchViewHeightConstraint.constant = notification.name == UIResponder.keyboardWillHideNotification ? view.frame.height : view.frame.height - keyboardFrame.height + 60
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    private func showSearchView() {
        let searchBar = UISearchBar()
        
        switch LocalizationManager.Language.language {
        case .english:
            searchBar.semanticContentAttribute = .forceLeftToRight
            searchBar.placeholder = "\(Constants.ResistanceExercises.Methods().searchString) \(groupKey.localizedString) \(Constants.ResistanceExercises.Methods().exercisesString)..."
        case .hebrew:
            searchBar.semanticContentAttribute = .forceRightToLeft
            searchBar.placeholder = "\(Constants.ResistanceExercises.Methods().searchString) \(Constants.ResistanceExercises.Methods().exercisesString) \(groupKey.localizedString)..."
        }
        
        searchBar.searchBarStyle = .default
        searchBar.isTranslucent = false
        searchBar.setImage(UIImage(), for: .search, state: .normal)
        
        if #available(iOS 13, *) {
            searchBar.searchTextField.adjustsFontSizeToFitWidth = true
            searchBar.searchTextField.backgroundColor = .white
            searchBar.searchTextField.textColor = .black
        }
        navigationItem.titleView = searchBar
        searchBar.delegate = self
        searchBar.becomeFirstResponder()
        refreshSearchViewConstraint()
    }
    
    private func refreshSearchViewConstraint() {
        UIView.animate(withDuration: 0.3) { [weak self] in
            self?.searchView.layoutIfNeeded()
        }
    }
    
    private func showExerciseInfo(exercise: Exercise) {
        guard !exercise.isCustom() else { return }
        let vc = ExerciseDetailsViewController.instantiate()
        vc.exercise = exercise
        navigationController?.pushViewController(vc, animated: true)
    }
    
    private func selectExercise(indexPath: IndexPath, content: Exercise, subcategoryName: String) {
        if state == .selectForSuperset {
            guard selectedData.count <= 5 else {
                self.showAlert(text: Constants.ResistanceExercises.Messages().theLimitIsSix)
                tableView.reloadRows(at: [indexPath], with: .automatic)
                return
            }
        }
        categoriesData[categoryIndex].subcategories[indexPath.section].exercises[indexPath.item].isSelected = true
        selectedData.append(content)
    }
    
    private func deselectExercise(indexPath: IndexPath, id: Int) {
        categoriesData[categoryIndex].subcategories[indexPath.section].exercises[indexPath.item].isSelected = false
        guard let index = selectedData.lastIndex(where: { $0.id == id }) else { return }
        selectedData.remove(at: index)
    }
    
    private func setupData() {
        categoriesData =  dataCollectionView.map({
            if $0.category == .favourites {
                return .init(subcategories: [.init(exercises: [], section: .init(imageName: "yellow_fav", title: "Favorite"))], category: .favourites)
            } else {
                return AddExercisesCategory(subcategories: [], category: $0.category)
            }
        })
        startAnimation()
        ExerciseProvider.getExercisesForMuscleGroup(groupKey.rawValue) { [weak self] (result) in
            guard let self = self else { return }
            self.stopAnimation()
            switch result {
            case .success(let data):
                var favouriteSet: Set<Exercise> = []
                data.componentList.forEach({ item in
                    guard let index = self.categoriesData.lastIndex(where: { $0.category.rawValue == item.category }) else { return }
                    self.categoriesData[index].subcategories = item.subcategory.map({ subcategory in
                        return AddExercisesSubCategoryModel(exercises: subcategory.exercises.map({
                            var newItem = $0
                            newItem.category = self.categoriesData[index].category
                            newItem.privateId = UUID().uuidString
                            if newItem.favourite {
                                favouriteSet.insert(newItem)
                            }
                            self.searchData.append(newItem)
                            return newItem
                            
                        }), section: .init(imageName: subcategory.imageUrl ?? "", title: subcategory.title))
                    })
                })
                self.expandFirstSection()
                self.tableView.reloadData()
                self.categoriesData[self.categoriesData.count - 1].subcategories[0].exercises = Array(favouriteSet)
            case .failure(let error):
                print(error)
            }
        }
    }
    
    private func setupLocalazibleUI() {
        switch LocalizationManager.Language.language {
            
        case .english:
            title = "\(Constants.ResistanceExercises.Methods().selectString) \(groupKey.localizedString) \(Constants.ResistanceExercises.Methods().exercisesString)"
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            collectionView.semanticContentAttribute = .forceLeftToRight
            tableView.semanticContentAttribute = .forceLeftToRight
        case .hebrew:
            title = "\(Constants.ResistanceExercises.Methods().selectString) \(Constants.ResistanceExercises.Methods().exercisesString)  \(groupKey.localizedString)"
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            collectionView.semanticContentAttribute = .forceRightToLeft
            tableView.semanticContentAttribute = .forceRightToLeft
        }
        if state == .circuit {
            doneButton.isHidden = true
        }
        searchViewHeightConstraint.constant = 0
    }
    
    private func setupCollectionView() {
        DispatchQueue.main.async {
            self.collectionView.collectionViewLayout.invalidateLayout()
        }
        
        collectionView.reloadData()
        collectionView.selectItem(at: IndexPath(item: 0, section: 0), animated: true, scrollPosition: .top)
        expandedSection = dataCollectionView.map({ _ in 0 })
    }
}

// MARK: - UICollectionViewDataSource
extension AddNewExercisesViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataCollectionView.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(with: AddExercisesCollectionCell.self, for: indexPath)
        cell.set(content: dataCollectionView[indexPath.item])
        
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension AddNewExercisesViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        categoryIndex = indexPath.item
        tableView.reloadData()
    }
}

// MARK: - UITableViewDataSource
extension AddNewExercisesViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        guard categoriesData.count > 0 else { return 0 }
        return categoriesData[categoryIndex].subcategories.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if categoryIndex == categoriesData.count - 1 {
            if categoriesData[categoryIndex].subcategories[0].exercises.isEmpty {
                return 1
            }
        }
        return categoriesData[categoryIndex].subcategories[section].section.isExpanded ? categoriesData[categoryIndex].subcategories[section].exercises.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if categoryIndex == categoriesData.count - 1 {
            if categoriesData[categoryIndex].subcategories[indexPath.section].exercises.isEmpty {
                return tableView.dequeueReusableCell(with: UITableViewCell.self, for: indexPath)
            }
        }
        let cell = tableView.dequeueReusableCell(with: AddExercisesTableCell.self, for: indexPath)
        let content = categoriesData[categoryIndex].subcategories[indexPath.section].exercises[indexPath.item]
        let subcategoryName = categoriesData[categoryIndex].subcategories[indexPath.section].section.title
        cell.set(content: content)
        cell.imageDidTap = { [weak self] in self?.showExerciseInfo(exercise: content) }
        cell.exerciseDidCheck = { [weak self] in
            guard let self = self else { return }
            self.selectExercise(indexPath: indexPath, content: content, subcategoryName: subcategoryName)
            if self.state == .circuit {
                self.didAddExerciseFromSearch?(content)
                self.navigationController?.popViewController(animated: true)
                
            }
        }
        cell.exerciseDidUncheck = { [weak self] in
            guard let self = self else { return }
            self.deselectExercise(indexPath: indexPath, id: content.id)
        }
        return cell
    }
}

// MARK: - UITableViewDelegate
extension AddNewExercisesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let cell = tableView.cellForRow(at: indexPath) as? AddExercisesTableCell else { return }
        let content = categoriesData[categoryIndex].subcategories[indexPath.section].exercises[indexPath.item]
        let subcategoryName = categoriesData[categoryIndex].subcategories[indexPath.section].section.title
        
        switch cell.selectionButton.isChecked {
        case true:
            deselectExercise(indexPath: indexPath, id: content.id)
        case false:
            selectExercise(indexPath: indexPath, content: content, subcategoryName: subcategoryName)
        }
        
        cell.selectionButton.isChecked.toggle()
        if state == .circuit {
            didAddExerciseFromSearch?(content)
            navigationController?.popViewController(animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: AddExercisesTableHeaderView.reuseIdentifier) as? AddExercisesTableHeaderView else { return nil }
        
        header.set(content: categoriesData[categoryIndex].subcategories[section].section, isFavoriteCategory: categoriesData[categoryIndex].category == .favourites )
        header.didTap = { [weak self] in
            guard let self = self else { return }
            self.expandSection(at: section)
            guard self.expandedSection[self.categoryIndex] != section else { return }
            
            guard let closedHeader = tableView.headerView(forSection: self.expandedSection[self.categoryIndex]) as? AddExercisesTableHeaderView else { return }
            guard self.categoriesData[self.categoryIndex].subcategories[self.expandedSection[self.categoryIndex]].section.isExpanded != false else {
                self.expandedSection[self.categoryIndex] = section
                return
            }
            self.expandSection(at: self.expandedSection[self.categoryIndex])
            closedHeader.tapHeader()
            self.expandedSection[self.categoryIndex] = section
        }
        return header
    }
}

// MARK: - UISearchBarDelegate
extension AddNewExercisesViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard let searchVC = searchViewController else { return }
        self.pendingRequestWorkItem?.cancel()
        let requestWorkItem = DispatchWorkItem { [weak self] in
            guard !searchText.isEmpty else {
                searchVC.searchText = Constants.ResistanceExercises.Messages().addYourOwnExercise
                searchVC.resultsData = []
                searchVC.tableView.reloadData()
                return
            }
            
            guard let self = self else { return }
            searchVC.searchText = searchText
            searchVC.resultsData = self.searchData.filter { (exercise: Exercise) -> Bool  in
                return exercise.title.lowercased().contains(searchText.lowercased())
            }
            searchVC.tableView.reloadData()
        }
        self.pendingRequestWorkItem = requestWorkItem
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(350), execute: requestWorkItem)
    }
}
