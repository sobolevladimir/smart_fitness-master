//
//  SearchExercisesViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 15.11.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class SearchExercisesViewController: BaseViewController {
    
    // MARK: - Views
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Properties
    var didAddExercise: ((Exercise) -> Void)?
    var resultsData: [Exercise] = []
    var searchText: String = Constants.ResistanceExercises.Messages().addYourOwnExercise
    var state: AddNewExercisesViewController.ExerciseScreenState = .addExercise
    
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 85, right: 0)
    }
}

// MARK: - UITableViewDataSource
extension SearchExercisesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        resultsData.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(with: SearchExercisesCell.self, for: indexPath)
        if indexPath.row == 0 {
            cell.set(title: searchText, isFirst: true)
            cell.addExercise = { [weak self] title  in
                guard let self = self else { return }
                if self.searchText == Constants.ResistanceExercises.Messages().addYourOwnExercise {
                    self.showAlert(text: Constants.ResistanceExercises.Messages().writeYourExercise, isCenter: true)
                } else {
                    var exercise = Exercise(id: 0,
                                            title: title,
                                            descriptions: [],
                                            videoUrl: nil,
                                            mainImageUrl: "placeholder",
                                            exerciseImages: [],
                                            favourite: false,
                                            category: .accessories,
                                            programExerciseId: 0,
                                            setsValue: 3,
                                            repsValue: 10,
                                            kgValue: [15],
                                            privateId: UUID().uuidString,
                                            indexOfKg: 0,
                                            notes: "",
                                            restTime: Constants.ResistanceExercises.Methods.restTimeDefault,
                                            machineNumber: Constants.ResistanceExercises.Methods.defaultMachineNumber)
                    exercise.privateId = UUID().uuidString
                    self.didAddExercise?(exercise)
                    if self.state == .circuit {
                        self.navigationController?.popViewController(animated: true)
                    }
                    self.showAlert(text: Constants.ResistanceExercises.Messages().yourExerciseAddedToList, isCenter: true)
                }
            }
        } else {
            cell.set(title: resultsData[indexPath.row - 1].title, isFirst: false )
            cell.addExercise = { [weak self] title  in
                guard let self = self else { return }
                var exercise = self.resultsData[indexPath.row - 1]
                exercise.privateId = UUID().uuidString
                self.didAddExercise?(exercise)
                if self.state == .circuit {
                    self.navigationController?.popViewController(animated: true)
                }
                self.showAlert(text: Constants.ResistanceExercises.Messages().exerciseAddedToList, isCenter: true)
            }
        }
        return cell
    }
}
