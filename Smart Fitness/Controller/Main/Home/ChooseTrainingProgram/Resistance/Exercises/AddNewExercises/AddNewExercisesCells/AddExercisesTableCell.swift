//
//  AddExercisesTableCell.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 14.11.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit
import Kingfisher

final class AddExercisesTableCell: UITableViewCell {
    
    // MARK: - Views
    @IBOutlet weak private var exerciseImageView: UIImageView!
    @IBOutlet weak private var exerciseTitle: UILabel!
    @IBOutlet weak var favoriteButton: RadioCheckButton!
    @IBOutlet weak var selectionButton: RadioCheckButton!
    
    // MARK: - Properties
    var exerciseDidCheck: (() -> Void)?
    var exerciseDidUncheck: (() -> Void)?
    var imageDidTap: (() -> Void)?
    
    // MARK: - Init
    override func prepareForReuse() {
        super.prepareForReuse()
        exerciseImageView.image = nil
        exerciseTitle.text = nil
        selectionButton.isChecked = false
    }
    
    // MARK: - Actions
    @IBAction private func imageDidTap(_ sender: Any) {
        imageDidTap?()
    }
    
    @IBAction private func checkButtonDidTap(_ sender: RadioCheckButton) {
        switch sender.isChecked {
        case true:
            exerciseDidUncheck?()
        case false:
            exerciseDidCheck?()
        }
    }
 
    // MARK: - Methods
    func set(content: Exercise) {
        exerciseImageView.downloadImage(from: content.mainImageUrl)
        exerciseTitle.text = content.title
        favoriteButton.isHidden = !content.favourite
        selectionButton.isChecked = content.isSelected
    }
}

// MARK: - Content
extension AddExercisesTableCell {
    struct Content: Equatable {
        var id = UUID().uuidString
        var imageName: String
        var title: String
        var isFavorite: Bool
        var isSelected: Bool
        var category: Category
    }
}
