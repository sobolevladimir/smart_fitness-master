//
//  ParentExerciseCell.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 21.12.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

class ParentExerciseCell: UITableViewCell {
    
    // MARK: - Views
    var topSeparator: UIView!
    var arrowButton: UIButton!
    var machineNumberButton: LoginButton!
    var editButton: UIButton!
    
    // MARK: - Constraints
    var expandableBottomConstraint: NSLayoutConstraint!
    
    // MARK: - Properties
    var isExpanded: Bool = false 
    let machineNumberPickerData = ResistanceDataCreator().machineNumberData
    var method: DropSetMethodViewController.MethodType = .percentOfRM
    
    // MARK: - Properties Actions
    var didImageClicked: (() -> Void)?
    var didAddSet: (() -> Void)?
    var didReduceSet: (() -> Void)?
    var didDeleteMethod: (() -> Void)?
    var didEditData: (() -> Void)?
    var showRestTimePopUp: ((Int) -> Void)?
    var didMachineNumberSelected: ((Int) -> Void)?
    var didExpandClicked: (() -> Void)?
    var notesIsEditing: ((String) -> Void)?
    
    // MARK: - Init
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.3
        layer.shadowOffset = .zero
        layer.shadowRadius = 2
    }
    
    // MARK: - Methods
    func selectRow() {
        isExpanded.toggle()
        expandableBottomConstraint.isActive = isExpanded
        rotateArrow(if: isExpanded)
        topSeparator.isHidden = !isExpanded
    }
    
    func rotateArrow(if expanded: Bool) {
        var angle: CGFloat = 0
        switch LocalizationManager.Language.language {
        case .english:
            angle = 0.5 * .pi
        case .hebrew:
            angle = -0.5 * .pi
        }
        let transform: CGAffineTransform = expanded ? .init(rotationAngle: angle) : .identity
        UIView.animate(withDuration: 0.2) { [weak self] in
            self?.arrowButton.layer.setAffineTransform(transform)
        }
    }
    
    func expandPickerView(heightConstraint: NSLayoutConstraint) {
        heightConstraint.constant = (heightConstraint.constant == 0) ? 100 : 0
        UIView.animate(withDuration: 0.3) { [weak self] in self?.layoutIfNeeded() }
    }
}

// MARK: - UIPickerViewDataSource
extension ParentExerciseCell: UIPickerViewDataSource {
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        machineNumberPickerData.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
}

// MARK: - UIPickerViewDelegate
extension ParentExerciseCell: UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        machineNumberButton.setTitle(machineNumberPickerData[row], for: .normal)
        if machineNumberPickerData[row] == "--" {
            didMachineNumberSelected?(100)
        } else {
            didMachineNumberSelected?(Int(machineNumberPickerData[row]) ?? 0)
        }
        pickerView.reloadAllComponents()
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var color: UIColor!
        var label = UILabel()
        if let v = view { label = v as! UILabel }
        label.font = Constants.returnFontName(style: .regular, size: 16)
        label.textAlignment = .center
        label.text = machineNumberPickerData[row]
        
        if pickerView.selectedRow(inComponent: component) == row {
            color = UIColor.orange
        } else {
            color = UIColor.black
        }
        label.textColor = color
        
        return label
    }
}

// MARK: - UITextFieldDelegate
extension ParentExerciseCell: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        editButton.isHidden = false
    }
}

