//
//  ExerciseSupersetEditCell.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 10.12.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

protocol ExpandableCellProtocol {
    var expandHeightAnchor: NSLayoutConstraint! { get set}
    var cellHeightAnchor: NSLayoutConstraint! { get set}
    var contentView: UIView { get  }
    func layoutIfNeeded()
}

class ExerciseSupersetEditCell: ParentExerciseCell, ExpandableCellProtocol {
    
    enum State {
        case sameMuscles, differentMuscles
    }
    
    // MARK: - Views
    @IBOutlet weak private var tableView: UITableView!
    @IBOutlet weak private var unlinkButton: LoginButton!
    @IBOutlet weak private var textField: UITextField!
    @IBOutlet weak private var valuesStackView: UIStackView!
    @IBOutlet weak private var bottomButtonsStackView: UIStackView!
    @IBOutlet weak private var addReduceStackView: UIStackView!
    @IBOutlet weak private var addNewKgView: AddNewKg!
    @IBOutlet override var editButton: UIButton! {
              get { return super.editButton }
              set { super.editButton = newValue }
          }
    @IBOutlet weak private var kgLabel: UILabel!
    
    @IBOutlet weak var notesLabel: UILabel!
    @IBOutlet weak var notesView: RoundedView!
    
    // MARK: - Constraints
    @IBOutlet weak private var tableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var expandHeightAnchor: NSLayoutConstraint!
    @IBOutlet weak  var cellHeightAnchor: NSLayoutConstraint!
    @IBOutlet weak private var addNewKgHeightAnchor: NSLayoutConstraint!
    @IBOutlet weak var kgContainerViewWidthAnchor: NSLayoutConstraint?
    
    override func prepareForReuse() {
        super.prepareForReuse()
        didImageClicked = nil
        didAddSet = nil
        didReduceSet = nil
        didDeleteMethod = nil
        didEditData = nil
        showRestTimePopUp = nil
        didMachineNumberSelected = nil
        didExpandClicked = nil
        showAlert = nil
    }
    
    // MARK: - Properties
    private var data: [ExerciseSupersetSetCell.Content] = []
    var showAlert: (() -> Void)?
    var didAddNewKg: (() -> Void)?
    var didChangeKgIndex: ((Int) -> Void)?
    
    // MARk: - Action
    @IBAction private func addSetButtonAction(_ sender: Any) {
        data.append(.init(exercises: data[0].exercises, restTime: "", setNumber: 1))
        tableView.insertRows(at: [IndexPath(row: data.count - 1, section: 0)], with: .none)
        didAddSet?()
    }
    @IBAction func editingTextField(_ sender: UITextField) {
        editButton.isHidden = !sender.text!.isEmpty
        guard let notes = sender.text else { return }
        notesIsEditing?(notes)
    }
    
    @IBAction private func reduceSetButtonAction(_ sender: Any) {
        guard data.count > 1 else {
            showAlert?()
            return
        }
        data.removeLast()
        tableView.deleteRows(at: [IndexPath(row: data.count - 1, section: 0)], with: .none)
        didReduceSet?()
    }
    
    @IBAction private func unlinkSupersetButtonAction(_ sender: Any) { didDeleteMethod?() }
    
    @IBAction private func editDataButtonAction(_ sender: Any) { didEditData?() }
    
    @IBAction private func arrowButtonDidTap(_ sender: Any) { expandKgContainerView() }
    
    @objc private func hideNewKgView() {
        textField.resignFirstResponder()
        
        if let addNewKgHeightAnchor = addNewKgHeightAnchor {
            addNewKgHeightAnchor.constant = 0
        }
        UIView.animate(withDuration: 0.3) { [weak self] in self?.layoutIfNeeded() }
    }
    
    
    // MARK: - Init
    override func awakeFromNib() {
        super.awakeFromNib()
        tableView.dataSource = self
        textField?.delegate = self
        valuesStackView.semanticContentAttribute = .forceLeftToRight
        let gesture = UITapGestureRecognizer(target: self, action: #selector(hideNewKgView))
                       gesture.cancelsTouchesInView = false
                       gesture.delegate = self
                       addGestureRecognizer(gesture)
        switch LocalizationManager.Language.language {
        case .english:
            kgContainerViewWidthAnchor?.constant = 95
        case .hebrew:
            kgContainerViewWidthAnchor?.constant = 105
        }
        guard let addReduceStackView = addReduceStackView else { return }
        addReduceStackView.semanticContentAttribute = .forceLeftToRight
        bottomButtonsStackView.semanticContentAttribute = .forceLeftToRight
    }
    
    // MARK: - Methods
    func set(content: [ExerciseSupersetSetCell.Content], notes: String, state: DropSetMethodViewController.MethodType, isExpanded: Bool) {
        switch state {
            case .superset:
                guard let unlinkButton = unlinkButton else { break }
                unlinkButton.setTitle(Constants.ResistanceExercises.Methods().unlinkSupersetString, for: .normal)
            case .supersetDifferent:
           
               guard let unlinkButton = unlinkButton else { break }
               unlinkButton.setTitle(Constants.ResistanceExercises.Methods().deleteString, for: .normal)
            default: break
        }
        
        data = content
        textField?.text = notes
        notesLabel?.text = notes
        notesView?.isHidden = notes.count == 0
        editButton?.isHidden = !(textField?.text!.isEmpty ?? false)
        if let addNewKgView = addNewKgView {
                addNewKgView.set(countAmountOfKg: content[0].exercises[0].kgValues.count == 1 ? 0 : content[0].exercises[0].kgValues.count)
                   addNewKgView.didSelectRow = { [weak self] index in
                       self?.didChangeKgIndex?(index)
                       self?.addNewKgHeightAnchor.constant = 0
                       UIView.animate(withDuration: 0.3) {
                           self?.contentView.layoutIfNeeded()
                       }
                   }
                   addNewKgView.didAddNewKg = { [weak self] in
                       self?.didAddNewKg?()
                       self?.addNewKgHeightAnchor.constant = 0
                       UIView.animate(withDuration: 0.3) {
                           self?.contentView.layoutIfNeeded()
                       }
                   }
               }
        
        tableView.reloadData()
        guard content.count > 0 else {
            tableViewHeightConstraint.constant = 0
             layoutIfNeeded()
            return
        }
        cellHeightAnchor.isActive = !isExpanded
        expandHeightAnchor.isActive = isExpanded
        contentView.isHidden = !isExpanded
        let cellHeight: CGFloat = 23.5 * CGFloat(content[0].exercises.count)
        let restTimeButtonHeight = content.count == 1 ? 0 : 30
        kgLabel?.text = Constants.Circuit().kg + (content[0].exercises[0].kgValues.count == 1 ? "" : "\(content[0].exercises[0].indexOfKg + 1)")
        tableViewHeightConstraint.constant = CGFloat(data.count) * (cellHeight + 30) - CGFloat(restTimeButtonHeight)
        layoutIfNeeded()
    }
    
    // MARK: - Private
    private func expandKgContainerView() {
        guard let kgAnchor = addNewKgHeightAnchor else { return }
        kgAnchor.constant = kgAnchor.constant == 0 ? 137 : 0
        UIView.animate(withDuration: 0.3) { [weak self] in
            self?.contentView.layoutIfNeeded()
        }
    }
}

// MARK: - UITableViewDataSource
extension ExerciseSupersetEditCell: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(with: ExerciseSupersetSetCell.self, for: indexPath)
        let view2 = valuesStackView.arrangedSubviews[1]
        let view1 = valuesStackView.arrangedSubviews[0]
        cell.set(content: data[indexPath.row], spacing: view2.frame.minX - view1.frame.maxX)
        if indexPath.row == data.count - 1 && data.count != 1 {
            cell.restTimeButton.isHidden = true
        }
        cell.showRestTimePopUp = { [weak self] in self?.showRestTimePopUp?(indexPath.row) }
        cell.didValueTap = { [weak self] in self?.didEditData?() }
        return cell
    }
}

// MARK: - UIGestureRecognizerDelegate
extension ExerciseSupersetEditCell {
    override func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
          if touch.view is UIStackView || touch.view is UITableViewCell || touch.view is UIButton  {
              return false
          }
          return true
      }
}
