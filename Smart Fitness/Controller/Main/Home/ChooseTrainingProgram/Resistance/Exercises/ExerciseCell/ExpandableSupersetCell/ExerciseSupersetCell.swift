//
//  ExericiseSupersetCell.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 06.12.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class ExerciseSupersetCell: UITableViewCell {
    
    // MARK: - Views
    @IBOutlet weak var containerView: RoundedView!
    @IBOutlet weak private var exerciseImageView: UIImageView!
    @IBOutlet weak private var exerciseTitleLabel: UILabel!
    @IBOutlet weak var arrowButton: UIButton!
    @IBOutlet weak var moveButton: UIButton!
    @IBOutlet weak var titleButton: UIButton!
    @IBOutlet weak var expandImageView: UIButton!
    @IBOutlet weak private var topMergeImage: UIImageView!
    @IBOutlet weak private var bottomMergeImage: UIImageView!
    
    // MARK: - Constraints
    @IBOutlet weak var bottomContainerViewAnchor: NSLayoutConstraint!
    
    // MARK: - Properties
    var didTapImageView: (() -> Void)?
    var didTapExpand: (() -> Void)?
    var isExpanded = false
    var isViewMode = false
    
    // MARK: - Init
    override func prepareForReuse() {
        super.prepareForReuse()
        exerciseImageView.image = nil
    }
    
    // MARK: - Methods
    func set(isFirst: Bool, isLast: Bool, content: Exercise, isExpanded: Bool) {
        self.tag = 666
        self.isExpanded = isExpanded
        self.layoutMargins = UIEdgeInsets.zero
        self.separatorInset = UIEdgeInsets.zero
        if content.isCustom() {
            exerciseImageView.image = "placeholder".image()
        } else {
            exerciseImageView.downloadImage(from: content.mainImageUrl)
        }
        exerciseTitleLabel.text = content.title
        if let moveButton = moveButton {
             moveButton.isHidden = !isFirst
        }
       
        expandImageView.isHidden = !isFirst
        containerView.corners = isFirst ? .top : .none
        if isViewMode {
            containerView.corners = .none
        }
        if isLast && !isExpanded {
            containerView.corners = .bottom
        }
        
        if isLast && isExpanded {
            containerView.corners = .none
        }
        
        if isFirst && isLast {
             containerView.corners = .all
        }
        if isViewMode {
            containerView.corners = .none
        }
        containerView.cornerRadius = 4
        topMergeImage.isHidden = isFirst
        bottomMergeImage.isHidden = isLast
        rotateArrow(if: isExpanded)
    }
    
       func rotateArrow(if expanded: Bool) {
           var angle: CGFloat = 0
           switch LocalizationManager.Language.language {
                 case .english:
                    if arrowButton.imageView?.image?.imageOrientation != UIImage.Orientation.up {
                        guard var image = arrowButton.imageView?.image else { return }
                        image = UIImage(cgImage: image.cgImage!, scale: image.scale, orientation: UIImage.Orientation.downMirrored)
                        arrowButton.setImage(image, for: .normal)
                    }
                     angle = 0.5 * .pi
                 case .hebrew:
                     angle = -0.5 * .pi
                 }
           let transform: CGAffineTransform = expanded ? .init(rotationAngle: angle) : .identity
           UIView.animate(withDuration: 0.2) { [weak self] in
               self?.arrowButton.layer.setAffineTransform(transform)
           }
       }
    
    // MARK: - Actions
    @IBAction private func imageButtonAction(sender: Any) { didTapImageView?() }

    @IBAction func expandButtonAction(_ sender: Any) {
        didTapExpand?()
        isExpanded.toggle()
    }
}
