//
//  ExerciseCell.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 18.11.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

protocol ExpandedCellProtocol {
    var isExpanded: Bool { get set }
    var didExpandClicked: (() -> Void)? { get set }
}

final class ExerciseCell: ParentExerciseCell, ExpandedCellProtocol {
    
    // MARK: - Views
    @IBOutlet weak var roundedView: RoundedView!
    @IBOutlet weak private var exerciseImageView: UIImageView!
    @IBOutlet weak private var exerciseTitleLabel: UILabel!
    @IBOutlet  override var topSeparator: UIView! {
        get { return super.topSeparator }
        set { super.topSeparator = newValue }
    }
    @IBOutlet  override var arrowButton: UIButton! {
        get { return super.arrowButton }
        set { super.arrowButton = newValue }
    }
    @IBOutlet weak private var machineStackView: UIStackView!
    @IBOutlet weak private var weightStackView: UIStackView!
    @IBOutlet override var editButton: UIButton! {
           get { return super.editButton }
           set { super.editButton = newValue }
       }
    @IBOutlet weak private var textField: LocalizableTextField!
    @IBOutlet weak var moveRowButton: UIButton!
    @IBOutlet weak var titleButton: UIButton!
    @IBOutlet weak private var weightValueLabel: UILabel!
    @IBOutlet weak private var repsValueLabel: UILabel!
    @IBOutlet weak private var setsValueLabel: UILabel!
    @IBOutlet weak private var restTimeButton: UIButton!
    @IBOutlet weak private var machinePickerView: CardioPickerView!
    @IBOutlet override var machineNumberButton: LoginButton! {
        get { return super.machineNumberButton }
        set { super.machineNumberButton = newValue }
    }
    @IBOutlet weak private var valuesStackView: UIStackView!
    @IBOutlet weak private var buttonsStackView: UIStackView!
    
    @IBOutlet weak private var addNewKgContainerView: AddNewKg!
    @IBOutlet weak var kgLabel: UILabel!
    
    @IBOutlet weak var notesLabel: UILabel!
    @IBOutlet weak var notesView: RoundedView!
    
    // MARK: - Constraints
    @IBOutlet weak var machinePickerHeightAnchor: NSLayoutConstraint!
    @IBOutlet override var expandableBottomConstraint: NSLayoutConstraint! {
        get { return super.expandableBottomConstraint }
        set { super.expandableBottomConstraint = newValue }
    }
    
    @IBOutlet var kgContainerViewHeightAnchor: NSLayoutConstraint!
    @IBOutlet weak var kgContainerViewWidthAnchor: NSLayoutConstraint?
    
    // MARK: Closure Actions
    var didDelete: (() -> Void)?
    var didRestClicked: (() -> Void)?
    var didValuesClicked: (() -> Void)?
    var willEditTextField: (() -> Void)?
    
    
    var didAddNewKg: (() -> Void)?
    var didChangeKgIndex: ((Int) -> Void)?
    
    // MARK: - Init
    override func awakeFromNib() {
        super.awakeFromNib()
        valuesStackView.semanticContentAttribute = .forceLeftToRight
         let gesture = UITapGestureRecognizer(target: self, action: #selector(hideMachinePickerView))
                gesture.cancelsTouchesInView = false
                gesture.delegate = self
                addGestureRecognizer(gesture)
        switch LocalizationManager.Language.language {
        case .english:
            kgContainerViewWidthAnchor?.constant = 95
        case .hebrew:
            kgContainerViewWidthAnchor?.constant = 105
        }
        guard let buttonsStackView = buttonsStackView else { return }
        machinePickerView.delegate = self
        machinePickerView.dataSource = self
        textField?.delegate = self
       
        buttonsStackView.semanticContentAttribute = .forceLeftToRight
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        exerciseImageView.image = nil
    }
    
    // MARK: - Methods
    func set(content: Exercise) {
        if let addNewKgContainerView = addNewKgContainerView {
            addNewKgContainerView.set(countAmountOfKg: content.kgValue.count == 1 ? 0 : content.kgValue.count)
            addNewKgContainerView.didSelectRow = { [weak self] index in
                self?.didChangeKgIndex?(index)
                self?.kgContainerViewHeightAnchor.constant = 0
                UIView.animate(withDuration: 0.3) {
                    self?.contentView.layoutIfNeeded()
                }
            }
            addNewKgContainerView.didAddNewKg = { [weak self] in
                self?.didAddNewKg?()
                self?.kgContainerViewHeightAnchor.constant = 0
                UIView.animate(withDuration: 0.3) {
                    self?.contentView.layoutIfNeeded()
                }
            }
        }
        if content.isCustom() {
            exerciseImageView.image = "placeholder".image()
        } else {
            exerciseImageView.downloadImage(from: content.mainImageUrl)
        }
        
        exerciseTitleLabel.text = content.title
        setsValueLabel.text = "\(content.setsValue)"
        textField?.text = content.notes
        notesLabel?.text = content.notes
        notesView?.isHidden = content.notes.count == 0
        editButton?.isHidden = !(textField?.text!.isEmpty ?? false)
     
        kgLabel?.text = Constants.Circuit().kg + (content.kgValue.count == 1 ? "" : "\(content.indexOfKg + 1)")
   
        
        repsValueLabel.text = (content.repsValue == 1000) ? Constants.ResistanceExercises.Methods().maxString : "\(content.repsValue)"
        if content.repsValue == 999 {
            repsValueLabel.text = content.repsTime
        }
        let kgValue = content.kgValue[content.indexOfKg]
        weightValueLabel.text = kgValue == 301 ? "?" : "\(kgValue)"
        weightValueLabel.font = Constants.returnFontName(style: kgValue == 301 ? .bold : .regular, size: 17)
        restTimeButton.setTitle("\(Constants.ResistanceExercises.Methods().restString) \(content.restTime)", for: .normal)
        setCellUIElements(by: content.category)
        isExpanded = content.isExpanded
        topSeparator.isHidden = !isExpanded
        rotateArrow(if: isExpanded)
        expandableBottomConstraint.isActive = isExpanded
        var machineNumber = ""
        if content.machineNumber == 100 {
            machineNumber = "--"
        } else {
            machineNumber = "\(content.machineNumber)"
        }
        machineNumberButton.setTitle(machineNumber, for: .normal)
        guard let index = machineNumberPickerData.lastIndex(of: machineNumber) else { return }
        guard let machinePickerView = machinePickerView else { return }
        machinePickerView.selectRow(index, inComponent: 0, animated: false)
    }
    
    func setRest(with time: String) {
        restTimeButton.setTitle("\(Constants.ResistanceExercises.Methods().restString) \(time)", for: .normal)
    }
    
    // MARK: - Actions
    @IBAction private func didImageTap(_ sender: Any) { didImageClicked?() }
    @IBAction private func textFieldIsEditing(_ sender: UITextField) {
        editButton.isHidden = !sender.text!.isEmpty
        guard let notes = sender.text else { return }
        notesIsEditing?(notes)
    }
    @IBAction private func expandButtonAction(_ sender: Any) { didExpandClicked?() }
    @IBAction private func deleteDidTap(_ sender: Any) { didDelete?() }
    @IBAction private func editDidTap(_ sender: Any) { didValuesClicked?() }
    
    @IBAction private func restDidTap(_ sender: Any) { didRestClicked?() }
    @IBAction private func machineNumberDidTap(_ sender: Any) { expandPickerView(heightConstraint: machinePickerHeightAnchor) }
    
    @IBAction func textFieldAction(_ sender: Any) { willEditTextField?() }
    
    @objc private func hideMachinePickerView() {
        textField?.resignFirstResponder()
        if let machinePickerHeightAnchor = machinePickerHeightAnchor {
             machinePickerHeightAnchor.constant =  0
        }
        
        if let kgContainerViewHeightAnchor = kgContainerViewHeightAnchor {
             kgContainerViewHeightAnchor.constant = 0
        }
        UIView.animate(withDuration: 0.3) { [weak self] in self?.layoutIfNeeded() }
    }

    @IBAction private func newKgArrowDidTap(_ sender: Any) { expandKgContainerView() }
    
    // MARK: - Private
    private func setCellUIElements(by category: Category) {
        switch category {
        case .machines:
            machineStackView.isHidden = false
            weightStackView.isHidden = false
        case .dumbbells:
            machineStackView.isHidden = true
            weightStackView.isHidden = false
        case .barbells:
            machineStackView.isHidden = true
            weightStackView.isHidden = false
        case .pulley:
            machineStackView.isHidden = false
            weightStackView.isHidden = false
        case .bodyweight:
            machineStackView.isHidden = true
            weightStackView.isHidden = true
        case .accessories:
            machineStackView.isHidden = true
            weightStackView.isHidden = false
        case .favourites: break
        }
        layoutSubviews()
    }
    
    private func expandKgContainerView() {
        guard let kgAnchor = kgContainerViewHeightAnchor else { return }
        kgAnchor.constant = kgAnchor.constant == 0 ? 137 : 0
        UIView.animate(withDuration: 0.3) { [weak self] in
            self?.contentView.layoutIfNeeded()
        }
    }
}

extension ExerciseCell {
    override func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view is UIStackView || touch.view is UITableViewCell || touch.view is UIButton  {
            return false
        }
        return true
    }
}
