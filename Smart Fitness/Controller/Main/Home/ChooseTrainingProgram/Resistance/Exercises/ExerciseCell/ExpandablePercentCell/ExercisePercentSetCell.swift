//
//  ExerciseSupersetSetCell.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 12.12.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class ExercisePercentSetCell: UITableViewCell {
    
    // MARK: - Views
    @IBOutlet weak private var setLabel: UILabel!
    @IBOutlet weak private var rmLabel: UILabel!
    @IBOutlet weak private var kgLabel: UILabel!
    @IBOutlet weak private var percentLabel: UILabel!
    @IBOutlet weak var restTimeButton: UIButton!
    @IBOutlet weak var valuesStackView: UIStackView!
    @IBOutlet weak var percentStackView: UIStackView!
    @IBOutlet weak var repsLabel: UILabel!
    
    // MARK: - Constraints
    @IBOutlet weak var topStackViewLeadingAnchor: NSLayoutConstraint!
    
    @IBOutlet weak var topStackViewTrailingAnchor: NSLayoutConstraint!
 
    // MARK: - Properties
    var showRestTimePopUp: (() -> Void)?
    var kgDidTap: (() -> Void)?
    
    // MARK: - Init
    override func prepareForReuse() {
        super.prepareForReuse()
        restTimeButton.isHidden = false
        valuesStackView.semanticContentAttribute = .forceLeftToRight
    }
    
    // MARK: - Actions
    @IBAction private func restTimeButtonAction(_ sender: Any) { showRestTimePopUp?() }
    
    @IBAction private func kgDidTap(_ sender: Any) {
        kgDidTap?()
    }
    
    // MARK: - Method
    func set(content: Content, method: DropSetMethodViewController.MethodType) {
        switch method {
        case .pyramid:
            repsLabel.text = Constants.ResistanceExercises.Methods().repsWithNoPointsString
            
            topStackViewLeadingAnchor.constant = 18
            topStackViewTrailingAnchor.constant = 31
            ///
            percentLabel.isHidden = true
            percentStackView.isHidden = true
        case .percentOfRM:
            percentStackView.isHidden = false
            percentLabel.isHidden = false
            
            
            repsLabel.text = Constants.ResistanceExercises.Methods().rmString
            topStackViewTrailingAnchor.constant = 16
            topStackViewLeadingAnchor.constant = 8
            
            
        default:
            break
        }
        setLabel.text = "\(content.set)"
        if content.rm == 1000 {
            rmLabel.text = Constants.ResistanceExercises.Methods().maxString
        } else if content.rm == 999 {
            rmLabel.text = content.repsTime
        } else {
            rmLabel.text = "\(content.rm)"
        }
        let kgValue = content.kg[content.indexOfKg]
        kgLabel.text = kgValue == 301 ? "?" : "\(kgValue)"
        percentLabel.text = "\(content.percent)%"
        restTimeButton.setTitle("\(Constants.ResistanceExercises.Methods().restString) \(content.restTime)", for: .normal)
        valuesStackView.semanticContentAttribute = .forceLeftToRight
        valuesStackView.layoutIfNeeded()
    }
}

// MARK: - Content
extension ExercisePercentSetCell {
    struct Content: Codable {
        var set: Int
        var rm: Int
        var kg: [Double]
        var indexOfKg = 0
        var percent: Int
        var restTime: String
        var repsTime: String
        var id: Int?
    }
}
