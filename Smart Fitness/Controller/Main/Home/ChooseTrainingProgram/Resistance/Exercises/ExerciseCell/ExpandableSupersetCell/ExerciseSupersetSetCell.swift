//
//  ExerciseSupersetSetCell.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 12.12.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class ExerciseSupersetSetCell: UITableViewCell {
    
    // MARK: - Views
    @IBOutlet weak private var tableView: UITableView!
    @IBOutlet weak private var setNumberLabel: UILabel!
    @IBOutlet weak var restTimeButton: UIButton!
    @IBOutlet weak var contentStackView: UIStackView!
    
    // MARK: - Constraints
    @IBOutlet weak var tableViewHeightAnchor: NSLayoutConstraint!
    
    // MARK: - Properties
    private var data: [ExerciseSupersetSetExerciseCell.Content] = []
    var showRestTimePopUp: (() -> Void)?
    var didValueTap: (() -> Void)?
    
    // MARK: - Init
    override func awakeFromNib() {
        super.awakeFromNib()
        tableView.dataSource = self
        contentStackView.semanticContentAttribute = .forceLeftToRight
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        restTimeButton.isHidden = false
    }
    
    // MARK: - Actions
    @IBAction func restTimeButtonAction(_ sender: Any) { showRestTimePopUp?() }
    
    // MARK: - Method
    func set(content: Content, spacing: CGFloat) {
        data = content.exercises.enumerated().map({ ExerciseSupersetSetExerciseCell.Content(exerciseName: "\($0 + 1)", repsValue: $1.repsValue, kgValue: $1.kgValue, kgValues: $1.kgValues, indexOfKg: $1.indexOfKg)})
        setNumberLabel.text = "\(content.setNumber)"
        restTimeButton.setTitle("\(Constants.ResistanceExercises.Methods().restString) \(content.restTime)", for: .normal)
        tableView.reloadData()
        tableViewHeightAnchor.constant = CGFloat(data.count) * 23.5
        contentStackView.spacing = spacing
        layoutIfNeeded()
    }
}

// MARK: - UITableViewDataSource
extension ExerciseSupersetSetCell: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(with: ExerciseSupersetSetExerciseCell.self, for: indexPath)
        cell.set(content: data[indexPath.row])
        cell.didValueTap = { [weak self] in self?.didValueTap?() }
        return cell
    }
}

// MARK: - Content
extension ExerciseSupersetSetCell {
    struct Content: Codable {
        var exercises: [ExerciseSupersetSetExerciseCell.Content]
        var restTime: String
        var setNumber: Int
        var notes: String = ""
        var id: Int?
    }
}
