//
//  ExerciseSupersetCell.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 17.12.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class ExerciseSupersetMainCell: ParentExerciseCell {
    
    // MARK: - Views
    @IBOutlet weak var tableView: UITableView!

    // MARK: - Constraints
    @IBOutlet weak private var tableViewHeightConstraint: NSLayoutConstraint!
    
    // MARK: - Properties
    var data: ResistanceExerciseModel?
    var didImageClickedForExID: ((Exercise) -> Void)?
    var isViewMode: Bool = false
    
    var didAddNewKg: (() -> Void)?
    var didChangeKgIndex: ((Int) -> Void)?
    var showAlert: (() -> Void)?
    
    // MARK: - Init
    override func prepareForReuse() {
        super.prepareForReuse()
        tableViewHeightConstraint.constant = 600
        showAlert = nil
        
        didAddSet = nil
        didReduceSet = nil
        didDeleteMethod = nil
        didEditData = nil
        showRestTimePopUp = nil
        didAddNewKg = nil
        didChangeKgIndex = nil
        notesIsEditing = nil
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tableView.dataSource = self
    }
    
    func set(content: ResistanceExerciseModel) {
        data = content

        data?.supersetMethodData.sort(by: { $0.setNumber < $1.setNumber })
        tableView.reloadData()
        setHeightForCell(indexOfLastCell: content.exercisesForSuperset.count)
    }
  
    // MARK: - Private
    private func setHeightForCell(indexOfLastCell: Int) {
        guard let cellExpandable = tableView.cellForRow(at: IndexPath(row: 0, section: 1)),
            let cellExercise = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) else { return }
        tableViewHeightConstraint.constant = cellExpandable.frame.height + (cellExercise.frame.height * CGFloat(indexOfLastCell))
        layoutIfNeeded()
    }
    
    private func expandCell(expandableCell: ExpandableCellProtocol, isExpanded: Bool) {
        if isExpanded {
            expandableCell.contentView.isHidden = false
        }
        
        expandableCell.expandHeightAnchor.isActive.toggle()
        expandableCell.cellHeightAnchor.isActive.toggle()
  
        tableView.performBatchUpdates({
            expandableCell.layoutIfNeeded()
        }) { (_) in
            
            if !isExpanded {
                expandableCell.contentView.isHidden = true
            }
        }

        
        self.setHeightForCell(indexOfLastCell: self.data!.exercisesForSuperset.count)
        self.didExpandClicked?()
    }
}

// MARK: - UITableViewDataSource
extension ExerciseSupersetMainCell: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let _ = data else { return 0}
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            guard let data = data else { return 0 }
                   return data.exercisesForSuperset.count
        } else {
              return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(with: ExerciseSupersetEditCell.self, for: indexPath)
            guard let data = data else { return cell }
            cell.set(content: data.supersetMethodData, notes: data.supersetMethodData[0].notes, state: data.method, isExpanded: data.isExpanded)
            cell.showAlert = { [weak self] in self?.showAlert?() }
            cell.didAddSet = { [weak self] in self?.didAddSet?() }
            cell.didReduceSet = { [weak self] in self?.didReduceSet?() }
            cell.didDeleteMethod = { [weak self] in self?.didDeleteMethod?() }
            cell.didEditData = { [weak self]  in self?.didEditData?() }
            cell.showRestTimePopUp = { [weak self] index in self?.showRestTimePopUp?(index) }
            cell.didAddNewKg = { [weak self] in self?.didAddNewKg?() }
            cell.didChangeKgIndex = { [weak self] index in self?.didChangeKgIndex?(index) }
            cell.notesIsEditing = { [weak self] notes in self?.notesIsEditing?(notes) }
            cell.updateConstraints()
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(with: ExerciseSupersetCell.self, for: indexPath)
        cell.isViewMode = isViewMode
        guard let data = data else { return cell}
        let isFirst: Bool = (indexPath.row == 0)
        let isLast: Bool = (indexPath.row == (data.exercisesForSuperset.count) - 1)
        cell.set(isFirst: isFirst, isLast: isLast, content: data.exercisesForSuperset[indexPath.row], isExpanded: data.isExpanded )
        cell.didTapExpand = { [weak self] in
            guard let self = self else { return }
            self.data?.isExpanded.toggle()
            
            if let firstCell = tableView.cellForRow(at: IndexPath(row: 0, section: indexPath.section)) as? ExerciseSupersetCell {
                firstCell.rotateArrow(if: self.data?.isExpanded ?? true)
            }
            
            if let lastCell = tableView.cellForRow(at: IndexPath(row: (self.data?.exercisesForSuperset.count ?? 0) - 1, section: indexPath.section)) as? ExerciseSupersetCell {
                lastCell.containerView.corners = (lastCell.containerView.corners ==  .bottom) ? .none : .bottom
                lastCell.containerView.cornerRadius = 4
                if self.isViewMode {
                    lastCell.containerView.corners = .none
                    lastCell.containerView.cornerRadius = 0
                }
            }
            
            if let expandableCell = tableView.cellForRow(at: IndexPath(row: 0, section: 1)) as? ExerciseSupersetEditCell {
                self.expandCell(expandableCell: expandableCell, isExpanded: self.data?.isExpanded ?? true)
            }
        }
        cell.didTapImageView = { [weak self] in self?.didImageClickedForExID?(data.exercisesForSuperset[indexPath.row]) }
        cell.updateConstraints()
        return cell
    }
}
