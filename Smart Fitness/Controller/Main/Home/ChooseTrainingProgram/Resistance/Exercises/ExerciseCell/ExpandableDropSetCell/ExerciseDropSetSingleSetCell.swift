//
//  ExerciseDropSetSingleSetCell.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 13.12.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class ExerciseDropSetSingleSetCell: UITableViewCell {
    
    // MARK: - Views
    @IBOutlet weak private var setLabel: UILabel!
    @IBOutlet weak private var dropSetLabel: UILabel!
    @IBOutlet weak private var kgSetLabel: UILabel!
    @IBOutlet weak var restTimeButton: UIButton!
    @IBOutlet weak var contentStackView: UIStackView!
    
    // MARK: - Properties
    var showRestTimePopUp: (() -> Void)?
    var addSet: (() -> Void)?
    var reduceSet: (() -> Void)?
    var kgDidTap: (() -> Void)?
    
    // MARK: - Init
    override func awakeFromNib() {
        super.awakeFromNib()
        contentStackView.semanticContentAttribute = .forceLeftToRight
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        restTimeButton.isHidden = false
    }
    
    // MARK: - Actions
    @IBAction private func restTimeButtonAction(_ sender: Any) { showRestTimePopUp?() }
    
    @IBAction private func addButtonAction(_ sender: UIButton) { addSet?() }
    
    @IBAction private func reduceButtonAction(_ sender: UIButton) { reduceSet?() }
    
    @IBAction func kgDidTap(_ sender: Any) {
        kgDidTap?()
    }
    
    // MARK: - Method
    func set(content: Content) {
        setLabel.text = "\(content.set)"
        var textDropSet = ""
        content.dropSetValues.forEach({
            if $0 == 1000 {
                textDropSet += "\(Constants.ResistanceExercises.Methods().maxString) - "
            } else if  $0 == 999 {
                textDropSet += "\(Constants.ResistanceExercises.Methods().timeString) - "
            } else {
                textDropSet += "\($0) - "
            }
        })
        textDropSet.removeLast()
        textDropSet.removeLast()
        dropSetLabel.text = textDropSet
        
        var textKg = ""
        content.dropKgValues[content.indexOfKgValue].forEach({
            if $0 == 301 {
                textKg += "? - "
            } else {
                textKg += "\($0) - "
            }
        })
        
        textKg.removeLast()
        textKg.removeLast()
        kgSetLabel.text = textKg
        restTimeButton.setTitle("\(Constants.ResistanceExercises.Methods().restString) \(content.restTime)", for: .normal)
        contentStackView.layoutIfNeeded()
    }
}

// MARK: - Content
extension ExerciseDropSetSingleSetCell {
    struct Content: Codable {
        enum CodingKeys: String, CodingKey {
            case set
            case dropSetValues
            case dropKgValues
            case indexOfKgValue
            case restTime
            case id
        }
        
        var set: Int
        var dropSetValues: [Int]
        var dropKgValues: [[Double]]
        var indexOfKgValue: Int = 0
        var restTime: String
        var id: Int?
        
        init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            set = try container.decode(Int.self, forKey: .set)
            dropSetValues = try container.decode([Int].self, forKey: .dropSetValues)
            
            if let dropKgValues = try container.decode([[Double]]?.self, forKey: .dropKgValues) {
                self.dropKgValues = dropKgValues
            } else {
                self.dropKgValues = [[111]]
            }
            indexOfKgValue = try container.decode(Int.self, forKey: .indexOfKgValue)
            restTime = try container.decode(String.self, forKey: .restTime)
            id = try container.decodeIfPresent(Int.self, forKey: .id)
        }
        
        init(set: Int,
        dropSetValues: [Int],
        dropKgValues: [[Double]],
        restTime: String){
            self.set = set
            self.dropSetValues = dropSetValues
            self.dropKgValues = dropKgValues
            self.restTime = restTime
        }
    }
}
