//
//  ExerciseSupersetSetExerciseCell.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 12.12.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class ExerciseSupersetSetExerciseCell: UITableViewCell {
    
    // MARK: - Views
    @IBOutlet weak private var exerciseNumberLabel: UILabel!
    @IBOutlet weak private var repsLabel: UILabel!
    @IBOutlet weak private var kgLabel: UILabel!
    @IBOutlet weak private var valuesStackView: UIStackView!
    
    // MARK: - Properties
    var didValueTap: (() -> Void)?
    
    // MARK: - Init
    override func awakeFromNib() {
        super.awakeFromNib()
        guard let valuesStackView = valuesStackView else { return }
        valuesStackView.semanticContentAttribute = .forceLeftToRight
    }
    
    @IBAction func didValueTap(_ sender: Any) {
        didValueTap?()
    }
    
    // MARK: - Method
    func set(content: Content) {
        exerciseNumberLabel.text = "\(Constants.ResistanceExercises.Methods().exerciseString) \(content.exerciseName)"
        repsLabel.text = (content.repsValue == 1000) ? Constants.ResistanceExercises.Methods().maxString : "\(content.repsValue)"
        if content.repsValue == 999 {
                   repsLabel.text = Constants.ResistanceExercises.Methods().timeString
               }
        let kg: String = content.kgValues[content.indexOfKg] == 301 ? "?" : "\(content.kgValues[content.indexOfKg])"
        kgLabel.text = kg
    }
}

// MARK: - Content
extension ExerciseSupersetSetExerciseCell {
    struct Content: Codable {
        var exerciseName: String
        var repsValue: Int
        var kgValue: Double
        var kgValues: [Double]
        var indexOfKg: Int = 0
        var id: Int?
    }
}
