//
//  ExerciseDropSetMainCell.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 17.12.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class ExerciseDropSetMainCell: ParentExerciseCell, ExpandedCellProtocol {
   
    // MARK: - Views
    @IBOutlet weak var roundedView: RoundedView!
    @IBOutlet weak private var tableView: UITableView!
    @IBOutlet  override var topSeparator: UIView! {
        get { return super.topSeparator }
        set { super.topSeparator = newValue }
    }
    @IBOutlet weak private var exerciseImageView: UIImageView!
    @IBOutlet weak private var exerciseTitleLabel: UILabel!
    @IBOutlet  override var arrowButton: UIButton! {
        get { return super.arrowButton }
        set { super.arrowButton = newValue }
    }
    @IBOutlet weak var moveRowButton: UIButton!
    @IBOutlet weak var titleButton: UIButton!
    @IBOutlet weak private var textField: LocalizableTextField!
    @IBOutlet weak private var machinePickerView: CardioPickerView!
    @IBOutlet override var machineNumberButton: LoginButton! {
        get { return super.machineNumberButton }
        set { super.machineNumberButton = newValue }
    }
    @IBOutlet weak private var valuesStackView: UIStackView!
    @IBOutlet weak private var addReduceStackView: UIStackView!
    @IBOutlet weak private var bottomButtonsStackView: UIStackView!
    
    @IBOutlet weak var machineStackView: UIStackView!
    @IBOutlet weak private var addNewKgView: AddNewKg!
    
    @IBOutlet override var editButton: UIButton! {
        get { return super.editButton }
        set { super.editButton = newValue }
    }
    @IBOutlet weak var kgLabel: UILabel!
    
    @IBOutlet weak var notesLabel: UILabel!
    @IBOutlet weak var notesView: RoundedView!
    
    // MARK: - Constraints
    @IBOutlet weak var machinePickerHeightAnchor: NSLayoutConstraint!
    @IBOutlet override var expandableBottomConstraint: NSLayoutConstraint! {
        get { return super.expandableBottomConstraint }
        set { super.expandableBottomConstraint = newValue }
    }
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var valuesLeadingAnchor: NSLayoutConstraint!
    @IBOutlet weak var valuesTrailingAnchor: NSLayoutConstraint!
    
    @IBOutlet weak var addNewKgHeightAnchor: NSLayoutConstraint!
    
    @IBOutlet weak var kgContainerViewWidthAnchor: NSLayoutConstraint?
    
    // MARK: - Properties
    var addDropSet: ((Int) -> Void)?
    var reduceDropSet: ((Int) -> Void)?
    var didAddNewKg: (() -> Void)?
    var didChangeKgIndex: ((Int) -> Void)?
    private var data: [ExerciseDropSetSingleSetCell.Content] = []
    
    // MARK: - Init
    override func awakeFromNib() {
        super.awakeFromNib()
        tableView.dataSource = self
       
        valuesStackView.semanticContentAttribute = .forceLeftToRight
        let gesture = UITapGestureRecognizer(target: self, action: #selector(hideMachinePickerView))
        gesture.cancelsTouchesInView = false
        gesture.delegate = self
        addGestureRecognizer(gesture)
        switch LocalizationManager.Language.language {
        case .english:
            kgContainerViewWidthAnchor?.constant = 95
        case .hebrew:
            kgContainerViewWidthAnchor?.constant = 105
        }
        guard let addReduceStackView = addReduceStackView else { return }
        machinePickerView.delegate = self
        machinePickerView.dataSource = self
        textField?.delegate = self
        addReduceStackView.semanticContentAttribute = .forceLeftToRight
        bottomButtonsStackView.semanticContentAttribute = .forceLeftToRight
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        exerciseImageView.image = nil
    }

    // MARK: - Actions
    @IBAction private func didImageTap(_ sender: Any) { didImageClicked?() }
    
    @IBAction private func textFieldIsEditing(_ sender: UITextField) {
         editButton.isHidden = !sender.text!.isEmpty
             guard let notes = sender.text else { return }
             notesIsEditing?(notes)
    }
    
    @IBAction func expandButtonAction(_ sender: Any) { didExpandClicked?() }
    
    @IBAction private func deleteDidTap(_ sender: Any) { didDeleteMethod?() }
    
    @IBAction private func editDidTap(_ sender: Any) { didEditData?() }
    
    @IBAction func addSetButtonAction(_ sender: Any) { didAddSet?() }
    
    @IBAction func reduceSetButtonAction(_ sender: Any) { didReduceSet?() }

    @IBAction private func machineNumberDidTap(_ sender: Any) { expandPickerView(heightConstraint: machinePickerHeightAnchor) }
    
    @IBAction func addNewKgArrowDidTap(_ sender: Any) {
        expandKgContainerView()
    }
    
    
    // MARK: - Methods
    func set(content: Content, exercise: Exercise) {
        data = content.sets
        textField?.text = content.notes
        notesLabel?.text = content.notes
        notesView?.isHidden = content.notes.count == 0
        editButton?.isHidden = !(textField?.text!.isEmpty ?? true)
        if let addNewKgView = addNewKgView {
            addNewKgView.set(countAmountOfKg: content.sets[0].dropKgValues.count == 1 ? 0 : content.sets[0].dropKgValues.count)
            addNewKgView.didSelectRow = { [weak self] index in
                self?.didChangeKgIndex?(index)
                self?.addNewKgHeightAnchor.constant = 0
                UIView.animate(withDuration: 0.3) {
                    self?.contentView.layoutIfNeeded()
                }
            }
            addNewKgView.didAddNewKg = { [weak self] in
                self?.didAddNewKg?()
                self?.addNewKgHeightAnchor.constant = 0
                UIView.animate(withDuration: 0.3) {
                    self?.contentView.layoutIfNeeded()
                }
            }
        }
        tableViewHeightConstraint.constant =  CGFloat(data.count) * 91.5
        layoutIfNeeded()
        if exercise.isCustom() {
            exerciseImageView.image = "placeholder".image()
        } else {
            exerciseImageView.downloadImage(from: exercise.mainImageUrl)
        }
        exerciseTitleLabel.text = exercise.title
     
        isExpanded = exercise.isExpanded
         topSeparator.isHidden = !isExpanded
        rotateArrow(if: isExpanded)
        expandableBottomConstraint.isActive = isExpanded
        tableView.reloadData()
        heightForTableView()
        
        let machineNumber: String = (content.machineNumber == 100) ? "--" : "\(content.machineNumber)"
        machineNumberButton.setTitle(machineNumber, for: .normal)
        if content.machineNumber == 999 {
            machineStackView.isHidden = true
        } else {
            machineStackView.isHidden = false
        }
        kgLabel?.text = Constants.Circuit().kg + (content.sets[0].dropKgValues.count == 1 ? "" : "\(content.sets[0].indexOfKgValue + 1)")
        guard let machinePickerView = machinePickerView else { return }
        guard let index = machineNumberPickerData.lastIndex(of: machineNumber) else { return }
        machinePickerView.selectRow(index, inComponent: 0, animated: false)

    }

    // MARK: - Private
    @objc private func hideMachinePickerView() {
        textField?.resignFirstResponder()
        if let machinePickerHeightAnchor = machinePickerHeightAnchor {
            machinePickerHeightAnchor.constant =  0
        }
        
        if let addNewKgHeightAnchor = addNewKgHeightAnchor {
            addNewKgHeightAnchor.constant = 0
        }
        UIView.animate(withDuration: 0.3) { [weak self] in self?.layoutIfNeeded() }
    }
    
    private func heightForTableView() {
        var heights: [CGFloat] = []
        data.enumerated().forEach { (i, _) in
            heights.append(tableView.cellForRow(at: IndexPath(row: i, section: 0))?.bounds.height ?? 0)
        }
        let totalHeight = heights.reduce(0, +)
        let restTimeButtonHeight = data.count == 1 ? 0 : 30
               
        tableViewHeightConstraint.constant =  totalHeight - CGFloat(restTimeButtonHeight)
        layoutIfNeeded()
    }
    
    private func expandKgContainerView() {
        guard let kgAnchor = addNewKgHeightAnchor else { return }
        kgAnchor.constant = kgAnchor.constant == 0 ? 137 : 0
        UIView.animate(withDuration: 0.3) { [weak self] in
            self?.contentView.layoutIfNeeded()
        }
    }
}

// MARK: - Content
extension ExerciseDropSetMainCell {
    struct Content: Codable {
        var sets: [ExerciseDropSetSingleSetCell.Content]
        var machineNumber: Int
        var notes: String
        var id: Int?
    }
}

// MARK: - UITableViewDataSource
extension ExerciseDropSetMainCell: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(with: ExerciseDropSetSingleSetCell.self, for: indexPath)
        cell.set(content: data[indexPath.row])
        if indexPath.row == data.count - 1 && data.count != 1 {
            cell.restTimeButton.isHidden = true
        }
        cell.showRestTimePopUp = { [weak self] in self?.showRestTimePopUp?(indexPath.row) }
        cell.addSet = { [weak self] in self?.addDropSet?(indexPath.row) }
        cell.reduceSet = { [weak self] in self?.reduceDropSet?(indexPath.row) }
        cell.kgDidTap = { [weak self] in self?.didEditData?() }
        return cell
    }
    
    override func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view is UIStackView || touch.view is UITableViewCell || touch.view is UIButton  {
            return false
        }
        return true
    }
}
