//
//  ExerciseSupersetEditCell.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 10.12.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class ExercisePercentMainCell: ParentExerciseCell, ExpandedCellProtocol {
    
    // MARK: - Views
    @IBOutlet weak var roundedView: RoundedView!
    @IBOutlet weak private var exerciseImageView: UIImageView!
    @IBOutlet weak private var exerciseTitleLabel: UILabel!
    @IBOutlet  override var topSeparator: UIView! {
        get { return super.topSeparator }
        set { super.topSeparator = newValue }
    }

    @IBOutlet  override var arrowButton: UIButton! {
        get { return super.arrowButton }
        set { super.arrowButton = newValue }
    }
    
    @IBOutlet override var editButton: UIButton! {
        get { return super.editButton }
        set { super.editButton = newValue }
    }
    
    @IBOutlet weak var moveRowButton: UIButton!
    @IBOutlet weak var titleButton: UIButton!
    @IBOutlet weak private var tableView: UITableView!
    @IBOutlet weak private var textField: UITextField!
    @IBOutlet weak private var machinePickerView: CardioPickerView!
    @IBOutlet override var machineNumberButton: LoginButton! {
        get { return super.machineNumberButton }
        set { super.machineNumberButton = newValue }
    }
    @IBOutlet weak private var percentageStackView: UIStackView!
    @IBOutlet weak private var repsLabel: UILabel!
    @IBOutlet weak private var valuesStackView: UIStackView!
    @IBOutlet weak private var addReduceStackView: UIStackView!
    @IBOutlet weak private var bottomButtonsStackView: UIStackView!
    @IBOutlet weak private var machineStackView: UIStackView!
    @IBOutlet weak private var addNewKgView: AddNewKg!
    @IBOutlet weak private var kgLabel: UILabel!
    @IBOutlet weak private var notesLabel: UILabel!
    @IBOutlet weak var notesView: RoundedView!
    
    // MARK: - Constraints
    @IBOutlet override var expandableBottomConstraint: NSLayoutConstraint! {
        get { return super.expandableBottomConstraint }
        set { super.expandableBottomConstraint = newValue }
    }
    @IBOutlet weak var topStackViewLeadingAnchor: NSLayoutConstraint!
    @IBOutlet weak var topStackViewTrailingAnchor: NSLayoutConstraint!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var machinePickerHeightAnchor: NSLayoutConstraint!
    
    @IBOutlet weak var addNewKgHeightAnchor: NSLayoutConstraint!
    @IBOutlet weak var kgContainerViewWidthAnchor: NSLayoutConstraint?
    
    // MARK: - Properties
    private var data: [ExercisePercentSetCell.Content] = []
    var didChangeKgIndex: ((Int) -> Void)?
    var didAddNewKg: (() -> Void)?
    
    
    // MARK: - Init
    override func awakeFromNib() {
        super.awakeFromNib()
        tableView?.dataSource = self
        tableView.semanticContentAttribute = .forceLeftToRight
        valuesStackView.semanticContentAttribute = .forceLeftToRight
        let gesture = UITapGestureRecognizer(target: self, action: #selector(hideMachinePickerView))
                gesture.cancelsTouchesInView = false
                gesture.delegate = self
                addGestureRecognizer(gesture)
        switch LocalizationManager.Language.language {
        case .english:
            kgContainerViewWidthAnchor?.constant = 95
        case .hebrew:
            kgContainerViewWidthAnchor?.constant = 110
        }
        guard let machinePickerView = machinePickerView else { return }
        textField.delegate = self
        machinePickerView.delegate = self
        machinePickerView.dataSource = self
        addReduceStackView.semanticContentAttribute = .forceLeftToRight
        bottomButtonsStackView.semanticContentAttribute = .forceLeftToRight
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        exerciseImageView.image = nil
    }
    
    // MARk: - Action
    @IBAction private func didImageTap(_ sender: Any) { didImageClicked?() }
    
    @IBAction private func textFieldIsEditing(_ sender: UITextField) {
        editButton.isHidden = !sender.text!.isEmpty
        guard let notes = sender.text else { return }
        notesIsEditing?(notes)
    }
    
    @IBAction private func expandButtonAction(_ sender: Any) { didExpandClicked?() }
    
    @IBAction private func addSetButtonAction(_ sender: Any) { didAddSet?() }
    
    @IBAction private func reduceSetButtonAction(_ sender: Any) { didReduceSet?() }
    
    @IBAction private func deleteDidTap(_ sender: Any) { didDeleteMethod?() }
    
    @IBAction private func editDataButtonAction(_ sender: Any) { didEditData?() }
    
    @IBAction private func machineNumberDidTap(_ sender: Any) { expandPickerView(heightConstraint: machinePickerHeightAnchor) }
    
    @IBAction private func newKgArrowDidTap(_ sender: Any) {
          expandKgContainerView()
    }
    
    @objc private func hideMachinePickerView() {
        textField?.resignFirstResponder()
        if let machinePickerHeightAnchor = machinePickerHeightAnchor {
            machinePickerHeightAnchor.constant =  0
        }
        
        if let addNewKgHeightAnchor = addNewKgHeightAnchor {
            addNewKgHeightAnchor.constant = 0
        }
        UIView.animate(withDuration: 0.3) { [weak self] in self?.layoutIfNeeded() }
    }

    // MARK: - Methods
    func set(content: Content, state: DropSetMethodViewController.MethodType, exercise: Exercise) {
        data = content.sets
        textField?.text = content.notes
        notesLabel?.text = content.notes
        notesView?.isHidden = content.notes.count == 0
        editButton?.isHidden = !(textField?.text!.isEmpty ?? false)
        if let addNewKgView = addNewKgView {
            addNewKgView.set(countAmountOfKg: content.sets[0].kg.count == 1 ? 0 : content.sets[0].kg.count)
            addNewKgView.didSelectRow = { [weak self] index in
                self?.didChangeKgIndex?(index)
                self?.addNewKgHeightAnchor.constant = 0
                UIView.animate(withDuration: 0.3) {
                    self?.contentView.layoutIfNeeded()
                }
            }
            addNewKgView.didAddNewKg = { [weak self] in
                self?.didAddNewKg?()
                self?.addNewKgHeightAnchor.constant = 0
                UIView.animate(withDuration: 0.3) {
                    self?.contentView.layoutIfNeeded()
                }
            }
        }
        method = state
        switch state {
        case .pyramid:
            repsLabel.text = Constants.ResistanceExercises.Methods().repsWithNoPointsString
            percentageStackView.isHidden = true
            topStackViewLeadingAnchor.constant = 18
            topStackViewTrailingAnchor.constant = 31
        case .percentOfRM:
            repsLabel.text = Constants.ResistanceExercises.Methods().rmString
            topStackViewTrailingAnchor.constant = 16
            topStackViewLeadingAnchor.constant = 8
            percentageStackView.isHidden = false
        default:
            break
        }
           if exercise.isCustom() {
                 exerciseImageView.image = "placeholder".image()
             } else {
            exerciseImageView.downloadImage(from: exercise.mainImageUrl)
             }
        exerciseTitleLabel.text = exercise.title
        isExpanded = exercise.isExpanded
        topSeparator.isHidden = !isExpanded
        rotateArrow(if: isExpanded)
        expandableBottomConstraint.isActive = isExpanded
        let machineNumber: String = (content.machineNumber == 100) ? "--" : "\(content.machineNumber)"
        machineNumberButton.setTitle(machineNumber, for: .normal)
        kgLabel?.text = Constants.Circuit().kg + (content.sets[0].kg.count == 1 ? "" : "\(content.sets[0].indexOfKg + 1)")
        if content.machineNumber == 999 {
            machineStackView.isHidden = true
        } else {
            machineStackView.isHidden = false
        }
        setCellHeight()
        tableView.reloadData()
        
        //
        guard let machinePickerView = machinePickerView else { return }
        guard let index = machineNumberPickerData.lastIndex(of: machineNumber) else { return }
        machinePickerView.selectRow(index, inComponent: 0, animated: false)
    }
    
    // MARK: - Private
    private func setCellHeight() {
        guard data.count > 0 else {
            tableViewHeightConstraint.constant = 0
            layoutIfNeeded()
            return
        }
        let restTimeButtonHeight = data.count == 1 ? 0 : 30
        tableViewHeightConstraint.constant = (CGFloat(data.count) * 66) - CGFloat(restTimeButtonHeight)
        layoutIfNeeded()
    }
    
    private func setValues(from value: String, pickerView: UIPickerView, button: UIButton, data: [String]) {
        guard value != "" else { return }
        if let index = data.lastIndex(where: { $0 == value } ) {
            pickerView.selectRow(index, inComponent: 0, animated: false)
            button.setTitle(data[index], for: .normal)
        }
    }
    
    private func expandKgContainerView() {
           guard let kgAnchor = addNewKgHeightAnchor else { return }
           kgAnchor.constant = kgAnchor.constant == 0 ? 137 : 0
           UIView.animate(withDuration: 0.3) { [weak self] in
               self?.contentView.layoutIfNeeded()
           }
       }
}

// MARK: - Content
extension ExercisePercentMainCell {
    struct Content: Codable {
        var sets: [ExercisePercentSetCell.Content]
        var machineNumber: Int
        var notes: String
        var id: Int?
    }
}

// MARK: - UITableViewDataSource
extension ExercisePercentMainCell: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(with: ExercisePercentSetCell.self, for: indexPath)
        cell.set(content: data[indexPath.row], method: method )
        if indexPath.row == data.count - 1 && data.count != 1 {
            cell.restTimeButton.isHidden = true
        }
        cell.showRestTimePopUp = { [weak self] in self?.showRestTimePopUp?(indexPath.row) }
        cell.kgDidTap = { [weak self] in self?.didEditData?() }
        return cell
    }
}

extension ExercisePercentMainCell {
    override func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view is UIStackView || touch.view is UITableViewCell || touch.view is UIButton {
               return false
           }
           return true
       }
}
