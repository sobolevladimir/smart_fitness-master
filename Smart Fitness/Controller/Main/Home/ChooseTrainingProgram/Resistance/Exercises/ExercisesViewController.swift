//
//  ExercisesViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 31.10.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class ExercisesViewController: BaseViewController {
    
    // MARK: - Views
    @IBOutlet weak var titlesStackView: UIStackView!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var workoutMethodsButton: UIView!
    @IBOutlet weak var addExerciseButton: UIView!
    @IBOutlet weak var addNewExercisesButton: UIView!
    @IBOutlet weak var bottomButtonsStackView: UIStackView!
    
    @IBOutlet weak var stackTopConstraint: NSLayoutConstraint!
    @IBOutlet var stackBottomConstraint: NSLayoutConstraint!
    
    // MARK: - Properties
    var isUIUpdated: Bool = false
    var backgroundImageName: String!
    var groupKey: GroupsData.MuscleGroupKey!
    var data: [ResistanceExerciseModel] = [] {
        didSet {
            setupUI()
        }
    }
    
    var didTapBack: (([ResistanceExerciseModel]) -> Void)?
    private var cellHeights: [IndexPath: CGFloat?] = [:]
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        backgroundImageView.image = UIImage(named: backgroundImageName)
        tableView.reorder.delegate = self
        tableView.contentInset = UIEdgeInsets(top: 4, left: 0, bottom: 216, right: 0)
        tableView.tableFooterView = UIView()
        bottomButtonsStackView.semanticContentAttribute = .forceLeftToRight
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        fixDirection()
        tableView.reorder.tableView?.layoutIfNeeded()
        if !isUIUpdated {
            setupUI()
            view.layoutIfNeeded()
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        isUIUpdated = true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if !isUIUpdated {
            setupUI()
            view.layoutIfNeeded()
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        data.enumerated().forEach({ index, item in
            if item.exercise.isExpanded {
                data[index].exercise.isExpanded = false
            }
            data[index].isExpanded = false
        })
        isUIUpdated = false
    }
    
    // MARK: - Action
    @IBAction private func viewDidTap(_ sender: Any) {
        view.endEditing(true)
    }
    
    @IBAction private func backTapped(_ sender: Any) {
        data.enumerated().forEach({ index, item in
            if item.exercise.isExpanded {
                data[index].exercise.isExpanded = false
            }
            data[index].isExpanded = false
        })
        
        didTapBack?(data)
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction private func addNewExercises(_ sender: Any) {
        let vc = AddNewExercisesViewController.instantiate()
        vc.groupKey = groupKey
        navigationController?.pushViewController(vc, animated: true)
        vc.didAddExerciseFromSearch = { [weak self] item in
            self?.data.append(.init(exercise: item, method: .none, id: nil))
            self?.tableView.reloadData()
        }
        
        vc.didAddSelectedExercises = { [weak self] items in
            let exItems = items.map({  ResistanceExerciseModel(exercise: $0, method: .none, id: nil)})
            self?.data += exItems
            self?.tableView.reloadData()
        }
    }
    
    @IBAction private func workoutMethodsDidTap(_ sender: Any) {
        let vc = WorkoutMethodsViewController.instantiate()
        vc.isSubscriber = UserDefaults.standard.bool(forKey: Constants.isSubscribedKey)
        self.present(vc, animated: false, completion: nil)
        vc.methodIsChosen = { [weak self] method in
            switch method {
            case .superset, .dropSet, .pyramid, .percentOfRM:
                guard let methodVC: DropSetMethodViewController = self?.makeSupersetScreen(method: method) else { return }
                methodVC.didFinishClicked = { [weak self] items in
                    guard let self = self else { return }
                    guard let index = self.data.lastIndex(where: { $0.exercise.privateId == items.first?.privateId }) else { return }
                    self.data[index] = self.createMethodSection(from: items, for: method, index: index)
                    if method == .superset {
                        var ex = self.data[index].exercisesForSuperset
                        ex.removeFirst()
                        ex.forEach { exercise in
                            guard let index = self.data.lastIndex(where: {
                                $0.exercise.privateId == exercise.privateId
                                
                            }) else { return }
                            self.data.remove(at: index)
                        }
                    }
                    self.tableView.reloadData()
                    switch method {
                    case .dropSet:
                        self.showAlert(text: Constants.ResistanceExercises.Messages().dropSetCreated)
                    case .pyramid:
                        self.showAlert(text: Constants.ResistanceExercises.Messages().pyramidCreated)
                    case .percentOfRM:
                        self.showAlert(text: Constants.ResistanceExercises.Messages().percentageCreated)
                    default: break
                    }
                }
            case .supersetDifferent:
                guard let methodVC: SupersetMethodViewController = self?.makeSupersetScreen(method: .supersetDifferent) else { return }
                methodVC.didFinishClicked = { [weak self] items in
                    guard let self = self else { return }
                    guard let index = self.data.lastIndex(where: { $0.exercise.privateId == items.first?.privateId }) else { return }
                    self.data[index] = self.createMethodSection(from: items, for: method, index: index)
                    
                    self.tableView.performBatchUpdates({
                        self.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
                    }) { (_) in
                        self.tableView.performBatchUpdates({
                            self.tableView.layoutIfNeeded()
                        })
                    }
                }
            default:
                break
            }
        }
    }
    
    // MARK: - Private
    private func setupUI() {
        guard titlesStackView != nil else { return }
        titlesStackView.isHidden = !data.isEmpty
        workoutMethodsButton.isHidden = data.isEmpty
        stackTopConstraint.isActive = data.isEmpty
        stackBottomConstraint.isActive = !data.isEmpty
        addExerciseButton.isHidden = data.isEmpty
        addNewExercisesButton.isHidden = !data.isEmpty
    //    workoutMethodsButton.layoutIfNeeded()
        if data.isEmpty {
            navigationItem.rightBarButtonItem = nil
            fixDirection()
        } else {
               navigationItem.leftBarButtonItem?.image = nil
            navigationItem.rightBarButtonItem = .init(title: Constants.ProgramInfo().done, style: .done, target: self, action: #selector(backTapped))
        }
        self.setBarItemsFont()
    }
    
    private func makeDefaultDropSetData(item: Exercise) -> ExerciseDropSetMainCell.Content {
        var data: [ExerciseDropSetSingleSetCell.Content] = []
        for i in 0..<3 {
            data.append(.init(set: i + 1,
                              dropSetValues: Constants.ResistanceExercises.Methods.dropSetRepsDefault,
                              dropKgValues: [Constants.ResistanceExercises.Methods.dropSetKGDefault],
                              restTime: Constants.ResistanceExercises.Methods.restTimeDefault))
        }
        let machineNumber: Int
        if item.category == .machines || item.category == .pulley {
            machineNumber = item.machineNumber
        } else {
            machineNumber = 999
        }
        return .init(sets: data, machineNumber: machineNumber, notes: "")
    }
    
    private func makeDefaultSupersetData(items: [Exercise]) -> [ExerciseSupersetSetCell.Content] {
        let exercises = items.map({ _ in ExerciseSupersetSetExerciseCell.Content(exerciseName: "",
                                                                                 repsValue: Constants.ResistanceExercises.Methods.supersetDefaultReps,
                                                                                 kgValue: Constants.ResistanceExercises.Methods.supersetDefaultKg, kgValues: [Constants.ResistanceExercises.Methods.supersetDefaultKg]) })
        let time = Constants.ResistanceExercises.Methods.restTimeDefault
        
        var data: [ExerciseSupersetSetCell.Content] = []
        for i in 0..<3 {
            data.append(.init(exercises: exercises, restTime: time, setNumber: i + 1))
        }
        
        return data
    }
    
    private func makeDefaultPercentData(item: Exercise) -> ExercisePercentMainCell.Content {
        var data: [ExercisePercentSetCell.Content] = []
        let kg = Constants.ResistanceExercises.Methods.percentKgDefaultValue
        let rm = Constants.ResistanceExercises.Methods.percentRMDefaultValue
        let percent = Constants.ResistanceExercises.Methods.percentValue
        let time = Constants.ResistanceExercises.Methods.restTimeDefault
        
        data.append(.init(set: 1, rm: rm - 2, kg: [kg + 5.0], percent: percent + 15, restTime: time, repsTime: ""))
        data.append(.init(set: 2, rm: rm, kg: [kg], percent: percent, restTime: time, repsTime: ""))
        data.append(.init(set: 3, rm: rm + 2, kg: [kg - 5.0], percent: percent - 35, restTime: time, repsTime: ""))
        let machineNumber: Int
        if item.category == .machines || item.category == .pulley {
            machineNumber = item.machineNumber
        } else {
            machineNumber = 999
        }
        
        return ExercisePercentMainCell.Content(sets: data, machineNumber: machineNumber, notes: "")
    }
    
    private func  makeDefaultPyramidData(item: Exercise) -> ExercisePercentMainCell.Content {
        var data: [ExercisePercentSetCell.Content] = []
        let kg = [Constants.ResistanceExercises.Methods.supersetDefaultKg]
        let time = Constants.ResistanceExercises.Methods.restTimeDefault
        let reps = Constants.ResistanceExercises.Methods.pyramidRepsDefaultValue
        data.append(.init(set: 1, rm: reps + 4, kg: kg, percent: 0, restTime: time, repsTime: ""))
        data.append(.init(set: 2, rm: reps + 2, kg: kg, percent: 0, restTime: time, repsTime: ""))
        data.append(.init(set: 3, rm: reps, kg: kg, percent: 0, restTime: time, repsTime: ""))
        
        let machineNumber: Int
        if item.category == .machines || item.category == .pulley {
            machineNumber = item.machineNumber
        } else {
            machineNumber = 999
        }
        return ExercisePercentMainCell.Content(sets: data, machineNumber: machineNumber, notes: "")
    }
    
    private func createMethodSection(from items: [Exercise], for method: DropSetMethodViewController.MethodType, index: Int) -> ResistanceExerciseModel {
        switch method {
        case .superset, .supersetDifferent:
            let supersetMethodData = makeDefaultSupersetData(items: items)
            let exercisesForSuperset = items 
            return .init(exercise: self.data[index].exercise, exercisesForSuperset: exercisesForSuperset, method: method, supersetMethodData: supersetMethodData)
        case .dropSet:
            let dropSetMethodData = makeDefaultDropSetData(item: items[0])
            return .init(exercise: self.data[index].exercise, method: method, dropSetMethodData: dropSetMethodData)
        case .pyramid:
            let pyramid = makeDefaultPyramidData(item: items[0])
            return .init(exercise: self.data[index].exercise, method: method, percentMethodData: pyramid)
        case .percentOfRM:
            let percentMethodData = makeDefaultPercentData(item: items[0])
            return .init(exercise: self.data[index].exercise, method: method, percentMethodData: percentMethodData)
        default:
            return .init(exercise: self.data[index].exercise, method: method)
        }
    }
    
    private func makeSupersetScreen<T: UIViewController>(method: DropSetMethodViewController.MethodType) -> T? {
        var vc: UIViewController!
        let itemsWithoutMethods = data.filter({ $0.method == .none })
        let superSetData = itemsWithoutMethods.map({ $0.exercise })
        
        if  itemsWithoutMethods.count < 1 {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
                let popUpVC = ComingSoonViewController.instantiate()
                popUpVC.state = .noExercises
                self?.present(popUpVC, animated: false, completion: nil)
               
            }
           return nil
        }
        
        if (method == .superset && itemsWithoutMethods.count < 2)  {
                  DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
                      let popUpVC = ComingSoonViewController.instantiate()
                      popUpVC.state = .twoExercises
                      self?.present(popUpVC, animated: false, completion: nil)
                     
                  }
                 return nil
              }
        
        if  method == .supersetDifferent {
            let differentMuscleVC = SupersetMethodViewController.instantiate()
            differentMuscleVC.backgroundImageName = self.backgroundImageName
            differentMuscleVC.data = [superSetData]
            vc = differentMuscleVC
        } else {
            let supersetVC = DropSetMethodViewController.instantiate()
            supersetVC.method = method
            supersetVC.data = [superSetData]
            supersetVC.backgroundImageName = backgroundImageName
            vc = supersetVC
        }
        navigationController?.pushViewController(vc, animated: false)
        return vc as? T
    }
    
    private func showExerciseInfo(exercise: Exercise) {
        guard !exercise.isCustom() else { return }
        let vc = ExerciseDetailsViewController.instantiate()
        vc.exercise = exercise
        navigationController?.pushViewController(vc, animated: true)
    }
    
    private func addLongPressGesture() -> UILongPressGestureRecognizer {
        let reg = UILongPressGestureRecognizer(target: tableView.reorder, action: #selector(tableView.reorder.handleReorderGesture))
        reg.minimumPressDuration = 0.2
        reg.delegate = tableView.reorder
        reg.cancelsTouchesInView = false
        return reg
    }
    
    private func didExpandClicked(cell: ParentExerciseCell, indexPath: IndexPath) {
        data[indexPath.row].exercise.isExpanded.toggle()
        tableView.beginUpdates()
        cell.selectRow()
        tableView.endUpdates()
        tableView.reorder.tableView?.layoutIfNeeded()
        view.endEditing(true)
    }
    
    private func deleteExercise(indexPath: IndexPath) {
        let popUpVc = LogoutViewController.instantiate()
        popUpVc.state = .deleteExercise
        popUpVc.didDelete = { [weak self] in
            self?.data.remove(at: indexPath.row)
            self?.tableView.performBatchUpdates({
                self?.tableView.deleteRows(at: [indexPath], with: .automatic)
            }) { [weak self] (_) in
                self?.tableView.reloadData()
                
                self?.showAlert(text: Constants.ResistanceExercises.Messages().exerciseDelete)
            }
        }
        present(popUpVc, animated: false, completion: nil)
    }
    
    private func setNoneMethodCellActions(cell: ExerciseCell, indexPath: IndexPath) {
        cell.moveRowButton.addGestureRecognizer(addLongPressGesture())
        cell.didDelete = { [weak self] in
            self?.deleteExercise(indexPath: indexPath)
        }
        
        cell.didValuesClicked = { [weak self] in
            guard let self = self else { return }
            let popUpVC = EditNormalSetViewController.instantiate()
            popUpVC.item = self.data[indexPath.row].exercise
            self.present(popUpVC, animated: false, completion: nil)
            popUpVC.saveDidTap = { editedItem in
                self.data[indexPath.row].exercise  = editedItem
                cell.set(content: editedItem)
            }
        }
        
        cell.didRestClicked = { [weak self] in
            guard let self = self else { return }
            let popUpVC = EditRestTimeViewController.instantiate()
            popUpVC.time = self.data[indexPath.row].exercise.restTime
            self.present(popUpVC, animated: false, completion: nil)
            popUpVC.saveDidTap = { editedTime in
                self.data[indexPath.row].exercise.restTime = editedTime
                cell.setRest(with: editedTime)
            }
        }
        
        cell.didMachineNumberSelected = { [weak self] number in
            guard let self = self else { return }
            self.data[indexPath.row].exercise.machineNumber = number
        }
        
        cell.willEditTextField = { [weak self] in
            self?.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
        
        cell.didExpandClicked = { [weak self]  in
            self?.didExpandClicked(cell: cell, indexPath: indexPath)
        }
        
        cell.didImageClicked = { [weak self] in
            guard let self = self else { return }
            self.showExerciseInfo(exercise: self.data[indexPath.row].exercise)
        }
        cell.notesIsEditing = { [weak self] notes in
            self?.data[indexPath.row].exercise.notes = notes
        }
    }
    
    private func editRestTime(indexPath: IndexPath, method: DropSetMethodViewController.MethodType, index: Int) {
        let popUpVC = EditRestTimeViewController.instantiate()
        switch method {
        case .superset, .supersetDifferent:
            popUpVC.time = data[indexPath.row].supersetMethodData[index].restTime
        case .dropSet:
            popUpVC.time = data[indexPath.row].dropSetMethodData?.sets[index].restTime ?? ""
        case .pyramid, .percentOfRM:
            popUpVC.time = self.data[indexPath.row].percentMethodData?.sets[index].restTime ?? ""
        case .none:
            popUpVC.time = data[indexPath.row].exercise.restTime
        }
        self.present(popUpVC, animated: false, completion: nil)
        popUpVC.saveDidTap = { [weak self] editedTime in
            switch method {
            case .superset, .supersetDifferent:
                self?.data[indexPath.row].supersetMethodData[index].restTime = editedTime
            case .dropSet:
                self?.data[indexPath.row].dropSetMethodData?.sets[index].restTime = editedTime
            case .pyramid, .percentOfRM:
                self?.data[indexPath.row].percentMethodData?.sets[index].restTime = editedTime
            case .none:
                self?.data[indexPath.row].exercise.restTime = editedTime
            }
            self?.tableView.reloadData()
        }
    }
    
    private func updateRow(for indexPath: IndexPath) {
        if #available(iOS 13, *) {
            tableView.reloadRows(at: [indexPath], with: .fade)
        } else {
            tableView.reloadData()
        }
    }
}

// MARK: - UITableViewDataSource
extension ExercisesViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let spacer = tableView.reorder.spacerCell(for: indexPath) { return spacer }
        let method = data[indexPath.row].method
        switch method {
            
        // MARK: - Superset
        case .supersetDifferent, .superset:
            let cell = tableView.dequeueReusableCell(with: ExerciseSupersetMainCell.self, for: indexPath)
            cell.set(content: data[indexPath.row])
            (cell.tableView.cellForRow(at: IndexPath(item: 0, section: 0)) as? ExerciseSupersetCell)?.moveButton.addGestureRecognizer(addLongPressGesture())
            cell.didExpandClicked = { [weak self] in
                self?.data[indexPath.row].isExpanded.toggle()
                tableView.beginUpdates()
                tableView.layoutIfNeeded()
                tableView.endUpdates()
                self?.view.endEditing(true)
            }
            cell.showAlert = { [weak self] in self?.showAlert(text: "\(Constants.ResistanceExercises.Messages().minimumIsOneSet)")}
            cell.notesIsEditing = { [weak self] notes in
                self?.data[indexPath.row].supersetMethodData[0].notes = notes
            }
            cell.didImageClickedForExID = { [weak self] exercise in self?.showExerciseInfo(exercise: exercise) }
            cell.didAddSet = { [weak self] in
                guard let self = self else { return }
                let exercisesAmount = self.data[indexPath.row].exercisesForSuperset.count
                var exercises: [ExerciseSupersetSetExerciseCell.Content] = []
                for _ in 0..<exercisesAmount {
                    exercises.append(.init(exerciseName: "", repsValue: Constants.ResistanceExercises.Methods.supersetDefaultReps, kgValue: Constants.ResistanceExercises.Methods.supersetDefaultKg, kgValues: [Constants.ResistanceExercises.Methods.supersetDefaultKg]))
                }
                self.data[indexPath.row].supersetMethodData.append(ExerciseSupersetSetCell.Content(exercises: exercises, restTime: Constants.ResistanceExercises.Methods.restTimeDefault, setNumber: self.data[indexPath.row].supersetMethodData.count + 1))
                cell.set(content: self.data[indexPath.row])
                tableView.beginUpdates()
                UIView.animate(withDuration: 0.3) {
                    tableView.layoutIfNeeded()
                }
                tableView.endUpdates()
            }
            
            cell.didReduceSet = { [weak self] in
                guard let self = self else { return }
                guard self.data[indexPath.row].supersetMethodData.count > 1 else {
                    self.showAlert(text: Constants.ResistanceExercises.Messages().minimumIsOneSet)
                    return
                }
                self.data[indexPath.row].supersetMethodData.removeLast()
                
                cell.set(content: self.data[indexPath.row])
                tableView.beginUpdates()
                UIView.animate(withDuration: 0.3) {
                    tableView.layoutIfNeeded()
                }
                tableView.endUpdates()
            }
            
            cell.didDeleteMethod = { [weak self] in
                guard let self = self else { return }
                if method == .supersetDifferent {
                    self.data.remove(at: indexPath.row)
                    tableView.reloadData()
                } else {
                    let popUpVC = LogoutViewController.instantiate()
                    popUpVC.state = .unlinkExercises
                    self.present(popUpVC, animated: false, completion: nil)
                    popUpVC.didDelete = { [weak self] in
                        guard let self = self else { return }
                        self.data[indexPath.row].method = .none
                        self.data[indexPath.row].exercisesForSuperset.removeFirst()
                        self.data[indexPath.row].exercisesForSuperset.forEach({
                            var exercise = $0
                            exercise.isSelected = false
                            self.data.insert(.init(exercise: exercise, method: .none), at: indexPath.row + 1)
                        })
                        self.data[indexPath.row].exercisesForSuperset = []
                        tableView.reloadData()
                    }
                }
            }
            
            cell.didEditData = { [weak self]  in
                guard let self = self else { return }
                let editPopUp = EditDropSetViewController.instantiate()
                editPopUp.method = .superSet
                let data = self.data[indexPath.row].supersetMethodData.map({ ExerciseSupersetSetCell.Content(exercises: $0.exercises.map({
                    ExerciseSupersetSetExerciseCell.Content(exerciseName: $0.exerciseName, repsValue: $0.repsValue, kgValue: $0.kgValues[$0.indexOfKg], kgValues: $0.kgValues, indexOfKg: $0.indexOfKg)
                }), restTime: $0.restTime, setNumber: $0.setNumber) })
                editPopUp.data = data
                self.present(editPopUp, animated: false, completion: nil)
                editPopUp.saveDidTap = { [weak self] sets in
                    guard let self = self else { return }
                    let updatedSets = sets.map({ ExerciseSupersetSetCell.Content(exercises: $0.exercises.map({
                        var kgs = $0.kgValues
                        kgs[$0.indexOfKg] = $0.kgValue
                        return ExerciseSupersetSetExerciseCell.Content(exerciseName: $0.exerciseName, repsValue: $0.repsValue, kgValue: $0.kgValues[$0.indexOfKg], kgValues: kgs, indexOfKg: $0.indexOfKg)
                    }), restTime: $0.restTime, setNumber: $0.setNumber) })
                    self.data[indexPath.row].supersetMethodData = updatedSets
                    tableView.reloadData()
                }
            }
            
            cell.showRestTimePopUp = { [weak self] index in
                self?.editRestTime(indexPath: indexPath, method: method, index: index)
            }
            
            return cell
            
        // MARK: - None, simple exercise
        case .none:
            let cell = tableView.dequeueReusableCell(with: ExerciseCell.self, for: indexPath)
            cell.set(content: data[indexPath.row].exercise)
            setNoneMethodCellActions(cell: cell, indexPath: indexPath)
            return cell
        // MARK: - DropSet Method Cell
        case .dropSet:
            let cell = tableView.dequeueReusableCell(with: ExerciseDropSetMainCell.self, for: indexPath)
            guard let dropData = data[indexPath.row].dropSetMethodData else { return cell }
            // move gesture
            cell.moveRowButton.addGestureRecognizer(addLongPressGesture())
            cell.set(content: .init(sets: dropData.sets, machineNumber: dropData.machineNumber, notes: dropData.notes), exercise: data[indexPath.row].exercise)
            
            cell.didImageClicked = { [weak self] in
                guard let self = self else { return }
                self.showExerciseInfo(exercise: self.data[indexPath.row].exercise)
            }
            
            cell.notesIsEditing = { [weak self] notes in
                self?.data[indexPath.row].dropSetMethodData?.notes = notes
            }
            
            cell.didExpandClicked = { [weak self] in self?.didExpandClicked(cell: cell, indexPath: indexPath) }
            
            cell.didAddSet = { [weak self] in
                guard let self = self else { return }
                self.data[indexPath.row].dropSetMethodData?.sets.append(.init(set: (self.data[indexPath.row].dropSetMethodData?.sets.count ?? 0) + 1, dropSetValues: Constants.ResistanceExercises.Methods.dropSetRepsDefault, dropKgValues: [Constants.ResistanceExercises.Methods.dropSetKGDefault], restTime: Constants.ResistanceExercises.Methods.restTimeDefault))
                self.updateRow(for: indexPath)
            }
            
            cell.didReduceSet = { [weak self] in
                guard let self = self else { return }
                guard self.data[indexPath.row].dropSetMethodData?.sets.count ?? 0 > 1 else {
                    self.showAlert(text: Constants.ResistanceExercises.Messages().minimumIsOneSet)
                    return
                }
                let _ = self.data[indexPath.row].dropSetMethodData?.sets.popLast()
                self.updateRow(for: indexPath)
            }
            
            cell.didDeleteMethod = { [weak self] in self?.deleteExercise(indexPath: indexPath) }
            
            cell.didEditData = { [weak self]  in
                guard let self = self else { return }
                let editPopUp = EditDropSetViewController.instantiate()
                editPopUp.method = .dropSet
                
                var sets: [ExerciseSupersetSetCell.Content] = []
                self.data[indexPath.row].dropSetMethodData?.sets.forEach {
                    
                    var ex: [ExerciseSupersetSetExerciseCell.Content] = []
                    for (i, kg) in $0.dropKgValues[$0.indexOfKgValue].enumerated() {
                        var item = ExerciseSupersetSetExerciseCell.Content(exerciseName: "", repsValue: 0, kgValue: kg, kgValues: [])
                        item.repsValue = $0.dropSetValues[i]
                        ex.append(item)
                    }
                    sets.append(.init(exercises: ex, restTime: $0.restTime, setNumber: 0))
                }
                editPopUp.data = sets
                
                self.present(editPopUp, animated: false, completion: nil)
                editPopUp.saveDidTap = { [weak self] sets in
                    guard let self = self else { return }
                    let index = dropData.sets[0].indexOfKgValue
                    var setsDropSet: [ExerciseDropSetSingleSetCell.Content] = []
                    sets.enumerated().forEach {
                        var kgArray = dropData.sets[$0].dropKgValues
                        kgArray[index] = []
                        var repsValue: [Int] = []
                        for  item in $1.exercises {
                            kgArray[index].append(item.kgValue)
                            repsValue.append(item.repsValue)
                        }
                        
                        setsDropSet.append(.init(set: $0 + 1, dropSetValues: repsValue, dropKgValues: kgArray, restTime: $1.restTime))
                    }
                    self.data[indexPath.row].dropSetMethodData?.sets = setsDropSet
                    
                    tableView.reloadData()
                }
            }
            
            cell.showRestTimePopUp = { [weak self] index in
                self?.editRestTime(indexPath: indexPath, method: method, index: index)
            }
            
            cell.didMachineNumberSelected = { [weak self] number in
                self?.data[indexPath.row].dropSetMethodData?.machineNumber = number
            }
            
            cell.addDropSet = { [weak self] index in
                guard let self = self else { return }
                guard self.data[indexPath.row].dropSetMethodData?.sets[index].dropKgValues[dropData.sets[0].indexOfKgValue].count ?? 0 < 6 else {
                    self.showAlert(text: Constants.ResistanceExercises.Messages().maximumIsSix)
                    return
                }
                self.data[indexPath.row].dropSetMethodData?.sets[index].dropKgValues[dropData.sets[0].indexOfKgValue].append(Constants.ResistanceExercises.Methods.dropSetAddDropKgValue)
                self.data[indexPath.row].dropSetMethodData?.sets[index].dropSetValues.append(Constants.ResistanceExercises.Methods.dropSetAddDropRepsValue)
                self.updateRow(for: indexPath)
            }
            
            cell.reduceDropSet = { [weak self] index in
                guard let self = self else { return }
                guard self.data[indexPath.row].dropSetMethodData?.sets[index].dropKgValues[dropData.sets[0].indexOfKgValue].count ?? 0 > 2 else {
                    self.showAlert(text: Constants.ResistanceExercises.Messages().minimumIsTwoDrops)
                    return
                }
                self.data[indexPath.row].dropSetMethodData?.sets[index].dropKgValues[dropData.sets[0].indexOfKgValue].removeLast()
                self.data[indexPath.row].dropSetMethodData?.sets[index].dropSetValues.removeLast()
                self.updateRow(for: indexPath)
            }
            return cell
            
        // MARK: - Pyramid, PercentOfRM
        case .pyramid, .percentOfRM:
            let cell = tableView.dequeueReusableCell(with: ExercisePercentMainCell.self, for: indexPath)
            guard let percent = data[indexPath.row].percentMethodData else { return cell }
            // move gesture
            cell.moveRowButton.addGestureRecognizer(addLongPressGesture())
            cell.set(content: .init(sets: percent.sets, machineNumber: percent.machineNumber, notes: percent.notes), state: data[indexPath.row].method , exercise: data[indexPath.row].exercise)
            //
            cell.didImageClicked = { [weak self] in
                guard let self = self else { return }
                self.showExerciseInfo(exercise: self.data[indexPath.row].exercise)
            }
            
            cell.didExpandClicked = { [weak self] in self?.didExpandClicked(cell: cell, indexPath: indexPath) }
            
            cell.notesIsEditing = { [weak self] notes in
                self?.data[indexPath.row].percentMethodData?.notes = notes
            }
            
            cell.didAddSet = { [weak self] in
                guard let self = self else { return }
                var rm = Constants.ResistanceExercises.Methods.percentRMDefaultValue
                var kg = [Constants.ResistanceExercises.Methods.percentKgDefaultValue]
                if self.data[indexPath.section].method == .pyramid {
                    rm = Constants.ResistanceExercises.Methods.pyramidRepsDefaultValue
                    kg = [Constants.ResistanceExercises.Methods.supersetDefaultKg]
                }
                self.data[indexPath.row].percentMethodData?.sets.append(.init(set: (self.data[indexPath.row].percentMethodData?.sets.count ?? 0) + 1, rm: rm, kg: kg, percent: Constants.ResistanceExercises.Methods.percentValue, restTime: Constants.ResistanceExercises.Methods.restTimeDefault, repsTime: ""))
                self.updateRow(for: indexPath)
            }
            
            cell.didReduceSet = { [weak self] in
                guard let self = self else { return }
                guard self.data[indexPath.row].percentMethodData?.sets.count ?? 0 > 1 else {
                    self.showAlert(text: Constants.ResistanceExercises.Messages().minimumIsOneSet)
                    return
                }
                let _ = self.data[indexPath.row].percentMethodData?.sets.popLast()
                self.updateRow(for: indexPath)
            }
            
            cell.didDeleteMethod = { [weak self] in self?.deleteExercise(indexPath: indexPath) }
            
            cell.didEditData = { [weak self]  in
                guard let self = self else { return }
                let editPopUp = EditPercentOfRMViewController.instantiate()
                editPopUp.method = (self.data[indexPath.row].method == .percentOfRM) ? .percent : .pyramid
                
                editPopUp.data = self.data[indexPath.row].percentMethodData?.sets ?? []
                self.present(editPopUp, animated: false, completion: nil)
                editPopUp.saveDidTap = { [weak self] sets in
                    guard let self = self else { return }
                    self.data[indexPath.row].percentMethodData?.sets = sets
                    tableView.reloadData()
                }
            }
            
            cell.showRestTimePopUp = { [weak self] index in
                self?.editRestTime(indexPath: indexPath, method: method, index: index)
            }
            
            cell.didMachineNumberSelected = { [weak self] number in
                self?.data[indexPath.row].percentMethodData?.machineNumber = number
            }
            
            return cell
        }
    }
}

// MARK: - TableViewReorderDelegate
extension ExercisesViewController: TableViewReorderDelegate {
    
    func tableView(_ tableView: UITableView, reorderRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let movedObject = self.data[sourceIndexPath.row]
        data.remove(at: sourceIndexPath.row)
        data.insert(movedObject, at: destinationIndexPath.row)
    }
}

// MARK: - UITableViewDelegate
extension ExercisesViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: .init(x: 0, y: 0, width: tableView.frame.width, height: 4))
        view.backgroundColor = .clear
        return view
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeights[indexPath] = cell.frame.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if let height = cellHeights[indexPath] {
            return height ?? UITableView.automaticDimension
        }
        return UITableView.automaticDimension
    }
}

