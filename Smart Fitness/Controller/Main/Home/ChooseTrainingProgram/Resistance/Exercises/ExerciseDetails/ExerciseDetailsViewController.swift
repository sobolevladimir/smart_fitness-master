//
//  ExerciseDetailsViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 18.11.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit
import AVKit
import Kingfisher

final class ExerciseDetailsViewController: BaseViewController {

    // MARK: - Views
    @IBOutlet weak private var collectionView: UICollectionView!
    @IBOutlet weak private var imageView: UIImageView!
    @IBOutlet weak private var pageLabel: UILabel!
    @IBOutlet weak private var exerciseName: UILabel!
    @IBOutlet weak private var watchVideoButton: LoginButton!
    
    // MARK: - Constraints
    @IBOutlet weak var imageTopConstraint: NSLayoutConstraint!
    
    // MARK: - Properties
    var exercise: Exercise?
    private var currentIndexPath: IndexPath = .init(item: 0, section: 0)
    private var data: [ExerciseDetailsCell.Content] = []
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        exerciseName.text = exercise?.title
        switch LocalizationManager.Language.language {
        case .english:
            if watchVideoButton.imageView?.image?.imageOrientation != UIImage.Orientation.up {
                guard var image = watchVideoButton.imageView?.image else { return }
                image = UIImage(cgImage: image.cgImage!, scale: image.scale, orientation: UIImage.Orientation.downMirrored)
                watchVideoButton.setImage(image, for: .normal)
            }
        case .hebrew:
            break
        }
        guard let description = exercise?.descriptions else { return }
        data = description.map({ ExerciseDetailsCell.Content(title: $0.title, body: $0.description) })
        guard let images = exercise?.exerciseImages else { return }
        guard let firstImage = images.first else  { return }
        imageView.downloadImage(from: firstImage.url)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.setFullTranslucent()
        animateChangingImage()
        fixDirection()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(false)
        navigationController?.navigationBar.setBlueGradient()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        imageTopConstraint.constant = UIApplication.shared.statusBarFrame.height
    }
   
    // MARK: - Actions
    @IBAction func backDidTap(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func backControlDidTap(_ sender: Any) {
        scrollToPage(isNext: false)
    }
    
    @IBAction func nextControlDidTap(_ sender: Any) {
        scrollToPage(isNext: true)
    }
    
    @IBAction func videoDidTap(_ sender: Any) {
        if UserDefaults.standard.bool(forKey: Constants.isSubscribedKey) {
            guard let exercise = exercise,
                      let videoUrl = exercise.videoUrl else { return }
                  let url = URL(string: videoUrl)!
                  let player = AVPlayer(url: url)
                  let vc = AVPlayerViewController()
                  vc.player = player
                  present(vc, animated: true) {
                      vc.player?.play()
                  }
        } else {
            let navBar = UINavigationController(rootViewController: SubscriptionViewController.instantiate())
            navBar.modalPresentationStyle = .fullScreen
            present(navBar, animated: true)
        }
    }
    
    // MARK: - Private
    private func scrollToPage(isNext: Bool) {
        if !isNext {
            guard currentIndexPath.item != 0 else { return }
            currentIndexPath.item -= 1
        } else {
            guard currentIndexPath.item != 2 else { return }
            currentIndexPath.item += 1
        }
        collectionView.scrollToItem(at: currentIndexPath, at: .centeredHorizontally, animated: true)
    }
    
    private func animateChangingImage() {
        var index = 1
        guard let imageData = exercise?.exerciseImages else { return }
        guard imageData.count > 1 else { return }
        Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { [weak self] timer in
            guard let self = self else { return }
            if index >= imageData.count {
                index = 0
            }
            
            UIView.transition(with: self.imageView,
                              duration: 0.3,
                              options: .transitionCrossDissolve,
                              animations: { self.imageView.downloadImage(from: imageData[index].url)},
                              completion: nil)
            index += 1
        }
    }
}

// MARK: - UICollectionViewDataSource
extension ExerciseDetailsViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(with: ExerciseDetailsCell.self, for: indexPath)
        cell.set(content: data[indexPath.item])
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension ExerciseDetailsViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        collectionView.frame.size
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageWidth = scrollView.frame.size.width
        let item = floor((scrollView.contentOffset.x * 2.0 + pageWidth) / (pageWidth * 2.0))
        currentIndexPath.item = Int(item)
        pageLabel.text = "\(currentIndexPath.item + 1)/3"
    }
}
