//
//  ExerciseDetailsCell.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 18.11.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class ExerciseDetailsCell: UICollectionViewCell {
    
    // MARK: - Views
    @IBOutlet weak private var titleLabel: UILabel!
    @IBOutlet weak private var bodyLabel: UILabel!
    
    // MARK: - Methods
    func set(content: Content) {
        let underlineAttribute = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue]
        titleLabel.attributedText = NSAttributedString(string: content.title ?? "", attributes: underlineAttribute)
        bodyLabel.text = content.body
    }
}

// MARK: - Content
extension ExerciseDetailsCell {
    struct Content {
        var title: String?
        var body: String?
    }
}

