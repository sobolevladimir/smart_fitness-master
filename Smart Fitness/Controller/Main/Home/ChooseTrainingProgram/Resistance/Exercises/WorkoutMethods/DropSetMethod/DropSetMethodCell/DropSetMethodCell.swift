//
//  DropSetMethodCell.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 21.11.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class DropSetMethodCell: UITableViewCell {
    
    // MARK: - Views
    @IBOutlet weak var checkedButton: RadioCheckButton!
    @IBOutlet weak var checkedNumberLabel: UILabel!
    @IBOutlet weak private var blueView: UIView!
    @IBOutlet weak private var imageExerciseView: UIImageView!
    @IBOutlet weak private var exerciseNameLabel: UILabel!
    
    // MARK: - Properties
    var id: String?
    var didSelectedExercise: (() -> Void)?
    var didImageClicked: (() -> Void)?
    
    // MARK: - Actions
    @IBAction private func didImageTap(_ sender: Any) { didImageClicked?() }
    
    @IBAction private func cellButtonAction(_ sender: Any) {  didSelectedExercise?() }
    
    // MARK: - Methods
    func set(content: Exercise) {
        imageExerciseView.downloadImage(from: content.mainImageUrl)
        exerciseNameLabel.text = content.title
        id = content.privateId
        checkedButton.isChecked = content.isSelected
        blueView.backgroundColor = content.isSelected ? Constants.Colors.blueDark : .white
    }
}


