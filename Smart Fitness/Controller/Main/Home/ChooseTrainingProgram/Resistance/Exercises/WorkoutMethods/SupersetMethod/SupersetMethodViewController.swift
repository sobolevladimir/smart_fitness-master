//
//  SupersetMethodViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 21.11.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class SupersetMethodViewController: BaseViewController {
    
    // MARK: - Views
    @IBOutlet weak private var tableView: UITableView!
    @IBOutlet weak private var backgroundImage: UIImageView!
    
    // MARK: - Properties
    var backgroundImageName: String = ""
    var data: [[Exercise]] = [[]]
    var didFinishClicked: (([Exercise]) -> Void)?
    private var isAll: Bool = false
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.contentInset = UIEdgeInsets(top: 16, left: 0, bottom: 40, right: 0)
        navigationItem.hidesBackButton = true
        backgroundImage.image = UIImage(named: backgroundImageName)
    }
    
    // MARK: - Actions
    @IBAction private func cancelDidTap(_ sender: Any) {
        navigationController?.popViewController(animated: false)
    }
    
    @IBAction private func finishDidTap(_ sender: Any) {
        data.enumerated().forEach {
            if isAll {
                didFinishClicked?($1)
                return
            }
            guard $0 != data.count - 1 else { return }
            didFinishClicked?($1)
        }
        navigationController?.popViewController(animated: false)
    }
    
    // MARK: - Private
    private func createSuperset(with items: [Exercise]) {
        self.showAlert(text: Constants.ResistanceExercises.Messages().supersetIsCreated)
        if data[data.count - 1].count == 0 {
            data.remove(at: data.count - 1)
            isAll = true
        }
        data.insert(items, at: 0)
        tableView.reloadData()
    }
    
    private func removeMerge(at section: Int) {
        let popUpVC = LogoutViewController.instantiate()
        popUpVC.state = .unlinkExercises
        present(popUpVC, animated: false)
        popUpVC.didDelete = { [weak self] in
            guard let self = self else { return }
            guard let item = self.data[section].first else { return }
            self.data.remove(at: section)
            let index = self.data.count - 1
            if self.isAll {
                self.data.append([item])
            } else {
                self.data[index] += [item]
            }
            self.isAll = false
            self.tableView.reloadData()
        }
    }
    
    private func showExerciseInfo(exercise: Exercise) {
        guard !exercise.isCustom() else { return }
        let vc = ExerciseDetailsViewController.instantiate()
        vc.exercise = exercise
        navigationController?.pushViewController(vc, animated: true)
    }
    
    private func showMusclesGroupPopUp(at indexPath: IndexPath, isEditingSuperset: Bool) {
        let popUpVC = MusclesGroupPopUpViewController.instantiate()
        present(popUpVC, animated: false, completion: nil)
        popUpVC.didGroupSelected = { [weak self] key in
            guard let self = self else { return }
            let vc = AddNewExercisesViewController.instantiate()
            vc.state = .selectForSuperset
            
            var items: [Exercise] = []
            if isEditingSuperset {
                 items = self.data[indexPath.section]
            } else {
                items.append(self.data[indexPath.section][indexPath.item])
            }
            
            vc.selectedData = items
            vc.groupKey = key

            self.navigationController?.pushViewController(vc, animated: true)
            vc.didAddSelectedExercises  = { [weak self] items in
                guard let self = self else { return }
                guard items.count > 1 else { return }
                let supersetItems = items
                if isEditingSuperset {
                    self.data[indexPath.section] = supersetItems
                    self.tableView.reloadData()
                    return
                }
                
                self.data[indexPath.section].remove(at: indexPath.item)
                self.createSuperset(with: supersetItems)
            }
        }
    }
}

// MARK: - UITableViewDataSource
extension SupersetMethodViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        data.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        data[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if data.count == 1 && !isAll || indexPath.section == data.count - 1 && !isAll {
            let cell = tableView.dequeueReusableCell(with: SupersetMethodCell.self, for: indexPath)
            cell.set(content: data[indexPath.section][indexPath.row])
            cell.didImageClicked = { [weak self] in
                guard let self = self else { return }
                self.showExerciseInfo(exercise:  self.data[indexPath.section][indexPath.row])
            }
            cell.didHandClicked = { self.showMusclesGroupPopUp(at: indexPath, isEditingSuperset: false) }
            return cell
        }
        let cell = tableView.dequeueReusableCell(with: SupersetMergedCell.self, for: indexPath)
        let isFirst: Bool = (indexPath.row == 0)
        let isLast: Bool = (indexPath.row == data[indexPath.section].count - 1)
        cell.set(isFirst: isFirst, isLast: isLast, content: data[indexPath.section][indexPath.row])
        cell.didTapImageView = { [weak self] in
            guard let self = self else { return }
            self.showExerciseInfo(exercise: self.data[indexPath.section][indexPath.row]) }
        cell.didTapDeleteMerge = { [weak self] in self?.removeMerge(at: indexPath.section) }
        cell.didHandClicked = { [weak self] in
            guard let self = self else { return }
            guard self.data[indexPath.section].count < 6 else {
                 let alert = ComingSoonViewController.instantiate()
                alert.state = .limitIsSix("6")
                       self.present(alert, animated: false)
                       Timer.scheduledTimer(withTimeInterval: 1, repeats: false) {_ in alert.dismissVC() }
                return
            }
            self.showMusclesGroupPopUp(at: indexPath, isEditingSuperset: true)
        }
        return cell
    }
}

// MARK: - UITableViewDelegate
extension SupersetMethodViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: .init(x: 0, y: 0, width: tableView.frame.width, height: 4))
        view.backgroundColor = .clear
        return view
    }
}
