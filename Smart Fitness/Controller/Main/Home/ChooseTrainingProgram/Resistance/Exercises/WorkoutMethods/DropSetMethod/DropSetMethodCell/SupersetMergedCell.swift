//
//  SupersetMergedCell.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 03.12.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class SupersetMergedCell: UITableViewCell {
    
    // MARK: - Views
    @IBOutlet weak private var containerView: RoundedView!
    @IBOutlet weak private var separatorView: UIView!
    @IBOutlet weak private var exerciseImageView: UIImageView!
    @IBOutlet weak private var exerciseTitleLabel: UILabel!
    @IBOutlet weak private var deleteMergeButton: UIButton!
    @IBOutlet weak private var topMergeImage: UIImageView!
    @IBOutlet weak private var bottomMergeImage: UIImageView!
    @IBOutlet weak private var handButton: UIButton!
    
    // MARK: - Constraints
    @IBOutlet weak var bottomContainerViewAnchor: NSLayoutConstraint!
    
    // MARK: - Properties
    var didTapImageView: (() -> Void)?
    var didTapDeleteMerge: (() -> Void)?
    var didHandClicked: (() -> Void)?
    
    // MARK: - Methods
    func set(isFirst: Bool, isLast: Bool, content: Exercise) {
        self.layoutMargins = UIEdgeInsets.zero
        self.separatorInset = UIEdgeInsets.zero
        exerciseImageView.downloadImage(from: content.mainImageUrl)
        exerciseTitleLabel.text = content.title
        deleteMergeButton.isHidden = !isFirst
        containerView.corners = isFirst ? .top : .none
        if isLast {
            containerView.corners = .bottom
        }
        bottomContainerViewAnchor.constant = isLast ? 10 : 0
        separatorView.isHidden = isLast
        containerView.cornerRadius = 4
        
        topMergeImage.isHidden = isFirst
        bottomMergeImage.isHidden = isLast
        guard let handButton = handButton else { return }
        handButton.isHidden = !isFirst
    }
    
    // MARK: - Actions
    @IBAction private func imageButtonAction(sender: Any) {
        didTapImageView?()
    }
    
    @IBAction private func handButtonAction(_ sender: Any) {
        didHandClicked?()
    }
    
    @IBAction private func deleteMergeButtonAction(_ sender: Any) {
        didTapDeleteMerge?()
    }
}
