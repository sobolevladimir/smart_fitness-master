//
//  DropSetMethodViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 21.11.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class DropSetMethodViewController: BaseViewController {
    
    enum MethodType: String, Codable {
        case superset, supersetDifferent, dropSet, pyramid, percentOfRM, none
        
        var localizedString: String {
            switch self {
                
            case .superset:
              return  Constants.ResistanceExercises.Methods().supersetString
            case .supersetDifferent:
              return   Constants.ResistanceExercises.Methods().supersetString
            case .dropSet:
               return  Constants.ResistanceExercises.Methods().dropSetString
            case .pyramid:
              return   Constants.ResistanceExercises.Methods().pyramidString
            case .percentOfRM:
              return   Constants.ResistanceExercises.Methods().percentOfRMString
            case .none:
              return  ""
            }
        }
    }
    
    // MARK: - Views
    @IBOutlet weak private var tableView: UITableView!
    @IBOutlet weak private var backgroundImage: UIImageView!
    @IBOutlet weak private var createButton: LoginButton!
    @IBOutlet weak private var finishButton: LoginButton!
    
    // MARK: - Constraints
    @IBOutlet weak var stackViewWidthAnchor: NSLayoutConstraint!
    
    @IBOutlet weak var bottomButtonsStackView: UIStackView!
    // MARK: - Properties
    var method: MethodType = .superset
    var backgroundImageName: String = ""
    var data: [[Exercise]] = [[]]
    var didFinishClicked: (([Exercise]) -> Void)?
    private var selectedExercises: [Exercise] = []
    private var isAll: Bool = false
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.contentInset = UIEdgeInsets(top: 16, left: 0, bottom: 40, right: 0)
        navigationItem.hidesBackButton = true
        backgroundImage.image = UIImage(named: backgroundImageName)
        tableView.layoutMargins = UIEdgeInsets.zero
        tableView.separatorInset = UIEdgeInsets.zero
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        setViews(to: method)
        bottomButtonsStackView.semanticContentAttribute = .forceLeftToRight
    }
    
    // MARK: - Actions
    @IBAction private func backDidTap(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction private func createDropSetDidTap(_ sender: Any) { createSuperset() }
    
    @IBAction func finishDidTap(_ sender: Any) {
        data.enumerated().forEach {
            if isAll {
                didFinishClicked?($1)
                return 
            }
            guard $0 != data.count - 1 else { return }
            didFinishClicked?($1)
        }
        navigationController?.popViewController(animated: false)
    }
    
    // MARK: - Private
    private func createSuperset() {
        if method == .superset {
            guard selectedExercises.count >= 2 else {
                self.showAlert(text:  Constants.ResistanceExercises.Messages().chooseAtLeastTwoEx)
                return
            }
        }
        
        selectedExercises.forEach { selected in
            if let index = data.last?.lastIndex(where: { $0.privateId == selected.privateId }) {
                data[data.count - 1].remove(at: index)
                if data[data.count - 1].count == 0 {
                    data.remove(at: data.count - 1)
                    isAll = true
                }
            }
        }
        data.insert(selectedExercises, at: 0)
        guard method == .superset else {
            navigationController?.popViewController(animated: true)
            selectedExercises.forEach({ didFinishClicked?([$0]) })
            return
        }
        
        selectedExercises = []
        tableView.reloadData()
        self.showAlert(text: Constants.ResistanceExercises.Messages().supersetIsCreated)
    }
    
    private func removeMerge(at section: Int) {
        let popUpVC = LogoutViewController.instantiate()
        popUpVC.state = .unlinkExercises
        present(popUpVC, animated: false)
        popUpVC.didDelete = { [weak self] in
            guard let self = self else { return }
            var items = self.data[section]
            items.mutateEach({ $0.isSelected = false})
            self.data.remove(at: section)
            let index = self.data.count - 1
            if self.isAll {
                self.data.append(items)
            } else {
                self.data[index] += items
            }
            self.isAll = false
            self.tableView.reloadData()
        }
    }
    
    private func setViews(to method: MethodType) {
        let name = method.localizedString
        title = "\(name) \(Constants.ResistanceExercises.Methods().methodString)"
        createButton.setTitle("\(Constants.ResistanceExercises.Methods().createString) \(name) ", for: .normal)
        if method == .superset {
            finishButton.isHidden = false
        }
        if method == .percentOfRM {
            DispatchQueue.main.async { [weak self] in
                self?.stackViewWidthAnchor.isActive = true
                self?.view.layoutIfNeeded()
            }
        }
    }
    
    private func showExerciseInfo(exercise: Exercise) {
        guard !exercise.isCustom() else { return }
        let vc = ExerciseDetailsViewController.instantiate()
        vc.exercise = exercise
        navigationController?.pushViewController(vc, animated: true)
    }
    
    private func selectionOf(cell: DropSetMethodCell, at indexPath: IndexPath) {
        cell.didSelectedExercise = { [weak self] in
            guard let self = self else { return }
            if cell.checkedButton.isChecked {
                guard let index = self.selectedExercises.lastIndex(where: { $0.privateId == cell.id }) else { return }
                self.selectedExercises.remove(at: index)
                           self.data[indexPath.section][indexPath.row].isSelected = false
                           self.tableView.reloadData()
            } else {
                if self.method == .superset {
                    guard self.selectedExercises.count <= 5 else {
                        self.showAlert(text: Constants.ResistanceExercises.Messages().theLimitIsSix)
                        cell.checkedButton.isChecked = false
                        self.tableView.reloadData()
                        return
                    }
                }
                self.data[indexPath.section][indexPath.row].isSelected = true
                
//                if let index = self.selectedExercises.lastIndex(where: { $0.id == cell.id }) {
//                     self.selectedExercises[index].id += 10001
//                    cell.id = self.selectedExercises[index].id
//                }
                self.selectedExercises.append(self.data[indexPath.section][indexPath.row])
                self.tableView.reloadData()
                return
            }
        }
        
        guard let index = selectedExercises.lastIndex(where: { $0.privateId == cell.id }) else { return  }
        if method == .superset {
            cell.checkedNumberLabel.text = "\(index + 1)"
            
        } else {
            cell.checkedNumberLabel.isHidden = true
        }
    }
}

// MARK: - UITableViewDataSource
extension DropSetMethodViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        data.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        data[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if data.count == 1 && !isAll || indexPath.section == data.count - 1 && !isAll {
            let cell = tableView.dequeueReusableCell(with: DropSetMethodCell.self, for: indexPath)
            cell.set(content: data[indexPath.section][indexPath.row])
            selectionOf(cell: cell, at: indexPath)
            cell.didImageClicked = { [weak self] in
                guard let self = self else { return }
                self.showExerciseInfo(exercise: self.data[indexPath.section][indexPath.row]) }
            return cell
        }
        let cell = tableView.dequeueReusableCell(with: SupersetMergedCell.self, for: indexPath)
        let isFirst: Bool = (indexPath.row == 0)
        let isLast: Bool = (indexPath.row == data[indexPath.section].count - 1)
        cell.set(isFirst: isFirst, isLast: isLast, content: data[indexPath.section][indexPath.row])
        cell.didTapImageView = { [weak self] in
            guard let self = self else { return }
            self.showExerciseInfo(exercise: self.data[indexPath.section][indexPath.row]) }
        cell.didTapDeleteMerge = { [weak self] in self?.removeMerge(at: indexPath.section) }
        return cell
    }
}

// MARK: - UITableViewDelegate
extension DropSetMethodViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: .init(x: 0, y: 0, width: tableView.frame.width, height: 4))
        view.backgroundColor = .clear
        return view
    }
}
