//
//  MusclesGroupPopUpViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 03.12.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

class MusclesGroupPopUpViewController: BaseViewController {
    
    enum State {
        case superset, circuit
    }
    
    // MARK: - Views
    @IBOutlet weak private var containerView: UIView!
    @IBOutlet weak private var containerViewTopAnchor: NSLayoutConstraint!
    @IBOutlet weak private var collectionView: UICollectionView!
    @IBOutlet weak var titleLabel: UILabel!
    
    // MARK: - Properties
    var state: State = .superset
    var didGroupSelected: ((GroupsData.MuscleGroupKey) -> Void)?
    var data: [ResistanceGroup] = GroupsData().getData().map({ ResistanceGroup(key: $0.key, group: .none, id: $0.id)})
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        switch state {
        case .circuit:
            titleLabel.text = Constants.Circuit().fromWhatMuscle
        default: break
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        containerViewTopAnchor.constant = -containerView.frame.height
        UIView.animate(withDuration: 0.3, animations: { [weak self] in self?.view.layoutIfNeeded()})
        DispatchQueue.main.async { [weak self] in self?.collectionView.collectionViewLayout.invalidateLayout() }
    }

    // MARK: - Actions
    @IBAction private func cancelDidTap(_ sender: Any) {
        dismissWithAnimation()
    }
    
    // MARK: - Private
    private func dismissWithAnimation() {
        containerViewTopAnchor.constant = 0
        UIView.animate(withDuration: 0.3, animations: { [weak self] in self?.view.layoutSubviews()
        }) { [weak self] (_) in self?.dismiss(animated: false, completion: nil) }
    }
}

// MARK: - UICollectionViewDataSource
extension MusclesGroupPopUpViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(with: ResistanceCell.self, for: indexPath)
        cell.set(content: data[indexPath.item], hasExercises: false)
        cell.didClickedOnCell = { [weak self] in
            guard let self = self else { return }
            self.dismissWithAnimation()
            self.didGroupSelected?(self.data[indexPath.item].key)
        }
        
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension MusclesGroupPopUpViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.frame.width - 16 - 28) / 3
        let height = (collectionView.frame.height - 16 - 36) / 4
        if indexPath.row == data.count - 1 {
            return .init(width: collectionView.frame.width - 24, height: height)
        }
        return .init(width: width, height: height)
    }
}
