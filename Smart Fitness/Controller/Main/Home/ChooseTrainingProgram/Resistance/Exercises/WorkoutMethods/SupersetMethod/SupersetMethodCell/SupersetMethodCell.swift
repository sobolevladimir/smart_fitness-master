//
//  SupersetMethodCell.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 22.11.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class SupersetMethodCell: UITableViewCell {
    
    // MARK: - Views
    @IBOutlet weak private var imageExerciseView: UIImageView!
    @IBOutlet weak private var exerciseNameLabel: UILabel!
    
    // MARK: - Properties
    var id: String?
    
    // MARK: - Closure Actions
    var didImageClicked: (() -> Void)?
    var didHandClicked: (() -> Void)?
    
    // MARK: - Actions
    @IBAction private func didImageTap(_ sender: Any) { didImageClicked?() }
    @IBAction private func handButtonAction(_ sender: Any) { didHandClicked?() }
    
    // MARK: - Methods
    func set(content: Exercise) {
        imageExerciseView.downloadImage(from: content.mainImageUrl)
        exerciseNameLabel.text = content.title
        id = content.privateId
    }
}
