//
//  WorkoutMethodsViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 21.11.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class WorkoutMethodsViewController: BaseViewController {
    
    // MARK: - Views
    @IBOutlet weak private var containerView: UIView!
    
    @IBOutlet weak private var crownButton: UIButton!
    // MARK: - Constraints
    @IBOutlet weak private var topConstraint: NSLayoutConstraint!
    
    // MARK: - Properties
    var methodIsChosen: ((DropSetMethodViewController.MethodType) -> Void)?
    var isSubscriber: Bool = true
    
    // MARK: - Lifecycle
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        crownButton.isHidden = isSubscriber
        topConstraint.constant = containerView.frame.height
        UIView.animate(withDuration: 0.3) { [weak self] in
            self?.view.layoutSubviews()
        }
    }
    
    // MARK: - Actions
    @IBAction private func closeDidTap(_ sender: Any) {
        topConstraint.constant = 0
        UIView.animate(withDuration: 0.3, animations: { [weak self]  in
            self?.view.layoutSubviews()
        }) { [weak self] (_) in
            self?.dismiss(animated: false, completion: nil)
        }
    }
    
    @IBAction private func sameMuscleDidTap(_ sender: Any) {
        if isSubscriber {
            methodIsChosen?(.superset)
            closeDidTap(UIButton())
        } else {
            showPurchaseScreen()
        }
    }
    
    @IBAction private func differentMuscleDidTap(_ sender: Any) {
        if isSubscriber {
            methodIsChosen?(.supersetDifferent)
            closeDidTap(UIButton())
        } else {
            showPurchaseScreen()
        }
    }
    
    @IBAction private func dropSetDidTap(_ sender: Any) {
        if isSubscriber {
            methodIsChosen?(.dropSet)
            closeDidTap(UIButton())
        } else {
            showPurchaseScreen()
        }
    }
    
    @IBAction private func pyramidDidTap(_ sender: Any) {
        if isSubscriber {
            methodIsChosen?(.pyramid)
            closeDidTap(UIButton())
        } else {
            showPurchaseScreen()
        }
    }
    
    @IBAction private func percentOfRMDidTap(_ sender: Any) {
        if isSubscriber {
            methodIsChosen?(.percentOfRM)
            closeDidTap(UIButton())
        } else {
            showPurchaseScreen()
        }
    }
    
    @IBAction private func crownDidTap(_ sender: Any) {
        showPurchaseScreen()
    }
    
    // MARK: - Private
    func showPurchaseScreen() {
        let navBar = UINavigationController(rootViewController: SubscriptionViewController.instantiate())
        navBar.modalPresentationStyle = .fullScreen
        present(navBar, animated: true)
    }
}
