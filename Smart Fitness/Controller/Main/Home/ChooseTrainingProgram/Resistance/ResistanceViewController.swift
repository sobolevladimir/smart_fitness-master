//
//  ResistanceViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 31.10.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class ResistanceViewController: BaseViewController {
    
    enum SplitButtonState {
        case split, delete
    }
    
    // MARK: - Views
    @IBOutlet weak private var collectionView: UICollectionView!
    @IBOutlet weak private var createSplitTrainingButton: LoginButton!
    @IBOutlet weak private var saveButton: UIBarButtonItem!
    @IBOutlet weak private var discardButton: UIBarButtonItem!
    
    // MARK: - Properties
    var editState: EditingState = .build
    var didSaveProgram: ((Program) -> Void)?
    var state = SplitButtonState.split
    var isFirstAppearingOnEditing = false
    var saveState: SaveState = .back
    var data: [Resistance] = [] {
        didSet {
            var hasExercises = false
            data.forEach({
                if  $0.exercisesData.count > 0 {
                    hasExercises = true
                }
            })
            if isFirstAppearingOnEditing {
                saveState = .back
                saveButton.isEnabled = false
                setupNavigationLeftItem(for: saveState)
            } else {
                saveState = hasExercises ? .save : .back
                saveButton.isEnabled = hasExercises
                setupNavigationLeftItem(for: saveState)
            }
           
        }
    }
    var program: Program? {
        didSet {
            print("Program ID: \(program?.id)")
        }
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        saveButton.isEnabled = false
        collectionView.contentInset.bottom = 90
        switch editState {
        case .build:
            data =  GroupsData().getData().map({
                Resistance(group: ResistanceGroup(key: $0.key, group: .none, id: $0.id), exercisesData: [], id: nil)
            })
            
        case .edit:
            fixDirection()
            isFirstAppearingOnEditing = true
            guard let resistanceProgram = program?.resistanceProgram else { return }
            data = GroupsData().getData().map({ item in
                if let resistance = resistanceProgram.last(where: { $0.group.id == item.id }) {
                    if let _ = resistanceProgram.last(where: { $0.group.group != .none }) {
                        state = .delete
                        self.createSplitTrainingButton.setTitle(Constants.ResistanceExercises.Methods().deleteSplitTr, for: .normal)
                    }
                    return resistance
                } else {
                    return Resistance(group: ResistanceGroup(key: item.key, group: .none, id: item.id), exercisesData: [], id: nil)
                }
            })
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setupNavigationLeftItem(for: saveState)
        DispatchQueue.main.async { [weak self] in
            self?.collectionView.collectionViewLayout.invalidateLayout()
            self?.collectionView.reloadData()
        }
        self.setBarItemsFont()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        isFirstAppearingOnEditing = false
    }
    
    // MARK: - Actions
    @IBAction private func discardTapped(_ sender: Any) {
        if saveState == .back {
            navigationController?.popViewController(animated: true)
            return
        }
        let popUpVc = LogoutViewController.instantiate()
        switch editState {
            
        case .build:
            popUpVc.state = .deleteProgram
        case .edit:
            popUpVc.state = .leaveWithoutSaving
        }
        
        present(popUpVc, animated: false, completion: nil)
        popUpVc.didDelete = { [weak self] in
            self?.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction private func saveTapped(_ sender: Any) {
        let vc = LogoutViewController.instantiate()
        vc.state = .save
        present(vc, animated: false, completion: nil)
        
        vc.didFinishTapped = { [weak self] in
            guard let self = self else { return }
            let programVC = ProgramInfoViewController.instantiate()
            programVC.didSaveProgram = { [weak self] program in self?.didSaveProgram?(program) }
            programVC.programType = .resistance
            programVC.id = self.program?.id
            programVC.resistanceTraining = self.data
            programVC.editingState = self.editState
            if let cardioProgram = self.program?.cardioProgram {
                programVC.cardioTraining = cardioProgram
            }
            if self.editState == .edit {
                guard let programInfo = self.program?.programInfo else { return }
                programVC.programInfo = programInfo
            }
            self.navigationController?.pushViewController(programVC, animated: true)
        }
        
        vc.didAddTapped = { [weak self] in
            guard let self = self else { return }
            let cardioVC = CardioTrainingViewController.instantiate()
            if self.editState == .edit {
                cardioVC.editingState = self.editState
                cardioVC.program = self.program
                
            }
            cardioVC.resistanceData = self.data
            cardioVC.didSaveProgram = { [weak self] program in self?.didSaveProgram?(program)  }
            self.navigationController?.pushViewController(cardioVC, animated: true)
        }
    }
    
    @IBAction private func createSplitMusclesDidTap(_ sender: Any) {
        switch state {
        case .split:
            let vc = SplitMusclesViewController.instantiate()
            navigationController?.pushViewController(vc, animated: true)
            vc.splitedTrainings = { [weak self] splitedTrainingData in
                guard let self = self else { return }
                let newData: [ResistanceGroup] = self.data.map { item in
                    var content = item
                    splitedTrainingData.forEach { splitItem in
                        if splitItem.id == item.group.id {
                            content.group.group = splitItem.groupName
                            content.group.orderNumber = splitItem.order
                            self.state = .delete
                            self.createSplitTrainingButton.setTitle(Constants.ResistanceExercises.Methods().deleteSplitTr, for: .normal)
                        }
                    }
                    return content.group
                }
                newData.enumerated().forEach {
                    self.data[$0].group = $1
                }
                self.collectionView.reloadData()
            }
        case .delete:
            deleteSplits()
        }
    }
    
    // MARK: - Private
    private func deleteSplits() {
        let popUpVc = LogoutViewController.instantiate()
        popUpVc.state = .deleteSplit
        present(popUpVc, animated: false, completion: nil)
        popUpVc.didDelete = { [weak self]  in
            guard let self = self else { return }
            self.state = .split
            let newData: [ResistanceGroup] = self.data.map { item in
                var content = item
                content.group.group = .none
                return content.group
            }
            newData.enumerated().forEach {
                self.data[$0].group = $1
            }
            self.collectionView.reloadData()
            self.createSplitTrainingButton.setTitle(Constants.ResistanceExercises.Methods().splitTrainingString, for: .normal)
        }
    }
    
    private func setupNavigationLeftItem(for state: SaveState) {
          switch state {
          case .back:
              fixDirection()
          case .save:
              navigationItem.leftBarButtonItem?.title = Constants.Cardio().discardString
              navigationItem.leftBarButtonItem?.image = nil
          }
          self.setBarItemsFont()
      }
}

// MARK: - UICollectionViewDataSource
extension ResistanceViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(with: ResistanceCell.self, for: indexPath)
        cell.set(content: data[indexPath.item].group, hasExercises: data[indexPath.item].exercisesData.count > 0)
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension ResistanceViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 24, left: 12, bottom: 0, right: 12)
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.frame.width - 16 - 28) / 3
        if SizeManager.device == .iPad {
            let width = (collectionView.frame.width - 16 - 130) / 3
            if indexPath.row == data.count - 1 {
               return .init(width: collectionView.frame.width - 16, height: width * 1.15)
            }
            return .init(width: width, height: width * 1.15)
        }
        if indexPath.row == data.count - 1 {
            return .init(width: collectionView.frame.width - 24, height: width * 1.17)
        }
        return .init(width: width, height: width * 1.17)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = ExercisesViewController.instantiate()
        switch LocalizationManager.Language.language {
            
        case .english:
            vc.title = "\(data[indexPath.item].group.title) \(Constants.ResistanceExercises.Methods().exercisesString)"
        case .hebrew:
            vc.title = "\(Constants.ResistanceExercises.Methods().exercisesString) \(data[indexPath.item].group.title)"
            
        }
        
        vc.groupKey = data[indexPath.item].group.key
        vc.backgroundImageName = data[indexPath.item].group.imageUrl + "Image"
        vc.data = data[indexPath.row].exercisesData
        navigationController?.pushViewController(vc, animated: true)
        vc.didTapBack = { [weak self] exercises in
            self?.data[indexPath.row].exercisesData = exercises
            collectionView.reloadData()
        }
    }
}
