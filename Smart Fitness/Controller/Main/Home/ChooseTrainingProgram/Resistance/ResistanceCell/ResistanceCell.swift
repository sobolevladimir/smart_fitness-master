//
//  ResistanceCell.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 31.10.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class ResistanceCell: UICollectionViewCell {
    
    // MARK: - Views
    @IBOutlet weak private var containerShadowView: RoundedView!
    @IBOutlet weak private var muscleImageView: UIImageView!
    @IBOutlet weak private var titleLabel: UILabel!
    @IBOutlet weak private var groupView: RoundedView!
    @IBOutlet weak private var groupLabel: UILabel!
    
    // MARK: - Constraints
    @IBOutlet weak var groupLabelTopAnchor: NSLayoutConstraint!
    
    // MARK: - Properties
    var didClickedOnCell: (() -> Void)?
    
    // MARK: - Actions
    @IBAction private func didTapCell(_ sender: Any) {
        didClickedOnCell?()
    }
    
    // MARK: - Methods
    func set(content: ResistanceGroup, hasExercises: Bool) {
        containerShadowView.backgroundColor = hasExercises ? Constants.Colors.blueDark : .white
        titleLabel.textColor = hasExercises ? .white : .black
        muscleImageView.image = UIImage(named: content.imageUrl)
        titleLabel.text = content.title
        if content.group == .none {
             groupView.isHidden = true
        } else {
            groupView.isHidden = false
            groupLabel.text = content.group.string
            if content.title == Constants.ResistanceExercises.Muscles().legs {
                groupLabelTopAnchor.constant = containerShadowView.frame.height / 2
            } else {
                groupLabelTopAnchor.constant = 4
            }
            containerShadowView.layoutIfNeeded()
        }
    }
}
