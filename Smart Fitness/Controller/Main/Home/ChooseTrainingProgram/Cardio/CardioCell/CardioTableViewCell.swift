//
//  CardioTableViewCell.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 31.10.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class CardioTableViewCell: UITableViewCell {

    // MARK: - Views
    @IBOutlet weak private var containerView: UIView!
    @IBOutlet weak private var cellImageView: UIImageView!
    @IBOutlet weak private var cellTimeLabel: UILabel!
    
    @IBOutlet weak private var cellSpeedValueLabel: UILabel!
    @IBOutlet weak private var cellInclineValueLabel: UILabel!
    @IBOutlet weak private var cellPaceValueLabel: UILabel!
    
    @IBOutlet weak private var cellSpeedLabel: UILabel!
    @IBOutlet weak private var cellInclineLabel: UILabel!
    @IBOutlet weak private var cellPaceLabel: UILabel!
    
    // MARK: - Properties
    var editDitTap: (() -> Void)?
    var deleteDidTap: (() -> Void)?
    
    // MARK: - Actions
    @IBAction private func editDidTap(_ sender: Any) { editDitTap?() }
    
    @IBAction private func deleteDidTap(_ sender: Any) { deleteDidTap?() }
    
    // MARK: - Methods
    func set(content: Content, state: CardioActivity) {
        cellTimeLabel.text = (content.timeValue == "") ? content.distanceValue : content.timeValue
        cellImageView.image = UIImage(named: content.imageName)
        cellSpeedValueLabel.text = content.speedValue
        cellInclineValueLabel.text = content.inclineValue
        cellPaceValueLabel.text = content.paceValue
        
        var speedText = Constants.Cardio().speedString
        var inclineText = Constants.Cardio().inclineString
        var paceText = Constants.Cardio().paceString
        
        switch state {
        case .running:
            break
        case .cycling:
            speedText = Constants.Cardio().resistanceLevelString
            inclineText = Constants.Cardio().rPMString
            paceText = ""
        case .elliptical:
            speedText = Constants.Cardio().resistanceString
            paceText = Constants.Cardio().rPMString
        case .rowingMachine:
            speedText = Constants.Cardio().intensityString
            inclineText = Constants.Cardio().paceString
            paceText = Constants.Cardio().sPMString
        case .stairClimbing:
            speedText = Constants.Cardio().resistanceString
            inclineText = Constants.Cardio().stepsString
            paceText = Constants.Cardio().sPMStairClimbingString
        case .swimming:
            speedText = Constants.Cardio().styleString
            inclineText = Constants.Cardio().exerciseString
            paceText = Constants.Cardio().intensityString
        case .jumpingRope:
            break
        }
        speedText.append(":")
        inclineText.append(":")
        if state != .cycling {
            paceText.append(":")
        }
        cellSpeedLabel.text = speedText
        cellInclineLabel.text = inclineText
        cellPaceLabel.text = paceText
    }
}

// MARK: - Content
extension CardioTableViewCell {
    struct Content: Equatable, Codable {
        var imageName: String
        var timeValue: String
        var speedValue: String
        var inclineValue: String
        var paceValue: String
        var distanceValue: String
        var activityType: CardioActivity
        var id: Int?
    }
}

