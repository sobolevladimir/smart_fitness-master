//
//  CardioAddNewCell.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 30.10.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class CardioCollectionViewCell: UICollectionViewCell {
    
    // MARK: - Views
    @IBOutlet weak private var cardioImageView: UIImageView!
    @IBOutlet weak private var cardioTitleLabel: UILabel!
    @IBOutlet weak private var newCardioContent: UIView!
    @IBOutlet weak var borderView: RoundedView!
    @IBOutlet weak private var newCardioImageView: UIImageView!
    @IBOutlet weak private var newCardioTitleLabel: UILabel!
    
    
    // MARK: - Properties
    var isSelectedCell: Bool = false {
        didSet {
            borderView.layer.borderColor = isSelectedCell ? Constants.Colors.blueDark.cgColor : UIColor.white.cgColor
        }
    }

    // MARK: - Methods
    func set(content: Content) {
        newCardioContent.isHidden = content.isAdded
        isSelectedCell = content.isSelected
        if content.isAdded {
            cardioImageView.image = UIImage(named: content.imageName)
            cardioTitleLabel.text = content.title
        } else {
            newCardioImageView.image = UIImage(named: content.imageName)
            newCardioTitleLabel.text = content.title
        }
    }
}

// MARK: - Content
extension CardioCollectionViewCell {
    struct Content: Codable {
        var title: String
        var imageName: String
        var isAdded: Bool = false
        var isSelected: Bool = false
    }
}
