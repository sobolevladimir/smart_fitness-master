//
//  CardioTableViewFooter.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 31.10.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class CardioTableViewFooter: UITableViewHeaderFooterView {
    
    // MARK: - Properties
    var isMultipliable: Bool = false
    var multiplyData: (() -> Void)?
    var showErrorAlert: (() -> Void)?
    var addNewRow: (() -> Void)?
    
    // MARK: - Actions
    @IBAction private func multiplyDataTapped(_ sender: Any) { isMultipliable ? multiplyData?() : showErrorAlert?() }
    @IBAction private func addNewRowTapped(_ sender: Any) { isMultipliable ? addNewRow?() : showErrorAlert?() }
}
