//
//  EditDataViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 26.11.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class EditDataViewController: UIViewController, Storyboarded {
    
    typealias Item = CardioTableViewCell.Content
    
    // MARK: - Views
    @IBOutlet weak private var containerView: UIView!
    @IBOutlet weak private var distancePopUpView: EditCardioDropDownView!
    @IBOutlet weak private var timeLabel: UILabel!
    @IBOutlet weak private var distanceLabel: UILabel!
    @IBOutlet weak private var speedLabel: UILabel!
    @IBOutlet weak private var inclineLabel: UILabel!
    @IBOutlet weak private var paceLabel: UILabel!
    @IBOutlet weak private var paceSubtitleLabel: UILabel!
    @IBOutlet weak private var timeButton: LoginButton!
    @IBOutlet weak private var distanceButton: LoginButton!
    @IBOutlet weak private var speedButton: LoginButton!
    @IBOutlet weak private var inclineButton: LoginButton!
    @IBOutlet weak private var paceButton: LoginButton!
    @IBOutlet weak private var dropDownDistanceButton: UIButton!
    @IBOutlet weak private var timePickerView: UIPickerView!
    @IBOutlet weak private var distancePickerView: UIPickerView!
    @IBOutlet weak private var speedPickerView: UIPickerView!
    @IBOutlet weak private var inclinePickerView: UIPickerView!
    @IBOutlet weak private var pacePickerView: UIPickerView!
    @IBOutlet weak private var downArrowButton: UIButton!
    
    // MARK: - Constraints
    @IBOutlet weak private var topConstraint: NSLayoutConstraint!
    @IBOutlet weak private var timePickerViewHeightAnchor: NSLayoutConstraint!
    @IBOutlet weak private var distancePickerViewHeightAnchor: NSLayoutConstraint!
    @IBOutlet weak private var speedPickerViewHeightAnchor: NSLayoutConstraint!
    @IBOutlet weak private var inclinePickerViewHeightAnchor: NSLayoutConstraint!
    @IBOutlet weak private var pacePickerViewHeightAnchor: NSLayoutConstraint!
    @IBOutlet weak private var speedButtonWidthAnchor: NSLayoutConstraint!
    @IBOutlet weak private var inclineButtonWidthAnchor: NSLayoutConstraint!
    @IBOutlet weak private var paceButtonWidthAnchor: NSLayoutConstraint!
    @IBOutlet private var timeDropDawnViewHeightConstraint: NSLayoutConstraint!
    private var allPickerConstraints: [NSLayoutConstraint] {
        [timePickerViewHeightAnchor, distancePickerViewHeightAnchor, speedPickerViewHeightAnchor, inclinePickerViewHeightAnchor, pacePickerViewHeightAnchor, timeDropDawnViewHeightConstraint]
    }
    
    // MARK: - Public Properties
    var state: CardioActivity = .running
    var saveDidTap: ((Item, Bool) -> Void)?
    var item: Item!
    
    // MARK: - Properties
    private var dataManager = CardioDataCreator(state: .running)
    private var isTimeSelected: Bool = true
    private var outputTime: [String] = []
    private var outputDistance: [String] = ["0", "0", "0"]
    private var outputSpeed: [String] = []
    private var outputIncline: [String] = []
    private var outputPace: [String] = []
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        dataManager.set(state: state)
        setupUI(for: state)
        setTimeState()
        containerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(viewDidTap)))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        topConstraint.constant = containerView.frame.height
        UIView.animate(withDuration: 0.3) { [weak self] in
            self?.view.layoutSubviews()
        }
    }
    
    // MARK: - Actions
    @objc private func viewDidTap() {
        allPickerConstraints.forEach {
            if $0.constant != 0 { $0.constant = 0 }
            UIView.animate(withDuration: 0.3) { [weak self] in self?.view.layoutIfNeeded() }
        }
    }
    
    @IBAction private func timeDidTap(_ sender: Any) { expandPickerView(heightConstraint: timePickerViewHeightAnchor)}
    
    @IBAction private func distanceDidTap(_ sender: Any) { expandPickerView(heightConstraint: distancePickerViewHeightAnchor)}
    
    @IBAction private func speedDidTap(_ sender: Any) { expandPickerView(heightConstraint: speedPickerViewHeightAnchor)}
    
    @IBAction private func inclineDidTap(_ sender: Any) { expandPickerView(heightConstraint: inclinePickerViewHeightAnchor) }
    
    @IBAction private func paceDidTap(_ sender: Any) { expandPickerView(heightConstraint: pacePickerViewHeightAnchor) }
    
    @IBAction private func cancelDidTap(_ sender: Any) { dismissWithAnimation() }
    
    @IBAction private func saveDidTap(_ sender: Any) {
        saveEditing()
        dismissWithAnimation()
    }
    
    @IBAction private func downArrowDidTap(_ sender: Any) { expandDistanceDropDownView() }
    
    @IBAction func dropDownDistanceDidTap(_ sender: UIButton) {
        isTimeSelected.toggle()
        setTimeState()
        var title = isTimeSelected ? Constants.Cardio().distanceString : Constants.Cardio().timeString
        if isTimeSelected {
            outputTime = ["00" ,":", "00"]
        } else {
            outputDistance = ["0", ".", "0"]
        }
        if state == .jumpingRope {
                   title = isTimeSelected ? "Skips" : Constants.Cardio().timeString
                   if isTimeSelected {
                       outputTime = ["00" ,":", "00"]
                   } else {
                       outputDistance = ["0"]
                   }
               }
        expandDistanceDropDownView()
        sender.setTitle(title, for: .normal)
    }
    
    // MARK: - Private
    private func expandDistanceDropDownView() {
        timeDropDawnViewHeightConstraint.constant = (timeDropDawnViewHeightConstraint.constant == 50) ? 0 : 50
        allPickerConstraints.forEach {
            if $0.constant != 0 {
                if $0.constant == 50 {
                    return
                }
                $0.constant = 0
            }
        }
        self.distancePopUpView.setNeedsDisplay()
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
            self.distancePopUpView.layoutSubviews()
        })
    }
    
    private func dismissWithAnimation() {
        topConstraint.constant = 0
        UIView.animate(withDuration: 0.3, animations: { [weak self] in self?.view.layoutSubviews()
        }) { [weak self] (_) in self?.dismiss(animated: false, completion: nil) }
    }
    
    private func expandPickerView(heightConstraint: NSLayoutConstraint) {
        allPickerConstraints.forEach {
            if $0 == heightConstraint { return }
            if $0.constant != 0 { $0.constant = 0 }
        }
        heightConstraint.constant = (heightConstraint.constant == 0) ? 127 : 0
        UIView.animate(withDuration: 0.3) { [weak self] in self?.view.layoutIfNeeded() }
    }
    
    private func setupUI(for state: CardioActivity) {
        switch state {
        case .running:
            paceButton.isEnabled = false
            paceButton.imageTrailing = nil
        case .cycling:
            speedLabel.text = "\(Constants.Cardio().resistanceString):"
            inclineLabel.text = "\(Constants.Cardio().rPMString):"
            paceLabel.isHidden = true
            paceButton.isHidden = true
        case .elliptical:
            speedLabel.text = "\(Constants.Cardio().resistanceString):"
            paceLabel.text = "\(Constants.Cardio().rPMString):"
        case .rowingMachine:
            speedButtonWidthAnchor.constant +=  10
            speedLabel.text = "\(Constants.Cardio().intensityString):"
            inclineLabel.text = "\(Constants.Cardio().paceString):"
            paceSubtitleLabel.isHidden = false
            paceLabel.text = "\(Constants.Cardio().sPMString):"
        case .stairClimbing:
            distanceLabel.text = "\(Constants.Cardio().resistanceLevelString):"
            speedLabel.text = "\(Constants.Cardio().stepsString):"
            inclineLabel.text = "\(Constants.Cardio().sPMString):"
            paceLabel.isHidden = true
            paceButton.isHidden = true
            downArrowButton.isHidden = true
        case .swimming:
            speedButtonWidthAnchor.constant += 44
            inclineButtonWidthAnchor.constant += 26
            paceButtonWidthAnchor.constant += 26
            speedLabel.text = "\(Constants.Cardio().styleString):"
            inclineLabel.text = "\(Constants.Cardio().exerciseString):"
            paceLabel.text = "\(Constants.Cardio().intensityString):"
        case .jumpingRope:
            distanceLabel.text = Constants.Circuit().skips
            distanceButton.setTitle("0", for: .normal)
            speedLabel.text = "\(Constants.Cardio().intensityString):"
            inclineLabel.isHidden = true
            inclineButton.isHidden = true
            paceLabel.isHidden = true
            paceButton.isHidden = true
            dropDownDistanceButton.setTitle(Constants.Circuit().skips, for: .normal)
        }
        
        if item.timeValue == "" {
            isTimeSelected.toggle()
            setTimeState()
            var title = isTimeSelected ? Constants.Cardio().distanceString : Constants.Cardio().timeString
            if state == .jumpingRope {
            title = isTimeSelected ? Constants.Circuit().skips : Constants.Cardio().timeString
            }
            dropDownDistanceButton.setTitle(title, for: .normal)
        }
        
        let speedValue = (state == .stairClimbing) ? item.inclineValue : item.speedValue
        let inclineValue = (state == .stairClimbing) ? item.paceValue : item.inclineValue
        let distanceValue = (state == .stairClimbing) ? item.speedValue : item.distanceValue
        setValues(from: item.timeValue , pickerView: timePickerView, button: timeButton, data: dataManager.timeData, outputTemplate: &outputTime)
        setValues(from: distanceValue , pickerView: distancePickerView, button: distanceButton, data: dataManager.distanceData, outputTemplate: &outputDistance)
        setValues(from: speedValue , pickerView: speedPickerView, button: speedButton, data: dataManager.speedData, outputTemplate: &outputSpeed)
        setValues(from: inclineValue , pickerView: inclinePickerView, button: inclineButton, data: dataManager.inclineData, outputTemplate: &outputIncline)
        if state == .running {
            paceButton.setTitle(item.paceValue, for: .normal)
        } else {
            if state == .stairClimbing { return }
            setValues(from: item.paceValue , pickerView: pacePickerView, button: paceButton, data: dataManager.paceData, outputTemplate: &outputPace)
        }
    }
    
    private func setTimeState() {
        guard state != .stairClimbing else { return }
        distanceLabel.isHidden = isTimeSelected
        distanceButton.isHidden = isTimeSelected
        timeLabel.isHidden = !isTimeSelected
        timeButton.isHidden = !isTimeSelected
        isTimeSelected ? (distancePickerViewHeightAnchor.constant = 0) : (timePickerViewHeightAnchor.constant = 0)
        view.layoutIfNeeded()
    }
    
    private func saveEditing() {
        var item = CardioTableViewCell.Content(imageName: self.item.imageName,
                                               timeValue: getText(from: timeButton),
                                               speedValue: getText(from: speedButton),
                                               inclineValue: getText(from: inclineButton),
                                               paceValue: getText(from: paceButton),
                                               distanceValue: getText(from: distanceButton), activityType: state, id: self.item.id)
        var hasChanged = true
        if item == self.item {
            hasChanged = false
        }
        switch state {
        case .running, .elliptical, .rowingMachine, .swimming, .cycling, .jumpingRope:
            if isTimeSelected {
                item.distanceValue = ""
                item.imageName = "ic_clock"
            } else {
                item.timeValue = ""
                item.imageName = "ic_distance"
            }
            if state == .cycling {
                item.paceValue = ""
            }
        case .stairClimbing:
            item.speedValue = getText(from: distanceButton)
            item.inclineValue = getText(from: speedButton)
            item.paceValue = getText(from: inclineButton)
            item.distanceValue = ""
        }
        saveDidTap?(item, hasChanged)
        print(hasChanged)
    }
    
    private func getText(from button: UIButton) -> String {
        guard let text = button.title(for: .normal) else { return ""}
        return text
    }
    
    private func setValues(from value: String, pickerView: UIPickerView, button: UIButton, data: [[String]], outputTemplate: inout [String]) {
        guard value != "" else { return }
        if data.count >= 2 {
            let separator = data[1][0]
            let lastComponentRange: Range<String.Index> = value.range(of: separator)!
            let lastComponentString = String(value[lastComponentRange.upperBound...])
            
            let firstComponentRange: Range<String.Index> = value.range(of: separator)!
            let firstComponentString = String(value[..<firstComponentRange.lowerBound])
            
            if let selectedIndexForFirstComponent = data.first?.lastIndex(where: { $0 == firstComponentString} ) {
                pickerView.selectRow(selectedIndexForFirstComponent, inComponent: 0, animated: false)
                
                if let selectedIndexForLastComponent = data.last?.lastIndex(where: { $0 == lastComponentString } ) {
                    pickerView.selectRow(selectedIndexForLastComponent, inComponent: 2, animated: false)
                    button.setTitle(data[0][selectedIndexForFirstComponent] + separator + data[2][selectedIndexForLastComponent], for: .normal)
                    outputTemplate = [data[0][selectedIndexForFirstComponent], separator, data[2][selectedIndexForLastComponent]]
                }
            }
        } else {
            if let selectedIndexForLastComponent = data.last?.lastIndex(where: { $0 == value } ) {
                pickerView.selectRow(selectedIndexForLastComponent, inComponent: 0, animated: false)
                button.setTitle(data[0][selectedIndexForLastComponent], for: .normal)
                outputTemplate = [data[0][selectedIndexForLastComponent]]
            }
        }
    }
}

// MARK: - UIPickerViewDataSource
extension EditDataViewController: UIPickerViewDataSource {
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView {
        case timePickerView: return dataManager.timeData[component].count
        case distancePickerView: return dataManager.distanceData[component].count
        case speedPickerView: return dataManager.speedData[component].count
        case inclinePickerView: return dataManager.inclineData[component].count
        case pacePickerView: return dataManager.paceData[component].count
        default: return 0
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        switch pickerView {
        case timePickerView: return dataManager.timeData.count
        case distancePickerView: return dataManager.distanceData.count
        case speedPickerView: return dataManager.speedData.count
        case inclinePickerView: return dataManager.inclineData.count
        case pacePickerView: return dataManager.paceData.count
        default: return 0
        }
    }
}

// MARK: - UIPickerViewDelegate
extension EditDataViewController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerView {
        case timePickerView:
            outputTime[component] = dataManager.timeData[component][row]
            timeButton.setTitle("\(outputTime.first!):\(outputTime.last!)", for: .normal)
        case distancePickerView:
            outputDistance[component] = dataManager.distanceData[component][row]
            distanceButton.setTitle("\(outputDistance.first!).\(outputDistance.last!)", for: .normal)
            if state == .stairClimbing {
                distanceButton.setTitle("\(outputDistance.first!)", for: .normal)
            }
            if state == .jumpingRope {
                  distanceButton.setTitle("\(outputDistance.first!)", for: .normal)
            }
        case speedPickerView:
            outputSpeed[component] = dataManager.speedData[component][row]
            speedButton.setTitle("\(outputSpeed.first!).\(outputSpeed.last!)", for: .normal)
            if state == .running {
                let value = 60.00 / Double("\(outputSpeed.first!).\(outputSpeed.last!)")!
                var valueString = String(format: "%.2f", value)
                if valueString.count == 4 {
                    valueString.insert("0", at: valueString.startIndex)
                }
                let timeFormatString = valueString.replacingOccurrences(of: ".", with: ":", options: .literal, range: nil)
                paceButton.setTitle(timeFormatString, for: .normal)
            }
            if state == .rowingMachine || state == .swimming || state == .cycling || state == .stairClimbing || state == .elliptical || state == .jumpingRope {
                speedButton.setTitle("\(outputSpeed.first!)", for: .normal)
            }
            
        case inclinePickerView:
            outputIncline[component] = dataManager.inclineData[component][row]
            var separator = "."
            if state == .rowingMachine {
                separator = ":"
            }
            if state == .cycling || state == .stairClimbing {
                separator = "\(Constants.defaultRPMandSPMSeparator)"
            }
            inclineButton.setTitle("\(outputIncline.first!)\(separator)\(outputIncline.last!)", for: .normal)
            if state == .swimming {
                inclineButton.setTitle("\(outputIncline.first!)", for: .normal)
            }
        case pacePickerView:
            outputPace[component] = dataManager.paceData[component][row]
            paceButton.setTitle("\(outputPace.first!)\(Constants.defaultRPMandSPMSeparator)\(outputPace.last!)", for: .normal)
            if state == .swimming {
                paceButton.setTitle("\(outputPace.first!)", for: .normal)
            }
        default: break
        }
        pickerView.reloadAllComponents()
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var color: UIColor!
        var label = UILabel()
        if let v = view { label = v as! UILabel }
        label.font = Constants.returnFontName(style: .regular, size: 16)
        label.textAlignment = .center
        switch pickerView {
        case timePickerView: label.text = dataManager.timeData[component][row]
        case distancePickerView: label.text = dataManager.distanceData[component][row]
        case speedPickerView: label.text = dataManager.speedData[component][row]
        case inclinePickerView: label.text = dataManager.inclineData[component][row]
        case pacePickerView: label.text = dataManager.paceData[component][row]
        default: label.text = "0"
        }
        
        if pickerView.selectedRow(inComponent: component) == row {
            color = UIColor.orange
        } else {
            color = UIColor.black
        }
        label.textColor = color
        return label
    }
}
