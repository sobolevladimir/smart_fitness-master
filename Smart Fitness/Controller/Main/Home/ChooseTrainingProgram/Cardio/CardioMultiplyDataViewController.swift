//
//  CardioMultiplyDataViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 28.11.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class CardioMultiplyDataViewController: UIViewController, Storyboarded {
    
    // MARK: - Views
    @IBOutlet weak private var pickerView: UIPickerView!
    
    // MARK: - Properties
    var didSelectValue: ((Int) -> Void)?
    
    // MARK: - Lifecycle
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        pickerView.semanticContentAttribute = .forceLeftToRight
        UIView.animate(withDuration: 0.1) { [weak self] in
            self?.view.alpha = 1
        }
    }
   
    // MARK: - Actions
    @IBAction private func okDidTap(_ sender: Any) {
        didSelectValue?(pickerView.selectedRow(inComponent: 0) + 2)
        dismissVC()
    }
    @IBAction private func cancelDidTap(_ sender: Any) {
        dismissVC() 
    }
    
    // MARK: - Private
    private func dismissVC() {
        UIView.animate(withDuration: 0.1, animations: { [weak self] in
            self?.view.alpha = 0
        }) { [weak self] _ in
            self?.dismiss(animated: false, completion: nil)
        }
    }
}

// MARK: - UIPickerViewDataSource
extension CardioMultiplyDataViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        9
    }
}

// MARK: - UIPickerViewDelegate
extension CardioMultiplyDataViewController: UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label = UILabel()
        if let v = view { label = v as! UILabel }
        label.font = Constants.returnFontName(style: .regular, size: 17)
        label.textAlignment = .center
        label.text = "\(row + 2)"
        return label
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        41
    }
}
