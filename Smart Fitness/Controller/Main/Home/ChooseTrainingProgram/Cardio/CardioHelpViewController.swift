//
//  CardioViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 27.11.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class CardioHelpViewController: BaseViewController {
    
    // View
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var stepThreeLabel: UILabel!
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = Constants.Cardio().helpString
        let attributedTopString = NSMutableAttributedString(string: Constants.Cardio().topText, attributes: [
            .font: Constants.returnFontName(style: .regular, size: 14),
            .foregroundColor: UIColor(white: 0.0, alpha: 1.0)
        ])
        
        let attributedStepThreeString = NSMutableAttributedString(string: Constants.Cardio().stepThreeText, attributes: [
            .font: Constants.returnFontName(style: .bold, size: 14),
            .foregroundColor: UIColor(white: 0.0, alpha: 1.0)
        ])
        
        switch LocalizationManager.Language.language {
        case .english:
            attributedTopString.addAttribute(.font, value: Constants.returnFontName(style: .extraBold, size: 14), range: NSRange(location: 31, length: 14))
            attributedTopString.addAttributes([NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue], range: NSRange(location: 170, length: 15))
            
            attributedStepThreeString.addAttribute(.font, value: Constants.returnFontName(style: .regular, size: 14), range: NSRange(location: 0, length: 7))
            attributedStepThreeString.addAttribute(.font, value: Constants.returnFontName(style: .extraBold, size: 14), range: NSRange(location: 68, length: 15))
            attributedStepThreeString.addAttributes([NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue], range: NSRange(location: 68, length: 15))
        case .hebrew:
            attributedTopString.addAttribute(.font, value: Constants.returnFontName(style: .extraBold, size: 14), range: NSRange(location: 36, length: 12))
            attributedTopString.addAttributes([NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue], range: NSRange(location: 148, length: 14))
            
            attributedStepThreeString.addAttribute(.font, value: Constants.returnFontName(style: .regular, size: 14), range: NSRange(location: 0, length: 7))
            attributedStepThreeString.addAttribute(.font, value: Constants.returnFontName(style: .extraBold, size: 14), range: NSRange(location: 62, length: 19))
            attributedStepThreeString.addAttributes([NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue], range: NSRange(location: 62, length: 19))
            
        }
        topLabel.attributedText = attributedTopString
        stepThreeLabel.attributedText = attributedStepThreeString
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        fixDirection()
    }
    
    // MARK: - Actions
    @IBAction private func backDidTap(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}
