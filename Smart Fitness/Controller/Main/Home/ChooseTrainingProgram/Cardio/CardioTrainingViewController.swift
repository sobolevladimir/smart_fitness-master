//
//  CardioTrainingViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 30.10.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

enum EditingState {
       case build, edit
   }

enum SaveState {
       case back, save
   }

final class CardioTrainingViewController: BaseViewController {
    
    // MARK: - Views
    @IBOutlet weak private var collectionView: UICollectionView!
    @IBOutlet weak private var tableView: UITableView!
    @IBOutlet weak private var emptyCardioStackView: UIStackView!
    @IBOutlet weak private var categoryTitleLabel: UILabel!
    @IBOutlet weak private var deleteActivityButton: LoginButton!
    @IBOutlet weak private var bottomContainerView: RoundedView!
    @IBOutlet weak private var totalTimeLabel: UILabel!
    @IBOutlet weak private var totalDistanceLabel: UILabel!
    lazy private var tableFooterView: CardioTableViewFooter = .fromNib()
    var saveButton: UIBarButtonItem?
    
    // MARK: - Properties
    var editingState: EditingState = .build
    var didSaveProgram: ((Program) -> Void)?
    var saveState = SaveState.back
    var selectedIndex = 0 {
        didSet {
            tableView.reloadData()
            hideOrShowUIElements(isHidden: selectedIndex < 0)
            guard selectedIndex >= 0 else { return }
            categoryTitleLabel.text = data[selectedIndex].cardioType.title
            setTotalDistanceAndTime(state: data[selectedIndex].state)
        }
    }
    var program: Program?
    var resistanceData: [Resistance] = []
    var isFirstAppearOnEditing: Bool = false
    var data: [Cardio] = [] {
        didSet {
            if let _ = data.last(where: { $0.cardioType.isAdded == true }), !isFirstAppearOnEditing {
                saveButton?.isEnabled = true
                navigationItem.leftBarButtonItem?.title = Constants.Cardio().discardString
                self.setBarItemsFont()
                navigationItem.leftBarButtonItem?.image = nil
                saveState = .save
            } else {
                fixDirection()
                saveButton?.isEnabled = false
                saveState = .back
            }
            navigationController?.view.layoutIfNeeded()
            guard selectedIndex >= 0 else { return }
            guard selectedIndex < data.count else { return }
            guard tableView != nil else { return }
            tableView.isHidden = data[selectedIndex].items.isEmpty
            emptyCardioStackView.isHidden = !data[selectedIndex].items.isEmpty
            setTotalDistanceAndTime(state: data[selectedIndex].state)
            guard let item = data[selectedIndex].items.last else { return }
            if item != CardioData.createDefaultRow(to: item.activityType) {
                tableFooterView.isMultipliable = true
            } else {
                tableFooterView.isMultipliable = false
            }
        }
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        switch editingState {
            
        case .build:
            data = CardioData.getData().map({
                Cardio(state: $0.activityType, cardioType: .init(title: $0.title, imageName: $0.imageName), items: [])
            })
        case .edit:
            fixDirection()
            guard let cardioProgram = program?.cardioProgram else { return }
            data = CardioData.getData().enumerated().map({ defaultItem in
                if let item = cardioProgram.first(where: { $0.state == defaultItem.element.activityType }) {
                    isFirstAppearOnEditing = true
                    return item
                }
                 isFirstAppearOnEditing = true
                return Cardio(state: defaultItem.element.activityType, cardioType: .init(title: defaultItem.element.title, imageName: defaultItem.element.imageName), items: [])
            })
            data.sort(by: { $0.cardioType.isAdded && !$1.cardioType.isAdded})
        }
        
        setupFooter()
        emptyCardioStackView.isHidden = !data[selectedIndex].items.isEmpty
        tableView.reorder.tableGestureIsOn = true
        tableView.reorder.delegate = self
        setupNavigationRightItems()
        tableView.isHidden = true
        guard let index = data.lastIndex(where: { $0.cardioType.isAdded == true }) else {
            selectedIndex = -1
            return
        }
        selectedIndex = index
        collectionView.reloadData()
        tableView.reloadData()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        tableView.layoutIfNeeded()
        tableView.tableFooterView?.frame.size.height = 40
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        setupNavigationLeftItem(for: saveState)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(false)
         isFirstAppearOnEditing = false
    }
    
    // MARK: - Actions
    @IBAction private func backButtonTapped(_ sender: Any) {
        switch saveState {
        case .back:
            navigationController?.popViewController(animated: true)
        case .save:
            let popUpVc = LogoutViewController.instantiate()
            switch editingState {
                
            case .build:
                 popUpVc.state = .deleteProgram
            case .edit:
                 popUpVc.state = .leaveWithoutSaving
            }
           
            present(popUpVc, animated: false, completion: nil)
            popUpVc.didDelete = { [weak self] in
                self?.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    @IBAction private func deleteActivityDidTap(_ sender: Any) {
        let popUpVc = LogoutViewController.instantiate()
        popUpVc.state = .deleteCardioActivity
        present(popUpVc, animated: false, completion: nil)
        popUpVc.didDelete = { [weak self] in
            self?.deleteActivity()
        }
    }
    
    @objc private func helpDidPress() { navigationController?.pushViewController(CardioHelpViewController.instantiate(), animated: true) }
    
    @objc private func saveDidPress() {
        let programVC = ProgramInfoViewController.instantiate()
        programVC.editingState = editingState
        if resistanceData.count > 0 {
            programVC.resistanceTraining = resistanceData
              programVC.programType = .resistance
        }
        for index in 0..<data.count {
            data[index].totalTime = calculateTotalTime(index: index)
            data[index].totalDistance = calculateTotalDistance(index: index)
        }
        
        programVC.cardioTraining = data
        if editingState == .edit {
            guard let programInfo = program?.programInfo else { return }
            programVC.programInfo = programInfo
            programVC.editingState = editingState
            programVC.id = program?.id
            programVC.didSaveProgram = { [weak self] program in self?.didSaveProgram?(program)}
        }
        programVC.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(programVC, animated: true)
        programVC.didSaveProgram = { [weak self] program in self?.didSaveProgram?(program)}
    }
    
    // MARK: - Private
    private func setupNavigationRightItems() {
        let helpButton = UIBarButtonItem(image: UIImage(named: "questionMark"), style: .done, target: self, action: #selector(helpDidPress))
        helpButton.tintColor = .white
        saveButton = UIBarButtonItem(title: Constants.Cardio().saveString, style: .done, target: self, action: #selector(saveDidPress))
        saveButton?.tintColor = .white
        saveButton?.setTitleTextAttributes([NSAttributedString.Key.font: Constants.returnFontName(style: .semibold, size: 15)], for: .normal)
        navigationItem.rightBarButtonItems = [saveButton!, helpButton]
    }
    
    private func setupNavigationLeftItem(for state: SaveState) {
        switch state {
        case .back:
            fixDirection()
        case .save:
            navigationItem.leftBarButtonItem?.title = Constants.Cardio().discardString
            navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSAttributedString.Key.font: Constants.returnFontName(style: .semibold, size: 15)], for: .normal)
            navigationItem.leftBarButtonItem?.image = nil
        }
        self.setBarItemsFont()
    }
    
    private func setupFooter() {
        tableView.tableFooterView = tableFooterView
        tableFooterView.addNewRow = { [weak self] in
            guard let self = self else { return }
            let item = CardioData.createDefaultRow(to: self.data[self.selectedIndex].state)
            self.data[self.selectedIndex].items.append(item)
            self.tableView.insertRows(at: [IndexPath(item: self.data[self.selectedIndex].items.count - 1, section: 0)], with: .automatic)
        }
        tableFooterView.multiplyData = { [weak self] in self?.multiplyData() }
        tableFooterView.showErrorAlert = { [weak self] in
            let vc = ComingSoonViewController.instantiate()
            vc.state = .oneValue
            self?.present(vc, animated: false, completion: nil)
        }
    }
    
    private func hideOrShowUIElements(isHidden: Bool) {
        categoryTitleLabel.isHidden = isHidden
        deleteActivityButton.isHidden = isHidden
        bottomContainerView.isHidden = isHidden
    }
    
    private func addActivity(at indexPath: IndexPath, index: Int) {
        var item = self.data[indexPath.item]
        item.cardioType.isAdded = true
        data.remove(at: indexPath.item)
        data.insert(item, at: index)
        collectionView.performBatchUpdates({
            collectionView.moveItem(at: indexPath, to: IndexPath(item: index, section: 0))
            collectionView.scrollToItem(at: IndexPath(item: index, section: 0), at: .centeredHorizontally, animated: true)
        }) { [weak self] (ready) in
            guard let self = self else { return }
            self.selectedIndex = index
            let state = self.data[self.selectedIndex].state
            self.data[self.selectedIndex].items.append(CardioData.createDefaultRow(to: state))
            self.collectionView.reloadData()
            self.tableView.reloadData()
        }
    }
    
    private func deleteActivity() {
        data[selectedIndex].cardioType.isAdded = false
        data[selectedIndex].items = []
        let item = data[selectedIndex]
        data.remove(at: selectedIndex)
        data.append(item)
        hideOrShowUIElements(isHidden: true)
        emptyCardioStackView.isHidden = false
        
        self.collectionView.performBatchUpdates({
            self.collectionView.moveItem(at: IndexPath(item: self.selectedIndex, section: 0), to: IndexPath(item: 5, section: 0))
            self.collectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)
        }) { [weak self] (ready) in
            guard let self = self else { return }
            if let _ = self.data.lastIndex(where: { $0.cardioType.isAdded == true }) {
                self.selectedIndex = 0
                
            } else {
                self.selectedIndex = -1
            }
            self.collectionView.reloadData()
            self.tableView.reloadData()
        }
    }
    
    private func editTableViewRow(at indexPath: IndexPath) {
        let editVC = EditDataViewController.instantiate()
        editVC.state = data[selectedIndex].state
        editVC.item = data[selectedIndex].items[indexPath.row]
        present(editVC, animated: false, completion: nil)
        editVC.saveDidTap = { [weak self] item, isMultipliable in
            guard let self = self else { return }
            self.data[self.selectedIndex].items[indexPath.row] = item
            self.tableView.reloadRows(at: [indexPath], with: .fade)
            self.tableFooterView.isMultipliable = isMultipliable
        }
    }
    
    private func deleteRow(at indexPath: IndexPath) {
        defer {
            tableView.reloadData()
        }
        if self.data[self.selectedIndex].items.count == 1 {
            let popUpVc = LogoutViewController.instantiate()
            popUpVc.state = .deleteCardioActivity
            self.present(popUpVc, animated: false, completion: nil)
            popUpVc.didDelete = { [weak self] in
                guard let self = self else { return }
                self.data[self.selectedIndex].items.remove(at: indexPath.row)
                self.tableView.deleteRows(at: [indexPath], with: .fade)
                self.deleteActivity()
            }
        } else {
            self.data[self.selectedIndex].items.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    private func multiplyData() {
        let multiplyVC = CardioMultiplyDataViewController.instantiate()
        present(multiplyVC, animated: false, completion: nil)
        multiplyVC.didSelectValue = { [weak self] index in
            guard let self = self else { return }
            var items = [CardioTableViewCell.Content]()
            for _ in 0...index - 2 {
                self.data[self.selectedIndex].items.forEach {
                    items.append($0)
                }
            }
            self.data[self.selectedIndex].items += items
            self.tableView.reloadData()
        }
    }
    
    private func calculateTotalTime(index: Int) -> String {
        let timeString = data[index].items.map({ $0.timeValue })
        var minutes: Int = 0
        var seconds: Int = 0
        timeString.forEach {
            minutes += Int(String($0.prefix(2))) ?? 0
            seconds += Int(String($0.suffix(2))) ?? 0
        }
        minutes += seconds / 60
        seconds = seconds % 60
        let minutesString = minutes < 10 ? "0\(minutes)" : "\(minutes)"
        let secondsString = seconds < 10 ? "0\(seconds)" : "\(seconds)"
        return "\(minutesString):\(secondsString)"
    }
    
    private func calculateTotalDistance(index: Int) -> String {
        var kilometres: Int = 0
        var meters: Int = 0
        let distanceString = data[index].items.map({ $0.distanceValue })
        
        distanceString.forEach {
            if let idx = $0.firstIndex(of: ".") {
                let index = $0.distance(from: $0.startIndex, to: idx)
                kilometres += Int(String($0.prefix(index))) ?? 0
                meters += Int(String($0.suffix($0.count - index - 1))) ?? 0
            }
            kilometres += meters / 100
            meters = meters % 100
        }
        return "\(kilometres).\(meters)"
    }
    
    private func setTotalDistanceAndTime(state: CardioActivity) {
        switch state {
        case .running, .rowingMachine, .cycling, .swimming, .elliptical:
            totalDistanceLabel.isHidden = false
            totalDistanceLabel.text = "\(Constants.Cardio().totalDistanceString) \(calculateTotalDistance(index: selectedIndex))"
        case .stairClimbing, .jumpingRope:
            totalDistanceLabel.isHidden = true
        }
        totalTimeLabel.text = "\(Constants.Cardio().totalTimeString) \(calculateTotalTime(index: selectedIndex)) "
    }
}

// MARK: - UICollectionViewDataSource
extension CardioTrainingViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(with: CardioCollectionViewCell.self, for: indexPath)
        data[indexPath.row].cardioType.isSelected = (selectedIndex == indexPath.item)
        cell.set(content: data[indexPath.item].cardioType)
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension CardioTrainingViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: 76, height: 76)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if !data[indexPath.item].cardioType.isAdded {
            guard let index = data.lastIndex(where: { $0.cardioType.isAdded == true }) else {
                addActivity(at: indexPath, index: 0)
                return
            }
            addActivity(at: indexPath, index: index + 1)
        } else {
            selectedIndex = indexPath.item
            collectionView.reloadData()
            tableView.reloadData()
        }
    }
}

// MARK: - UITableViewDataSource
extension CardioTrainingViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard selectedIndex >= 0 else { return 0 }
        return data[selectedIndex].items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let spacer = tableView.reorder.spacerCell(for: indexPath) { return spacer }
        let cell = tableView.dequeueReusableCell(with: CardioTableViewCell.self, for: indexPath)
        cell.set(content: data[selectedIndex].items[indexPath.row], state: data[selectedIndex].state)
        cell.editDitTap = { [weak self] in self?.editTableViewRow(at: indexPath) }
        cell.deleteDidTap = { [weak self] in self?.deleteRow(at: indexPath) }
        return cell
    }
}

// MARK: - TableViewReorderDelegate
extension CardioTrainingViewController: TableViewReorderDelegate {
    func tableView(_ tableView: UITableView, reorderRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let movedObject = self.data[self.selectedIndex].items[sourceIndexPath.row]
        self.data[self.selectedIndex].items.remove(at: sourceIndexPath.row)
        self.data[self.selectedIndex].items.insert(movedObject, at: destinationIndexPath.row)
    }
}
