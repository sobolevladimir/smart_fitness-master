//
//  CircuitResistanceCell.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 13.02.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class CircuitResistanceCell: UITableViewCell {
    
    // MARK: - Views
    @IBOutlet weak private var firstValueView: CircuitValueView!
    @IBOutlet weak private var secondValueView: CircuitValueView!
    
    @IBOutlet weak private var titleLabel: UILabel!
    @IBOutlet weak private var exerciseImageView: UIImageView!
    
    @IBOutlet weak private var newKgArrowButton: UIButton!
    @IBOutlet weak private var addNewKgView: AddNewKg!
    
    // MARK: - Constraints
    @IBOutlet weak private var arrowButtonTrailingAnchor: NSLayoutConstraint?
    @IBOutlet weak private var addNewKgViewLeadingAnchor: NSLayoutConstraint?
    @IBOutlet weak private var addNewKgViewWidthAnchor: NSLayoutConstraint?
    @IBOutlet weak private var addNewKgViewHeightAnchor: NSLayoutConstraint!
    
    // MARK: - Properties
    var deleteCell: (() -> Void)?
    var showExerciseInfo: (() -> Void)?
    var editValues: (() -> Void)?
    var updateCell: (() -> Void)?
    
    var didAddNewKg: (() -> Void)?
    var didChangeKgIndex: ((Int) -> Void)?
    
    // MARK: - Init
    override func awakeFromNib() {
        super.awakeFromNib()
        let gesture = UITapGestureRecognizer(target: self, action: #selector(hideNewKgView))
        gesture.cancelsTouchesInView = false
        gesture.delegate = self
        addGestureRecognizer(gesture)
        switch LocalizationManager.Language.language {
            
        case .english:
            addNewKgViewLeadingAnchor?.constant = 24
            arrowButtonTrailingAnchor?.constant = 0
            addNewKgViewWidthAnchor?.constant = 95
        case .hebrew:
            arrowButtonTrailingAnchor?.constant = 8
            addNewKgViewWidthAnchor?.constant = 110
            addNewKgViewLeadingAnchor?.constant = -28
            
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        firstValueView.setTitle("")
        secondValueView.setTitle("")
        exerciseImageView.image = nil
        exerciseImageView.kf.cancelDownloadTask()
        deleteCell = nil
        showExerciseInfo = nil
        editValues = nil
        updateCell = nil
    }
    
    // MARK: - Methods
    func set(content: Exercise) {
        setResistanceExerciseUI(resistance: content)
        exerciseImageView.contentMode = .scaleAspectFill
        
        if let addNewKgView = addNewKgView {
            if content.category == .bodyweight || content.category == .accessories {
                newKgArrowButton.isHidden = true
                
            } else {
                newKgArrowButton.isHidden = false
                
                newKgArrowButton.isHidden = false
                addNewKgView.isHidden = false
                addNewKgView.set(countAmountOfKg: content.kgValue.count == 1 ? 0 : content.kgValue.count)
                addNewKgView.didSelectRow = { [weak self] index in
                    self?.didChangeKgIndex?(index)
                    self?.addNewKgViewHeightAnchor.constant = 0
                    UIView.animate(withDuration: 0.3) {
                        self?.contentView.layoutIfNeeded()
                    }
                }
                addNewKgView.didAddNewKg = { [weak self] in
                    self?.didAddNewKg?()
                    self?.addNewKgViewHeightAnchor.constant = 0
                    UIView.animate(withDuration: 0.3) {
                        self?.contentView.layoutIfNeeded()
                    }
                }
            }
        }
    }
    
    func setViewMode() {
        [firstValueView, secondValueView].forEach { $0?.setViewMode() }
    }
    
    // MARK: - Actions
    @IBAction private func closeDidTap(_ sender: Any) { deleteCell?()}
    
    @IBAction private func imageViewDidTap(_ sender: Any) { showExerciseInfo?() }
    
    @IBAction private func editValuesDidTap(_ sender: Any) { editValues?() }
    
    @IBAction private func newKgArrowDidTap(_ sender: Any) { expandKgContainerView() }
    
    @objc private func hideNewKgView() {
        if let addNewKgViewHeightAnchor = addNewKgViewHeightAnchor {
            addNewKgViewHeightAnchor.constant = 0
            updateCell?()
            UIView.animate(withDuration: 0.3) { [weak self] in self?.layoutIfNeeded() }
        }
    }
    
    // MARK: - Private
    private func setResistanceExerciseUI(resistance: Exercise) {
        titleLabel.text = resistance.title
        exerciseImageView.downloadImage(from: resistance.mainImageUrl)
        firstValueView.setTitle(Constants.ResistanceExercises.Methods().repsString, fontSize: 15)
        if resistance.repsValue == 999 {
            firstValueView.setButtonValue(resistance.repsTime)
        } else if resistance.repsValue == 1000 {
            firstValueView.setButtonValue(Constants.ResistanceExercises.Methods().maxString)
        } else {
            firstValueView.setButtonValue("\(resistance.repsValue)")
        }
        switch resistance.category {
        case .machines, .dumbbells, .barbells, .pulley:
            secondValueView.setTitle(Constants.Circuit().kg + (resistance.kgValue.count == 1 ? "" : "\(resistance.indexOfKg + 1)"), fontSize: 15)
            secondValueView.setButtonValue(resistance.kgValue[resistance.indexOfKg] == 301 ? "?" : "\(resistance.kgValue[resistance.indexOfKg])", font: Constants.returnFontName(style: resistance.kgValue[resistance.indexOfKg] == 301 ? .bold : .regular, size: 17) )
        case .bodyweight, .accessories:
            secondValueView.setTitle("")
        case .favourites:
            break
            
        }
   
        updateCell?()
    }
    
    private func expandKgContainerView() {
        guard let kgAnchor = addNewKgViewHeightAnchor else { return }
        kgAnchor.constant = kgAnchor.constant == 0 ? 137 : 0
        
        UIView.animate(withDuration: 0.3) { [weak self] in
            self?.contentView.layoutIfNeeded()
            self?.updateCell?()
        }
    }
}

// MARK: - UIGestureRecognizer
extension CircuitResistanceCell {
    override func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view is UIStackView || touch.view is UITableViewCell || touch.view is UIButton  {
            return false
        }
        return true
    }
}

