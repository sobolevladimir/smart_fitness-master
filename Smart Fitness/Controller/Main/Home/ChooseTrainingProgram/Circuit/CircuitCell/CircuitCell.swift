//
//  CircuitCell.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 17.01.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class CircuitCell: UITableViewCell {
    
    // MARK: - Views
    @IBOutlet weak private var firstValueView: CircuitValueView!
    @IBOutlet weak private var secondValueView: CircuitValueView!
    @IBOutlet weak private var thirdValueView: CircuitValueView!
    @IBOutlet weak private var forthValueView: CircuitValueView!
    @IBOutlet weak private var fifthValueView: CircuitValueView!
    
    @IBOutlet weak private var titleLabel: UILabel!
    @IBOutlet weak private var exerciseImageView: UIImageView!
    @IBOutlet weak private var stackView: UIStackView!

    // MARK: - Properties
    var deleteCell: (() -> Void)?
    var editValues: (() -> Void)?

    // MARK: - Init
    override func prepareForReuse() {
        super.prepareForReuse()
        stackView.spacing = 6
        stackView.distribution = .fill
        firstValueView.setTitle("")
        secondValueView.setTitle("")
        thirdValueView.setTitle("")
        thirdValueView.setSubtitle("")
        forthValueView.setTitle("")
        fifthValueView.setTitle("")
        exerciseImageView.image = nil
        exerciseImageView.kf.cancelDownloadTask()
    }
    
    // MARK: - Methods
    func set(content: Content) {
        if let cardioExercise = content.cardioExercise {
            titleLabel.text = content.cardioTitle
            exerciseImageView.image = content.cardioImageName?.image()
            setCardioExerciseUI(cardio: cardioExercise)
            exerciseImageView.contentMode = .scaleAspectFit
        }
    }
    
    func setViewMode() {
        [firstValueView, secondValueView, thirdValueView, forthValueView, fifthValueView].forEach { $0?.setViewMode() }
    }
    
    // MARK: - Actions
    @IBAction private func closeDidTap(_ sender: Any) { deleteCell?()}
    @IBAction private func editValuesDidTap(_ sender: Any) { editValues?() }
    
    // MARK: - Private
    private func setCardioExerciseUI(cardio: CardioTableViewCell.Content) {
        if cardio.timeValue == "" {
            firstValueView.setTitle("")
            firstValueView.setButtonValue("\(cardio.timeValue)")
            secondValueView.setTitle(cardio.activityType == .jumpingRope ? Constants.Circuit().skips : Constants.Cardio().distanceString, fontSize: 13)
            secondValueView.setButtonValue("\(cardio.distanceValue)")
        } else {
            firstValueView.setTitle(Constants.Cardio().timeString, fontSize: 13)
            firstValueView.setButtonValue("\(cardio.timeValue)")
            secondValueView.setTitle("")
        }
        thirdValueView.setButtonValue("\(cardio.speedValue)")
        forthValueView.setButtonValue("\(cardio.inclineValue)")
        fifthValueView.setButtonValue("\(cardio.paceValue)")
        switch cardio.activityType {
        case .running:
            thirdValueView.setTitle(Constants.Cardio().speedString, fontSize: 13)
            forthValueView.setTitle(Constants.Cardio().inclineString, fontSize: 13)
            fifthValueView.setTitle(Constants.Cardio().paceString, fontSize: 13, isRunningPace: true)
        case .cycling:
            thirdValueView.setTitle(Constants.Cardio().resistanceString, fontSize: 13)
            thirdValueView.setSubtitle(Constants.Circuit().level)
            forthValueView.setTitle(Constants.Cardio().rPMString, fontSize: 13)
        case .elliptical:
            thirdValueView.setTitle(Constants.Cardio().resistanceString, fontSize: 13)
            forthValueView.setTitle(Constants.Cardio().inclineString, fontSize: 13)
            fifthValueView.setTitle(Constants.Cardio().rPMString, fontSize: 13)
        case .rowingMachine:
            thirdValueView.setTitle(Constants.Cardio().intensityString, fontSize: 13)
            forthValueView.setTitle(Constants.Cardio().paceString, fontSize: 13)
            fifthValueView.setTitle(Constants.Cardio().sPMString, fontSize: 13)
        case .stairClimbing:
            thirdValueView.setTitle(Constants.Cardio().resistanceString, fontSize: 13)
            forthValueView.setTitle(Constants.Cardio().stepsString, fontSize: 13)
            fifthValueView.setTitle(Constants.Cardio().sPMStairClimbingString, fontSize: 13)
        case .swimming: break
        case .jumpingRope:
            thirdValueView.setTitle(Constants.Cardio().intensityString, fontSize: 13)
            stackView.spacing = 16
            stackView.distribution = .fillEqually
        }
    }
}

// MARK: - Content
extension CircuitCell {
    struct Content: Codable {
        var resistanceExercise: Exercise?
        var cardioExercise: CardioTableViewCell.Content?
        var cardioImageName: String?
        var cardioTitle: String?
        var id: Int?
    }
}
