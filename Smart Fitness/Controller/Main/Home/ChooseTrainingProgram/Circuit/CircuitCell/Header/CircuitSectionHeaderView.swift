//
//  CircuitSectionHeaderView.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 17.01.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class CircuitSectionHeaderView: UITableViewHeaderFooterView {
    
    // MARK: - Views
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var deleteRoundButton: LoginButton!
    
    // MARK: - Properties
    var deleteRound: (() -> Void)?
    
    // MARK: - Init
    static let reuseIdentifier: String = String(describing: self)
    
    static var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: nil)
    }
    
    // MARK: - Action
    @IBAction func deleteDidTap(_ sender: Any) { deleteRound?() }
}
