//
//  CircuitSectionFooterView.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 17.01.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//


import UIKit

final class CircuitSectionFooterView: UITableViewHeaderFooterView {
    
    // MARK: - Views
    @IBOutlet weak var restTimeButton: SpaceImageButton!
    @IBOutlet weak private var addExerciseContainerView: RoundedView!
    
    // MARK: - Properties
    var restTime: (() -> Void)?
    var addResistance: (() -> Void)?
    var addCardio: (() -> Void)?
    
    // MARK: - Init
    static let reuseIdentifier: String = String(describing: self)
    
    static var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: nil)
    }
    
    func set(restTime: String, isLast: Bool) {
        restTimeButton.setTitle("\(Constants.ResistanceExercises.Methods().restString) \(restTime)", for: .normal)
        restTimeButton.isHidden = isLast
        addExerciseContainerView.isHidden = !isLast
    }
    
    // MARK: - Action
    @IBAction private func restTimeDidTap(_ sender: Any) { restTime?() }
    
    @IBAction private func addResistanceDidTap(_ sender: Any) { addResistance?() }
      
    @IBAction private func addCardioDiTap(_ sender: Any) { addCardio?() }
}


