//
//  CircuitTrainingViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 16.01.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class CircuitTrainingViewController: BaseViewController {
    
    struct ExerciseType {
        var resistanceCategory: Category?
        var cardioActivity: CardioActivity?
    }
    
    struct Content: Codable {
        var id: Int?
        var restTime: String
        var items: [CircuitCell.Content]
    }
    
    // MARK: - Views
    @IBOutlet weak private var tableView: UITableView!
    @IBOutlet weak private var firstExerciseView: RoundedView!
    @IBOutlet weak private var bottomButtonsStackView: UIStackView!
    private var saveButton: UIBarButtonItem?
    
    // MARK: - Properties
    var isFirstAppearingOnEditing = false
    var didSaveProgram: ((Program) -> Void)?
    var program: Program?
    var heightCache: [IndexPath: CGFloat] = [:]
    var data: [Content] = [] {
        didSet {
            if isFirstAppearingOnEditing {
                saveState = .back
                saveButton?.isEnabled = false
            } else {
                saveState = data.count == 0 ? .back : .save
                saveButton?.isEnabled = data.count != 0
            }
            firstExerciseView.isHidden = !(data.count == 0)
            bottomButtonsStackView.isHidden = data.count == 0
            setupNavigationLeftItem(for: saveState)
        }
    }
    
    var saveState = SaveState.back
    var editState: EditingState = .build
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationRightItems()
        saveButton?.isEnabled = false
        switch editState {
        case .build: break
        case .edit:
            fixDirection()
            guard let circuit = program?.circuitProgram else { return }
            isFirstAppearingOnEditing = true
            self.data = circuit
        }
        tableView.contentInset = UIEdgeInsets(top: 4, left: 0, bottom: 80, right: 0)
        firstExerciseView.isHidden = !(data.count == 0)
        bottomButtonsStackView.isHidden = data.count == 0
        tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        setupNavigationLeftItem(for: saveState)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        isFirstAppearingOnEditing = false
    }
    
    // MARK: - Actions
    @IBAction func discardDidTap(_ sender: Any) {
        switch saveState {
        case .back:
            navigationController?.popViewController(animated: true)
        case .save:
            let popUpVc = LogoutViewController.instantiate()
            switch editState {
                
            case .build:
                popUpVc.state = .deleteProgram
            case .edit:
                popUpVc.state = .leaveWithoutSaving
            }
            
            present(popUpVc, animated: false, completion: nil)
            popUpVc.didDelete = { [weak self] in
                self?.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    @IBAction func editDidTap(_ sender: Any) {
        UIView.animate(withDuration: 0.1, animations: { [weak self] in
            self?.tableView.isEditing.toggle()
            }, completion: { [weak self] (_) in
                self?.tableView.reloadData()
        })
    }
    
    @IBAction func saveDidTap(_ sender: Any) {
        finishEditing()
        let programVC = ProgramInfoViewController.instantiate()
       
        programVC.programType = .circuit
        programVC.circuitProgram = data
        programVC.editingState = editState
        programVC.id = program?.id
        if editState == .edit {
            guard let programInfo = program?.programInfo else { return }
            programVC.programInfo = programInfo
        }
         programVC.didSaveProgram = { [weak self] program in self?.didSaveProgram?(program) }
        navigationController?.pushViewController(programVC, animated: true)
    }
    
    @IBAction func addRoundDidTap(_ sender: Any) {
        finishEditing()
        guard data.last!.items.count > 0 else {
            self.showAlert(text: "Add at least one exercise to the round")
            return
        }
        data.append(.init(restTime: "01:00", items: []))
        tableView.reloadData()
    }
    
    @IBAction func multiplyCircuitDidTap(_ sender: Any) {
        finishEditing()
        let multiplyCV = CardioMultiplyDataViewController.instantiate()
        multiplyCV.didSelectValue = { [weak self] index in
            guard let self = self else { return }
            var newData: [Content] = []
            for _ in 0...index - 1 {
                self.data.forEach { item in
                    var items = item.items
                    items.mutateEach({ $0.resistanceExercise?.programExerciseId = nil })
                    items.mutateEach({ $0.cardioExercise?.id = nil })
                    newData.append(.init(restTime: item.restTime, items: items))
                }
            }
            self.data.enumerated().forEach { item in
                newData[item.offset] = item.element
            }
            self.data = newData
            self.tableView.reloadData()
        }
        present(multiplyCV, animated: false)
    }
    
    @IBAction func addResistanceExerciseDidTap(_ sender: Any) { addResistanceExercise(index: 0) }
    
    @IBAction func addCardioActivityDidTap(_ sender: Any) { addCardioExercise(index: 0) }
    
    // MARK: - Private
    private func setupNavigationRightItems() {
        let helpButton = UIBarButtonItem(image: UIImage(named: "up_dwon_arrow"), style: .done, target: self, action: #selector(editDidTap))
        helpButton.tintColor = .white
        saveButton = UIBarButtonItem(title: Constants.Cardio().saveString, style: .done, target: self, action: #selector(saveDidTap))
        saveButton?.tintColor = .white
        saveButton?.setTitleTextAttributes([NSAttributedString.Key.font: Constants.returnFontName(style: .semibold, size: 15)], for: .normal)
        navigationItem.rightBarButtonItems = [saveButton!, helpButton]
    }
    
    private func addCardioExercise(index: Int) {
        finishEditing()
        let popUpVC = CircuitCardioActivityPopUpViewController.instantiate()
        popUpVC.didGroupSelected = { [weak self] item in
            guard let self = self else { return }
            if self.data.count == 0 {
                self.data.append(.init(restTime: "01:00", items: []))
                let indexSet = IndexSet(integer: index)
                self.tableView.insertSections(indexSet, with: .fade)
            }
            self.data[index].items.append(.init(resistanceExercise: nil, cardioExercise: CardioData.createDefaultRow(to: item.type), cardioImageName: item.bigImageName != "" ? item.bigImageName : item.imageName, cardioTitle: item.title))
            self.tableView.insertRows(at: [IndexPath(item: self.data[index].items.count - 1, section: index)], with: .fade)
        }
        present(popUpVC, animated: false)
    }
    
    private func addResistanceExercise(index: Int) {
        finishEditing()
        let popUpVC = MusclesGroupPopUpViewController.instantiate()
        popUpVC.state = .circuit
        present(popUpVC, animated: false, completion: nil)
        popUpVC.didGroupSelected = { [weak self] key in
            guard let self = self else { return }
            let vc = AddNewExercisesViewController.instantiate()
            vc.state = .circuit
            vc.groupKey = key
            self.navigationController?.pushViewController(vc, animated: true)
            vc.didAddExerciseFromSearch  = { [weak self] item in
                guard let self = self else { return }
                if self.data.count == 0 {
                    self.data.append(.init(restTime: "01:00", items: []))
                    let indexSet = IndexSet(integer: index)
                    self.tableView.insertSections(indexSet, with: .fade)
                }
                self.data[index].items.append(.init(resistanceExercise: item, cardioExercise: nil, cardioImageName: "", cardioTitle: nil))
                self.tableView.insertRows(at: [IndexPath(item: self.data[index].items.count - 1, section: index)], with: .fade)
            }
        }
    }
    
    private func editTableViewRow(at indexPath: IndexPath) {
        finishEditing()
        if let cardioData = data[indexPath.section].items[indexPath.row].cardioExercise {
            let editVC = EditDataViewController.instantiate()
            editVC.state = cardioData.activityType
            editVC.item = cardioData
            present(editVC, animated: false, completion: nil)
            editVC.saveDidTap = { [weak self] item, isMultipliable in
                guard let self = self else { return }
                self.data[indexPath.section].items[indexPath.row].cardioExercise = item
                self.tableView.reloadRows(at: [indexPath], with: .fade)
            }
        } else if let exerciseData = data[indexPath.section].items[indexPath.row].resistanceExercise {
            let popUpVC = EditNormalSetViewController.instantiate()
            popUpVC.state = .circuit
            popUpVC.item = exerciseData
            self.present(popUpVC, animated: false, completion: nil)
            popUpVC.saveDidTap = { [weak self] editedItem in
                guard let self = self else { return }
                self.data[indexPath.section].items[indexPath.row].resistanceExercise = editedItem
                self.tableView.reloadRows(at: [indexPath], with: .fade)
            }
        }
    }
    
    private func setupNavigationLeftItem(for state: SaveState) {
        switch state {
        case .back:
            fixDirection()
        case .save:
            navigationItem.leftBarButtonItem?.title = Constants.Cardio().discardString
            navigationItem.leftBarButtonItem?.image = nil
        }
        self.setBarItemsFont()
    }
    
    private func showExerciseInfo(exercise: Exercise) {
        guard !exercise.isCustom() else { return }
        finishEditing()
        let vc = ExerciseDetailsViewController.instantiate()
        vc.exercise = exercise
        URLCache.shared.removeAllCachedResponses()
        navigationController?.pushViewController(vc, animated: true)
        
    }
}

// MARK: - UITableViewDataSource
extension CircuitTrainingViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        data.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        data[section].items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let resistanceExercise = data[indexPath.section].items[indexPath.row].resistanceExercise {
            let cell = tableView.dequeueReusableCell(with: CircuitResistanceCell.self, for: indexPath)
            cell.isEditing = tableView.isEditing
            cell.set(content: resistanceExercise)
            cell.editValues = { [weak self] in self?.editTableViewRow(at: indexPath) }
            cell.deleteCell = { [weak self] in
                self?.finishEditing()
                if self?.data.count == 1 && self?.data[indexPath.section].items.count == 1 {
                    let popUpVC = LogoutViewController.instantiate()
                    popUpVC.state = .deleteExercise
                    popUpVC.didDelete = { [weak self] in
                        self?.data[indexPath.section].items.remove(at: indexPath.row)
                        tableView.performBatchUpdates({
                            tableView.deleteRows(at: [indexPath], with: .fade)
                        }) { (_) in
                            tableView.reloadData()
                        }
                        self?.showAlert(text: Constants.ResistanceExercises.Messages().exerciseDelete)
                    }
                      self?.present(popUpVC, animated: false, completion: nil)
                } else
                    if self?.data[indexPath.section].items.count ?? 0 <= 1 {
                        let popUpVC = LogoutViewController.instantiate()
                        popUpVC.state = .deleteRound
                        popUpVC.didDelete = {  [weak self] in
                            self?.data.remove(at:  indexPath.section)
                            self?.tableView.reloadData()
                            self?.showAlert(text: Constants.ResistanceExercises.Messages().exerciseDelete)
                        }
                        self?.present(popUpVC, animated: false)
                    } else {
                        let popUpVC = LogoutViewController.instantiate()
                        popUpVC.state = .deleteExercise
                        popUpVC.didDelete = { [weak self] in
                            self?.data[indexPath.section].items.remove(at: indexPath.row)
                            tableView.performBatchUpdates({
                                tableView.deleteRows(at: [indexPath], with: .fade)
                            }) { (_) in
                                tableView.reloadData()
                            }
                            self?.showAlert(text: Constants.ResistanceExercises.Messages().exerciseDelete)
                        }
                        self?.present(popUpVC, animated: false, completion: nil)
                }
            }
            cell.updateCell = { tableView.performBatchUpdates(cell.layoutIfNeeded) }
            cell.showExerciseInfo = { [weak self] in
                guard let self = self else { return }
                self.finishEditing()
                guard let exercise = self.data[indexPath.section].items[indexPath.row].resistanceExercise else { return }
                self.showExerciseInfo(exercise: exercise)
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(with: CircuitCell.self, for: indexPath)
            cell.set(content: data[indexPath.section].items[indexPath.row])
            cell.editValues = { [weak self] in self?.editTableViewRow(at: indexPath) }
            cell.deleteCell = { [weak self] in
                self?.finishEditing()
                cell.isEditing = tableView.isEditing
                if self?.data.count == 1 && self?.data[indexPath.section].items.count == 1 {
                    let popUpVC = LogoutViewController.instantiate()
                    popUpVC.state = .deleteExercise
                    popUpVC.didDelete = { [weak self] in
                        self?.data[indexPath.section].items.remove(at: indexPath.row)
                        tableView.performBatchUpdates({
                            tableView.deleteRows(at: [indexPath], with: .fade)
                        }) { (_) in
                            tableView.reloadData()
                        }
                        self?.showAlert(text: Constants.ResistanceExercises.Messages().exerciseDelete)
                    }
                    self?.present(popUpVC, animated: false, completion: nil)
                } else
                    if self?.data[indexPath.section].items.count ?? 0 <= 1 {
                        let popUpVC = LogoutViewController.instantiate()
                        popUpVC.state = .deleteRound
                        popUpVC.didDelete = {  [weak self] in
                            self?.data.remove(at:  indexPath.section)
                            self?.tableView.reloadData()
                            self?.showAlert(text: Constants.ResistanceExercises.Messages().exerciseDelete)
                        }
                        self?.present(popUpVC, animated: false)
                    } else {
                        let popUpVC = LogoutViewController.instantiate()
                        popUpVC.state = .deleteExercise
                        popUpVC.didDelete = { [weak self] in
                            self?.data[indexPath.section].items.remove(at: indexPath.row)
                            tableView.performBatchUpdates({
                                tableView.deleteRows(at: [indexPath], with: .fade)
                            }) { (_) in
                                tableView.reloadData()
                            }
                            self?.showAlert(text: Constants.ResistanceExercises.Messages().exerciseDelete)
                        }
                        self?.present(popUpVC, animated: false, completion: nil)
                }
            }
            return cell
        }
    }
    
    private func finishEditing() {
        if tableView.isEditing {
            tableView.performBatchUpdates({
                tableView.isEditing = false
            }) { [weak self] (_) in
                self?.tableView.reloadData()
            }
        }
    }
}

// MARK - UITableViewDelegate
extension CircuitTrainingViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header: CircuitSectionHeaderView = .fromNib()
        header.numberLabel.text = "\(section + 1)"
        header.deleteRound = { [weak self] in
            self?.finishEditing()
            let popUpVC = LogoutViewController.instantiate()
            popUpVC.state = .deleteRound
            
            popUpVC.didDelete = {  [weak self] in
                self?.data.remove(at: section)
                self?.tableView.reloadData()
                self?.showAlert(text: Constants.Circuit().roundDeleted, isCenter: false)
            }
            self?.present(popUpVC, animated: false)
        }
        return header
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer: CircuitSectionFooterView = .fromNib()
        footer.set(restTime: data[section].restTime, isLast: section == data.count - 1)
        footer.addCardio = { [weak self] in self?.addCardioExercise(index:section) }
        footer.addResistance = { [weak self] in self?.addResistanceExercise(index: section) }
        footer.restTime = { [weak self] in
            guard let self = self else { return }
            self.finishEditing()
            let restTimeVC = EditRestTimeViewController.instantiate()
            restTimeVC.time = self.data[section].restTime
            self.present(restTimeVC, animated: false)
            restTimeVC.saveDidTap = { [weak self] time in
                self?.data[section].restTime = time
                footer.restTimeButton.setTitle("\(Constants.Cardio().restString) \(time)", for: .normal)
            }
        }
        return footer
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        indexPath.row != data[indexPath.section].items.count
    }
    
    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let movedObject = self.data[sourceIndexPath.section].items[sourceIndexPath.row]
        self.data[sourceIndexPath.section].items.remove(at: sourceIndexPath.row)
        data[destinationIndexPath.section].items.insert(movedObject, at: destinationIndexPath.row)
        //   tableView.performBatchUpdates(nil) { (_) in
        tableView.reloadData()
        //   }
    }
    
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        heightCache[indexPath] = cell.bounds.size.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return heightCache[indexPath] ?? UITableView.automaticDimension
    }
}
