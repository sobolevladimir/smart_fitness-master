//
//  CircuitCardioActivityPopUpViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 16.01.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

class CircuitCardioActivityPopUpViewController: UIViewController, Storyboarded {

    // MARK: - Views
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak private var containerView: UIView!
    @IBOutlet weak private var containerViewTopAnchor: NSLayoutConstraint!
    
    // MARK: - Properties
    var didGroupSelected: ((CircuitCardioCell.Content) -> Void)?
    var data: [CircuitCardioCell.Content] = []
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        data = CardioData.getData().map({ CircuitCardioCell.Content(title: $0.title, imageName: $0.imageName, bigImageName: $0.bigImageName, type: $0.activityType) })
        if let index = data.lastIndex(where: { $0.type == .swimming }) {
            data.remove(at: index)
        }
        data.append( CircuitCardioCell.Content(title: Constants.Circuit().jumpingRope, imageName: "cardioJumpingRope", bigImageName: "", type: .jumpingRope))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        containerViewTopAnchor.constant = -containerView.frame.height
        UIView.animate(withDuration: 0.3, animations: { [weak self] in self?.view.layoutIfNeeded()})
        DispatchQueue.main.async { [weak self] in self?.collectionView.collectionViewLayout.invalidateLayout() }
    }
    
    // MARK: - Actions
    @IBAction func cancelDidTap(_ sender: Any) {
        dismissWithAnimation()
    }
    
    // MARK: - Private
    private func dismissWithAnimation() {
        containerViewTopAnchor.constant = 0
        UIView.animate(withDuration: 0.3, animations: { [weak self] in self?.view.layoutSubviews()
        }) { [weak self] (_) in self?.dismiss(animated: false, completion: nil) }
    }
  
}

// MARK: - UICollectionViewDataSource
extension CircuitCardioActivityPopUpViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(with: CircuitCardioCell.self, for: indexPath)
        cell.set(content: data[indexPath.row])
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension CircuitCardioActivityPopUpViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           let width = (collectionView.frame.width - 16 - 28) / 2
           let height = (collectionView.frame.height  - (7 * 2) - 4) / 3
           
           return .init(width: width, height: height)
       }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.dismissWithAnimation()
        didGroupSelected?(data[indexPath.item])
    }
}
