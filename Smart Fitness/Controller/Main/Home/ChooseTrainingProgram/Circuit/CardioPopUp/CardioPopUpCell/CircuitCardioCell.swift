//
//  CircuitCardioCell.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 17.01.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class CircuitCardioCell: UICollectionViewCell {
    
    // MARK: - Views
    @IBOutlet weak private var imageView: UIImageView!
    @IBOutlet weak private var titleLabel: UILabel!
    
    // MARK: - Init Method
    func set(content: Content) {
        imageView.image = content.imageName.image()
        let title = content.title.replacingOccurrences(of: " ", with: "\n")
        titleLabel.text = title
    }
}

// MARK: - Content
extension CircuitCardioCell {
    struct Content {
        var title: String
        var imageName: String
        var bigImageName: String
        var type: CardioActivity
    }
}

