//
//  ProgramInfoViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 28.11.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit
import SwiftyGif

enum ProgramType: String, Codable {
    case cardio, resistance, circuit
}

final class ProgramInfoViewController: BaseViewController {
    
    struct Content: Codable {
        var traineeName: String?
        var trainerName: String?
        var buildingDate: Int64
        var updateDate: Int64
        var programName: String
        var imageUrl: String?
    }
    
    // MARK: - Views
    @IBOutlet weak private var traineeTextField: LocalizableTextField!
    @IBOutlet weak private var trainerTextField: LocalizableTextField!
    @IBOutlet weak private var buildingTextField: LocalizableTextField!
    @IBOutlet weak private var updateTextFiled: LocalizableTextField!
    @IBOutlet weak private var programNameTextField: LocalizableTextField!
    
    @IBOutlet weak var traineeLabel: UILabel!
    @IBOutlet weak var trainerName: UILabel!
    @IBOutlet weak var buildingDate: UILabel!
    @IBOutlet weak var updateDate: UILabel!
    @IBOutlet weak var programName: UILabel!
    
    
    private var buildingPickerView: UIDatePicker!
    private var updatePickerView: UIDatePicker!
    
    // MARK: - Properties
    var cardioTraining: [Cardio] = []
    var resistanceTraining: [Resistance] = []
    var circuitProgram: [CircuitTrainingViewController.Content] = []
    var programType: ProgramType = .cardio
    var didSaveProgram: ((Program) -> Void)?
    var programInfo: Content?
    var editingState: EditingState = .build
    var id: Int?
    
    lazy private var dateFormatter: DateFormatter = {
        let df = DateFormatter()
        df.dateFormat = "dd/MM/yyyy"
        return df
    }()
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = Constants.ProgramInfo().programInfo
        setupPickerViews()
        let today = Date()
        updateTextFiled.text = dateFormatter.string(from: today.add(days: 1))
        buildingTextField.text = dateFormatter.string(from: today)
        traineeTextField.delegate = self
        trainerTextField.delegate = self
        buildingTextField.delegate = self
        updateTextFiled.delegate = self
        programNameTextField.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        fixDirection()
        guard let programInfo = programInfo else { return }
        setEditingMode(programInfo: programInfo)
    }
    
    // MARK: - Actions
    @IBAction func editingChanged(_ sender: UITextField) {
        guard let text = sender.text else { return }
        switch sender {
        case traineeTextField:
            animateLabelAlpha(label: traineeLabel, isHidden: text.count == 0)
        case trainerTextField:
            animateLabelAlpha(label: trainerName, isHidden: text.count == 0)
        case buildingTextField:
            animateLabelAlpha(label: buildingDate, isHidden: text.count == 0)
        case updateTextFiled:
            animateLabelAlpha(label: updateDate, isHidden: text.count == 0)
        case programNameTextField:
            animateLabelAlpha(label: programName, isHidden: text.count == 0)
        default:
            break
        }
    }
    
    private func animateLabelAlpha(label: UILabel, isHidden: Bool) {
        UIView.animate(withDuration: 0.2) {
            label.alpha = isHidden ? 0 : 1
        }
    }
    
    @IBAction private func backDidTap(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction private func okDidTap(_ sender: UIButton) {
        let result = checkCredentials()
        switch result {
            
        case .success(let content):
            startAnimation()
            while let index = cardioTraining.lastIndex(where: { $0.items.count == 0 }) {
                cardioTraining.remove(at: index)
            }
            switch programType {
            case .cardio:
                var program = Program(cardioProgram: cardioTraining, programInfo: content, programType: programType)
                switch editingState {
                    
                case .build:
                    ProgramProvider.createProgram(program: program) { [weak self] (result) in
                        self?.stopAnimation()
                        switch result {
                        case .success(let apiProgram):
                            self?.showAlert(text: Constants.Internet().saved)
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                let programViewVC = CardioReadyTrainingViewController.instantiate()
                                self?.didSaveProgram?(apiProgram)
                                programViewVC.data = apiProgram
                                programViewVC.isCreated = true
                                self?.navigationController?.pushViewController(programViewVC, animated: true)
                            }
                        case .failure(let error):
                            self?.showAlert(text: "There is a problem with server, \(error) ")
                        }
                    }
                case .edit:
                    guard let id = id else { fatalError() }
                    program.id = id
                    ProgramProvider.editProgram(id: id, program: program) { [weak self] (result) in
                        self?.stopAnimation()
                        switch result {
                        case .success(let apiProgram):
                            let programViewVC = CardioReadyTrainingViewController.instantiate()
                            self?.didSaveProgram?(apiProgram)
                            programViewVC.data = apiProgram
                            self?.navigationController?.pushViewController(programViewVC, animated: true)
                        case .failure(let error):
                            self?.showAlert(text: "There is a problem with server, \(error) ")
                        }
                    }
                }
                
            case .resistance:
                var program = Program(cardioProgram: cardioTraining, resistanceProgram: resistanceTraining, programInfo: content, programType: programType)
                
                switch editingState {
                case .build:
                    ProgramProvider.createProgram(program: program) { [weak self] (result) in
                        guard let self = self else  { return }
                        self.stopAnimation()
                        switch result {
                        case .success(let apiProgram):
                            self.showAlert(text: Constants.Internet().saved)
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                let resistanceContent = self.makeContentForSplitResistance(program: apiProgram)
                                if resistanceContent.count == 1 {
                                    let detailsVC = ResistanceReadyTrainingViewController.instantiate()
                                    detailsVC.data = resistanceContent[0].exerciseData
                                    detailsVC.programInfo = apiProgram.programInfo
                                    detailsVC.isCreated = true
                                    detailsVC.id = apiProgram.id
                                    if program.cardioProgram.count > 0 {
                                        detailsVC.cardioData = apiProgram.cardioProgram
                                    }
                                    self.didSaveProgram?(apiProgram)
                                    self.navigationController?.pushViewController(detailsVC, animated: true)
                                } else {
                                    let detailsVC = SplitTrainingReadyViewController.instantiate()
                                    detailsVC.content = resistanceContent
                                    detailsVC.programInfo = apiProgram.programInfo
                                    detailsVC.cardioData = apiProgram.cardioProgram
                                    detailsVC.isCreated = true
                                    detailsVC.id = apiProgram.id
                                    self.didSaveProgram?(apiProgram)
                                    self.navigationController?.pushViewController(detailsVC, animated: true)
                                }
                            }
                        case .failure(let error):
                            self.showAlert(text: "There is a problem with server, \(error) ")
                        }
                    }
                case .edit:
                    guard let id = id else { fatalError() }
                    program.id = id
                    ProgramProvider.editProgram(id: id, program: program) { [weak self] (result) in
                        guard let self = self else  { return }
                        self.stopAnimation()
                        switch result {
                        case .success(let apiProgram):
                            let resistanceContent = self.makeContentForSplitResistance(program: apiProgram)
                            if resistanceContent.count == 1 {
                                let detailsVC = ResistanceReadyTrainingViewController.instantiate()
                                detailsVC.data = resistanceContent[0].exerciseData
                                detailsVC.programInfo = apiProgram.programInfo
                                detailsVC.id = apiProgram.id
                                if program.cardioProgram.count > 0 {
                                    detailsVC.cardioData = apiProgram.cardioProgram
                                }
                                self.didSaveProgram?(apiProgram)
                                detailsVC.didSaveProgram?(apiProgram)
                                self.navigationController?.pushViewController(detailsVC, animated: true)
                            } else {
                                let detailsVC = SplitTrainingReadyViewController.instantiate()
                                detailsVC.content = resistanceContent
                                detailsVC.programInfo = apiProgram.programInfo
                                detailsVC.cardioData = apiProgram.cardioProgram
                                detailsVC.didSaveProgram?(apiProgram)
                                detailsVC.id = apiProgram.id
                                self.didSaveProgram?(apiProgram)
                                self.navigationController?.pushViewController(detailsVC, animated: true)
                            }
                        case .failure(let error):
                            self.showAlert(text: "There is a problem with server, \(error) ")
                        }
                        
                    }
                }
            case .circuit:
                var program = Program(circuitProgram: circuitProgram, programInfo: content, programType: programType)
                switch editingState {
                case .build:
                    ProgramProvider.createProgram(program: program) { [weak self] (result) in
                        self?.stopAnimation()
                        guard let self = self else  { return }
                        switch result {
                        case .success(let apiProgram):
                            self.showAlert(text: Constants.Internet().saved)
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                let programViewVC = CircuitReadyTrainingViewController.instantiate()
                                programViewVC.program = apiProgram
                                programViewVC.didSaveProgram?(apiProgram)
                                self.didSaveProgram?(apiProgram)
                                self.navigationController?.pushViewController(programViewVC, animated: true)
                            }
                        case .failure(let error):
                            self.showAlert(text: "There is a problem with server, \(error) ")
                        }
                    }
                case .edit:
                    guard let id = id else { fatalError() }
                    program.id = id
                    ProgramProvider.editProgram(id: id, program: program) { [weak self] (result) in
                        self?.stopAnimation()
                        guard let self = self else  { return }
                        switch result {
                        case .success(let apiProgram):
                            let programViewVC = CircuitReadyTrainingViewController.instantiate()
                            programViewVC.program = apiProgram
                            programViewVC.didSaveProgram?(apiProgram)
                            self.didSaveProgram?(apiProgram)
                            self.navigationController?.pushViewController(programViewVC, animated: true)
                        case .failure(let error):
                            self.showAlert(text: "There is a problem with server, \(error) ")
                        }
                    }
                }
            }
            
        case .failure(let error):
            switch error {
            case .trainee:
                traineeTextField.attributedPlaceholder = NSAttributedString(string: "Please, fill this field", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            case .trainer:
                trainerTextField.attributedPlaceholder = NSAttributedString(string: "Please, fill this field", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            case .building:
                buildingTextField.attributedPlaceholder = NSAttributedString(string: "Please, fill this field", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            case .update:
                updateTextFiled.attributedPlaceholder = NSAttributedString(string: "Please, fill this field", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            case .programName:
                programNameTextField.attributedPlaceholder = NSAttributedString(string: "Please, fill this field", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            }
        }
    }
    
    @IBAction private func viewDidTap(_ sender: Any) { view.endEditing(true) }
    
    @objc private func handleDatePicker(sender: UIDatePicker) {
        if sender == updatePickerView {
            if sender.date < buildingPickerView.date.add(days: 1) {
                updatePickerView.setDate(buildingPickerView.date.add(days: 1), animated: true)
                showAlert(text: Constants.ProgramInfo().wrongUploadDate)
            } else {
                updateTextFiled.text = dateFormatter.string(from: sender.date)
            }
        } else {
            buildingTextField.text = dateFormatter.string(from: sender.date)
        }
        
        if sender == buildingPickerView {
            if sender.date > updatePickerView.date.add(days: 1) {
                updatePickerView.setDate(buildingPickerView.date.add(days: 1), animated: true)
                updateTextFiled.text = dateFormatter.string(from: updatePickerView.date)
            }
        }
    }
    
    @IBAction private func buildingDateButtonDidTap(_ sender: Any) { buildingTextField.becomeFirstResponder() }
    @IBAction private func updateDateButtonDidTap(_ sender: Any) { updateTextFiled.becomeFirstResponder() }
    @objc private func doneBuildingDatePickerPressed() { updateTextFiled.becomeFirstResponder() }
    @objc private func doneUpdateDatePickerPressed() { programNameTextField.becomeFirstResponder() }
    
    // MARK: - Private
    private func setEditingMode(programInfo: Content) {
        traineeTextField.text = programInfo.traineeName
        trainerTextField.text = programInfo.trainerName
        buildingTextField.text = dateFormatter.string(from: Date.init(milliseconds: programInfo.buildingDate))
        updateTextFiled.text = dateFormatter.string(from: Date.init(milliseconds: programInfo.updateDate))
        programNameTextField.text = programInfo.programName
    }
    
    private func setupPickerViews() {
        buildingPickerView = UIDatePicker()
        updatePickerView = UIDatePicker()
        switch LocalizationManager.Language.language {
        case .english:
            updatePickerView.locale = Locale(identifier: "en")
            buildingPickerView.locale = Locale(identifier: "en")
        case .hebrew:
            updatePickerView.locale = Locale(identifier: "he_IL")
            buildingPickerView.locale = Locale(identifier: "he_IL")
        }
        buildingPickerView.datePickerMode = UIDatePicker.Mode.date
        updatePickerView.datePickerMode = UIDatePicker.Mode.date
        updatePickerView.setDate(Date().add(days: 1), animated: false)
        buildingPickerView.addTarget(self, action: #selector(handleDatePicker), for: UIControl.Event.valueChanged)
        updatePickerView.addTarget(self, action: #selector(handleDatePicker), for: UIControl.Event.valueChanged)
        buildingTextField.inputView = buildingPickerView
        updateTextFiled.inputView = updatePickerView
        buildingTextField.inputAccessoryView = makeToolBar(with: #selector(doneBuildingDatePickerPressed))
        updateTextFiled.inputAccessoryView = makeToolBar(with: #selector(doneUpdateDatePickerPressed))
    }
    
    private func makeToolBar(with selector: Selector) -> UIToolbar {
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        let space = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: Constants.ProgramInfo().done, style: UIBarButtonItem.Style.done, target: self, action: (selector))
        doneButton.tintColor = Constants.Colors.blueDark
        toolBar.setItems([space, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        return toolBar
    }
    
    private func checkCredentials() -> Result<Content, ProgramInfoError> {
        guard !buildingTextField.text!.isEmpty else { return .failure(.building)}
        guard !updateTextFiled.text!.isEmpty else { return .failure(.update)}
        var programName: String = ""
        if let programText = programNameTextField.text {
            if programText.count > 0 {
                programName = programText
            } else {
                switch programType {
                case .cardio:
                    programName = Constants.ProgramInfo().cardioProgram
                case .resistance:
                    programName = Constants.ProgramInfo().resistanceProgram
                case .circuit:
                    programName = Constants.ProgramInfo().circuitProgram
                }
            }
        }
        
        guard let buildingDateString = buildingTextField.text,
            let updateDateString = updateTextFiled.text,
            let buildingDate = dateFormatter.date(from: buildingDateString),
            let updateDate = dateFormatter.date(from: updateDateString) else { return .failure(.building)}
        
        let content = Content(traineeName: traineeTextField.text ?? "",
                              trainerName: trainerTextField.text ?? "",
                              buildingDate: buildingDate.millisecondsSince1970,
                              updateDate: updateDate.millisecondsSince1970,
                              programName: programName)
        return .success(content)
    }
    
    private func makeContentForSplitResistance(program: Program) -> [SplitTrainingReadyViewController.Content] {
        let resistance = program.resistanceProgram
        var splitData = Dictionary(grouping: resistance) { $0.group.group }
        splitData[.none] = splitData[.none]?.filter({ $0.exercisesData.count > 0 })
        if splitData[.none]?.count == 0 {
            splitData.removeValue(forKey: .none)
        }
        var content = splitData.map({ SplitTrainingReadyViewController.Content(day: $0.key, exerciseData: $0.value) })
        content.sort(by: { $0.day.number < $1.day.number })
        return content
    }
}

// MARK: - UITextFieldDelegate
extension ProgramInfoViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1
        if let nextResponder = textField.superview?.viewWithTag(nextTag) {
            nextResponder.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
}

enum ProgramInfoError: Error {
    case trainee
    case trainer
    case building
    case update
    case programName
}

