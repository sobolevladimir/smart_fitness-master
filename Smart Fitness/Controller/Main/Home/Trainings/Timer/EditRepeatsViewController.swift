//
//  EditRepeatsViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 14.01.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

class EditRepeatsViewController: BaseViewController {
    
    // MARK: - Views
    @IBOutlet weak private var containerView: UIView!
    @IBOutlet weak private var pickerView: UIPickerView!
    @IBOutlet weak private var mainStackView: UIStackView!
    
    // MARK: - Constraints
    @IBOutlet weak private var topConstraint: NSLayoutConstraint!
    
    // MARK: - Public Properties
    var saveDidTap: ((Int) -> Void)?
    var repeats: Int = 2
    var cancelDidTap: (() -> Void)?
    
    private let pickerViewData = Array(2...99)
    private let pickerViewRows = 10_000
    private var pickerViewMiddle: Int {
      ((pickerViewRows / pickerViewData.count) / 2) * pickerViewData.count
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        self.pickerView.selectRow(pickerViewMiddle + repeats - 2, inComponent: 0, animated: false)
        mainStackView.semanticContentAttribute = .forceLeftToRight
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        topConstraint.constant = containerView.frame.height
        UIView.animate(withDuration: 0.3) { [weak self] in
            self?.view.layoutSubviews()
        }
    }
    
    // MARK: - Actions
    @IBAction private func cancelDidTap(_ sender: Any) {
        cancelDidTap?()
        dismissWithAnimation() }
    
    @IBAction private func saveDidTap(_ sender: Any) {
        saveEditing()
        dismissWithAnimation()
    }
    
    // MARK: - Private
    private func dismissWithAnimation() {
        topConstraint.constant = 0
        UIView.animate(withDuration: 0.3, animations: { [weak self] in self?.view.layoutSubviews()
        }) { [weak self] (_) in self?.dismiss(animated: false, completion: nil) }
    }
    
    private func saveEditing() {
        saveDidTap?(repeats)
    }
}

// MARK: - UIPickerViewDataSource
extension EditRepeatsViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        pickerView.subviews.forEach({
            $0.isHidden = $0.frame.height < 1.0
        })
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
       pickerViewRows
    }
}


// MARK: - UIPickerViewDelegate
extension EditRepeatsViewController: UIPickerViewDelegate {
    
    func valueForRow(row: Int) -> Int {
        return pickerViewData[row % pickerViewData.count]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        repeats = valueForRow(row: row)
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label = UILabel()
        if let v = view { label = v as! UILabel }
        label.font = Constants.returnFontName(style: .regular, size: 17)
        label.textAlignment = .center
        label.text = "\(valueForRow(row: row))"
        return label
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        41
    }
}


