//
//  TimerViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 08.01.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit
import AVFoundation

final class TimerViewController: UIViewController, Storyboarded {
    
    enum TimerNavigation {
        case back, menu
    }
    
    enum TimerState {
        case play, pause, stop
        
        mutating func nextState() {
            switch self {
                
            case .play:
                self = TimerViewController.TimerState.pause
            case .pause:
                self = TimerViewController.TimerState.play
            case .stop:
                self = TimerViewController.TimerState.play
            }
        }
    }
    
    // MARK: - Views
    @IBOutlet weak private var subscribeView: RoundedView!
    @IBOutlet weak private var timerView: TimerView!
    @IBOutlet weak private var timerTimeLabel: UILabel!
    @IBOutlet weak private var restTimeLabel: UILabel!
    @IBOutlet weak private var repeatButton: LoginButton!
    @IBOutlet weak private var stopButton: LoginButton!
    @IBOutlet weak private var pauseButton: LoginButton!
    @IBOutlet weak private var startButton: LoginButton!
    @IBOutlet weak private var checkButton: RadioCheckButton!
    @IBOutlet weak private var buttonsStackView: UIStackView!
    @IBOutlet weak private var valuesStackView: UIStackView!
    @IBOutlet weak private var restAndRepeatsStackView: UIStackView!
    @IBOutlet weak private var timerStackView: UIStackView!
    
    // MARK: - Properties
    var  navigationState: TimerNavigation = .back
    var menuDidTapAction: (() -> Void)?
    
    private var repeats = 2
    private let defaultRepeats = 2
    private var time: Int = 45
    private var restTime: Int = 30
    private var restRepeats: Int = 0
    private var timerRepeats: CGFloat = 0
    
    private var timerState = TimerState.stop
    private var globalTimer: Timer?
    private var restTimer: Timer?
    
    private var isTimer: Bool = true
    private lazy var mainTimerPlayer: AVAudioPlayer = {
        var player = AVAudioPlayer()
        let path = Bundle.main.path(forResource: "mainTimer", ofType: "mp3")!
        let url = URL(fileURLWithPath: path)
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
            try AVAudioSession.sharedInstance().setActive(true)
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
            player.prepareToPlay()
        } catch let error {
            print(error.localizedDescription)
        }
        return player
    }()
    
    private lazy var restTimerPlayer: AVAudioPlayer = {
        var player = AVAudioPlayer()
        let path = Bundle.main.path(forResource: "restTimer", ofType: "mp3")!
        let url = URL(fileURLWithPath: path)
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
            try AVAudioSession.sharedInstance().setActive(true)
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
            player.prepareToPlay()
        } catch let error {
            print(error.localizedDescription)
        }
        return player
    }()
    
    private lazy var formatter: DateComponentsFormatter = {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.minute, .second]
        formatter.unitsStyle = .positional
        formatter.zeroFormattingBehavior = .pad
        return formatter
    }()
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if UserDefaults.standard.bool(forKey: Constants.isSubscribedKey) {
            subscribeView.isHidden = true
        }
        repeatButton.setTitle("\(repeats) \(Constants.Timer().times)", for: .normal)
        timerTimeLabel.text = getFormattedTime(time: time)
        restTimeLabel.text = getFormattedTime(time: restTime)
        fixDirection()
        buttonsStackView.semanticContentAttribute = .forceLeftToRight
        valuesStackView.semanticContentAttribute = .forceLeftToRight
        timerStackView.semanticContentAttribute = .forceLeftToRight
        restAndRepeatsStackView.semanticContentAttribute = .forceLeftToRight
        
        if navigationState == .menu {
            navigationItem.leftBarButtonItem = .init(image: #imageLiteral(resourceName: "ic_menu"), landscapeImagePhone: nil, style: .done, target: self, action: #selector(menuDidTap))
            navigationItem.leftBarButtonItem?.tintColor = .white
         //   navigationController?.navigationBar.setBlueGradient()
            navigationController?.setTitleFont()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    // MARK: - Actions
    @objc func menuDidTap() { menuDidTapAction?() }
    
    @IBAction func backDidTap(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func subscribeButtonDidTap(_ sender: Any) {
        let navBar = UINavigationController(rootViewController: SubscriptionViewController.instantiate())
        navBar.modalPresentationStyle = .fullScreen
        present(navBar, animated: true)
    }
    
    @IBAction func timerDidTap(_ sender: Any) {
        guard let time = timerTimeLabel.text else { return }
        editTime(input: time, state: .timer) { [weak self] (newTime) in
            guard let self = self else { return }
            self.timerTimeLabel.text = newTime
            self.time = self.getTimeIntFromString(from: newTime)
        }
    }
    
    @IBAction func startDidTap(_ sender: Any) {
        startButton.isHidden = true
        pauseButton.isHidden = false
        stopButton.isHidden = false
        startTimer()
        timerState = .play
        pauseButton.setTitle(Constants.Timer().pause, for: .normal)
        pauseButton.setImage("timerPause".image(), for: .normal)
    }
    
    @IBAction func reduceDidTap(_ sender: Any) {
        guard time > 5 else { return }
        changeTotalTime(with: -5)
    }
    
    @IBAction func addDidTap(_ sender: Any) {
        changeTotalTime(with: +5)
    }
    
    @IBAction func pauseDidTap(_ sender: UIButton) {
        timerState.nextState()
        switch timerState {
        case .play:
            sender.setTitle(Constants.Timer().pause, for: .normal)
            sender.setImage("timerPause".image(), for: .normal)
            // startTimer()
            if isTimer {
                startTimer()
            } else {
                startRestTimer()
            }
            
        case .pause:
            sender.setTitle(Constants.Timer().play, for: .normal)
            sender.setImage("timerPlay".image(), for: .normal)
            globalTimer?.invalidate()
            restTimer?.invalidate()
        default:
            break
        }
    }
    
    @IBAction func stopDidTap(_ sender: Any) {
        setStopState()
    }
    
    @IBAction func timeDidTap(_ sender: Any) {
        if !UserDefaults.standard.bool(forKey: Constants.isSubscribedKey) {
            let navBar = UINavigationController(rootViewController: SubscriptionViewController.instantiate())
            navBar.modalPresentationStyle = .fullScreen
            present(navBar, animated: true)
        } else {
            guard let time = restTimeLabel.text else { return }
            editTime(input: time, state: .rest) { [weak self] (newTime) in
                guard let self = self else { return }
                self.restTime = self.getTimeIntFromString(from: newTime)
                self.restTimeLabel.text = newTime
                self.checkButton.isChecked = true
            }
        }
    }
    
    @IBAction private func subscribeDidTap(_ sender: Any) {
        
    }
    @IBAction private func repeatDidTap(_ sender: Any) {
        if !UserDefaults.standard.bool(forKey: Constants.isSubscribedKey) {
            let navBar = UINavigationController(rootViewController: SubscriptionViewController.instantiate())
            navBar.modalPresentationStyle = .fullScreen
            present(navBar, animated: true)
        } else {
            let repeatVC = EditRepeatsViewController.instantiate()
            if repeats <= 1{
                repeatVC.repeats = defaultRepeats
            } else {
                repeatVC.repeats = repeats
            }
            
            present(repeatVC, animated: false)
            repeatVC.saveDidTap = { [weak self] repeats in
                self?.repeatButton.setTitle("\(repeats) \(Constants.Timer().times)", for: .normal)
                self?.repeats = repeats
                self?.checkButton.isChecked = true
            }
        }
    }
    
    // MARK: - Private
    private func setStopState() {
        globalTimer?.invalidate()
        restTimer?.invalidate()
        startButton.isHidden = false
        pauseButton.isHidden = true
        stopButton.isHidden = true
        timerState = .stop
        timerTimeLabel.text = getFormattedTime(time: time)
        restTimeLabel.text = getFormattedTime(time: restTime)
        repeatButton.setTitle("\(defaultRepeats) \(Constants.Timer().times)", for: .normal)
        timerView.progress = 1
        timerRepeats = 0
        repeats = defaultRepeats
    }
    
    private func editTime(input time: String, state: EditRestTimeViewController.State, completion: @escaping (String) -> Void ) {
        let timeVC = EditRestTimeViewController.instantiate()
        timeVC.state = state
        var mutableTime = time
        
        if mutableTime.prefix(2) == "0:" {
            mutableTime.insert("0", at: time.startIndex)
        }
        timeVC.time = mutableTime
        present(timeVC, animated: false)
        timeVC.saveDidTap = { newTime in
            completion(newTime)
        }
      //  timeVC.cancelDidTap = { completion(time) }
    }
    
    private func changeTotalTime(with value: Int) {
        time += value
        timerTimeLabel.text = getFormattedTime(time: time)
        let progress = self.timerRepeats * 1 / CGFloat(self.time)
        self.timerView.progress = 1 - progress
    }
    
    private func startRestTimer() {
        restTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { [weak self] _ in
            guard let self = self else { return }
            self.restRepeats -= 1
            self.restTimeLabel.text = self.getFormattedTime(time: self.restRepeats)
            if self.restRepeats == 0 {
                self.restTimerPlayer.play()
                AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
                if self.repeats == 0 {
                    self.setStopState()
                    return
                }
                self.restTimer?.invalidate()
                
                self.restTimeLabel.text = self.getFormattedTime(time: self.restTime)
                
                self.startTimer()
            } else {
                self.isTimer = false
            }
        }
    }
    
    private func startTimer() {
        globalTimer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { [weak self] _ in
            guard let self = self else { return }
            let progress = self.timerRepeats * 1 / CGFloat(self.time)
            self.timerView.progress = 1 - progress
            self.timerRepeats += 0.1
            
            let interval = self.time - Int(self.timerRepeats)
            self.timerTimeLabel.text = self.getFormattedTime(time: interval)
            if self.timerView.progress <= 0 {
                self.globalTimer?.invalidate()
                self.mainTimerPlayer.play()
                AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
                self.restRepeats = self.restTime
                self.timerTimeLabel.text = self.getFormattedTime(time: self.time)
                if self.checkButton.isChecked {
                    if self.repeats <= 0 {
                        self.setStopState()
                        return
                    }
                    self.repeats -= 1
                    self.repeatButton.setTitle("\(self.repeats) \(Constants.Timer().times)", for: .normal)
                    self.timerView.progress = 1
                    self.timerRepeats = 0
                    self.startRestTimer()
                } else {
                    self.setStopState()
                }
            } else {
                self.isTimer = true
            }
        }
    }
    
    private func getFormattedTime(time: Int) -> String {
        guard var formattedString = self.formatter.string(from: TimeInterval(time)) else { return ""}
        guard let index = formattedString.lastIndex(of: ":") else { return ""}
        if index == String.Index(utf16Offset: 1, in: formattedString) {
            formattedString.insert("0", at: formattedString.startIndex)
        }
        return formattedString
    }
    
    private func getTimeIntFromString(from string: String) -> Int {
        if let index = string.firstIndex(of: ":") {
            guard let minutes = Int(string.prefix(upTo: index)) else { return 0 }
            guard var seconds = Int(string.suffix(2)) else { return 0 }
            seconds += minutes * 60
            return seconds
        }
        return 0
    }
}
