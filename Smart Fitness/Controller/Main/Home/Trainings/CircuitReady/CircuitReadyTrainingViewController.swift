//
//  CircuitReadyTrainingViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 23.01.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

protocol EditProgramDelegate: class {
    func didEdit(at indexPath: IndexPath)
}

class CircuitReadyTrainingViewController: BaseViewController {
    
    // MARK: - Views
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak private var traineeLabel: UILabel!
    @IBOutlet weak private var trainerLabel: UILabel!
    @IBOutlet weak private var buildingDateLabel: UILabel!
    @IBOutlet weak private var updateDateLabel: UILabel!
    @IBOutlet weak private var programTitleLabel: UILabel!
    
    // MARK: - Properties
    weak var delegate: EditProgramDelegate?
    var indexPath: IndexPath?
    var didSaveProgram: ((Program) -> Void)?
    var program: Program?
    var dateFormatter: DateFormatter = {
        let dt = DateFormatter()
        dt.dateFormat = "dd/MM/yyyy"
        return dt
    }()
    private var cellHeights: [IndexPath: CGFloat?] = [:]
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let programInfo = program?.programInfo else { return }
        setProgramData(content: programInfo)
        tableView.contentInset = UIEdgeInsets(top: 4, left: 0, bottom: 80, right: 0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        fixDirection()

        navigationItem.leftBarButtonItem?.image = nil
        navigationItem.rightBarButtonItem = .init(title: Constants.ProgramInfo().done, style: .done, target: self, action: #selector(backDidTap))
        self.setBarItemsFont()
        
        let editButton = UIButton(type: .custom)
          editButton.frame = CGRect(x: 0.0, y: 0.0, width: 30, height: 30)
          editButton.setImage("editNavBarIcon".image(), for: .normal)
        editButton.addTarget(self, action:  #selector(editDidTap), for: .touchUpInside)
        let editNavButton: UIBarButtonItem = UIBarButtonItem(customView: editButton)
        
        let shareButton = UIButton(type: .custom)
                 shareButton.frame = CGRect(x: 0.0, y: 0.0, width: 50, height: 30)
                 shareButton.setImage("shareNavBarIcon".image(), for: .normal)
               shareButton.addTarget(self, action:  #selector(share), for: .touchUpInside)
        
        if UserDefaults.standard.bool(forKey: Constants.isSubscribedKey) {
            shareButton.frame = CGRect(x: 0.0, y: 0.0, width: 30, height: 30)
            shareButton.setImage("sharePremiumNavBarIcon".image(), for: .normal)
            shareButton.tintColor = .white
        }
        
        let shareNavBar: UIBarButtonItem = .init(customView: shareButton)

        navigationItem.leftBarButtonItems = [shareNavBar, editNavButton]
    }
    
    // MARK: - Actions
    @IBAction func backDidTap(_ sender: Any) {
        guard let program = program else { return }
        didSaveProgram?(program)
        navigationController?.popToRootViewController(animated: true)
    }
    
    @objc private func editDidTap() {
         guard let program = program else { return }
        if let vc = navigationController?.viewControllers[1] as? CircuitTrainingViewController {
            didSaveProgram?(program)
            vc.program = program
            vc.editState = .edit
            navigationController?.popToViewController(vc, animated: true)
        } else if navigationController?.viewControllers.count ?? 1  >= 3, let vc =  navigationController?.viewControllers[2] as? CircuitTrainingViewController {
        vc.program = program
        vc.editState = .edit
        navigationController?.popToViewController(vc, animated: true)
        } else {
            guard let indexPath = indexPath else { fatalError() }
            delegate?.didEdit(at: indexPath)
        }
    }
    
    @IBAction func timerDidTap(_ sender: Any) {
        let timerVC = TimerViewController.instantiate()
        self.navigationController?.pushViewController(timerVC, animated: true)
    }
    
    @objc private func share() {
        if UserDefaults.standard.bool(forKey: Constants.isSubscribedKey) {
            guard let id = program?.id else { return }
            let vc = ShareProgramViewController.instantiate()
            vc.programId = id
            vc.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(vc, animated: true)
        } else {
            let navBar = UINavigationController(rootViewController: SubscriptionViewController.instantiate())
            navBar.modalPresentationStyle = .fullScreen
            present(navBar, animated: true)
        }
    }
    
    // MARK: - Private
    private func setProgramData(content: ProgramInfoViewController.Content) {
        traineeLabel.text = content.traineeName
        trainerLabel.text = content.trainerName
        buildingDateLabel.text = formateDateToString(from: Date.init(milliseconds: content.buildingDate))
        updateDateLabel.text = formateDateToString(from: Date.init(milliseconds: content.updateDate))
        programTitleLabel.text = content.programName
    }
    
    private func formateDateToString(from date: Date) -> String {
        return dateFormatter.string(from: date)
    }
    
    private func showExerciseInfo(exercise: Exercise) {
        guard !exercise.isCustom() else { return }
        let vc = ExerciseDetailsViewController.instantiate()
        vc.exercise = exercise
        navigationController?.pushViewController(vc, animated: true)
    }
    
    private func editTableViewRow(at indexPath: IndexPath) {
        if let exerciseData = program?.circuitProgram[indexPath.section].items[indexPath.row].resistanceExercise {
            let popUpVC = EditNormalSetViewController.instantiate()
            popUpVC.state = .onlyKg
            popUpVC.item = exerciseData
            self.present(popUpVC, animated: false, completion: nil)
            popUpVC.saveDidTap = { [weak self] editedItem in
                guard let self = self else { return }
                self.program?.circuitProgram[indexPath.section].items[indexPath.row].resistanceExercise = editedItem
                self.tableView.reloadRows(at: [indexPath], with: .fade)
            }
        }
    }
}

// MARK: - UITableViewDataSource
extension CircuitReadyTrainingViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let data = program?.circuitProgram else { return 0}
        return data.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let data = program?.circuitProgram else { return 0}
        return data[section].items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let data = program?.circuitProgram else { return UITableViewCell() }
        
        if let resistanceExercise = data[indexPath.section].items[indexPath.row].resistanceExercise {
            let cell = tableView.dequeueReusableCell(with: CircuitResistanceCell.self, for: indexPath)
            cell.set(content: resistanceExercise)
            cell.didAddNewKg = { [weak self] in
                guard let self = self else { return }
                self.program?.circuitProgram[indexPath.section].items[indexPath.row].resistanceExercise?.kgValue.append(301)
                self.program?.circuitProgram[indexPath.section].items[indexPath.row].resistanceExercise?.indexOfKg = (self.program?.circuitProgram[indexPath.section].items[indexPath.row].resistanceExercise?.kgValue.count ?? 1) - 1
                tableView.reloadRows(at: [indexPath], with: .fade)
            }
            
            cell.didChangeKgIndex = { [weak self] index in
                self?.program?.circuitProgram[indexPath.section].items[indexPath.row].resistanceExercise?.indexOfKg = index
                tableView.reloadRows(at: [indexPath], with: .fade)
            }
            cell.setViewMode()
            cell.editValues = { [weak self] in
                self?.editTableViewRow(at: indexPath)
            }
            
            cell.showExerciseInfo = { [weak self] in
                self?.showExerciseInfo(exercise: resistanceExercise)
            }
            
            cell.updateCell = { tableView.performBatchUpdates(cell.layoutIfNeeded) }
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(with: CircuitCell.self, for: indexPath)
        cell.set(content: data[indexPath.section].items[indexPath.row])
        cell.setViewMode()
        return cell
    }
}

// MARK - UITableViewDelegate
extension CircuitReadyTrainingViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header: CircuitSectionHeaderView = .fromNib()
        header.deleteRoundButton.isHidden = true
        header.numberLabel.text = "\(section + 1)"
        
        return header
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == (program?.circuitProgram.count ?? 1) - 1  {
            return nil
        }
        let footer: CircuitSectionFooterView = .fromNib()
        guard let data = program?.circuitProgram else { return footer }
        
        footer.set(restTime: data[section].restTime, isLast: false)
        
        return footer
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeights[indexPath] = cell.frame.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if let height = cellHeights[indexPath] {
            return height ?? UITableView.automaticDimension
        }
        return UITableView.automaticDimension
    }
}
