//
//  CardioReadyTrainingTableViewCell.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 09.01.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class CardioReadyTrainingTableViewCell: UITableViewCell, ConfigureCardioStateUI {
    
    // MARK: - Views
    @IBOutlet weak private var containerView: RoundedView!
    @IBOutlet weak private var cellImageView: UIImageView!
    @IBOutlet weak private var cellTimeLabel: UILabel!
    @IBOutlet weak private var cellSpeedValueLabel: UILabel!
    @IBOutlet weak private var cellInclineValueLabel: UILabel!
    @IBOutlet weak private var cellPaceValueLabel: UILabel!
    
    @IBOutlet weak private var selectionButton: UIButton!
    
    @IBOutlet weak var speedLabel: UILabel!
    @IBOutlet weak var inclineLabel: UILabel!
    @IBOutlet weak var paceLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var levelLabel: UILabel!
    
    // MARK: - Constraints
    @IBOutlet  var timeStackViewXCenterAnchor: NSLayoutConstraint?
    
    @IBOutlet  var timeStackViewLeadingAnchor: NSLayoutConstraint?
    // MARK: - Properties
    private var isDone: Bool = false
    
    override  func awakeFromNib() {
        super.awakeFromNib()
        timeStackViewLeadingAnchor?.isActive = false
        timeStackViewXCenterAnchor?.isActive = true
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        timeStackViewLeadingAnchor?.isActive = false
        timeStackViewXCenterAnchor?.isActive = true
    }
    
    // MARK: - Methods
    func set(content: CardioTableViewCell.Content, isGray: Bool, isLast: Bool, state: CardioActivity, isDistance: Bool, isTime: Bool) {
        cellTimeLabel.text = (content.timeValue == "") ? content.distanceValue : content.timeValue
        cellImageView.image = UIImage(named: content.imageName)
        cellSpeedValueLabel.text = content.speedValue
        cellInclineValueLabel.text = content.inclineValue
        cellPaceValueLabel.text = content.paceValue
        containerView.backgroundColor = isGray ? Constants.Colors.graySuperLight : .white
        containerView.corners = isLast ? .bottom : .none
        containerView.cornerRadius = 3
        
        if (isDistance && !isTime) {
            cellImageView.isHidden = true
            timeLabel.text = Constants.Cardio().distanceString
        } else {
             cellImageView.isHidden = false
            cellImageView.isHidden = !isDistance
        }
        
        if !isDistance && isTime {
            timeStackViewXCenterAnchor?.constant = LocalizationManager.Language.language == .english ? 8 : -10
        } else {
            timeStackViewXCenterAnchor?.constant = 0
        }
        
        configure(state: state, isDistance: isDistance, isTime: isTime)
        if state == .swimming {
            timeStackViewLeadingAnchor?.isActive = true
            timeStackViewXCenterAnchor?.isActive = false
            timeLabel.text = (content.timeValue == "") ? content.distanceValue : content.timeValue
        }
    }
    
    @IBAction func cellDidSelect(_ sender: UIButton) {
        if isDone {
            selectionButton.backgroundColor = .clear
                   selectionButton.setImage(nil, for: .normal)
        } else {
            selectionButton.backgroundColor = UIColor(white: 0, alpha: 0.6)
                   selectionButton.setImage("cardioDone".image(), for: .normal)
        }
        isDone.toggle()
    }
}
