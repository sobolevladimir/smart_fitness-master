//
//  CardioReadyTrainingTableFooter.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 09.01.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class CardioReadyTrainingTableFooter: UIView {

    // MARK: - Views
    @IBOutlet weak var totalTimeStackView: UIStackView!
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var totalDistanceStackView: UIStackView!
    @IBOutlet weak var distanceLabel: UILabel!
    
    // MARK: - Methods
    func set(content: Content) {
        totalTimeStackView.isHidden = false
        totalDistanceStackView.isHidden = false
        if content.time == "00:00" {
            totalTimeStackView.isHidden = true
        }
        if content.distance == "0.0" {
            totalDistanceStackView.isHidden = true
        }
        timeLabel.text = content.time
        distanceLabel.text = content.distance
    }
}

// MARK: - Content
extension CardioReadyTrainingTableFooter {
    struct Content {
        var time: String
        var distance: String
    }
}
