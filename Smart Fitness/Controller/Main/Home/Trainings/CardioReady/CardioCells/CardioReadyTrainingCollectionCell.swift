//
//  CardioReadyTrainingCollectionCell.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 09.01.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class CardioReadyTrainingCollectionCell: UICollectionViewCell {
    
    // MARK: - Views
    @IBOutlet weak private var cardioImageView: UIImageView!
    @IBOutlet weak var borderView: RoundedView!
    
    // MARK: - Properties
    var isSelectedCell: Bool = false {
        didSet {
            borderView.layer.borderColor = isSelectedCell ? Constants.Colors.blueDark.cgColor : UIColor.white.cgColor
        }
    }
    
    // MARK: - Methods
    func set(content: CardioCollectionViewCell.Content) {
        isSelectedCell = content.isSelected
        cardioImageView.contentMode = .scaleAspectFit
        cardioImageView.image = UIImage(named: "\(content.imageName)1") ?? UIImage(named: content.imageName)
    }
}


