//
//  CardioReadyTrainingTableHeader.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 09.01.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

protocol ConfigureCardioStateUI {
    
    var speedLabel: UILabel! { get set}
    var inclineLabel: UILabel! { get set}
    var paceLabel: UILabel! { get set}
    
    var timeLabel: UILabel! { get set}
    var distanceLabel: UILabel! { get set }
    var levelLabel: UILabel! { get set }
    
    func configure(state: CardioActivity, isDistance: Bool, isTime: Bool)
}

extension ConfigureCardioStateUI {
    func configure(state: CardioActivity, isDistance: Bool, isTime: Bool) {
        var speedText = Constants.Cardio().speedString
        var inclineText = Constants.Cardio().inclineString
        var paceText = Constants.Cardio().paceString
        distanceLabel.isHidden = !isDistance
        
        if (isDistance && !isTime) {
             distanceLabel.isHidden = true
            timeLabel.text = Constants.Cardio().distanceString
        } else {
             timeLabel.text = Constants.Cardio().timeString
        }
        paceLabel.isHidden = false
        levelLabel.isHidden = true
        switch state {
            
        case .running:
            speedText = Constants.Cardio().speedString
        case .cycling:
            speedText = Constants.Cardio().resistanceString
            inclineText = Constants.Cardio().rPMString
            paceLabel.isHidden = true
            levelLabel.isHidden = false
        case .elliptical:
            speedText = Constants.Cardio().resistanceString
            paceText = Constants.Cardio().rPMString
        case .rowingMachine:
            speedText = Constants.Cardio().intensityString
            inclineText = Constants.Cardio().paceString
            paceText = Constants.Cardio().sPMString
        case .stairClimbing:
            speedText = Constants.Cardio().resistanceString
            inclineText = Constants.Cardio().stepsString
            paceText = Constants.Cardio().sPMStairClimbingString
        case .swimming:
            speedText = Constants.Cardio().styleString
            inclineText = Constants.Cardio().exerciseString
            paceText = Constants.Cardio().intensityString
       
        case .jumpingRope:
            break
        }

        speedLabel.text = speedText
        inclineLabel.text = inclineText
        paceLabel.text = paceText
    }
}

final class CardioReadyTrainingTableHeader: UIView, ConfigureCardioStateUI {
   
    // MARK: - Views
    @IBOutlet weak var speedLabel: UILabel!
    @IBOutlet weak var inclineLabel: UILabel!
    @IBOutlet weak var paceLabel: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var levelLabel: UILabel!
    
    // MARK: - Methods
    func set(state: CardioActivity, isDistance: Bool, isTime: Bool) {
        configure(state: state, isDistance: isDistance, isTime: isTime)
    }
}
