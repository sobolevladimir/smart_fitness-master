//
//  CardioReadyTrainingViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 08.01.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class CardioReadyTrainingViewController: BaseViewController {
    
    // MARK: - Views
    @IBOutlet weak private var collectionView: UICollectionView!
    @IBOutlet weak private var tableView: UITableView!
    
    @IBOutlet weak private var trainingNameLabel: UILabel!
    @IBOutlet weak private var activityTitleLabel: UILabel!
    
    @IBOutlet weak private var traineeLabel: UILabel!
    
    @IBOutlet weak private var trainerLabel: UILabel!
    @IBOutlet weak private var buildingDateLabel: UILabel!
    @IBOutlet weak private var updateDateLabel: UILabel!
    @IBOutlet weak private var programTitleLabel: UILabel!
    
    lazy private var tableHeaderView: CardioReadyTrainingTableHeader = .fromNib()
    lazy private var tableFooterView: CardioReadyTrainingTableFooter = .fromNib()
    
    // MARK: - Properties
    weak var delegate: EditProgramDelegate?
    var indexPath: IndexPath?
    
    var isCreated: Bool = false
    var data: Program? 
    var selectedIndex: Int = 0 {
        didSet {
            guard let cardioData = data?.cardioProgram else { return }
            activityTitleLabel.text = data?.cardioProgram[selectedIndex].cardioType.title
            tableHeaderView.set(state: cardioData[selectedIndex].state, isDistance: checkDistanceInActivity(items: cardioData[selectedIndex].items), isTime: checkTimeInActivity(items: cardioData[selectedIndex].items))
            tableFooterView.set(content: .init(time: cardioData[selectedIndex].totalTime, distance: cardioData[selectedIndex].totalDistance))
        }
    }
    
    private lazy var dateFormatter: DateFormatter = {
       let dt = DateFormatter()
        dt.dateFormat = "dd/MM/yyyy"
        return dt
    }()
    
    private lazy var formatter: DateComponentsFormatter = {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.minute, .second]
        formatter.unitsStyle = .positional
        formatter.zeroFormattingBehavior = .pad
        return formatter
    }()
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
         tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 100, right: 0)
        guard let programData = data?.programInfo else { return }
        setProgramData(content: programData)
       
        if let cardio = data?.cardioProgram {
            if cardio.count > 0 {
                data?.cardioProgram[0].cardioType.isSelected = true
                for index in 0...cardio.count - 1 {
                    makeTimeAndDistanceValues(for: index)
                }
            }
        }
   
        collectionView.reloadData()
        tableView.reloadData()
        setupTableViewHeaderAndFooter()
        activityTitleLabel.text = data?.cardioProgram[selectedIndex].cardioType.title
        fixDirection()
        guard let cardioData = data?.cardioProgram else { return }
        tableHeaderView.set(state: cardioData[selectedIndex].state, isDistance: checkDistanceInActivity(items: cardioData[selectedIndex].items), isTime: checkTimeInActivity(items: cardioData[selectedIndex].items))
        tableFooterView.set(content: .init(time: cardioData[selectedIndex].totalTime, distance: cardioData[selectedIndex].totalDistance))
        
        
        // nav items
        navigationItem.leftBarButtonItem?.image = nil
        navigationItem.rightBarButtonItem = .init(title: Constants.ProgramInfo().done, style: .done, target: self, action: #selector(backDidTap))
        self.setBarItemsFont()
        
        let editButton = UIButton(type: .custom)
          editButton.frame = CGRect(x: 0.0, y: 0.0, width: 30, height: 30)
          editButton.setImage("editNavBarIcon".image(), for: .normal)
        editButton.addTarget(self, action:  #selector(editDidTap), for: .touchUpInside)
        let editNavButton: UIBarButtonItem = UIBarButtonItem(customView: editButton)
        
        let shareButton = UIButton(type: .custom)
                 shareButton.frame = CGRect(x: 0.0, y: 0.0, width: 50, height: 30)
                 shareButton.setImage("shareNavBarIcon".image(), for: .normal)
               shareButton.addTarget(self, action:  #selector(share), for: .touchUpInside)
        
        if UserDefaults.standard.bool(forKey: Constants.isSubscribedKey) {
            shareButton.frame = CGRect(x: 0.0, y: 0.0, width: 30, height: 30)
            shareButton.setImage("sharePremiumNavBarIcon".image(), for: .normal)
            shareButton.tintColor = .white
        }
        
        let shareNavBar: UIBarButtonItem = .init(customView: shareButton)

        navigationItem.leftBarButtonItems = [shareNavBar, editNavButton]
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        updateHeaderViewHeight(for: tableView.tableHeaderView)
        updateHeaderViewHeight(for: tableView.tableFooterView)
    }
    
    // MARK: - Actions
    @IBAction private func timerDidTap(_ sender: Any) {
        let timerVC = TimerViewController.instantiate()
        navigationController?.pushViewController(timerVC, animated: true)
    }
    
    @IBAction private func backDidTap(_ sender: Any) {
        navigationController?.popToRootViewController(animated: true)
    }
    
    @objc private func share() {
        if UserDefaults.standard.bool(forKey: Constants.isSubscribedKey) {
            guard let id = data?.id else { return }
            let vc = ShareProgramViewController.instantiate()
            vc.programId = id
            vc.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(vc, animated: true)
        } else {
            let navBar = UINavigationController(rootViewController: SubscriptionViewController.instantiate())
            navBar.modalPresentationStyle = .fullScreen
            present(navBar, animated: true)
        }
    }
    
    @objc private func editDidTap() {
        if let vc = navigationController?.viewControllers[1] as? CardioTrainingViewController {
            vc.program = data
            vc.editingState = .edit
            navigationController?.popToViewController(vc, animated: true)
        }  else if navigationController?.viewControllers.count ?? 1  >= 3, let vc =  navigationController?.viewControllers[2] as? CardioTrainingViewController {
            vc.program = data
            vc.editingState = .edit
            navigationController?.popToViewController(vc, animated: true)
        } else {
            guard let indexPath = indexPath else { fatalError() }
            delegate?.didEdit(at: indexPath)
        }
    }
    
    // MARK: - Private
    private func setupTableViewHeaderAndFooter() {
        tableView.tableHeaderView = tableHeaderView
        tableView.tableFooterView = tableFooterView
    }
    
    private func updateHeaderViewHeight(for header: UIView?) {
        guard let header = header else { return }
        header.frame.size.height = header.systemLayoutSizeFitting(CGSize(width: tableView.bounds.width, height: 0)).height
        tableView.reloadData()
    }
    
    private func setProgramData(content: ProgramInfoViewController.Content) {
        traineeLabel.text = content.traineeName
        trainerLabel.text = content.trainerName
        buildingDateLabel.text = formateDateToString(from: Date.init(milliseconds: content.buildingDate))
        updateDateLabel.text = formateDateToString(from: Date.init(milliseconds: content.updateDate))
        programTitleLabel.text = content.programName
    }
    
    private func formateDateToString(from date: Date) -> String {
        return dateFormatter.string(from: date)
    }
    
    private func checkDistanceInActivity(items: [CardioTableViewCell.Content]) -> Bool {
        var isDistance = false
        items.forEach({
            if $0.timeValue == "" {
                isDistance = true
            }
        })
        return isDistance
    }
    
    private func checkTimeInActivity(items: [CardioTableViewCell.Content]) -> Bool {
        var isTime = false
        items.forEach({
            if $0.distanceValue == "" {
                isTime = true
            }
        })
        return isTime
    }
    
    private func getTimeIntFromString(from string: String) -> Int {
          if let index = string.firstIndex(of: ":") {
              guard let minutes = Int(string.prefix(upTo: index)) else { return 0 }
              guard var seconds = Int(string.suffix(2)) else { return 0 }
              seconds += minutes * 60
              return seconds
          }
          return 0
      }
    
    private func getFormattedTime(time: Int) -> String {
           guard var formattedString = self.formatter.string(from: TimeInterval(time)) else { return ""}
           guard let index = formattedString.lastIndex(of: ":") else { return ""}
           if index == String.Index(utf16Offset: 1, in: formattedString) {
               formattedString.insert("0", at: formattedString.startIndex)
           }
           return formattedString
       }
       
    
    private func makeTimeAndDistanceValues(for indexOfActivity: Int) {
        guard let cardioData = data?.cardioProgram else { return }
        let isTime = checkTimeInActivity(items: cardioData[indexOfActivity].items)
        let isDistance = checkDistanceInActivity(items: cardioData[indexOfActivity].items)
        var tempData: [CardioTableViewCell.Content] = []
        if isTime && isDistance {
            return
            
        } else if isTime && !isDistance {
            data?.cardioProgram[indexOfActivity].items.enumerated().forEach({ index, item in
                if index == 0 {
                    guard  var itemWithTime = data?.cardioProgram[indexOfActivity].items[index] else { return }
                    itemWithTime.timeValue = "00:00 - \(item.timeValue)"
                    tempData.append(itemWithTime)
                } else {
                    guard let totalMaxTime: [Int] = (data?.cardioProgram[indexOfActivity].items[0...index].compactMap({ getTimeIntFromString(from: $0.timeValue)})) else { return }
                    guard let totalMinTime: [Int] = (data?.cardioProgram[indexOfActivity].items[0...index - 1].compactMap({ getTimeIntFromString(from: $0.timeValue)})) else { return }
                    guard  var itemWithTime = data?.cardioProgram[indexOfActivity].items[index] else { return }
                    let max = totalMaxTime.reduce(0, +)
                    let min = totalMinTime.reduce(0, +)
                    itemWithTime.timeValue = "\(getFormattedTime(time: min)) - \(getFormattedTime(time: max))"
                    tempData.append(itemWithTime)
                }
            })
        } else if isDistance && !isTime {
            data?.cardioProgram[indexOfActivity].items.enumerated().forEach({ index, item in
                if index == 0 {
                    guard var itemWithDistance = data?.cardioProgram[indexOfActivity].items[index] else { return }
                    itemWithDistance.distanceValue = "0.0 - \(item.distanceValue)"
                    tempData.append(itemWithDistance)
                } else {
                    guard let totalMaxDistance: [Double] = (data?.cardioProgram[indexOfActivity].items[0...index].compactMap({ Double($0.distanceValue) })) else { return }
                    guard let totalMinDistance: [Double] = (data?.cardioProgram[indexOfActivity].items[0...index - 1].compactMap({ Double($0.distanceValue) })) else { return }
                    guard  var itemWithDistance = data?.cardioProgram[indexOfActivity].items[index] else { return }
                    let max = totalMaxDistance.reduce(0, +)
                    let min = totalMinDistance.reduce(0, +)
                    let formattedStringMax = String(format: "%.1f", max)
                    let formattedStringMin = String(format: "%.1f", min)
                                   
                    itemWithDistance.timeValue = "\(formattedStringMin) - \(formattedStringMax)"
                    tempData.append(itemWithDistance)
                }
            })
        } else {
            tempData = cardioData[indexOfActivity].items
        }
        data?.cardioProgram[indexOfActivity].items = tempData
    }
}

// MARK: - UICollectionViewDataSource
extension CardioReadyTrainingViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let cardioData = data?.cardioProgram else { return 0 }
        return cardioData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(with: CardioReadyTrainingCollectionCell.self, for: indexPath)
        data?.cardioProgram[indexPath.row].cardioType.isSelected = (selectedIndex == indexPath.item)
        guard let cardioData = data?.cardioProgram else { return cell}
        cell.set(content: cardioData[indexPath.item].cardioType)
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension CardioReadyTrainingViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        .init(width: 82, height: 77)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.item
        collectionView.reloadData()
        tableView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        var defaultEdge: UIEdgeInsets = .zero
        defaultEdge.left = 20
        defaultEdge.right = 20
        guard let cardioData = data?.cardioProgram else { return defaultEdge }
        let width = collectionView.frame.width
        let itemWidth = 82
        let spacing = 4
   
        let itemsWidth = (cardioData.count * itemWidth) + (spacing * cardioData.count - 1)
        guard (width - CGFloat(itemsWidth)) >= 40 else { return defaultEdge }
        let leftEdge = (width - CGFloat(itemsWidth)) / 2
        return .init(top: 0, left: leftEdge, bottom: 0, right: 0)
    }
}

// MARK: - UITableViewDataSource
extension CardioReadyTrainingViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let cardioData = data?.cardioProgram else { return 0 }
        return cardioData[selectedIndex].items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(with: CardioReadyTrainingTableViewCell.self, for: indexPath)
        guard let cardioData = data?.cardioProgram else { return cell }
        let isTime = checkTimeInActivity(items: cardioData[selectedIndex].items)
        let isDistance = checkDistanceInActivity(items: cardioData[selectedIndex].items)
        cell.set(content: cardioData[selectedIndex].items[indexPath.row], isGray: indexPath.row % 2 == 1, isLast: indexPath.row == cardioData[selectedIndex].items.count - 1, state: cardioData[selectedIndex].state, isDistance: isDistance, isTime: isTime)
        return cell
    }
}
