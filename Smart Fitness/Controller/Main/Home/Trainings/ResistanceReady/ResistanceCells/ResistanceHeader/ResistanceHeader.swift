//
//  ResistanceHeader.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 24.06.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class ResistanceHeader: UIView {
    
    // MARK: - Outlets
    @IBOutlet weak private var programTitleLabel: UILabel!
    @IBOutlet weak private var traineeLabel: UILabel!
    @IBOutlet weak private var trainerLabel: UILabel!
    @IBOutlet weak private var buildingDateLabel: UILabel!
    @IBOutlet weak private var updateDateLabel: UILabel!
    
    
    // MARK: - Properties
    private var contentView: UIView?
    
    private var dateFormatter: DateFormatter = {
        let dt = DateFormatter()
        dt.dateFormat = "dd/MM/yyyy"
        return dt
    }()
    
    // MARK: - Init
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit() {
        guard let view = loadViewFromNib() else { return }
        view.frame = self.bounds
        self.addSubview(view)
        contentView = view
    }
    
    private func loadViewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        
        let nib = UINib(nibName: String(describing: Self.self), bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
    
    // MARK: - Public Methods
    func setProgramData(content: ProgramInfoViewController.Content?) {
        guard let program = content else { return }
        traineeLabel.text = program.traineeName
        trainerLabel.text = program.trainerName
        buildingDateLabel.text = formateDateToString(from: Date.init(milliseconds: program.buildingDate))
        updateDateLabel.text = formateDateToString(from: Date.init(milliseconds: program.updateDate))
        programTitleLabel.text = program.programName
        
    }
    
    // MARK: - Private
    private func formateDateToString(from date: Date) -> String {
        return dateFormatter.string(from: date)
    }
    
}
