//
//  ResistanceReadyTrainingCardioFooterView.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 16.01.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class ResistanceReadyTrainingCardioFooterView: UIView {
    
    // MARK: Subviews
    @IBOutlet weak private var collectionView: UICollectionView!
    @IBOutlet weak private var tableView: UITableView!
    
    @IBOutlet weak private var activityTitleLabel: UILabel!
    
    lazy private var tableHeaderView: CardioReadyTrainingTableHeader = .fromNib()
    lazy private var tableFooterView: CardioReadyTrainingTableFooter = .fromNib()
    
    @IBOutlet weak private var sectionNumberLabel: UILabel!
    // MARK: - Properties
    var sectionNumber: Int? {
        didSet {
            sectionNumberLabel.text = "\(sectionNumber ?? 1)"
        }
    }
    var data: [Cardio] = [] {
        didSet {
            updateUIFor(index: selectedIndex)
        }
    }
    
    var selectedIndex: Int = 0 {
        didSet {
            updateUIFor(index: selectedIndex)
        }
    }
    
    private func updateUIFor(index: Int) {
        guard index < data.count else { return }
        activityTitleLabel.text = data[index].cardioType.title
        tableHeaderView.set(state: data[index].state, isDistance: checkDistanceInActivity(items: data[index].items), isTime: checkTimeInActivity(items: data[index].items))
        tableFooterView.set(content: .init(time: data[index].totalTime, distance: data[index].totalDistance))
    }
    
    private lazy var dateFormatter: DateFormatter = {
        let dt = DateFormatter()
        dt.dateFormat = "dd/MM/yyyy"
        return dt
    }()
    
    private lazy var formatter: DateComponentsFormatter = {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.minute, .second]
        formatter.unitsStyle = .positional
        formatter.zeroFormattingBehavior = .pad
        return formatter
    }()
    
    // MARK: - Init
    func set(content: [Cardio]) {
        data = content
        collectionView.register(cellType: CardioReadyTrainingCollectionCell.self)
        tableView.register(cellType: CardioReadyTrainingTableViewCell.self)
        collectionView.dataSource = self
        collectionView.delegate = self
        tableView.dataSource = self
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 100, right: 0)
        
        if data.count > 0 {
            data[0].cardioType.isSelected = true
            for index in 0...data.count - 1 {
                makeTimeAndDistanceValues(for: index)
            }
        }
        
        collectionView.reloadData()
        tableView.reloadData()
        setupTableViewHeaderAndFooter()
        activityTitleLabel.text = data[selectedIndex].cardioType.title
        
        tableHeaderView.set(state: data[selectedIndex].state, isDistance: checkDistanceInActivity(items: data[selectedIndex].items), isTime: checkTimeInActivity(items: data[selectedIndex].items))
        tableFooterView.set(content: .init(time: data[selectedIndex].totalTime, distance: data[selectedIndex].totalDistance))
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateHeaderViewHeight(for: tableView.tableHeaderView)
        updateHeaderViewHeight(for: tableView.tableFooterView)
    }
    
    // MARK: - Private
    private func setupTableViewHeaderAndFooter() {
        tableView.tableHeaderView = tableHeaderView
        tableView.tableFooterView = tableFooterView
    }
    
    private func updateHeaderViewHeight(for header: UIView?) {
        guard let header = header else { return }
        header.frame.size.height = header.systemLayoutSizeFitting(CGSize(width: tableView.bounds.width, height: 0)).height
        tableView.reloadData()
    }
    
    private func formateDateToString(from date: Date) -> String {
        return dateFormatter.string(from: date)
    }
    
    private func checkDistanceInActivity(items: [CardioTableViewCell.Content]) -> Bool {
        var isDistance = false
        items.forEach({
            if $0.timeValue == "" {
                isDistance = true
            }
        })
        return isDistance
    }
    
    private func checkTimeInActivity(items: [CardioTableViewCell.Content]) -> Bool {
        var isTime = false
        items.forEach({
            if $0.distanceValue == "" {
                isTime = true
            }
        })
        return isTime
    }
    
    private func getTimeIntFromString(from string: String) -> Int {
        if let index = string.firstIndex(of: ":") {
            guard let minutes = Int(string.prefix(upTo: index)) else { return 0 }
            guard var seconds = Int(string.suffix(2)) else { return 0 }
            seconds += minutes * 60
            return seconds
        }
        return 0
    }
    
    private func getFormattedTime(time: Int) -> String {
        guard var formattedString = self.formatter.string(from: TimeInterval(time)) else { return ""}
        guard let index = formattedString.lastIndex(of: ":") else { return ""}
        if index == String.Index(utf16Offset: 1, in: formattedString) {
            formattedString.insert("0", at: formattedString.startIndex)
        }
        return formattedString
    }
    
    private func makeTimeAndDistanceValues(for indexOfActivity: Int) {
        let isTime = checkTimeInActivity(items: data[indexOfActivity].items)
        let isDistance = checkDistanceInActivity(items: data[indexOfActivity].items)
        var tempData: [CardioTableViewCell.Content] = []
        if isTime && isDistance {
            return
            
        } else if isTime && !isDistance {
            data[indexOfActivity].items.enumerated().forEach({ index, item in
                if index == 0 {
                    var itemWithTime = data[indexOfActivity].items[index]
                    itemWithTime.timeValue = "00:00 - \(item.timeValue)"
                    tempData.append(itemWithTime)
                } else {
                    let totalMaxTime: [Int] = (data[indexOfActivity].items[0...index].compactMap({ getTimeIntFromString(from: $0.timeValue)}))
                    let totalMinTime: [Int] = (data[indexOfActivity].items[0...index - 1].compactMap({ getTimeIntFromString(from: $0.timeValue)}))
                    var itemWithTime = data[indexOfActivity].items[index]
                    let max = totalMaxTime.reduce(0, +)
                    let min = totalMinTime.reduce(0, +)
                    itemWithTime.timeValue = "\(getFormattedTime(time: min)) - \(getFormattedTime(time: max))"
                    tempData.append(itemWithTime)
                }
            })
        } else if isDistance && !isTime {
            data[indexOfActivity].items.enumerated().forEach({ index, item in
                if index == 0 {
                    var itemWithDistance = data[indexOfActivity].items[index]
                    itemWithDistance.distanceValue = "0.0 - \(item.distanceValue)"
                    tempData.append(itemWithDistance)
                } else {
                    let totalMaxDistance: [Double] = (data[indexOfActivity].items[0...index].compactMap({ Double($0.distanceValue) }))
                    let totalMinDistance: [Double] = (data[indexOfActivity].items[0...index - 1].compactMap({ Double($0.distanceValue) }))
                    var itemWithDistance = data[indexOfActivity].items[index]
                    let max = totalMaxDistance.reduce(0, +)
                    let min = totalMinDistance.reduce(0, +)
                    let formattedStringMax = String(format: "%.1f", max)
                    let formattedStringMin = String(format: "%.1f", min)
                    
                    itemWithDistance.timeValue = "\(formattedStringMin) - \(formattedStringMax)"
                    tempData.append(itemWithDistance)
                }
            })
        } else {
            tempData = data[indexOfActivity].items
        }
        data[indexOfActivity].items = tempData
    }
}

// MARK: - UICollectionViewDataSource
extension ResistanceReadyTrainingCardioFooterView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(with: CardioReadyTrainingCollectionCell.self, for: indexPath)
        data[indexPath.row].cardioType.isSelected = (selectedIndex == indexPath.item)
        cell.set(content: data[indexPath.item].cardioType)
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension ResistanceReadyTrainingCardioFooterView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        .init(width: 82, height: 77)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.item
        collectionView.reloadData()
        tableView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        var defaultEdge: UIEdgeInsets = .zero
        defaultEdge.left = 20
        defaultEdge.right = 20
        let width = collectionView.frame.width
        let itemWidth = 82
        let spacing = 4
        
        let itemsWidth = (data.count * itemWidth) + (spacing * data.count - 1)
        guard (width - CGFloat(itemsWidth)) >= 40 else { return defaultEdge }
        let leftEdge = (width - CGFloat(itemsWidth)) / 2
        return .init(top: 0, left: leftEdge, bottom: 0, right: 0)
    }
}

// MARK: - UITableViewDataSource
extension ResistanceReadyTrainingCardioFooterView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data[selectedIndex].items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(with: CardioReadyTrainingTableViewCell.self, for: indexPath)
        let isTime = checkTimeInActivity(items: data[selectedIndex].items)
        let isDistance = checkDistanceInActivity(items: data[selectedIndex].items)
        cell.set(content: data[selectedIndex].items[indexPath.row], isGray: indexPath.row % 2 == 1, isLast: indexPath.row == data[selectedIndex].items.count - 1, state: data[selectedIndex].state, isDistance: isDistance, isTime: isTime)
        return cell
    }
}
