//
//  ResistanceReadyTrainingSectionHeaderCell.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 10.01.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class ResistanceReadyTrainingSectionHeaderCell: UITableViewHeaderFooterView {
    
    // MARK: - Views
    @IBOutlet weak private var imageView: UIImageView!
    @IBOutlet weak private var titleLabel: UILabel!
    @IBOutlet weak private var numberLabel: UILabel!
    @IBOutlet weak private var noExerciseLabel: UILabel!
    
    // MARK: - Properties
    var section: Int?
    
    // MARK: - Init
    static let reuseIdentifier: String = String(describing: self)

    static var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: nil)
    }
    
    // MARK: - Methods
    func set(content: Content, section: Int) {
        imageView.image = content.imageName.image()
        titleLabel.text = content.title
        numberLabel.text = content.number
        noExerciseLabel.isHidden = content.isExercise
        self.section = section
    }
}

// MARK: - Content
extension ResistanceReadyTrainingSectionHeaderCell {
    struct Content {
        var number: String
        var imageName: String
        var title: String
        var isExercise: Bool
    }
}
