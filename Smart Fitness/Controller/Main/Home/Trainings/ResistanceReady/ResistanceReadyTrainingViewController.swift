//
//  ResistanceReadyTrainingViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 10.01.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class ResistanceReadyTrainingViewController: BaseViewController {
    
    // MARK: - Views
    @IBOutlet weak private var tableView: UITableView!
    
    // MARK: - Properties
    weak var delegate: EditProgramDelegate?
    var indexPath: IndexPath?
    
    var isCreated: Bool = false
    var data: [Resistance] = []
    var cardioData: [Cardio] = []
    var programInfo: ProgramInfoViewController.Content?
    var didSaveProgram: ((Program) -> Void)?
    var didSaveResistance: (([Resistance]) -> Void)?
    var id: Int?
    
    private var cellHeights: [IndexPath: CGFloat?] = [:]
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        data.sort(by: { $0.group.orderNumber ?? 0 < $1.group.orderNumber ?? 0 })
        tableView.register(ResistanceReadyTrainingSectionHeaderCell.nib, forHeaderFooterViewReuseIdentifier: ResistanceReadyTrainingSectionHeaderCell.reuseIdentifier)
        tableView.reorder.delegate = self
        
        setProgramData(content: programInfo)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        fixDirection()
        setupCardioFooter()
        guard let index = navigationController?.viewControllers.count else { return }
        if  navigationController?.viewControllers[index - 2] is SplitTrainingReadyViewController {
           
        } else {
            navigationItem.leftBarButtonItem?.image = nil
            navigationItem.rightBarButtonItem = .init(title: Constants.ProgramInfo().done, style: .done, target: self, action: #selector(backDidTap))
            self.setBarItemsFont()
            
            let editButton = UIButton(type: .custom)
            editButton.frame = CGRect(x: 0.0, y: 0.0, width: 30, height: 30)
            editButton.setImage("editNavBarIcon".image(), for: .normal)
            editButton.addTarget(self, action:  #selector(editDidTap), for: .touchUpInside)
            let editNavButton: UIBarButtonItem = UIBarButtonItem(customView: editButton)
            
            let shareButton = UIButton(type: .custom)
            shareButton.frame = CGRect(x: 0.0, y: 0.0, width: 50, height: 30)
            shareButton.setImage("shareNavBarIcon".image(), for: .normal)
            shareButton.addTarget(self, action:  #selector(share), for: .touchUpInside)
            
            if UserDefaults.standard.bool(forKey: Constants.isSubscribedKey) {
                shareButton.frame = CGRect(x: 0.0, y: 0.0, width: 30, height: 30)
                shareButton.setImage("sharePremiumNavBarIcon".image(), for: .normal)
                shareButton.tintColor = .white
            }
            
            let shareNavBar: UIBarButtonItem = .init(customView: shareButton)
            
            navigationItem.leftBarButtonItems = [shareNavBar, editNavButton]
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        data.sort(by: { $0.group.orderNumber ?? 0 < $1.group.orderNumber ?? 0 })
        tableView.reloadData()
    }
    
    // MARK: - Actions
    @IBAction func backDidTap(_ sender: Any) {
        data.enumerated().forEach({ sectionIndex, section in
            section.exercisesData.enumerated().forEach({ index, item in
                if item.exercise.isExpanded {
                    data[sectionIndex].exercisesData[index].exercise.isExpanded = false
                }
                data[sectionIndex].exercisesData[index].isExpanded = false
            })
        })
        if navigationController?.viewControllers[(navigationController?.viewControllers.count ?? 2) - 2 ] is SplitTrainingReadyViewController {
            didSaveResistance?(data)
            navigationController?.popViewController(animated: true)
        } else {
            guard let programInfo = programInfo else { return }
            didSaveProgram?(Program(id: id, cardioProgram: cardioData, resistanceProgram: data, circuitProgram: [], programInfo: programInfo, programType: .resistance))
            
            navigationController?.popToRootViewController(animated: true)
        }
    }
    
    @IBAction func timerDidTap(_ sender: Any) {
        let timerVC = TimerViewController.instantiate()
        navigationController?.pushViewController(timerVC, animated: true)
    }
    
    @objc private func share() {
        if UserDefaults.standard.bool(forKey: Constants.isSubscribedKey) {
            guard let id = id else { return }
            let vc = ShareProgramViewController.instantiate()
            vc.programId = id
            vc.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(vc, animated: true)
        } else {
            let navBar = UINavigationController(rootViewController: SubscriptionViewController.instantiate())
            navBar.modalPresentationStyle = .fullScreen
            present(navBar, animated: true)
        }
    }
    
    @objc private func editDidTap() {
        guard let programInfo = programInfo else { return }
        guard let id = id else { return }
        var program = Program(id: id, cardioProgram: cardioData, resistanceProgram: data, circuitProgram: [], programInfo: programInfo, programType: .resistance)
        program.id = id
        didSaveProgram?(program)
        if let vc = navigationController?.viewControllers[1] as? ResistanceViewController {
            vc.program = program
            vc.editState = .edit
            navigationController?.popToViewController(vc, animated: true)
        } else if navigationController?.viewControllers.count ?? 1  >= 3, let vc =  navigationController?.viewControllers[2] as? ResistanceViewController {
            vc.program = program
            vc.editState = .edit
            navigationController?.popToViewController(vc, animated: true)
        } else {
            guard let indexPath = indexPath else { fatalError() }
            delegate?.didEdit(at: indexPath)
        }
    }
    
    // MARK: - Private
    private func didExpandClicked(cell: ParentExerciseCell, indexPath: IndexPath) {
        guard  !ReorderController.isDragging else { return }
        data[indexPath.section].exercisesData[indexPath.row].exercise.isExpanded.toggle()
        tableView.performBatchUpdates({
            cell.selectRow()
        })
    }
    
    private func showExerciseInfo(exercise: Exercise) {
        guard !exercise.isCustom() else { return }
        let vc = ExerciseDetailsViewController.instantiate()
        vc.exercise = exercise
        navigationController?.pushViewController(vc, animated: true)
    }
    
    private func setProgramData(content: ProgramInfoViewController.Content?) {
        if navigationController?.viewControllers[(navigationController?.viewControllers.count ?? 2) - 2 ] is SplitTrainingReadyViewController {
            
        } else {
            let header = ResistanceHeader()
            tableView.tableHeaderView = header
            tableView.tableHeaderView?.frame.size.height = 119
            header.setProgramData(content: content)
        }
    }
    
    
    private func setupCardioFooter() {
        guard cardioData.count > 0 else { return }
        let cardioFooter: ResistanceReadyTrainingCardioFooterView = .fromNib()
        cardioFooter.sectionNumber = (data.count) + 1
        cardioFooter.set(content: cardioData)
        tableView.tableFooterView = cardioFooter
    }
    
    private func addLongPressGesture() -> UILongPressGestureRecognizer {
        let reg = UILongPressGestureRecognizer(target: tableView.reorder, action: #selector(tableView.reorder.handleReorderGesture))
        reg.minimumPressDuration = 0.2
        reg.delegate = tableView.reorder
        reg.cancelsTouchesInView = false
        return reg
    }
    
    private func addLongPressGestureForHeader() -> UILongPressGestureRecognizer {
        let reg = UILongPressGestureRecognizer(target: self, action: #selector(longPressHeader))
        reg.minimumPressDuration = 0.2
        reg.cancelsTouchesInView = false
        return reg
    }
    
    var touchedHeader: UITableViewHeaderFooterView?
    var sourceSection: Int?
    var destinationSection: Int?
    
    @objc func longPressHeader(sender: UILongPressGestureRecognizer) {
        switch sender.state {
        case .began:
            guard let sectionHeader = sender.view as? ResistanceReadyTrainingSectionHeaderCell, let sSection = sectionHeader.section else { return }
            
            closeSections()
            touchedHeader = sectionHeader
            sourceSection = sSection
        case .changed:
            touchedHeader?.center.y = sender.location(in: tableView).y
            
        case .ended:
            guard let sourceSec = sourceSection else { return }
            let dataSection = data[sourceSec]
            
            for section in data.indices {
                let rect = tableView.rect(forSection: section)
                if abs(rect.midY - sender.location(in: tableView).y) < 60 {
                    destinationSection = section
                } else if section == data.count - 1 {
                    if sender.location(in: tableView).y - rect.midY > 0 {
                        destinationSection = section }
                }
            }
            guard let dSection = destinationSection else {
                expandAllSections()
                return
            }
            data.remove(at: sourceSec)
            tableView.deleteSections(IndexSet(integer: sourceSec), with: .fade)
            data.insert(dataSection, at: dSection)
            tableView.insertSections(IndexSet(integer: dSection), with: .fade)
            
            
            expandAllSections()
        case .cancelled:
            expandAllSections()
        default:
            break
        }
    }
    
    private func closeSections() {
        var indexPaths = [IndexPath]()
        isHeaderMoving = true
        for section in data.indices {
            for row in data[section].exercisesData.indices {
                let indexPath = IndexPath(row: row, section: section)
                indexPaths.append(indexPath)
                
            }
        }
        self.indexPaths = indexPaths
        
        tableView.deleteRows(at: indexPaths, with: .fade)
    }
    
    private func expandAllSections() {
        isHeaderMoving = false
        var indexPaths = [IndexPath]()
        for section in data.indices {
            data[section].group.orderNumber = section
            for row in data[section].exercisesData.indices {
                let indexPath = IndexPath(row: row, section: section)
                indexPaths.append(indexPath)
            }
        }
        self.indexPaths = indexPaths
        
        tableView.performBatchUpdates({
            self.tableView.insertRows(at: indexPaths, with: .fade)
        }) { (comp) in
            self.tableView.reloadData()
        }
        
    }
    
    var indexPaths = [IndexPath]()
    var isHeaderMoving = false
}

// MARK: - UITableViewDataSource
extension ResistanceReadyTrainingViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        data.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isHeaderMoving {
            return 0
        } else {
            return  data[section].exercisesData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let spacer = tableView.reorder.spacerCell(for: indexPath) { return spacer }
        let resistanceData = data[indexPath.section]
        switch resistanceData.exercisesData[indexPath.row].method {
            
        case .superset, .supersetDifferent:
            
            let cell = tableView.dequeueReusableCell(with: ExerciseSupersetMainCell.self, for: indexPath)
            cell.isViewMode = true
            cell.set(content: resistanceData.exercisesData[indexPath.row])
            (cell.tableView.cellForRow(at: IndexPath(item: 0, section: 0)) as? ExerciseSupersetCell)?.contentView.addGestureRecognizer(addLongPressGesture())
            cell.didImageClickedForExID = { [weak self] exercise in self?.showExerciseInfo(exercise: exercise) }
            cell.didExpandClicked = { [weak self] in
                self?.data[indexPath.section].exercisesData[indexPath.row].isExpanded.toggle()
                tableView.beginUpdates()
                cell.layoutIfNeeded()
                tableView.endUpdates()
            }
            
            cell.didEditData = { [weak self]  in
                guard let self = self else { return }
                let editPopUp = EditDropSetViewController.instantiate()
                editPopUp.method = .superSet
                let data = self.data[indexPath.section].exercisesData[indexPath.row].supersetMethodData.map({ ExerciseSupersetSetCell.Content(exercises: $0.exercises.map({
                    ExerciseSupersetSetExerciseCell.Content(exerciseName: $0.exerciseName, repsValue: $0.repsValue, kgValue: $0.kgValues[$0.indexOfKg], kgValues: $0.kgValues, indexOfKg: $0.indexOfKg)
                }), restTime: $0.restTime, setNumber: $0.setNumber) })
                editPopUp.data = data
                editPopUp.isKgOnly = true
                self.present(editPopUp, animated: false, completion: nil)
                editPopUp.saveDidTap = { [weak self] sets in
                    guard let self = self else { return }
                    let updatedSets = sets.map({ ExerciseSupersetSetCell.Content(exercises: $0.exercises.map({
                        var kgs = $0.kgValues
                        kgs[$0.indexOfKg] = $0.kgValue
                        return ExerciseSupersetSetExerciseCell.Content(exerciseName: $0.exerciseName, repsValue: $0.repsValue, kgValue: $0.kgValues[$0.indexOfKg], kgValues: kgs, indexOfKg: $0.indexOfKg)
                    }), restTime: $0.restTime, setNumber: $0.setNumber) })
                    self.data[indexPath.section].exercisesData[indexPath.row].supersetMethodData = updatedSets
                    tableView.reloadData()
                }
            }
            
            cell.didAddNewKg = { [weak self] in
                guard let self = self else { return }
                self.data[indexPath.section].exercisesData[indexPath.row].supersetMethodData.mutateEach({
                    $0.exercises.mutateEach({
                        $0.kgValues.append(301)
                        $0.indexOfKg = $0.kgValues.count - 1
                    })
                })
                
                tableView.reloadData()
            }
            
            cell.didChangeKgIndex = { [weak self] index in
                guard let self = self else { return }
                self.data[indexPath.section].exercisesData[indexPath.row].supersetMethodData.mutateEach({
                    $0.exercises.mutateEach({
                        
                        $0.indexOfKg = index
                    })
                })
                tableView.reloadData()
            }
            
            return cell
            
        case .dropSet:
            
            let cell = tableView.dequeueReusableCell(with: ExerciseDropSetMainCell.self, for: indexPath)
            guard let dropData = resistanceData.exercisesData[indexPath.row].dropSetMethodData else { return cell }
            
            cell.set(content: .init(sets: dropData.sets, machineNumber: dropData.machineNumber, notes: dropData.notes), exercise: resistanceData.exercisesData[indexPath.row].exercise)
            cell.contentView.addGestureRecognizer(addLongPressGesture())
            cell.didImageClicked = { [weak self] in
                guard let self = self else { return }
                self.showExerciseInfo(exercise: resistanceData.exercisesData[indexPath.row].exercise)
                
            }
            cell.didExpandClicked = { [weak self] in self?.didExpandClicked(cell: cell, indexPath: indexPath) }
            if resistanceData.exercisesData.count - 1 == indexPath.row {
                cell.roundedView.corners = .bottom
                cell.roundedView.cornerRadius = 4
            }
            
            cell.didAddNewKg = { [weak self] in
                guard let self = self else { return }
                self.data[indexPath.section].exercisesData[indexPath.row].dropSetMethodData?.sets.mutateEach({
                    var kgs: [Double] = []
                    for _ in 0..<$0.dropKgValues[$0.indexOfKgValue].count {
                        kgs.append(301)
                    }
                    $0.dropKgValues.append(kgs)
                    
                })
                
                self.data[indexPath.section].exercisesData[indexPath.row].dropSetMethodData?.sets.mutateEach({
                    $0.indexOfKgValue = $0.dropKgValues.count - 1
                })
                
                
                tableView.reloadRows(at: [indexPath], with: .fade)
            }
            
            cell.didChangeKgIndex = { [weak self] index in
                guard let self = self else { return }
                self.data[indexPath.section].exercisesData[indexPath.row].dropSetMethodData?.sets.mutateEach({
                    $0.indexOfKgValue = index
                })
                tableView.reloadRows(at: [indexPath], with: .fade)
            }
            
            cell.didEditData = { [weak self]  in
                guard let self = self else { return }
                let editPopUp = EditDropSetViewController.instantiate()
                editPopUp.method = .dropSet
                editPopUp.isKgOnly = true
                
                var sets: [ExerciseSupersetSetCell.Content] = []
                self.data[indexPath.section].exercisesData[indexPath.row].dropSetMethodData?.sets.forEach {
                    
                    var ex: [ExerciseSupersetSetExerciseCell.Content] = []
                    for (i, kg) in $0.dropKgValues[$0.indexOfKgValue].enumerated() {
                        var item = ExerciseSupersetSetExerciseCell.Content(exerciseName: "", repsValue: 0, kgValue: kg, kgValues: [])
                        item.repsValue = $0.dropSetValues[i]
                        ex.append(item)
                    }
                    sets.append(.init(exercises: ex, restTime: $0.restTime, setNumber: 0))
                }
                editPopUp.data = sets
                
                self.present(editPopUp, animated: false, completion: nil)
                editPopUp.saveDidTap = { [weak self] sets in
                    guard let self = self else { return }
                    let index = dropData.sets[0].indexOfKgValue
                    var setsDropSet: [ExerciseDropSetSingleSetCell.Content] = []
                    sets.enumerated().forEach {
                        var kgArray = dropData.sets[$0].dropKgValues
                        kgArray[index] = []
                        var repsValue: [Int] = []
                        for  item in $1.exercises {
                            kgArray[index].append(item.kgValue)
                            repsValue.append(item.repsValue)
                        }
                        
                        setsDropSet.append(.init(set: $0 + 1, dropSetValues: repsValue, dropKgValues: kgArray, restTime: $1.restTime))
                    }
                    self.data[indexPath.section].exercisesData[indexPath.row].dropSetMethodData?.sets = setsDropSet
                    self.data[indexPath.section].exercisesData[indexPath.row].dropSetMethodData?.sets.mutateEach({
                        $0.indexOfKgValue = index
                    })
                    
                    tableView.reloadData()
                }
            }
            return cell
            
        case .pyramid, .percentOfRM:
            
            let cell = tableView.dequeueReusableCell(with: ExercisePercentMainCell.self, for: indexPath)
            guard let percent = resistanceData.exercisesData[indexPath.row].percentMethodData else { return cell }
            cell.set(content: .init(sets: percent.sets, machineNumber: percent.machineNumber, notes: percent.notes), state: resistanceData.exercisesData[indexPath.row].method , exercise: resistanceData.exercisesData[indexPath.row].exercise)
            cell.contentView.addGestureRecognizer(addLongPressGesture())
            cell.didImageClicked = { [weak self] in
                guard let self = self else { return }
                self.showExerciseInfo(exercise: resistanceData.exercisesData[indexPath.row].exercise)
            }
            cell.didExpandClicked = { [weak self] in self?.didExpandClicked(cell: cell, indexPath: indexPath) }
            if resistanceData.exercisesData.count - 1 == indexPath.row {
                cell.roundedView.corners = .bottom
                cell.roundedView.cornerRadius = 4
            }
            
            cell.didEditData = { [weak self]  in
                guard let self = self else { return }
                let editPopUp = EditPercentOfRMViewController.instantiate()
                editPopUp.method = (self.data[indexPath.section].exercisesData[indexPath.row].method == .percentOfRM) ? .percent : .pyramid
                
                editPopUp.isKgOnly = true
                editPopUp.data = self.data[indexPath.section].exercisesData[indexPath.row].percentMethodData?.sets ?? []
                self.present(editPopUp, animated: false, completion: nil)
                editPopUp.saveDidTap = { [weak self] sets in
                    guard let self = self else { return }
                    self.data[indexPath.section].exercisesData[indexPath.row].percentMethodData?.sets = sets
                    tableView.reloadData()
                }
            }
            
            cell.didAddNewKg = { [weak self] in
                guard let self = self else { return }
                self.data[indexPath.section].exercisesData[indexPath.row].percentMethodData?.sets.mutateEach({
                    $0.kg.append(301)
                })
                self.data[indexPath.section].exercisesData[indexPath.row].percentMethodData?.sets.mutateEach({
                    $0.indexOfKg = $0.kg.count - 1
                })
                tableView.reloadRows(at: [indexPath], with: .fade)
            }
            
            cell.didChangeKgIndex = { [weak self] index in
                guard let self = self else { return }
                self.data[indexPath.section].exercisesData[indexPath.row].percentMethodData?.sets.mutateEach({
                    $0.indexOfKg = index
                })
                tableView.reloadRows(at: [indexPath], with: .fade)
            }
            
            return cell
            
        case .none:
            
            let cell = tableView.dequeueReusableCell(with: ExerciseCell.self, for: indexPath)
            cell.set(content: resistanceData.exercisesData[indexPath.row].exercise)
            cell.contentView.addGestureRecognizer(addLongPressGesture())
            cell.didExpandClicked = { [weak self]  in
                self?.didExpandClicked(cell: cell, indexPath: indexPath)
            }
            
            if resistanceData.exercisesData.count - 1 == indexPath.row {
                cell.roundedView.corners = .bottom
                cell.roundedView.cornerRadius = 4
            }
            
            cell.didImageClicked = { [weak self] in
                guard let self = self else { return }
                self.showExerciseInfo(exercise: resistanceData.exercisesData[indexPath.row].exercise)
            }
            
            cell.didAddNewKg = { [weak self] in
                guard let self = self else { return }
                self.data[indexPath.section].exercisesData[indexPath.row].exercise.kgValue.append(301)
                self.data[indexPath.section].exercisesData[indexPath.row].exercise.indexOfKg = self.data[indexPath.section].exercisesData[indexPath.row].exercise.kgValue.count - 1
                tableView.reloadRows(at: [indexPath], with: .fade)
            }
            
            cell.didChangeKgIndex = { [weak self] index in
                self?.data[indexPath.section].exercisesData[indexPath.row].exercise.indexOfKg = index
                tableView.reloadRows(at: [indexPath], with: .fade)
            }
            
            cell.didValuesClicked = { [weak self] in
                guard let self = self else { return }
                let popUpVC = EditNormalSetViewController.instantiate()
                popUpVC.state = .onlyKg
                popUpVC.item = self.data[indexPath.section].exercisesData[indexPath.row].exercise
                self.present(popUpVC, animated: false, completion: nil)
                popUpVC.saveDidTap = { editedItem in
                    self.data[indexPath.section].exercisesData[indexPath.row].exercise = editedItem
                    cell.set(content: editedItem)
                }
            }
            return cell
        }
    }
}

// MARK: - UITableViewDelegate
extension ResistanceReadyTrainingViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: ResistanceReadyTrainingSectionHeaderCell.reuseIdentifier) as? ResistanceReadyTrainingSectionHeaderCell else { return nil }
        header.set(content: .init(number: "\(data[section].group.group.string)\(section + 1)", imageName: data[section].group.imageUrl, title: data[section].group.title, isExercise: data[section].exercisesData.count > 0), section: section)
        
        header.addGestureRecognizer(self.addLongPressGestureForHeader())
        
        return header
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeights[indexPath] = cell.frame.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if let height = cellHeights[indexPath] {
            return height ?? UITableView.automaticDimension
        }
        return UITableView.automaticDimension
    }
}

// MARK: - TableViewReorderDelegate
extension ResistanceReadyTrainingViewController: TableViewReorderDelegate {
    
    func tableView(_ tableView: UITableView, reorderRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let movedObject = self.data[sourceIndexPath.section].exercisesData[sourceIndexPath.row]
        data[sourceIndexPath.section].exercisesData.remove(at: sourceIndexPath.row)
        if destinationIndexPath.section != sourceIndexPath.section {
            data[sourceIndexPath.section].exercisesData.insert(movedObject, at: sourceIndexPath.row)
        } else {
            data[destinationIndexPath.section].exercisesData.insert(movedObject, at: destinationIndexPath.row)
        }
        
    }
}
