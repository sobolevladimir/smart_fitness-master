//
//  SplitTrainingReadyCell.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 28.01.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class SplitTrainingReadyCell: UICollectionViewCell {
    
    // MARK: - Views
    @IBOutlet weak private var symbolLabel: UILabel!
    @IBOutlet weak private var dayNumberLabel: UILabel!
    @IBOutlet weak private var dayLabel: UILabel!
    @IBOutlet weak var dotsImageView: UIImageView!
    
    // MARK: - Init Methods
    func set(day: SplitDays) {
        if day == .none {
            dayLabel.isHidden = true
            dayNumberLabel.text = Constants.ResistanceExercises.Methods().other
            symbolLabel.isHidden = true
            dotsImageView.isHidden = false
            return
        }
        symbolLabel.text = day.string
        dayNumberLabel.text = "\(day.number)"
        symbolLabel.isHidden = false
        dotsImageView.isHidden = true
    }
}
