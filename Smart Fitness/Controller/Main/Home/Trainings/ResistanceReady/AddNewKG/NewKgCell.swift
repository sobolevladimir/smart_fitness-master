//
//  TableViewCell.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 29.01.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

class NewKgCell: UITableViewCell {
    
    // MARK: - Views
    @IBOutlet weak var kgNumberLabel: UILabel!
    
    
    // MARK: - Init Methods
    func set(number: Int) {
        kgNumberLabel.text = "\(number)"
    }
}
