//
//  AddNewKg.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 29.01.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

class AddNewKg: UIView {
    
    // MARK: - View
    @IBOutlet weak private var tableView: UITableView!
    private var contentView: UIView?
    
    // MARK: - Properties
    private var countAmountOfKg: Int = 0
    var didSelectRow: ((Int) -> Void)?
    var didAddNewKg: (() -> Void)?
    
    // MARK: - Init
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit() {
        guard let view = loadViewFromNib() else { return }
        view.frame = self.bounds
        self.addSubview(view)
        contentView = view
        tableView.register(cellType: NewKgCell.self)
        tableView.delegate = self
        tableView.dataSource = self
    }

    private func loadViewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
    
        let nib = UINib(nibName: String(describing: Self.self), bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
    
    func set(countAmountOfKg: Int) {
        self.countAmountOfKg = countAmountOfKg
        tableView.reloadData()
    }
    
    // MARK: - Action
    @IBAction func addNewKgDidTap(_ sender: Any) {
        didAddNewKg?()
    }
}

// MARK: - UITableViewDataSource
extension AddNewKg: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countAmountOfKg
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(with: NewKgCell.self, for: indexPath)
        cell.set(number: indexPath.row + 1)
        return cell
    }
}

extension AddNewKg: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        didSelectRow?(indexPath.row)
    }
}
