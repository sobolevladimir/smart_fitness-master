//
//  SplitTrainingReadyViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 28.01.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

class SplitTrainingReadyViewController: BaseViewController {
    
    struct Content {
        var day: SplitDays
        var exerciseData:  [Resistance]
    }

    // MARK: - Views
    @IBOutlet weak private var traineeLabel: UILabel!
    @IBOutlet weak private var trainerLabel: UILabel!
    @IBOutlet weak private var buildingDateLabel: UILabel!
    @IBOutlet weak private var updateDateLabel: UILabel!
    @IBOutlet weak private var programTitleLabel: UILabel!
    
    // MARK: - Properties
    weak var delegate: EditProgramDelegate?
    var indexPath: IndexPath?
    
    var isCreated: Bool = false
    var content: [Content] = []
    var cardioData: [Cardio] = []
    var programInfo: ProgramInfoViewController.Content?
    var didSaveProgram: ((Program) -> Void)?
    var dateFormatter: DateFormatter = {
        let dt = DateFormatter()
        dt.dateFormat = "dd/MM/yyyy"
        return dt
    }()
    var id: Int?
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        fixDirection()
        guard let program = programInfo else { return }
        setProgramData(content: program)
        
        navigationItem.leftBarButtonItem?.image = nil
        navigationItem.rightBarButtonItem = .init(title: Constants.ProgramInfo().done, style: .done, target: self, action: #selector(backDidTap))
        self.setBarItemsFont()
        
        let editButton = UIButton(type: .custom)
        editButton.frame = CGRect(x: 0.0, y: 0.0, width: 30, height: 30)
        editButton.setImage("editNavBarIcon".image(), for: .normal)
        editButton.addTarget(self, action:  #selector(editDidTap), for: .touchUpInside)
        let editNavButton: UIBarButtonItem = UIBarButtonItem(customView: editButton)
        
        let shareButton = UIButton(type: .custom)
        shareButton.frame = CGRect(x: 0.0, y: 0.0, width: 50, height: 30)
        shareButton.setImage("shareNavBarIcon".image(), for: .normal)
        shareButton.addTarget(self, action:  #selector(share), for: .touchUpInside)
        
        if UserDefaults.standard.bool(forKey: Constants.isSubscribedKey) {
            shareButton.frame = CGRect(x: 0.0, y: 0.0, width: 30, height: 30)
            shareButton.setImage("sharePremiumNavBarIcon".image(), for: .normal)
            shareButton.tintColor = .white
        }
        
        let shareNavBar: UIBarButtonItem = .init(customView: shareButton)
        
        navigationItem.leftBarButtonItems = [shareNavBar, editNavButton]
    }
    
    // MARK: - Actions
    @IBAction func backDidTap(_ sender: Any) {
        guard let programInfo = programInfo else { return }
        let resistance = content.flatMap({ $0.exerciseData })
        didSaveProgram?(Program(id: id, cardioProgram: cardioData, resistanceProgram: resistance, circuitProgram: [], programInfo: programInfo, programType: .resistance))
           navigationController?.popToRootViewController(animated: true)
       }
    
    @objc private func editDidTap() {
        guard let programInfo = programInfo else { return }
        let resistance = content.flatMap({ $0.exerciseData })
        var program = Program(id: id, cardioProgram: cardioData, resistanceProgram: resistance, circuitProgram: [], programInfo: programInfo, programType: .resistance)
        program.id = id
        didSaveProgram?(program)
        if let vc = navigationController?.viewControllers[1] as? ResistanceViewController {
            vc.program = program
            vc.editState = .edit
            navigationController?.popToViewController(vc, animated: true)
        } else if navigationController?.viewControllers.count ?? 1  >= 3, let vc =  navigationController?.viewControllers[2] as? ResistanceViewController {
            vc.program = program
            vc.editState = .edit
            navigationController?.popToViewController(vc, animated: true)
        }  else {
            guard let indexPath = indexPath else { fatalError() }
            delegate?.didEdit(at: indexPath)
        }
    }
    
    @objc private func share() {
        if UserDefaults.standard.bool(forKey: Constants.isSubscribedKey) {
            guard let id = id else { return }
            let vc = ShareProgramViewController.instantiate()
            vc.programId = id
            vc.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(vc, animated: true)
        } else {
            let navBar = UINavigationController(rootViewController: SubscriptionViewController.instantiate())
            navBar.modalPresentationStyle = .fullScreen
            present(navBar, animated: true)
        }
    }
    
    // MARK: - Private
    private func setProgramData(content: ProgramInfoViewController.Content) {
          traineeLabel.text = content.traineeName
          trainerLabel.text = content.trainerName
        buildingDateLabel.text = formateDateToString(from: Date.init(milliseconds: content.buildingDate))
        updateDateLabel.text = formateDateToString(from: Date.init(milliseconds: content.updateDate))
          programTitleLabel.text = content.programName
      }
      
    private func formateDateToString(from date: Date) -> String {
        return dateFormatter.string(from: date)
    }
}

// MARK: - UICollectionViewDataSource
extension SplitTrainingReadyViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        content.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(with: SplitTrainingReadyCell.self, for: indexPath)
        cell.set(day: content[indexPath.row].day)
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension SplitTrainingReadyViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.frame.width - (17 * 2)) / 3
        let height = (collectionView.frame.height - 10) / 2
        return  .init(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: false)
        let vc = ResistanceReadyTrainingViewController.instantiate()
        vc.data = content[indexPath.row].exerciseData
        vc.data.sort(by: { $0.group.orderNumber ?? 0 > $1.group.orderNumber ?? 0 })
        vc.programInfo = programInfo
        vc.didSaveResistance = { [weak self] resistance in
            self?.content[indexPath.row].exerciseData = resistance
        }
        if cardioData.count > 0 {
             vc.cardioData = cardioData
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        17
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let defaultEdge: UIEdgeInsets = .zero
        let width = collectionView.frame.width
        let itemWidth = (collectionView.frame.width - (17 * 2)) / 3
        let spacing: CGFloat = 17
        let countItem = content.count == 4 ? 2 : content.count
        let itemsWidth = (CGFloat(countItem) * itemWidth) + (spacing * CGFloat(countItem - 1))
        let leftEdge = (width - CGFloat(itemsWidth)) / 2
        guard (width - CGFloat(itemsWidth)) >= 40 else { return defaultEdge }
        return .init(top: 0, left: leftEdge, bottom: 0, right: leftEdge)
     }
}
