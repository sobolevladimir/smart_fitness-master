//
//  ShareProgramCell.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 13.02.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class ShareProgramCell: UITableViewCell {
    @IBOutlet weak private var userImageView: UIImageView!
    @IBOutlet weak private var userTitleLabel: UILabel!
    @IBOutlet weak private var sentView: RoundedView!
    @IBOutlet weak private var shareIcon: UIImageView!
    
    // MARK: - Properties
    var didSendProgram: (() -> Void)?
    
    // MARK: - Methods
    func set(user: User, isSent: Bool) {
        userImageView.downloadImage(from: user.imageUrl, placeholder: "ic_profile".image())
        userTitleLabel.text = user.username ?? "\(user.firstName ?? "") \(user.lastName ?? "")"

        isUserInteractionEnabled = !isSent
        sentView.alpha = isSent ? 1 : 0
        shareIcon.alpha = isSent ? 0 : 1
        
    }
    
    // MARK: - Actions
    @IBAction private func sendDidTap(_ sender: Any) { didSendProgram?() }
    
}
