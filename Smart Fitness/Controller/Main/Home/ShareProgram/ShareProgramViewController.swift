//
//  ShareProgramViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 13.02.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

class ShareProgramViewController: BaseViewController {
    
    // MARK: - Views
    @IBOutlet weak private var tableView: UITableView!
    @IBOutlet weak private var searchBar: UISearchBar!
    @IBOutlet weak private var noResultsLabel: UILabel!
    
    // MARK: - Properties
    var programId: Int!
    private var pendingRequestWorkItem: DispatchWorkItem?
    private var users: [User] = []
    private var isSentList: [Int: Bool] = [:]
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        fixDirection()
        tableView.tableFooterView = UIView()
        if #available(iOS 13, *) {
            searchBar.searchTextField.backgroundColor = .white
        }
    }
    
    // MARK: - Actions
    @IBAction func backDidTap(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func shareExternalAction(_ sender: Any) {
        guard let id = programId else { return }
        ShareManager.shareProgramWithLink(programId: id) { [weak self] (result) in
            switch result {
            case .success(let link):
                self?.share(link: link)
            case .failure(let error):
                self?.showAlert(text: "\(error)")
            }
        }
    }
    
    // MARK: - Private
    private func share(link: String) {
      
        if let myWebsite = URL(string: link) {
            let objectsToShare = [myWebsite, link] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
            if let popoverController = activityVC.popoverPresentationController {
                popoverController.sourceRect = CGRect(x: UIScreen.main.bounds.width / 2, y: UIScreen.main.bounds.height / 2, width: 0, height: 0)
                popoverController.sourceView = self.view
                popoverController.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
            }
            activityVC.popoverPresentationController?.sourceView = self.view
            self.present(activityVC, animated: true, completion: nil)
        }
    }
}

// MARK: - UITableViewDataSource
extension ShareProgramViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let id = users[indexPath.row].id else { return UITableViewCell() }
        var isSent = false
        if isSentList.keys.contains(id) {
            isSent = true
        }
        let cell = tableView.dequeueReusableCell(with: ShareProgramCell.self, for: indexPath)
        cell.set(user: users[indexPath.row], isSent: isSent)
        cell.didSendProgram = { [weak self] in
            guard let self = self else { return }
            tableView.cellForRow(at: indexPath)?.isUserInteractionEnabled = false
            if let id = self.users[indexPath.row].id {
                ShareManager.shareProgram(userId: id, programId: self.programId) { [weak self] (result) in
                    tableView.cellForRow(at: indexPath)?.isUserInteractionEnabled = true
                    switch result {
                    case .success:
                        self?.isSentList.updateValue(true, forKey: id)
                        tableView.reloadData()
                    case .failure(let error):
                        self?.showAlert(text: "\(error.message ?? "")")
                    }
                }
            }
        }
        return cell
    }
}

// MARK: - UITableViewDelegate
extension ShareProgramViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        searchBar.resignFirstResponder()
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// MARK: - UISearchBarDelegate
extension ShareProgramViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.pendingRequestWorkItem?.cancel()
        let requestWorkItem = DispatchWorkItem { [weak self] in
            guard !searchText.isEmpty else {
                return
            }
            guard let self = self else { return }
            UserProvider.searchUser(filter: searchText) { (result) in
                switch result {
                    
                case .success(let users):
                    self.users = users.componentList
                    self.noResultsLabel.isHidden = self.users.count != 0
                    
                    self.tableView.reloadData()
                case .failure(let error):
                    self.showAlert(text: "\(error)")
                }
            }
        }
        self.pendingRequestWorkItem = requestWorkItem
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(350), execute: requestWorkItem)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}

