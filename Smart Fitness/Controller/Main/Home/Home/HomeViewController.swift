//
//  HomeViewController.swift
//  Smart Fitness
//
//  Created by Rusłan Chamski on 27/10/2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

struct SectionProgram {
    var date: Date
    var data: [Program]
}

final class HomeViewController: BaseViewController {
    
    // MARK: - Views
    @IBOutlet weak private var menuButton: UIButton!
    @IBOutlet weak private var crownButton: UIButton!
    @IBOutlet weak private var collectionView: UICollectionView!
    @IBOutlet weak private var emptyProgramListLabelsStackView: UIStackView!
    
    // MARK: - Constraints
    @IBOutlet var bottomButtonConstraint: NSLayoutConstraint!
    @IBOutlet var topButtonConstraint: NSLayoutConstraint!
    
    // MARK: - Properties
    lazy var dateFormater: DateFormatter = {
        let df = DateFormatter()
        switch LocalizationManager.Language.language {
        case .english:
            df.locale = Locale(identifier: "en")
        case .hebrew:
            df.locale = Locale(identifier: "he_IL")
        }
        df.dateFormat = "MMMM, yyyy"
        return df
    }()
    
    var didOpenMenu: (() -> Void)?
    
    var sectionData: [SectionProgram] = [] {
        didSet {
            emptyProgramListLabelsStackView.isHidden = !sectionData.isEmpty
            bottomButtonConstraint.isActive = !sectionData.isEmpty
            topButtonConstraint.isActive = sectionData.isEmpty
        }
    }
    
    var isFirstLaunch: Bool = true
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.contentInset = UIEdgeInsets(top: 8, left: 12, bottom: 75, right: 12)
        collectionView.register(reusableViewType: HomeCollectionViewHeader.self)
        if let layout = collectionView.collectionViewLayout as? HomeCollectionViewLayout {
            layout.delegate = self
        }
        collectionView.delegate = self
        navigationController?.navigationBar.setBlueGradient()
        navigationController?.setTitleFont()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if isFirstLaunch {
            getAllPrograms()
            isFirstLaunch = false
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        collectionView.collectionViewLayout.invalidateLayout()
    }
    
    // MARK: - Actions
    @IBAction private func menuTapped(_ sender: Any) { didOpenMenu?() }
    
    @IBAction private func addNewProgramTapped(_ sender: Any) {
        let chooseTrainingVC = ChooseTrainingProgramViewController.instantiate()
        present(chooseTrainingVC, animated: false, completion: nil)
        chooseTrainingVC.didSelectedTraining = { [weak self] trainingType in
            self?.showTrainingFor(type: trainingType)
        }
    }
    
    @IBAction private func showSubscription() {
        let navBar = UINavigationController(rootViewController: SubscriptionViewController.instantiate())
        navBar.modalPresentationStyle = .fullScreen
        present(navBar, animated: true)
    }
    
    var animationIsRunning = false
    
    // MARK: - Private
    private func getAllPrograms() {
        startAnimation()
        ProgramProvider.allPrograms { [weak self] (result) in
            guard let self = self else { return }
            
            switch result {
            case .success(let programs):
                let groupDic = Dictionary(grouping: programs.componentList) { (program) -> DateComponents in
                    let date = Calendar.current.dateComponents([.year, .month], from: (Date(milliseconds: program.programInfo.buildingDate)))
                    return date
                }
                self.sectionData = groupDic.map({ key, item in
                    
                    return SectionProgram(date: self.makeDate(year: key.year ?? 0,
                                                              month: key.month ?? 0,
                                                              day: 1, hr: 5, min: 0, sec: 0), data: item)
                })
                self.sectionData.sort(by: { $0.date > $1.date })
                self.collectionView.collectionViewLayout.invalidateLayout()
                self.collectionView.reloadData()
            case .failure(let error):
                if error.code == 6 {
                } else {
                    self.showAlert(text: "\(error)")
                }
            }
            self.stopAnimation()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.stopAnimation()
            }
        }
    }
    
    func makeDate(year: Int, month: Int, day: Int, hr: Int, min: Int, sec: Int) -> Date {
        let calendar = Calendar(identifier: .gregorian)
        let components = DateComponents(year: year, month: month, day: day, hour: hr, minute: min, second: sec)
        return calendar.date(from: components)!
    }
    
    var isAds: Bool = true
    func updateAllViews(isAds: Bool) {
        self.isAds = isAds
        if !isAds {
            crownButton.isHidden = true
            bottomButtonConstraint.constant = 30
        } else {
            crownButton.isHidden = false
        }
        collectionView?.reloadData()
    }
    
    private func showTrainingFor(type: TrainingType) {
        switch type {
        case .resistance:
            let resistanceVC = ResistanceViewController.instantiate()
            resistanceVC.didSaveProgram = { [weak self] program in
                self?.insertProgram(program: program)
                
            }
            resistanceVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(resistanceVC, animated: true)
        case .circuit:
            let circuitVC = CircuitTrainingViewController.instantiate()
            circuitVC.didSaveProgram = { [weak self] program in
                self?.insertProgram(program: program)
            }
            circuitVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(circuitVC, animated: true)
        case .cardio:
            let cardioVC = CardioTrainingViewController.instantiate()
            cardioVC.didSaveProgram = { [weak self] program in
                self?.insertProgram(program: program)
            }
            cardioVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(cardioVC, animated: true)
        }
    }
    
    func insertProgram(program: Program) {
        let date = Calendar.current.dateComponents([.day, .year, .month], from: (Date(milliseconds: program.programInfo.buildingDate)))
        let newItemDate = makeDate(year: date.year ?? 2020, month: date.month ?? 1, day: 1, hr: 5, min: 0, sec: 0)
        if let section = sectionData.firstIndex(where: { $0.date == newItemDate }) {
            if let index = sectionData[section].data.firstIndex(where: { $0.id == program.id}) {
                sectionData[section].data[index] = program
            } else {
                sectionData[section].data.insert(program, at: 0)
            }
            
        } else {
            sectionData.insert(.init(date: newItemDate, data: [program]), at: 0)
            sectionData.sort(by: { $0.date > $1.date })
        }
        collectionView.collectionViewLayout.invalidateLayout()
        collectionView.reloadData()
    }
    
    private func makeContentForSplitResistance(program: Program) -> [SplitTrainingReadyViewController.Content] {
        let resistance = program.resistanceProgram
        var splitData = Dictionary(grouping: resistance) { $0.group.group }
        splitData[.none] = splitData[.none]?.filter({ $0.exercisesData.count > 0 })
        if splitData[.none]?.count == 0 {
            splitData.removeValue(forKey: .none)
        }
        var content = splitData.map({ SplitTrainingReadyViewController.Content(day: $0.key, exerciseData: $0.value) })
        content.sort(by: { $0.day.number < $1.day.number })
        return content
    }
    
    private func updateProgram(program: Program, at indexPath: IndexPath) {
        animationIsRunning = true
        guard let id = program.id else { return }
        ProgramProvider.editProgram(id: id, program: program) { [weak self] (result) in
            switch result {
            case .success:
                self?.sectionData[indexPath.section].data[indexPath.row] = program
                self?.collectionView.reloadItems(at: [indexPath])
            case .failure(let error):
                self?.showAlert(text: "There is a problem with server, \(error) ")
            }
        }
    }
}

// MARK: - UICollectionViewDataSource
extension HomeViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        sectionData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        sectionData[section].data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(with: HomeCell.self, for: indexPath)
        cell.set(content: .init(imageName: sectionData[indexPath.section].data[indexPath.row].programInfo.imageUrl, title: sectionData[indexPath.section].data[indexPath.row].programInfo.programName, timestamp: sectionData[indexPath.section].data[indexPath.row].programInfo.buildingDate), indexPath: indexPath)
        cell.delegate = self
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension HomeViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionHeader {
            let sectionHeader = collectionView.dequeueReusableView(with: HomeCollectionViewHeader.self, for: indexPath)
            let date = sectionData[indexPath.section].date
            sectionHeader.titleLabel.text = dateFormater.string(from: date)
            return sectionHeader
        } else {
            return UICollectionReusableView()
        }
    }
}

// MARK: - HomeLayoutDelegate
extension HomeViewController: HomeLayoutDelegate {
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
        let width = (collectionView.bounds.width - 24 - 20) / 2
        
        let labelWidth: CGFloat = width - 42 - 8
        let maxSize = CGSize(width: labelWidth, height: CGFloat(Float.infinity))
        let charSize = Constants.returnFontName(style: .semibold, size: 13).lineHeight
        let text = sectionData[indexPath.section].data[indexPath.row].programInfo.programName
        let textSize = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: Constants.returnFontName(style: .semibold, size: 13)], context: nil)
        let linesRoundedUp = Int(ceil(textSize.height/charSize))
        let labelHeight: CGFloat = CGFloat(linesRoundedUp * 19) + 26
        
        return  width * 0.8825 + labelHeight
    }
}

// MARK: - HomeCellDelegate
extension HomeViewController: HomeCellDelegate {
    
    func didDelete(at indexPath: IndexPath) {
        let popUpVC = LogoutViewController.instantiate()
        popUpVC.state = .deleteProgram
        popUpVC.didDelete = { [weak self] in
            guard let id = self?.sectionData[indexPath.section].data[indexPath.row].id else { return }
            ProgramProvider.deleteProgram(id: id) { (result) in
                switch result {
                    
                case .success:
                    self?.sectionData[indexPath.section].data.remove(at: indexPath.item)
                    if self?.sectionData[indexPath.section].data.count == 0 {
                        self?.sectionData.remove(at: indexPath.section)
                    }
                    self?.collectionView.reloadData()
                    if let layout = self?.collectionView.collectionViewLayout as? HomeCollectionViewLayout {
                        layout.invalidateLayout()
                    }
                    self?.showAlert(text: Constants.ResistanceExercises.Messages().programDeleted)
                case .failure(let error):
                    self?.showAlert(text: "Something went wrong, please write to developer, \(error)")
                }
            }
        }
        present(popUpVC, animated: false, completion: nil)
    }
    
    func didShare(at indexPath: IndexPath) {
        if UserDefaults.standard.bool(forKey: Constants.isSubscribedKey) {
            guard let id = sectionData[indexPath.section].data[indexPath.item].id else { return }
            let vc = ShareProgramViewController.instantiate()
            vc.programId = id
            vc.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(vc, animated: true)
        } else {
            let navBar = UINavigationController(rootViewController: SubscriptionViewController.instantiate())
            navBar.modalPresentationStyle = .fullScreen
            present(navBar, animated: true)
        }
    }
    
    func didEdit(at indexPath: IndexPath) {
        switch sectionData[indexPath.section].data[indexPath.item].programType {
        case .cardio:
            let cardioVC = CardioTrainingViewController.instantiate()
            cardioVC.editingState = .edit
            cardioVC.program = sectionData[indexPath.section].data[indexPath.item]
            cardioVC.didSaveProgram = { [weak self] program in
                self?.sectionData[indexPath.section].data[indexPath.item] = program
                self?.collectionView.reloadData()
            }
            cardioVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(cardioVC, animated: true)
        case .resistance:
            let resistanceVC = ResistanceViewController.instantiate()
            resistanceVC.program = sectionData[indexPath.section].data[indexPath.item]
            resistanceVC.editState = .edit
            resistanceVC.didSaveProgram = { [weak self] program in
                self?.sectionData[indexPath.section].data[indexPath.item] = program
                self?.collectionView.reloadData()
            }
            resistanceVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(resistanceVC, animated: true)
        case .circuit:
            let circuitVC = CircuitTrainingViewController.instantiate()
            circuitVC.program = sectionData[indexPath.section].data[indexPath.item]
            circuitVC.editState = .edit
            circuitVC.didSaveProgram = { [weak self] program in
                self?.sectionData[indexPath.section].data[indexPath.item] = program
                self?.collectionView.reloadData()
            }
            circuitVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(circuitVC, animated: true)
        }
    }
    
    func didSelected(at indexPath: IndexPath) {
        switch self.sectionData[indexPath.section].data[indexPath.item].programType {
            
        case .cardio:
            let detailsVC = CardioReadyTrainingViewController.instantiate()
            detailsVC.isCreated = true
            detailsVC.data = self.sectionData[indexPath.section].data[indexPath.row]
            detailsVC.hidesBottomBarWhenPushed = true
            detailsVC.indexPath = indexPath
            detailsVC.delegate = self
            self.navigationController?.pushViewController(detailsVC, animated: true)
        case .resistance:
            let resistanceContent = self.makeContentForSplitResistance(program: self.sectionData[indexPath.section].data[indexPath.item])
            if resistanceContent.count == 1 {
                let vc = ResistanceReadyTrainingViewController.instantiate()
                vc.data = resistanceContent[0].exerciseData
                vc.data.sort(by: { $0.group.orderNumber ?? 0 < $1.group.orderNumber ?? 0 })
                vc.id = self.sectionData[indexPath.section].data[indexPath.row].id
                vc.programInfo = self.sectionData[indexPath.section].data[indexPath.row].programInfo
                if self.sectionData[indexPath.section].data[indexPath.row].cardioProgram.count > 0 {
                    vc.cardioData = self.sectionData[indexPath.section].data[indexPath.row].cardioProgram
                }
                vc.isCreated = true
                vc.didSaveProgram = { [weak self] program in
                    var pr = program
                    pr.id = self?.sectionData[indexPath.section].data[indexPath.row].id
                    self?.sectionData[indexPath.section].data[indexPath.row] = pr
                    self?.updateProgram(program: pr, at: indexPath)
                }
                vc.indexPath = indexPath
                vc.delegate = self
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                let detailsVC = SplitTrainingReadyViewController.instantiate()
                detailsVC.isCreated = true
                detailsVC.content = resistanceContent
                detailsVC.id = self.sectionData[indexPath.section].data[indexPath.row].id
                detailsVC.programInfo = self.sectionData[indexPath.section].data[indexPath.row].programInfo
                detailsVC.cardioData = self.sectionData[indexPath.section].data[indexPath.row].cardioProgram
                detailsVC.didSaveProgram = { [weak self] program in
                    var pr = program
                    pr.id = self?.sectionData[indexPath.section].data[indexPath.row].id
                    self?.sectionData[indexPath.section].data[indexPath.row] = pr
                    self?.updateProgram(program: pr, at: indexPath)
                }
                detailsVC.indexPath = indexPath
                detailsVC.delegate = self
                detailsVC.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(detailsVC, animated: true)
            }
        case .circuit:
            let detailsVC = CircuitReadyTrainingViewController.instantiate()
            detailsVC.program = self.sectionData[indexPath.section].data[indexPath.row]
            detailsVC.didSaveProgram = { [weak self] program in
                self?.updateProgram(program: program, at: indexPath)
            }
            
            detailsVC.indexPath = indexPath
            detailsVC.delegate = self
           
            detailsVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(detailsVC, animated: true)
        }
    }
}

extension HomeViewController: EditProgramDelegate { }
