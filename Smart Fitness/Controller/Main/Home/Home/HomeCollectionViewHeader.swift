//
//  HomeCollectionViewHeader.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 26/07/2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class HomeCollectionViewHeader: UICollectionReusableView {
    
    // MARK: - Outlets
    @IBOutlet weak var titleLabel: UILabel!
    
}
