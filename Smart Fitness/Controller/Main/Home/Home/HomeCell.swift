//
//  HomeCell.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 29.10.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

protocol HomeCellDelegate: class {
    func didDelete(at indexPath: IndexPath)
    func didShare(at indexPath: IndexPath)
    func didEdit(at indexPath: IndexPath)
    func didSelected(at indexPath: IndexPath)
}

final class HomeCell: UICollectionViewCell {
    
    // MARK: - Views
    @IBOutlet weak private var containerView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak  var titleLabel: UILabel!
    @IBOutlet weak  var timeLabel: UILabel!
    @IBOutlet weak private var menuView: UIView!
    @IBOutlet weak var shareButton: UIButton!
    
    @IBOutlet weak var stackView: UIStackView!
    
    // MARK: - Properties
    weak var delegate: HomeCellDelegate?
    private var cellIndexPath: IndexPath?
    
    lazy var dateFormater: DateFormatter = {
        let df = DateFormatter()
        switch LocalizationManager.Language.language {
        case .english:
            df.locale = Locale(identifier: "en")
        case .hebrew:
            df.locale = Locale(identifier: "he_IL")
        }
        df.dateFormat = "MMM dd, yyyy"
        return df
    }()
    
    // MARK: - Properties Context Menu
  //  var didDeleteTapped: (() -> Void)?
 //   var didShareTapped: (() -> Void)?
 //   var didEditTapped: (() -> Void)?
//    var didCellTapped: (() -> Void)?
    
    // MARK: - Init
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.layer.shadowOpacity = 0.4
        menuView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(closeMenuTapped(_:))))
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imageView.image = nil
        titleLabel.text = nil
        timeLabel.text = nil
        menuView.isHidden = true
    }
    
    // MARK: - Action Cell
    @IBAction private func menuTapped(_ sender: Any) {
        menuView.isHidden = false
    }
    
    @IBAction private func closeMenuTapped(_ sender: Any) {
        menuView.isHidden = true
    }
    
    // MARK: - Action Context Menu
    @IBAction private func editAction(_ sender: Any) {
        guard let indexPath = cellIndexPath else { fatalError() }
        delegate?.didEdit(at: indexPath)
       // didEditTapped?()
        menuView.isHidden = true
    }
    
    @IBAction private func shareAction(_ sender: Any) {
         guard let indexPath = cellIndexPath else { fatalError() }
        delegate?.didShare(at: indexPath)
      //  didShareTapped?()
        menuView.isHidden = true
    }
    
    @IBAction private func deleteAction(_ sender: Any) {
         guard let indexPath = cellIndexPath else { fatalError() }
        delegate?.didDelete(at: indexPath)
      //  didDeleteTapped?()
        menuView.isHidden = true
    }
    
    @IBAction func didTapCell(_ sender: Any) {
         guard let indexPath = cellIndexPath else { fatalError() }
        delegate?.didSelected(at: indexPath)
      //  didCellTapped?()
    }
    
    // MARK: - Methods
    func set(content: Content, indexPath: IndexPath) {
        self.cellIndexPath = indexPath
        if let imageUrl = content.imageName {
            imageView.downloadImage(from: imageUrl)
        } else {
            imageView.image = "programHome2".image()
        }
        titleLabel.text = content.title
        timeLabel.text =  dateFormater.string(from: Date(milliseconds: content.timestamp))
        
        if !UserDefaults.standard.bool(forKey: Constants.isSubscribedKey) {
            shareButton.setImage("crown".image(), for: .normal)
            shareButton.setImage("crown".image(), for: .selected)
        }
    }
}

// MARK: - Content
extension HomeCell {
    struct Content {
        var imageName: String?
        var title: String
        var timestamp: Int64
    }
}
