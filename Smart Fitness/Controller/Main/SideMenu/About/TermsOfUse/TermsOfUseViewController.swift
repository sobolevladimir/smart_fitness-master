//
//  TermsOfUseViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 23.02.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import WebKit

final class TermsOfUseViewController: UIViewController, Storyboarded {
    
    // MARK: - Views
    @IBOutlet var progressView: UIProgressView!
    private  var webView: WKWebView!
    
    // MARK: - Properties
    private var observation: NSKeyValueObservation?
    var urlString: String!
    
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        fixDirection()
        
        progressView = UIProgressView(progressViewStyle: .default)
        progressView.sizeToFit()
        if let urlString = urlString {
            let url = URL(string: urlString)!
            webView.load(URLRequest(url: url))
            webView.allowsBackForwardNavigationGestures = true
          webView.addObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress), options: .new, context: nil)
        }
        
        
        navigationController?.navigationBar.setBlueGradient()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
       
        guard let backgroundView = navigationController?.navigationBar.value(forKey: "backgroundView") as? UIView else { return }
        
        for view in backgroundView.subviews {
            if view is UINavigationBarGradientView {
                view.removeFromSuperview()
                break
            }
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "estimatedProgress" {
            progressView.progress = Float(webView.estimatedProgress)
        }
    }

    deinit {
        observation = nil
    }
    
    override func loadView() {
        webView = WKWebView()
        webView.navigationDelegate = self
        view = webView
    }
    
    // MARK: - Action
    @IBAction func backDidTap(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}

extension TermsOfUseViewController: WKNavigationDelegate {
    
}

