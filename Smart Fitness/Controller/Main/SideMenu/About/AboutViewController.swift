//
//  AboutViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 29.10.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class AboutViewController: BaseViewController {
    
    @IBOutlet weak var versionLabel: UILabel!
    
    // MARK: - Properties
    var didOpenMenu: (() -> Void)?
    
    var appVersion: String {
        get {
            if let version = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String {
                return version
            }
            return "1.0"
        }
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.setBlueGradient()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.setBlueGradient()
        versionLabel.text = "v\(appVersion)"
    }
    
    // MARK: - Action
    @IBAction func menuDidTap(_ sender: Any) {
        didOpenMenu?()
    }
    
    @IBAction func termsOfUseDidTap(_ sender: Any) {
        let vc = TermsOfUseViewController.instantiate()
        vc.urlString = "https://www.smartfitnessapp.com/termsofservice/"
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func privacyPolicyDidTap(_ sender: Any) {
        let vc = TermsOfUseViewController.instantiate()
        vc.urlString = "https://www.smartfitnessapp.com/privacypolicy/"
        navigationController?.pushViewController(vc, animated: true)
    }
}
