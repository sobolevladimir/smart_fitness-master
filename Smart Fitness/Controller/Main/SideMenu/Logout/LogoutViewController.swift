//
//  LogoutViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 30.10.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import FBSDKLoginKit

enum PresentedLogoutVCState {
    case logout, save, deleteProgram, deleteCardioActivity, deleteSplit, unlinkExercises, deleteRound, leaveWithoutSaving, stopReceivingProgram(userName: String?), deleteImage, deleteExercise
}

final class LogoutViewController: UIViewController, Storyboarded {
    
    // MARK: - Views
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var noButton: LoginButton!
    @IBOutlet weak var yesButton: LoginButton!
    
   
    // MARK: - Properties
    var state: PresentedLogoutVCState = .logout
    var didDelete: (() -> Void)?
    var didAddTapped: (() -> Void)?
    var didFinishTapped: (() -> Void)?
          
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.alpha = 0
          setViewsTo(state: state)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        UIView.animate(withDuration: 0.1) { [weak self] in
            self?.view.alpha = 1
        }
    }
   
    // MARK: - Actions
    @IBAction func closeTapped(_ sender: Any) {
        dismissVC()
    }
    
    @IBAction func yesTapped(_ sender: Any) {
        switch state {
        case .logout:
            logout()
        case .save:
            didAddTapped?()
            dismissVC()
        case .deleteProgram, .deleteCardioActivity, .deleteSplit, .unlinkExercises, .leaveWithoutSaving, .stopReceivingProgram:
            didDelete?()
            dismissVC()
        case .deleteRound:
            didDelete?()
            dismissVC()
        case .deleteImage:
            dismissVC() { [weak self] in
                self?.didDelete?()
            }
        case .deleteExercise:
            didDelete?()
            dismissVC()
        }
    }
    
    @IBAction func noTapped(_ sender: Any) {
        if case .save = state {
            didFinishTapped?()
        }
        dismissVC()
    }
    
    // MARK: - Private
    private func dismissVC(completion: @escaping () -> Void = {}) {
        UIView.animate(withDuration: 0.1, animations: { [weak self] in
            self?.view.alpha = 0
        }) { [weak self] _ in
            self?.dismiss(animated: false, completion: completion)
        }
    }
    
    private func logout() {
        UserProvider.logout { [weak self] (result) in
            switch result {
            case .success(_):
                FBSDKLoginKit.LoginManager().logOut()
                UserAuthManager.logoutUser()
                RedirectManager.shared.presentRootController()
               // SubscriptionManager.bannerView = nil
                self?.dismiss(animated: false, completion: nil)
            case .failure(let error):
                print(error)
            }
        }
    }
    
    private func setViewsTo(state: PresentedLogoutVCState) {
        switch state {
        case .logout:
            break
        case .save:
            titleLabel.text = Constants.PopUps().addCardio
            yesButton.setTitle(Constants.PopUps().addString, for: .normal)
            noButton.setTitle(Constants.PopUps().noFinishString, for: .normal)
        case .deleteProgram:
            titleLabel.text = Constants.PopUps().deleteProgram
        case .deleteCardioActivity:
            titleLabel.text = Constants.PopUps().deleteActivity
        case .deleteSplit:
            titleLabel.numberOfLines = 3
            titleLabel.text = Constants.PopUps().deleteSplitTraining
        case .unlinkExercises:
             titleLabel.text = Constants.PopUps().unlinkExercises
        case .deleteRound:
            titleLabel.text = Constants.PopUps().deleteRound
        case .leaveWithoutSaving:
            titleLabel.text = Constants.PopUps().leaveWithoutSaving
        case .stopReceivingProgram(let userName):
             titleLabel.text = "\(stopReceiving) \(userName ?? "Uknown")\(permanently)"
        case .deleteImage:
            titleLabel.text = deleteImage
        case .deleteExercise:
            titleLabel.text = Constants.PopUps().deleteExercise
        }
    }
    
    var deleteImage: String { "Delete image ?".localized() }
    var stopReceiving: String { "Stop receiving programs\nfrom".localized() }
    var permanently: String { "\npermanently ?".localized()}
}

extension LogoutViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return touch.view == gestureRecognizer.view
    }
}


