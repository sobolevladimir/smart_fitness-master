//
//  SideMenuCell.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 28.10.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class SideMenuCell: UITableViewCell {
    
    // MARK: - Views
    @IBOutlet weak private var menuImage: UIImageView!
    @IBOutlet weak private var menuTitle: UILabel!
    @IBOutlet weak private var comingSoonImage: UIImageView!
    @IBOutlet weak private var shadowView: UIView!
    
    // MARK: - Constraints
    @IBOutlet weak private var bottomConstraint: NSLayoutConstraint!
    
    // MARK: - Properties
    var menuType: MenuType!
    var didSelectRow: (() -> Void)?

    @IBAction func cellDidTap(_ sender: Any) {
        didSelectRow?()
    }
    // MARK: - Methods
    func set(content: Content) {
        menuImage.image = UIImage(named: content.imageName)
        menuTitle.text = content.title
        comingSoonImage.isHidden = content.comingSoonImageIsHidden
        let size: CGFloat
        let deviceName = UIDevice.modelName
        if deviceName.contains("iPhone SE") || deviceName.contains("iPhone 5s") {
                       size = 12
            bottomConstraint.constant = 2
        } else {
            size = 16
        }
        menuTitle.font = content.viewSelected == menuType ? Constants.returnFontName(style: .extraBold, size: size) : Constants.returnFontName(style: .regular, size: size)
        shadowView.isHidden = !(content.viewSelected == menuType)
    }
}

extension SideMenuCell {
    struct Content {
        var imageName: String
        var title: String {
            return viewSelected.localizeString
        }
        var comingSoonImageIsHidden = true
        var viewSelected: MenuType
        var isSelected = false
    }
}
