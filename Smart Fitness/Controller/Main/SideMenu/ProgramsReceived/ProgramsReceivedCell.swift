//
//  ProgramsReceivedCell.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 29.10.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class ProgramsReceivedCell: UITableViewCell {
    
    // MARK: - Views
    @IBOutlet weak private var programImageView: UIImageView!
    @IBOutlet weak private var programNameLabel: UILabel!
    @IBOutlet weak private var receivedLabel: UILabel!
    @IBOutlet weak private var fromLabel: UILabel!
    @IBOutlet weak private var acceptRejectStackView: UIStackView!
    
    // MARK: - Properties
    var didPinTap: (() -> Void)?
    var deleteDidTap: (() -> Void)?
    var viewDidTap: (() -> Void)?
    
    var acceptDidTap: (() -> Void)?
    var rejectDidTap: (() -> Void)?
    
    lazy var dateFormatter: DateFormatter = {
        let df = DateFormatter()
        switch LocalizationManager.Language.language {
        case .english:
            df.locale = Locale(identifier: "en")
        case .hebrew:
           df.locale = Locale(identifier: "he_IL")
        }
       df.dateFormat = "MMM dd, yyyy"
        return df
    }()
    
    // MARK: - Methods
    func set(content: Content) {
        programImageView.downloadImage(from: content.imageUrl, placeholder: programImageView.image)
        programNameLabel.text = programName + " " + content.programName
        receivedLabel.text = dateFormatter.string(from: Date.init(milliseconds: content.receivedDate))
        fromLabel.text = content.senderName
    }
    
    // MARK: - Actions
    @IBAction private func didPinTap(_ sender: Any) { didPinTap?() }
    @IBAction private func viewDidTap(_ sender: Any) { viewDidTap?() }
    @IBAction private func deleteDidTap(_ sender: Any) { deleteDidTap?() }
    
    @IBAction func acceptDidTap(_ sender: Any) {
        acceptDidTap?()
    }
    
    @IBAction func rejectDidTap(_ sender: Any) {
        rejectDidTap?()
    }
}

extension ProgramsReceivedCell {
    struct Content {
        var imageUrl: String?
        var programName: String
        var receivedDate: Int64
        var senderName: String
        var accepted: Bool
    }
}

extension ProgramsReceivedCell {
    var programName: String { "Program Name:".localized() }
}
