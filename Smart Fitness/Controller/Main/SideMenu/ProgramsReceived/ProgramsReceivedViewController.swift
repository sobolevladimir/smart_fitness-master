//
//  ProgramsReceivedViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 29.10.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class ProgramsReceivedViewController: BaseViewController {
    
    // MARK: - Views
    @IBOutlet weak private var noProgramsLabel: UILabel!
    @IBOutlet weak private var tableView: UITableView!
    
    // MARK: - Properties
    var didOpenMenu: (() -> Void)?
    var data: [SharedProgram] = [] {
        didSet {
            checkProgramsCount(data: data)
        }
    }
    var didAddProgram: ((Program) -> Void)?
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.setBlueGradient()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getReceivedPrograms()
    }
    
    // MARK: - Action
    @IBAction func menuDidTap(_ sender: Any) {
        didOpenMenu?()
    }
    
    // MARK: - Private
    private func checkProgramsCount(data: [Any]) {
        if data.isEmpty {
            noProgramsLabel.isHidden = false
        } else {
            noProgramsLabel.isHidden = true
        }
    }
    
    private func makeContentForSplitResistance(program: Program) -> [SplitTrainingReadyViewController.Content] {
        let resistance = program.resistanceProgram
        var splitData = Dictionary(grouping: resistance) { $0.group.group }
        splitData[.none] = splitData[.none]?.filter({ $0.exercisesData.count > 0 })
        if splitData[.none]?.count == 0 {
            splitData.removeValue(forKey: .none)
        }
        var content = splitData.map({ SplitTrainingReadyViewController.Content(day: $0.key, exerciseData: $0.value) })
        content.sort(by: { $0.day.number < $1.day.number })
        return content
    }
    
    private func getReceivedPrograms() {
        startAnimation()
        ShareManager.getSharedPrograms { [weak self] (result) in
            guard let self = self else { return }
            self.stopAnimation()
            switch result {
            case .success(let programs):
                self.data = programs.componentList
                self.tableView.reloadData()
                self.checkProgramsCount(data: self.data)
            case .failure(let error):
                self.showAlert(text: "\(error)")
            }
        }
    }
}

// MARK: - UITableViewDataSource
extension ProgramsReceivedViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(with: ProgramsReceivedCell.self, for: indexPath)
        cell.set(content: .init(imageUrl: data[indexPath.row].program.programInfo.imageUrl, programName: data[indexPath.row].program.programInfo.programName, receivedDate: data[indexPath.row].date ?? 0, senderName: data[indexPath.row].username ?? "Unknown", accepted: data[indexPath.row].accept))
        cell.acceptDidTap = { [weak self] in
            guard let self = self else { return }
            guard let id = self.data[indexPath.row].program.id else { return }
            ShareManager.acceptSharedProgram(programId: id) { (result) in
                switch result {
                case .success:
                    let popUpVC = ComingSoonViewController.instantiate()
                    popUpVC.state = .pinned
                    self.didAddProgram?(self.data[indexPath.row].program)
                    self.present(popUpVC, animated: false)
                    self.data.remove(at: indexPath.row)
                    tableView.performBatchUpdates({
                        tableView.deleteRows(at: [indexPath], with: .left)
                    }) { (_) in
                        tableView.reloadData()
                    }
                case .failure(let error):
                    self.showAlert(text: "\(error)")
                }
            }
        }
         
        cell.rejectDidTap = {[weak self] in
            let popUpVC = LogoutViewController.instantiate()
            popUpVC.state = .stopReceivingProgram(userName: self?.data[indexPath.row].username)
            self?.present(popUpVC, animated: false, completion: nil)
            popUpVC.didDelete = { [weak self] in
                guard let self = self else { return }
                guard let id = self.data[indexPath.row].program.id else { return }
                ShareManager.rejectSharedProgram(programId: id) { (result) in
                    switch result {
                    case .success(_):
                        self.data.remove(at: indexPath.row)
                        tableView.performBatchUpdates({
                            tableView.deleteRows(at: [indexPath], with: .left)
                        }) { (_) in
                            tableView.reloadData()
                        }
                    case .failure(let error):
                        self.showAlert(text: "\(error)")
                    }
                }
            }
        }
        
        cell.deleteDidTap = { [weak self] in
            let popUpVC = LogoutViewController.instantiate()
            popUpVC.state = .deleteProgram
            self?.present(popUpVC, animated: false, completion: nil)
            popUpVC.didDelete = { [weak self] in
                guard let self = self else { return }
                 let id = self.data[indexPath.row].id
                ShareManager.deleteSharedProgram(programId: id) { (result) in
                    switch result {
                    case .success(_):
                        self.data.remove(at: indexPath.row)
                        tableView.performBatchUpdates({
                            tableView.deleteRows(at: [indexPath], with: .automatic)
                        }) { (_) in
                            tableView.reloadData()
                        }
                    case .failure(let error):
                        self.showAlert(text: "\(error)")
                    }
                }
            }
        }
        
        cell.viewDidTap = {  [weak self] in
            guard let self = self else { return }
            switch self.data[indexPath.item].program.programType {
                
            case .cardio:
                let detailsVC = CardioReadyTrainingViewController.instantiate()
                detailsVC.data = self.data[indexPath.row].program
                self.navigationController?.pushViewController(detailsVC, animated: true)
            case .resistance:
                let resistanceContent = self.makeContentForSplitResistance(program: self.data[indexPath.item].program)
                if resistanceContent.count == 1 {
                    let vc = ResistanceReadyTrainingViewController.instantiate()
                    vc.data = resistanceContent[0].exerciseData
                    vc.programInfo = self.data[indexPath.row].program.programInfo
                    if self.data[indexPath.row].program.cardioProgram.count > 0 {
                        vc.cardioData = self.data[indexPath.row].program.cardioProgram
                    }
                    vc.didSaveProgram = { [weak self] program in
                        self?.data[indexPath.item].program = program
                    }
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    let detailsVC = SplitTrainingReadyViewController.instantiate()
                    detailsVC.content = resistanceContent
                    detailsVC.programInfo = self.data[indexPath.row].program.programInfo
                    detailsVC.cardioData = self.data[indexPath.row].program.cardioProgram
                    detailsVC.didSaveProgram = { [weak self] program in
                        self?.data[indexPath.item].program = program
                    }
                    self.navigationController?.pushViewController(detailsVC, animated: true)
                }
            case .circuit:
                let detailsVC = CircuitReadyTrainingViewController.instantiate()
                detailsVC.program = self.data[indexPath.row].program
                detailsVC.didSaveProgram = { [weak self] program in
                    self?.data[indexPath.row].program = program
                }
                self.navigationController?.pushViewController(detailsVC, animated: true)
            }
        }
        return cell
    }
}
