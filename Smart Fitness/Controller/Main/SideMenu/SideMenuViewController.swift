//
//  SideV.swift
//  Smart Fitness
//
//  Created by Rusłan Chamski on 27/10/2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

enum MenuType {
    case home, program, language, activity, gps, feedback, share, about, logout, profile, none
    
    var localizeString: String {
        switch self {
        case .home:
            return Constants.SideMenu().homeString
        case .program:
            return Constants.SideMenu().programString
        case .language:
            return Constants.SideMenu().languageString
        case .activity:
            return Constants.SideMenu().activityString
        case .gps:
            return Constants.SideMenu().gpsString
        case .feedback:
            return Constants.SideMenu().feedbackString
        case .share:
            return Constants.SideMenu().shareString
        case .about:
            return Constants.SideMenu().aboutString
        case .logout:
            return Constants.SideMenu().logoutString
        case .profile:
            return "Profile"
        case .none:
            return "none"
        }
    }
}

final class SideMenuViewController: BaseViewController {
    
    
    
    @IBOutlet var contentView: DraggableHorizontalSideView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var selectionView: UIView!
    @IBOutlet weak var userImageView: UIImageView!
    
    // MARK: - Properties
    var closeAction: (() -> Void)?
    var didSelectMenuType: ((MenuType) -> Void)?
    var selectedCell: MenuType = .home {
        didSet {
            selectionView.isHidden = selectedCell != .profile
            usernameLabel.font = selectedCell == .profile ? Constants.returnFontName(style: .extraBold, size: 23) : Constants.returnFontName(style: .regular, size: 23)
        }
    }
    var data: [SideMenuCell.Content] = [SideMenuCell.Content(imageName: "homeIcon", viewSelected: .home),
                                        SideMenuCell.Content(imageName: "ic_program",  viewSelected: .program),
                                        SideMenuCell.Content(imageName: "ic_language", viewSelected: .language),
                                        SideMenuCell.Content(imageName: "ic_track", comingSoonImageIsHidden: false, viewSelected: .activity),
                                        SideMenuCell.Content(imageName: "ic_location", comingSoonImageIsHidden: false, viewSelected: .gps),
                                        SideMenuCell.Content(imageName: "ic_feedback", viewSelected: .feedback),
                                        SideMenuCell.Content(imageName: "ic_share_menu", viewSelected: .share),
                                        SideMenuCell.Content(imageName: "ic_about", viewSelected: .about),
                                        SideMenuCell.Content(imageName: "ic_logout", viewSelected: .logout)]
       
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        usernameLabel.text = UserAuthManager.user?.firstName ?? UserAuthManager.user?.username
        userImageView.downloadImage(from: UserAuthManager.user?.imageUrl, placeholder: userImageView.image)
        selectionView.isHidden = true
        tableView.reloadData()
        view.layoutSubviews()
    }

    // MARK: - Actions
    @IBAction func closeTapped(_ sender: Any) {
         closeAction?()
    }
    
    @IBAction func profileDidTap(_ sender: Any) {
        didSelectMenuType?(.profile)
        selectedCell = .profile
        tableView.reloadData()
    }
    
    // MARK: - Private
    private func selectCell(at indexPath: IndexPath) {
        didSelectMenuType?(data[indexPath.row].viewSelected)
        switch data[indexPath.row].viewSelected {
        case .home, .about, .program:
            selectedCell = data[indexPath.row].viewSelected
        default: break
        }
    }
}

// MARK: - UITableViewDataSource
extension SideMenuViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(with: SideMenuCell.self, for: indexPath)
        cell.menuType = selectedCell
        cell.set(content: data[indexPath.row])
        cell.didSelectRow = { [weak self] in
            self?.selectCell(at: indexPath)
            tableView.reloadData()
        }
        return cell
    }
}

// MARK: - UIGestureRecognizerDelegate
extension SideMenuViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
