//
//  ComingSoonViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 25.11.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class ComingSoonViewController: UIViewController, Storyboarded {
    
    enum PopUpState {
        case comingSoon, pinned, supersetCreated, twoExercises, limitIsSix(String), writeExercise, noExercises, oneValue, addStory, failedToUpload
    }
    
    // MARK: - Views
    @IBOutlet weak private var titleLabel: UILabel!
    @IBOutlet weak private var closeButton: UIButton!
    @IBOutlet weak private var worningImageView: UIImageView!
    @IBOutlet weak private var extraTitleLabel: UILabel!
    @IBOutlet weak private var textStackView: UIStackView!
    @IBOutlet weak private var okButton: LoginButton!
    
    // MARK: - Constraints
  
    // MARK: - Properties
    var state: PopUpState = .comingSoon
          
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.alpha = 0
        setViewsTo(state: state)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        UIView.animate(withDuration: 0.1) { [weak self] in
            self?.view.alpha = 1
        }
    }
    
    // MARK: - Actions
    @IBAction func closeTapped(_ sender: Any) {
        dismissVC()
    }
    
    // MARK: - Methods
    func dismissVC() {
        UIView.animate(withDuration: 0.1, animations: { [weak self] in
            self?.view.alpha = 0
        }) { [weak self] _ in
            self?.dismiss(animated: false, completion: nil)
        }
    }
    
    // MARK: - Private
    private func setViewsTo(state: PopUpState) {
        switch state {
        case .comingSoon:
            break
        case .pinned:
            titleLabel.text = Constants.PopUps().pinnedProgram
        case .supersetCreated:
            titleLabel.text = Constants.PopUps().supersetCreated
        case .twoExercises:
            titleLabel.text = Constants.PopUps().chooseTwoExercises
        case .limitIsSix(let amount):
            titleLabel.text = "\(Constants.PopUps().theLimitIs) \n\(amount) \(Constants.ResistanceExercises.Methods().exercisesString)."
        case .writeExercise:
            closeButton.isHidden = true
            titleLabel.text = Constants.ResistanceExercises.Messages().writeYourExercise
        case .noExercises:
            titleLabel.text = Constants.ResistanceExercises.Messages().noExercises
        case .oneValue:
            titleLabel.text = Constants.PopUps().fillOneValue
        case .addStory:
            okButton.setTitle(Constants.ProgramInfo().done.uppercased(), for: .normal)
            textStackView.spacing = -4
            extraTitleLabel.isHidden = false
            titleLabel.text = Constants.Profile().storyAdded
        case .failedToUpload:
             textStackView.spacing = 19
            worningImageView.isHidden = false
            titleLabel.text = Constants.Profile().failedToLoad
        }
    }
}

extension ComingSoonViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return touch.view == gestureRecognizer.view
    }
}
