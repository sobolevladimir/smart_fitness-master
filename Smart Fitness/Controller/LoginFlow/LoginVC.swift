//
//  LoginVC.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 13/03/19.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class LoginVC: BaseViewController {
    
    // MARK: - Views
    @IBOutlet weak private var usernameTextField: UITextField!
    @IBOutlet weak private var passwordTextField: UITextField!
    @IBOutlet weak private var errorUsernameButton: UIButton!
    @IBOutlet weak private var errorPasswordButton: UIButton!
    @IBOutlet weak private var appleButton: LoginButton!
    
    // MARK: - Constraints
    @IBOutlet weak private var logoTopConstraint: NSLayoutConstraint!
    
    // MARK: - Properties
    private lazy var socialLoginManager = SocialLoginManager()
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            appleButton.isHidden = false
        } else {
            appleButton.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        navigationController?.navigationBar.isHidden = false
        logoTopConstraint.constant = UIApplication.shared.statusBarFrame.height + 4
        fixDirection()
        view.layoutIfNeeded()
    }
    
    // MARK: - Actions
    @IBAction private func tapAction(_ sender: Any) {
        view.endEditing(true)
    }
    
    @IBAction private func backButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction private func errorTapped(_ sender: UIButton) {
        checkCredentials { [weak self] (result) in
            switch result {
            case .success(_): break
            case .failure(let error):
                if case .userNameAndPassword = error {
                    self?.showErrorMessage(title: "", text: ConstantMessages().incorrectUserAndPassword)
                } else {
                     self?.showErrorMessage(title: error.failureReason!, text: error.localizedDescription)
                }
            }
        }
    }
    
    @IBAction private func usernameIsEditing(_ sender: Any) {
        if !errorUsernameButton.isHidden {
            errorUsernameButton.isHidden.toggle()
        }
    }
    
    @IBAction private func passwordIsEditing(_ sender: Any) {
        if !errorPasswordButton.isHidden {
            errorPasswordButton.isHidden.toggle()
        }
    }
    
    
    @IBAction private func forgotPasswordTapped(_ sender: Any) {
        let vc = ForgotPasswordVC.instantiate()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction private func loginTapped(_ sender: Any) {
        view.endEditing(true)
        if !isInternetAvailable() {
            showErrorMessage(title: "", text: Constants.Internet().noInternet)
        } else {
           
            checkCredentials { [weak self] (result) in
                self?.stopAnimation()
                switch result {
                case .failure(let error):
                    if case .userNameAndPassword = error {
                        self?.showErrorMessage(title: "", text: ConstantMessages().incorrectUserAndPassword)
                    } else {
                        self?.showErrorMessage(title: error.failureReason!, text: error.localizedDescription)
                    }
                    self?.errorPasswordButton.isHidden = false
                    self?.errorUsernameButton.isHidden = false
                    
                case .success(let token):
                    if let token = token.token {
                        UserAuthManager.setToken(token)
                       RedirectManager.shared.presentRootController()
                    }
                }
            }
        }
    }
    
    @IBAction private func appleTapped(_ sender: Any) {
        socialLoginManager.apple(from: self) { [weak self] (result) in
            self?.handleLoginCompletion(result: result)
        }
    }
    
    @IBAction private func facebookTapped(_ sender: Any) {
        socialLoginManager.facebook(from: self) { [weak self] (result) in
            self?.handleLoginCompletion(result: result)
        }
    }
    
    @IBAction private func googleTapped(_ sender: Any) {
        socialLoginManager.google(from: self) { [weak self] (result) in
            self?.handleLoginCompletion(result: result)
        }
    }
    
    @IBAction private func twitterTapped(_ sender: Any) {
        socialLoginManager.twitter(from: self) { [weak self] (result) in
            self?.handleLoginCompletion(result: result)
        }
    }
    
    @IBAction private func termsOfUseTapped(_ sender: Any) {
        let vc = TermsOfUseViewController.instantiate()
        vc.urlString = "https://www.smartfitnessapp.com/termsofservice/"
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction private func privacyPolicyTapped(_ sender: Any) {
        let vc = TermsOfUseViewController.instantiate()
        vc.urlString = "https://www.smartfitnessapp.com/privacypolicy/"
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    // MARK: - Private
    private func handleLoginCompletion(result: Result<Token, Error>) {
        switch result {
        case .failure(let error):
            guard let error = error as? NetworkError else { return }
            showErrorMessage(title: "Error", text: error.message ?? "")
        case .success(let token):
            if let token = token.token {
                UserAuthManager.setToken(token)
               RedirectManager.shared.presentRootController()
            }
        }
    }
    
    private func checkCredentials(completion: @escaping (Result<Token, CredentialsError>) -> Void) {
       if usernameTextField.text!.count < 3 && passwordTextField.text!.count < 6 {
        completion(.failure(.userNameAndPassword))
        return
        }
        
        guard usernameTextField.text!.count >= 3 else {
            completion(.failure(.userNameAndPassword))
            return
        }
        
        guard !usernameTextField.text!.contains(" ") else {
            completion(.failure(.userNameAndPassword))
            return
        }
        
        guard isValidPassword(password: passwordTextField.text) else {
            completion(.failure(.userNameAndPassword))
            return
        }
        
        guard let username = usernameTextField.text,
            let password = passwordTextField.text else {
                completion(.failure(.userNameAndPassword))
                return
        }
         startAnimation()
        socialLoginManager.loginWithEmail(username: username, password: password) { (response) in
             self.stopAnimation()
            switch response {
                
            case .success(let result):
                completion(.success(result))
            case .failure(let error):
                if error.code == 404 {
                    completion(.failure(.userIsNotFound(message: error.message ?? "")))
                    return
                    }
                if error.code == 400 {
                    completion(.failure(.wrongPassword(message: error.message ?? "") ))
                    return
                }
               
            }
        }
    }
    
    private func showErrorMessage(title: String, text: String) {
        let alert = UIAlertController(title: title, message: text, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: Constants.LoginFlow.Decision().ok, style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    
    public func isValidPassword(password: String?) -> Bool {
        let passwordRegex = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{6,}$"
        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: password)
    }
}

// MARK: - UITextFieldDelegate
extension LoginVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    
        if textField == usernameTextField {
            passwordTextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
            loginTapped((Any).self)
        }
        return true
    }
}

extension LoginVC {
    struct ConstantMessages {
        var incorrectUserAndPassword: String { "The username/email or password is incorrect".localized() }
    }
}
