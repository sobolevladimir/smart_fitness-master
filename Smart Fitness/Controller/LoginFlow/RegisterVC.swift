//
//  LoginVC.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 13/03/19.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class RegisterVC: BaseViewController, KeyboardHandler {
    
    // MARK: - Views
    @IBOutlet weak private var usernameTextField: UITextField!
    @IBOutlet weak private var emailTextField: UITextField!
    @IBOutlet weak private var passwordTextField: UITextField!
    @IBOutlet weak private var errorUsernameButton: UIButton!
    @IBOutlet weak private var errorEmailButton: UIButton!
    @IBOutlet weak private var errorPasswordButton: UIButton!
    
    @IBOutlet weak private var appleButton: LoginButton!
    
    // MARK: - Constraints
    @IBOutlet weak private var logoTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    
    // MARK: - Properties
    private lazy var socialLoginManager = SocialLoginManager()
    private var emailErrorMessage = "email"
    private var usernameErrorMessage = "username"
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            appleButton.isHidden = false
        } else {
            appleButton.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        navigationController?.navigationBar.isHidden = false
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        logoTopConstraint.constant = UIApplication.shared.statusBarFrame.height
        fixDirection()
    }
    
    // MARK: - Actions
    @IBAction private func backButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction private func tapAction(_ sender: Any) {
        view.endEditing(true)
    }
    
    @IBAction private func errorTapped(_ sender: UIButton) {
        if sender.tag == 400 {
            showErrorMessage(title: "", text: Constants.LoginFlow.LocalizedDescription().passwordEmpty)
        } else
            if sender.tag == 1000 {
                showErrorMessage(title: "", text: usernameErrorMessage)
            } else {
                showErrorMessage(title: "", text: emailErrorMessage)
        }
    }
    
    @IBAction private func usernameIsEditing(_ sender: Any) {
        errorUsernameButton.isHidden = true
    }
    
    @IBAction private func emailIsEditing(_ sender: Any) {
        errorEmailButton.isHidden = true
    }
    
    @IBAction private func passwordIsEditing(_ sender: Any) {
        errorPasswordButton.isHidden = true
    }
    
    @IBAction private func signUpTapped(_ sender: Any) {
        view.endEditing(true)
        checkCredentials { [weak self] (result) in
            self?.stopAnimation()
            switch result {
            case .failure(let error):
                switch error {
                case .passwordEmpty, .wrongPassword:
                    self?.errorPasswordButton.isHidden = false
                case .emailAlreadyExists:
                    self?.errorEmailButton.isHidden = false
                    self?.emailErrorMessage = Constants.LoginFlow.LocalizedDescription().emailTaken
                case  .emailIsEmpty, .emailIsNotValid, .emailIsNotValidLength:
                    self?.errorEmailButton.isHidden = false
                    self?.emailErrorMessage = error.localizedDescription
                case  .userAlreadyExists:
                    self?.errorUsernameButton.isHidden = false
                    self?.usernameErrorMessage = Constants.LoginFlow.LocalizedDescription().usernameTaken
                case .usernameEmpty, .usernameHasSpace, .userIsNotFound:
                    self?.errorUsernameButton.isHidden = false
                    self?.usernameErrorMessage = error.localizedDescription
                case .wrongEmailUserName:
                     self?.errorEmailButton.isHidden = false
                     self?.errorUsernameButton.isHidden = false
                     self?.usernameErrorMessage = error.failureReason ?? ""
                     self?.emailErrorMessage = error.localizedDescription
                    return
                case .userNameAndPassword, .other:
                    break
                }
            case .success(let token):
                if let token = token.token {
                    UserAuthManager.setToken(token)
                    RedirectManager.shared.presentRootController()
                } else {
                    print("error needs to be fetched")
                }
            }
        }
    }
    
    @IBAction private func appleTapped(_ sender: Any) {
        socialLoginManager.apple(from: self) { [weak self] (result) in
            self?.handleLoginCompletion(result: result)
        }
    }
    
    @IBAction private func facebookTapped(_ sender: Any) {
        socialLoginManager.facebook(from: self) { [weak self] (result) in
            self?.handleLoginCompletion(result: result)
        }
    }
    
    @IBAction private func googleTapped(_ sender: Any) {
        socialLoginManager.google(from: self) { [weak self] (result) in
            self?.handleLoginCompletion(result: result)
        }
    }
    
    @IBAction private func twitterTapped(_ sender: Any) {
        socialLoginManager.twitter(from: self) { [weak self] (result) in
            self?.handleLoginCompletion(result: result)
        }
    }
    
    @IBAction private func termsOfUseTapped(_ sender: Any) {
        let vc = TermsOfUseViewController.instantiate()
        vc.urlString = "https://www.smartfitnessapp.com/termsofservice/"
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction private func privacyPolicyTapped(_ sender: Any) {
        let vc = TermsOfUseViewController.instantiate()
        vc.urlString = "https://www.smartfitnessapp.com/privacypolicy/"
        navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: - Private
    private func handleLoginCompletion(result: Result<Token, Error>) {
        switch result {
        case .failure(let error):
            guard let error = error as? NetworkError else { return }
            showErrorMessage(title: "Error", text: error.message ?? "")
        case .success(let token):
            if let token = token.token {
                UserAuthManager.setToken(token)
               RedirectManager.shared.presentRootController()
            } else {
                print("error needs to be fetched")
            }
        }
    }
    
    private func showErrorMessage(title: String, text: String) {
        let alert = UIAlertController(title: title, message: text, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: Constants.LoginFlow.Decision().ok, style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    
    private func handleUsername() -> CredentialsError? {
        guard let name = usernameTextField.text else { return CredentialsError.usernameEmpty}
        guard !name.contains(" ") else { return CredentialsError.usernameHasSpace}
        guard name.count >= 3 else { return CredentialsError.usernameEmpty}
        return nil
    }
    
    private func handleEmail(email: String) -> CredentialsError? {
        do {
            try Validations.email(email)
            return nil
        } catch let error {
            return (error as! CredentialsError)
        }
    }
    
    private func isValidPassword(password: String?) -> Bool {
        let passwordRegex = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{6,}$"
        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: password)
    }
    
    private func handlePassword()  -> CredentialsError?  {
        guard let password = passwordTextField.text else { return CredentialsError.passwordEmpty}
        guard password.count >= 6 else { return CredentialsError.passwordEmpty}
        guard password.count < 20 else { return CredentialsError.passwordEmpty}
        return nil
    }
    
    private func checkCredentials(completion: @escaping (Result<Token, CredentialsError>) -> Void)  {
        
        if let error = handleUsername() {
            if case .usernameHasSpace = error {
                errorUsernameButton.isHidden = false
                completion(.failure(error))
                return
            }
        }
        
        if let error = handleUsername() {
            errorUsernameButton.isHidden = false
            completion(.failure(error))
            // return
        }
        if let error = handleEmail(email: emailTextField.text!) {
            errorEmailButton.isHidden = false
            completion(.failure(error))
            //  return
        }
        
        if let error = handlePassword() {
            errorPasswordButton.isHidden = false
            completion(.failure(error))
            //  return
        }
       
        startAnimation()
        socialLoginManager.createNewUser(username: usernameTextField.text! , email: emailTextField.text!, password: passwordTextField.text!) { (response) in
            switch response {
                
            case .success((let result)):
                completion(.success(result))
            case .failure(let error):
                if error.code == 1000 {
                    completion(.failure(.emailAlreadyExists(message: error.message ?? "")))
                    return
                }
                if error.code == 1001 {
                    completion(.failure(.userAlreadyExists(message: error.message ?? "")))
                    
                    return
                }
                
                if error.code == 1002 {
                    completion(.failure(.wrongPassword(message: error.message ?? "")))
                    return
                }
                
                if error.code == 1003 {
                    completion(.failure(.wrongEmailUserName))
                    return 
                }
                if error.code == 400 {
                    completion(.failure(.other(message: error.message ?? "")))
                    //self?.showErrorMessage(title: "", text: error.message ?? "")
                }
               
            }
        }
    }
}

// MARK: - UITextFieldDelegate
extension RegisterVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == usernameTextField {
            emailTextField.becomeFirstResponder()
        } else if  textField == emailTextField  {
            passwordTextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
            signUpTapped((Any).self)
        }
        return true
    }
}
