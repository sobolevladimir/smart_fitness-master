//
//  LoginVC.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 13/03/19.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class ForgotPasswordVC: BaseViewController, KeyboardHandler {
    
    // MARK: - Views
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var errorEmailButton: UIButton!
    @IBOutlet weak var sendButton: LoginButton!
    
    // MARK: - Constraints
    @IBOutlet weak var logoTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        logoTopConstraint.constant = UIApplication.shared.statusBarFrame.height
        if view.frame.height < 668 {
            addKeyboardObservers()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        fixDirection()
    }
    
    // MARK: - Actions
    @IBAction func backButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func emailIsEditing(_ sender: Any) {
        if !errorEmailButton.isHidden {
            errorEmailButton.isHidden.toggle()
        }
    }
    
    @IBAction func errorTapped(_ sender: Any) {
        if let error = checkEmail() {
            showErrorMessage(title: error.failureReason!, text: error.localizedDescription)
            return
        }
    }
    
    @IBAction func tapAction(_ sender: Any) {
        view.endEditing(true)
    }
    
    @IBAction func sendTapped(_ sender: UIButton) {
        if let error = checkEmail() {
            errorEmailButton.isHidden = false
            showErrorMessage(title: error.failureReason!, text: error.localizedDescription)
            return
        }
        guard let email = emailTextField.text else { return }
        sender.isEnabled = false
        startAnimation()
        UserProvider.recoverPassword(email: email) { [weak self] (result) in
            self?.stopAnimation()
            guard let self = self else { return }
            switch result {
            case .success:
                let vc = ResetPasswordVC.instantiate()
                self.navigationController?.pushViewController(vc, animated: true)
            case .failure(let error):
                sender.isEnabled = true
                self.showErrorMessage(title: "Error", text: error.message ?? "")
            }
        }
    }
    
    // MARK: - Private
    private func checkEmail() -> CredentialsError? {
        do {
            guard let email = emailTextField.text else { return CredentialsError.emailIsEmpty }
            try Validations.email(email)
            return nil
        } catch {
            return (error as! CredentialsError)
        }
    }
    
    private func showErrorMessage(title: String, text: String) {
        let alert = UIAlertController(title: title, message: text, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: Constants.LoginFlow.Decision().ok, style: .default, handler: nil))
        self.present(alert, animated: true)
    }
}

extension ForgotPasswordVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        sendTapped((UIButton()).self)
        return true
    }
}
