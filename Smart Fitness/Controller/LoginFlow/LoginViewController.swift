//
//  LoginViewController.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 23.10.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class LoginViewController: BaseViewController {
    
    // MARK: - Views
    @IBOutlet weak var buttonsStackView: UIStackView!
    
    // MARK: - Properties
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
        navigationController?.setFullTranslucent()
        buttonsStackView.semanticContentAttribute = .forceLeftToRight
        
    }
  
    // MARK: - Actions
    @IBAction func languageTapped(_ sender: Any) {
        let vc = LanguageViewController.instantiate()
        present(vc, animated: false, completion: nil)
    }
    
    @IBAction func loginTapped(_ sender: Any) {
        let vc = LoginVC.instantiate()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func signUpTapped(_ sender: Any) {
        let vc = RegisterVC.instantiate()
        navigationController?.pushViewController(vc, animated: true)
    }
}
