//
//  LoginVC.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 13/03/19.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class ResetPasswordVC: BaseViewController {
    
    // MARK: - Constraint
    @IBOutlet weak var logoTopConstraint: NSLayoutConstraint!
    
    // MARK: - Lifecycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
        logoTopConstraint.constant = UIApplication.shared.statusBarFrame.height
    }
    
    // MARK: - Actions
    @IBAction func sendTapped(_ sender: Any) {
        navigationController?.popToRootViewController(animated: true)
    }
}
