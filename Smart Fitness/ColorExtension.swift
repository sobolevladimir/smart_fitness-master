//
//  ColorExtension.swift
//  MyInfinia
//
//  Created by Julien Henrard on 27/01/17.
//  Copyright © 2017 Broker Consult. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {

    /**********************************************************************************************/
    /*** THIS FILE IS USED FOR CHANGE MAIN COLOR OF THE APP AND COLOR USED FOR REPEATED ELEMENT ***/
    /*************** LIKE EACH MENU ITEM, SEPARATOR OR OTHERS ELEMENTS LIKE THAT ******************/
    /***** FOR ELEMENT WHO ARE NOT IN THIS EXTENSION, PLEASE CHECK IN ADAPTSCHEMECOLOR METHOD *****/
    /********************************** PRESENT IN EACH CLASSES ***********************************/
    /**********************************************************************************************/

    /***** DARK BLUE COLOR *****/
    static var darkBlueColor: UIColor {
        return UIColor(red: 0.0/255.0, green: 16.0/255.0, blue: 54.0/255.0, alpha: 1)
    }
    
    /***** LIGHT BLUE COLOR *****/
    static var lightBlueColor: UIColor {
        return UIColor(red: 16.0/255.0, green: 178.0/255.0, blue: 235.0/255.0, alpha: 1)
    }
    
    /*****  BLACK COLOR *****/
    static var BlackColor: UIColor {
        return UIColor.black
    }
}



