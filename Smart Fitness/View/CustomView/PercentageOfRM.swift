//
//  percentageOfRM.swift
//  Smart Fitness
//
//  Created by MCA 2 on 11/05/19.
//  Copyright © 2019 stratecore. All rights reserved.
//

import UIKit
protocol percentageOfRMViewProtocolDelegate {
    // DECLARE FUNCTIONS
    func methodSaveClicked()
    func methodCancelClicked()
}
class PercentageOfRM: UIView {
    
    
    // DECALRE THE OUTLET
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelRepsRm: UILabel!
    @IBOutlet weak var labelKg: UILabel!
    @IBOutlet weak var labelPercentage: UILabel!
    @IBOutlet weak var buttonYes: UIButton!
    @IBOutlet weak var buttonCancel: UIButton!
    @IBOutlet weak var labelRepsValue: UILabel!
    @IBOutlet weak var labelKgValue: UILabel!
    @IBOutlet weak var labelPercentageValue: UILabel!
    @IBOutlet weak var buttonReps: UIButton!
    @IBOutlet weak var buttonKg: UIButton!
    @IBOutlet weak var buttonPercentage: UIButton!
    
    // DECLARE VARUABLE
    var delegate:percentageOfRMViewProtocolDelegate?
    var customDropDownView:customDropDown?
    var customSingleDropDownView:customSingleDropDown?
    
    /// Set the inital view
    override func awakeFromNib() {
        
        // ADD SINGLE TAP TO VIEW
        let tapToHideGesture = UITapGestureRecognizer(target: self, action: #selector(PercentageOfRM.handleTapGesture(_:)))
        tapToHideGesture.numberOfTapsRequired = 1
        self.addGestureRecognizer(tapToHideGesture)
        
        // SET FONTS
        labelTitle.methodSetFontsForAll(val: SIZE_VIEW_TITLE)
        labelRepsValue.methodSetFontsForAll(val: 15)
        labelKgValue.methodSetFontsForAll(val: 15)
        labelPercentageValue.methodSetFontsForAll(val: 15)
        labelRepsRm.methodSetFontsForAll(val: 15)
        labelKg.methodSetFontsForAll(val: 15)
        labelPercentage.methodSetFontsForAll(val: 15)
        buttonYes.methodSetFontsForAll(val: 15)
        buttonCancel.methodSetFontsForAll(val: 15)
        
        viewBg.methodSetCorner(val: 6)
    }
    
    @objc func handleTapGesture(_ handleTapGesture: UITapGestureRecognizer) {
        // REMOVE ALL DROP DOWN FROM VIEW IF EXIST
        customDropDownView?.removeFromSuperview()
        customSingleDropDownView?.removeFromSuperview()
    }
    
    /// This will is for Reps DropDown
    ///
    /// - Parameter sender: This methods Receive the tapped button as param
    @IBAction func buttonRepsClicked(_ sender: UIButton) {
        if let window = UIApplication.shared.keyWindow {
            let locationInWindow : CGPoint = sender.convert(sender.bounds.origin, to: window)
            let buttonFrame:CGRect = self.convert(sender.frame, to: window)
            let pointY = locationInWindow.y + buttonFrame.size.height - 3
            let pointX = locationInWindow.x
            
            // CHECK IF SAME DROPDWON ADDED
            if (customSingleDropDownView?.isDescendant(of: appDelegate!.window!) ?? false){
                customSingleDropDownView?.removeFromSuperview()
                print("CHECK")
                return
            }
            // REMOVE VIEW FROM SUPERVIEW
            customDropDownView?.removeFromSuperview()
            customSingleDropDownView?.removeFromSuperview()
            
            // ADD DROP DOWN TO TEXTFIELD
            customSingleDropDownView?.removeFromSuperview()
            customSingleDropDownView = customSingleDropDown.fromNib()
            customSingleDropDownView?.frame = CGRect.init(x:pointX , y:pointY, width: buttonFrame.size.width, height: 120)
            customSingleDropDownView?.load(items: ["1","1","1","1","1","1"], with: { (selectedItem) in
            })
            //appDelegate?.window?.addSubview(customSingleDropDownView!)
        }
    }
    
    /// This will is for Kg DropDown
    ///
    /// - Parameter sender: This methods Receive the tapped button as param
    @IBAction func buttonKgClicked(_ sender: UIButton) {
    }
    /// This will is for percentage DropDown
    ///
    /// - Parameter sender: This Method Receive the tapped button as param
    @IBAction func buttonPerClicked(_ sender: UIButton) {
    }
    /// This will valuedate the value and save state and pass to origin view controller
    @IBAction func buttonSaveClicked() {
        if(self.delegate != nil){
            self.delegate?.methodSaveClicked()
        }
    }
    /// This will remove from super view
    @IBAction func buttonCancelClicked() {
        if(self.delegate != nil){
            self.delegate?.methodCancelClicked()
        }
        self.removeFromSuperview()
    }
}
