//
//  DropsetItemCell.swift
//  Smart Fitness
//
//  Created by MCA 2 on 16/05/19.
//  Copyright © 2019 stratecore. All rights reserved.
//

import UIKit
protocol supersetItemCellDelegate {
    func methodUpdateExeItem(model:MODEL_Exercise)
}

class SupersetsItemCell: UITableViewCell {
    
    // DECALRE OUTLER
    @IBOutlet weak var clview:UICollectionView!
    @IBOutlet weak var labelTitle:UILabel!
    
    
    var arrKg:[String] = [String]()
    var arrGrams:[String] = [String]()
    var arrReps:[String] = [String]()
    
    var itemCount:Int = 0
    var oneRowItemCount:Int = 6
    var cellIndex:Int = 0
    var cellWidth:CGFloat = 0
    var customSingleDropDownView:customSingleDropDown?
    var customDoubleDropDownView:customDoubleDropDown?
    var exercise:MODEL_Exercise?
    var setIndex:Int = 0
    var delegate:supersetItemCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // REGISTER NIB
        self.clview.register(UINib.init(nibName: "DropdownCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "DropdownCollectionViewCell")
        labelTitle.methodSetFontsForAll(val: 15)
    }
    func methodLoadData()  {
        if itemCount > oneRowItemCount {
            itemCount = oneRowItemCount
            clview.isScrollEnabled = true
        }
        cellWidth = (clview.frame.width - CGFloat(2*itemCount))/CGFloat(oneRowItemCount)
        self.clview.reloadData()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
extension  SupersetsItemCell:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.exercise?.dropDowndata.count ?? 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: cellWidth, height: clview.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DropdownCollectionViewCell", for: indexPath) as! DropdownCollectionViewCell
        cell.tag = indexPath.row
    
        if(cellIndex == 0){
           cell.labelValue.text =  self.exercise?.dropDowndata[indexPath.row].setsData[setIndex].repsValue
        }else{
            cell.labelValue.text = self.exercise?.dropDowndata[indexPath.row].setsData[setIndex].kgValue
        }
        cell.labelTitle.text = "Ex " + getStringFromAny(indexPath.row + 1)
        cell.buttonItem.tag = indexPath.item
        cell.buttonItem.addTarget(self, action: #selector(buttonItemClicked(_:)), for: .touchUpInside)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        var totalCellWidth = cellWidth * CGFloat( collectionView.numberOfItems(inSection: 0))
        totalCellWidth = totalCellWidth + (CGFloat(itemCount) * 2.0)
        return UIEdgeInsets.init(top: 0, left: (self.clview.frame.width - totalCellWidth)/2, bottom: 0, right: (self.clview.frame.width - totalCellWidth)/2)
    }
    @objc func buttonItemClicked(_ sender: UIButton) {
        let cellItemIndex = sender.tag
        
        if (cellIndex == 0){
            if let window = UIApplication.shared.keyWindow {
                let locationInWindow : CGPoint = sender.convert(sender.bounds.origin, to: window)
                let buttonFrame:CGRect = self.convert(sender.frame, to: window)
                let pointY = locationInWindow.y + buttonFrame.size.height - 3
                let pointX = locationInWindow.x
                
                // CHECK IF SAME DROPDWON ADDED
                if (customSingleDropDownView?.isDescendant(of: appDelegate!.window!) ?? false){
                    customSingleDropDownView?.removeFromSuperview()
                    customDoubleDropDownView?.removeFromSuperview()
                    print("CHECK")
                    return
                }
                // REMOVE VIEW FROM SUPERVIEW
                customSingleDropDownView?.removeFromSuperview()
                customDoubleDropDownView?.removeFromSuperview()
                
                
                // REMOVE VIEW FROM SUPER VIEW
                for view in appDelegate?.window?.subviews ?? []{
                    if view.isKind(of: customDoubleDropDown.self){
                        view.removeFromSuperview()
                    }
                }
                // REMOVE VIEW FROM SUPER VIEW
                for view in appDelegate?.window?.subviews ?? []{
                    if view.isKind(of: customSingleDropDown.self){
                        view.removeFromSuperview()
                    }
                }
                
                // set reps value
                 arrReps.removeAll()
                for i in 1..<100{
                    arrReps.append(getStringFromAny(i))
                }
                // ADD DROP DOWN TO TEXTFIELD
                customSingleDropDownView?.removeFromSuperview()
                customSingleDropDownView = customSingleDropDown.fromNib()
                customSingleDropDownView?.frame = CGRect.init(x:pointX , y:pointY, width: buttonFrame.size.width, height: 120)
                customSingleDropDownView?.load(items: arrReps, with: { (selectedItem) in
                    
                    let number = getStringFromAny(self.arrReps[selectedItem!])
                    self.exercise?.dropDowndata[cellItemIndex].setsData[self.setIndex].repsValue = number
                    
                    self.clview.reloadData()
                    self.customSingleDropDownView?.removeFromSuperview()
                    // SET DELEGATE
                    if((self.delegate) != nil){
                        self.delegate?.methodUpdateExeItem(model: self.exercise!)
                    }
                })
                appDelegate?.window?.addSubview(customSingleDropDownView ?? UIView.init())
            }
        }else{
            
            if let window = UIApplication.shared.keyWindow {
                let locationInWindow : CGPoint = sender.convert(sender.bounds.origin, to: window)
                let buttonFrame:CGRect = self.convert(sender.frame, to: window)
                let pointY = locationInWindow.y + buttonFrame.size.height - 3
                let pointX = locationInWindow.x
                
                // CHECK IF SAME DROPDWON ADDED
                if (customDoubleDropDownView?.isDescendant(of: appDelegate!.window!) ?? false){
                    customDoubleDropDownView?.removeFromSuperview()
                    print("CHECK")
                    return
                }
                
                // REMOVE VIEW FROM SUPERVIEW
                customSingleDropDownView?.removeFromSuperview()
                customDoubleDropDownView?.removeFromSuperview()
                
                for view in appDelegate?.window?.subviews ?? []{
                    if view.isKind(of: customDoubleDropDown.self){
                        view.removeFromSuperview()
                    }
                }
                // REMOVE VIEW FROM SUPER VIEW
                for view in appDelegate?.window?.subviews ?? []{
                    if view.isKind(of: customSingleDropDown.self){
                        view.removeFromSuperview()
                    }
                }
                // SET KG VALUE
                arrKg.removeAll()
                for i in 0..<301{
                    arrKg.append(getStringFromAny(i))
                }
                // SET GRAMS
                arrGrams.removeAll()
                arrGrams.append("0")
                arrGrams.append("25")
                arrGrams.append("5")
                arrGrams.append("75")
                
                // ADD DROP DOWN TO TEXTFIELD
                customDoubleDropDownView?.removeFromSuperview()
                customDoubleDropDownView = customDoubleDropDown.fromNib()
                customDoubleDropDownView?.frame = CGRect.init(x:pointX-30 , y:pointY, width: 120, height: 120)
                customDoubleDropDownView?.loadKg(leftItems: self.arrKg, rightItems: self.arrGrams,itemValue: self.arrKg[sender.tag], with: { (left, right) in
                    self.exercise?.dropDowndata[cellItemIndex].setsData[self.setIndex].kgValue = getStringFromAny(left as Any) + "." + getStringFromAny(right as Any)
                    self.clview.reloadData()
                    // SET DELEGATE
                    if((self.delegate) != nil){
                        self.delegate?.methodUpdateExeItem(model: self.exercise!)
                    }

                })
                appDelegate?.window?.addSubview(customDoubleDropDownView!)
            }
        }
    }
}
