//
//  DropSetView.swift
//  Smart Fitness
//
//  Created by MCA 2 on 11/05/19.
//  Copyright © 2019 stratecore. All rights reserved.
//

import UIKit

class SuperSetsMethodsView: UIView {
    // DECLARE OUTLET
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var tblView:UITableView!
    @IBOutlet weak var labelTitle: UILabel!
    
    @IBOutlet weak var buttonNext: UIButton!
    @IBOutlet weak var buttonPrev: UIButton!

    // DECLARE VARUABLE
    var exercise:MODEL_Exercise?
    var exerciseIndex:Int = -1
    var setIndex:Int = 0
    var rowCount:Int = 2
    
    override func awakeFromNib() {
        
        // ADD SINGLE TAP TO VIEW
        let tapToHideGesture = UITapGestureRecognizer(target: self, action: #selector(SuperSetsMethodsView.handleTapGesture(_:)))
        tapToHideGesture.numberOfTapsRequired = 1
        self.addGestureRecognizer(tapToHideGesture)

        
        // REGISTER NIB
        tblView.register(UINib(nibName: "SupersetsItemCell", bundle: nil), forCellReuseIdentifier: "SupersetsItemCell")
        // SET CORNER
        viewBg.methodSetCorner(val: 6)
    }
    @objc func handleTapGesture(_ handleTapGesture: UITapGestureRecognizer) {
        // REMOVE VIEW FROM SUPER VIEW
        for view in appDelegate?.window?.subviews ?? []{
            if view.isKind(of: customDoubleDropDown.self){
                view.removeFromSuperview()
            }
        }
        // REMOVE VIEW FROM SUPER VIEW
        for view in appDelegate?.window?.subviews ?? []{
            if view.isKind(of: customSingleDropDown.self){
                view.removeFromSuperview()
            }
        }
    }
    func methodSetView(){
        self.methodSetDataScreen()
        tblView.reloadData()
    }
    
    /// This will valuedate the value and save state and pass to origin view controller
    @IBAction func buttonSaveClicked() {
        
        // REMOVE VIEW FROM SUPER VIEW
        for view in appDelegate?.window?.subviews ?? []{
            if view.isKind(of: customSingleDropDown.self){
                view.removeFromSuperview()
            }
        }
        for view in appDelegate?.window?.subviews ?? []{
            if view.isKind(of: customDoubleDropDown.self){
                view.removeFromSuperview()
            }
        }
        self.removeFromSuperview()
        
        NotificationCenter.default.post(name: .superSetSaveButtonClicked, object: "myObject", userInfo: ["exe": self.exercise!,"index":exerciseIndex])
    }
    /// This will remove from super view
    @IBAction func buttonCancelClicked() {
        
        // REMOVE VIEW FROM SUPER VIEW
        for view in appDelegate?.window?.subviews ?? []{
            if view.isKind(of: customSingleDropDown.self){
                view.removeFromSuperview()
            }
        }
        for view in appDelegate?.window?.subviews ?? []{
            if view.isKind(of: customDoubleDropDown.self){
                view.removeFromSuperview()
            }
        }
        self.removeFromSuperview()
        
    }
    @IBAction func buttonPrevClicked(_ sender: Any) {
        if buttonPrev.isSelected == true && setIndex > 0{
                setIndex = setIndex - 1
        }
        if setIndex == 0 {
            buttonPrev.isSelected = false
            buttonNext.isSelected = true
        }
        self.tblView.reloadData()
        self.methodSetDataScreen()
    }
    @IBAction func buttonNextClicked(_ sender: Any) {
        if buttonNext.isSelected == true && setIndex < ((self.exercise?.totalSetCount ?? 0) - 1){
            setIndex = setIndex + 1
        }
        self.tblView.reloadData()
        self.methodSetDataScreen()
    }
    func methodSetDataScreen(){
        // REMOVE VIEW FROM SUPER VIEW
        for view in appDelegate?.window?.subviews ?? []{
            if view.isKind(of: customSingleDropDown.self){
                view.removeFromSuperview()
            }
        }
        for view in appDelegate?.window?.subviews ?? []{
            if view.isKind(of: customDoubleDropDown.self){
                view.removeFromSuperview()
            }
        }
        
        if (self.exercise?.totalSetCount ?? 0) == 1{
            buttonPrev.isSelected = true;
            buttonNext.isSelected = true;
        }else if (setIndex == 0){
            buttonPrev.isSelected = false;
            buttonNext.isSelected = true;
        }else if setIndex >= (self.exercise?.totalSetCount ?? 0) - 1{
            buttonPrev.isSelected = true;
            buttonNext.isSelected = false;
        }else{
            buttonPrev.isSelected = true;
            buttonNext.isSelected = true;
        }
        labelTitle.text = "Superset Number " + "\((setIndex) + 1 )"

    }
}

extension SuperSetsMethodsView:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:SupersetsItemCell = SupersetsItemCell.fromNib()
        cell.selectionStyle = .none
        
        // SET TITLE
        if(indexPath.row == 0){
            cell.labelTitle.text = "Reps:"
        }else{
            cell.labelTitle.text = "Kg:"
        }
        cell.cellIndex = indexPath.row
        cell.setIndex  = self.setIndex
            cell.exercise = self.exercise
        cell.delegate=self
        cell.methodLoadData()
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}

extension SuperSetsMethodsView:supersetItemCellDelegate{
    func methodUpdateExeItem(model: MODEL_Exercise) {
        self.exercise = model
    }
}
