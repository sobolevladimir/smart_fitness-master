//
//  DropsetItemCell.swift
//  Smart Fitness
//
//  Created by MCA 2 on 16/05/19.
//  Copyright © 2019 stratecore. All rights reserved.
//

import UIKit

class SupersetItemCell: UITableViewCell {

    // DECALRE OUTLER
    @IBOutlet weak var clview:UICollectionView!
    @IBOutlet weak var labelTitle:UILabel!

    // DECALRE VARIABLE
    var arrData:[String:Any] =  [String:Any]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // REGISTER NIB
        self.clview.register(UINib.init(nibName: "DropdownCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "DropdownCollectionViewCell")
        labelTitle.methodSetFontsForAll(val: SIZE_VIEW_TITLE)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension  SupersetItemCell:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 7
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: (clview.frame.width-14)/7, height: clview.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DropdownCollectionViewCell", for: indexPath) as! DropdownCollectionViewCell
        cell.tag = indexPath.row
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    
}
