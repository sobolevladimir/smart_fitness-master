//
//  PyramidSetView.swift
//  Smart Fitness
//
//  Created by MCA 2 on 11/05/19.
//  Copyright © 2019 stratecore. All rights reserved.
//

import UIKit

protocol pyramidSetViewProtocolDelegate {
    // DECLARE FUNCTIONS
    func methodPyramidSaveClicked()
    func methodPyramidCancelClicked()
}

class PyramidSetView: UIView {
    
    
    // DECALRE THE OUTLET
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelRepsRm: UILabel!
    @IBOutlet weak var labelKg: UILabel!
    @IBOutlet weak var buttonYes: UIButton!
    @IBOutlet weak var buttonCancel: UIButton!
    @IBOutlet weak var labelRepsValue: UILabel!
    @IBOutlet weak var labelKgValue: UILabel!
    @IBOutlet weak var buttonReps: UIButton!
    @IBOutlet weak var buttonKg: UIButton!
    
    // DECALRE THE VARIABLE
    var delegate:pyramidSetViewProtocolDelegate?
    
    /// Set the inital view
    override func awakeFromNib() {
        
        // ADD SINGLE TAP TO VIEW
        let tapToHideGesture = UITapGestureRecognizer(target: self, action: #selector(PyramidSetView.handleTapGesture(_:)))
        tapToHideGesture.numberOfTapsRequired = 1
        self.addGestureRecognizer(tapToHideGesture)
        
        // SET FONTS
        labelTitle.methodSetFontsForAll(val: SIZE_VIEW_TITLE)
        labelRepsValue.methodSetFontsForAll(val: 15)
        labelKgValue.methodSetFontsForAll(val: 15)
        labelRepsRm.methodSetFontsForAll(val: 15)
        labelKg.methodSetFontsForAll(val: 15)
        buttonYes.methodSetFontsForAll(val: 15)
        buttonCancel.methodSetFontsForAll(val: 15)
        
        viewBg.methodSetCorner(val: 6)
    }
    
    @objc func handleTapGesture(_ handleTapGesture: UITapGestureRecognizer) {
        
        // REMOVE ALL DROP DOWN FROM VIEW IF EXIST
        
    }
    
    /// This will is for Reps DropDown
    ///
    /// - Parameter sender: This methods Receive the tapped button as param
    @IBAction func buttonRepsClicked(_ sender: UIButton) {
    }
    
    /// This will is for Kg DropDown
    ///
    /// - Parameter sender: This methods Receive the tapped button as param
    @IBAction func buttonKgClicked(_ sender: UIButton) {
    }
   
    /// This will valuedate the value and save state and pass to origin view controller
    @IBAction func buttonSaveClicked() {
        if(self.delegate != nil){
            self.delegate?.methodPyramidSaveClicked()
        }
    }
    /// This will remove from super view
    @IBAction func buttonCancelClicked() {
        if(self.delegate != nil){
            self.delegate?.methodPyramidCancelClicked()
        }
        self.removeFromSuperview()
    }
}
