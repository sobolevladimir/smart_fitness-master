//
//  DropSetView.swift
//  Smart Fitness
//
//  Created by MCA 2 on 11/05/19.
//  Copyright © 2019 stratecore. All rights reserved.
//

import UIKit
protocol superSetViewProtocolDelegate {
    // DECLARE FUNCTIONS
    func methodSuperSetSaveClicked()
    func methodSuperSetCancelClicked()
}
class SuperSetView: UIView{
    
    // DECLARE OUTLET
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var tblView:UITableView!
    
    // DECLARE VARUABLE
    var delegate:dropSetViewProtocolDelegate?
    
    override func awakeFromNib() {
        
        // REGISTER NIB
        tblView.register(UINib(nibName: "DropsetItemCell", bundle: nil), forCellReuseIdentifier: "DropsetItemCell")
        viewBg.methodSetCorner(val: 6)
        
    }
    /// This will valuedate the value and save state and pass to origin view controller
    @IBAction func buttonSaveClicked() {
        self.removeFromSuperview()
//        if(self.delegate != nil){
//            self.delegate?.methodDropSetSaveClicked(exe: self.ex)
//        }
    }
    /// This will remove from super view
    @IBAction func buttonCancelClicked() {
        self.removeFromSuperview()
        if(self.delegate != nil){
            self.delegate?.methodDropSetCancelClicked()
        }
    }
}

extension SuperSetView:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:DropsetItemCell = DropsetItemCell.fromNib()
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}
