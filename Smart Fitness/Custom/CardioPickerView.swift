//
//  CardioPickerView.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 29.11.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class CardioPickerView: UIPickerView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 4
        layer.borderColor = Constants.Colors.grayLight.cgColor
        layer.borderWidth = 1
        semanticContentAttribute = .forceLeftToRight
    }
}

final class CircuitPickerView: UIPickerView {
    
    // MARK: - Public Properties
    var didSelectValue: ((String) -> Void)?
    
    // MARK: - Private
    private var pickerData: [[String]] = [[]]
    private var output: [String] = []
    
    // MARK: - Init
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 4
        layer.borderColor = Constants.Colors.grayLight.cgColor
        layer.borderWidth = 1
        semanticContentAttribute = .forceLeftToRight
        self.dataSource = self
        self.delegate = self
    }
    
    public func initDataSource(data: [[String]]) {
        pickerData = data
        if pickerData.count > 2 {
            output.append(pickerData[0][0])
             output.append(pickerData[1][0])
             output.append(pickerData[2][0])
        }
        self.reloadAllComponents()
    }
}

// MARK: - UIPickerViewDataSource
extension CircuitPickerView: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        pickerData[component].count
    }
}

// MARK: - UIPickerViewDelegate
extension CircuitPickerView: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if !output.isEmpty {
            output[component] = pickerData[component][row]
            didSelectValue?("\(output[0])\(output[1])\(output[2])")
            
        } else {
            didSelectValue?(pickerData[component][row])
        }
        pickerView.reloadAllComponents()
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var color: UIColor!
        var label = UILabel()
        if let v = view { label = v as! UILabel }
        label.font = Constants.returnFontName(style: .regular, size: 16)
        label.textAlignment = .center
        
        label.text = pickerData[component][row]
        
        if pickerView.selectedRow(inComponent: component) == row {
            color = UIColor.orange
        } else {
            color = UIColor.black
        }
        label.textColor = color
        return label
    }
}

