//
//  ProfileSegmentedControl.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 05.02.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

protocol CustomSegmentedControlDelegate:class {
    func changeToIndex(index:Int)
}

class ProfileSegmentedControl: UIView {
    
    struct Content {
        var title: String
        var selectedBubbleImage: String
        var deselectedBubbleImage: String
        var selectedItemImage: String
        var deselectedItemImage: String
    }
    
    private var content: [Content] = [
        .init(title: Constants.Profile().weight, selectedBubbleImage: "oranfeBuble", deselectedBubbleImage: "grayBuble", selectedItemImage: "profileKGSelected", deselectedItemImage: "profileKg"),
        .init(title: Constants.Profile().measurements, selectedBubbleImage: "oranfeBuble", deselectedBubbleImage: "grayBuble", selectedItemImage: "profileRulesSlected", deselectedItemImage: "profileRulers"),
        .init(title: Constants.Profile().water, selectedBubbleImage: "oranfeBuble", deselectedBubbleImage: "grayBuble", selectedItemImage: "profileWaterSelected", deselectedItemImage: "profileBottle"),
        .init(title: Constants.Profile().bmi, selectedBubbleImage: "oranfeBuble", deselectedBubbleImage: "grayBuble", selectedItemImage: "profileBMI", deselectedItemImage: "profileBMIDeselected")]
    
    private var itemViews: [ItemView] = []
    private var stack = UIStackView()
    
    weak var delegate: CustomSegmentedControlDelegate?
    
    public private(set) var selectedIndex : Int = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .clear
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMinXMinYCorner]
        layer.cornerRadius = 4
        updateView()
        setIndex(index: 0)
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
    }
    
    func setIndex(index:Int) {
        itemViews.enumerated().forEach({
            $1.update(title: content[$0].title, bubbleImage: content[$0].deselectedBubbleImage, itemImage: content[$0].deselectedItemImage, isShadow: false)
        })
        let itemView = itemViews[index]
        selectedIndex = index
        stack.bringSubviewToFront(itemView)
        itemView.update(title: content[index].title, bubbleImage: content[index].selectedBubbleImage, itemImage: content[index].selectedItemImage, isShadow: true)
    }
    
    @objc func buttonAction(sender: UIButton) {
        for (viewIndex, item) in itemViews.enumerated() {
            if item.button == sender {
                selectedIndex = viewIndex
                setIndex(index: selectedIndex)
                delegate?.changeToIndex(index: selectedIndex)
            }
        }
    }
}

//Configuration View
extension ProfileSegmentedControl {
    private func updateView() {
        createItemView()
        configStackView()
    }
    
    private func configStackView() {
        stack = UIStackView(arrangedSubviews: itemViews)
        stack.axis = .horizontal
        stack.alignment = .fill
        stack.distribution = .fillEqually
        addSubview(stack)
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        stack.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        stack.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        stack.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
    }
    
    private func createItemView() {
        itemViews = [ItemView]()
        itemViews.removeAll()
        subviews.forEach({$0.removeFromSuperview()})
        for index in 0..<content.count {
            let itemView = ItemView(frame: .init(x: 0, y: 0, width: frame.width / 4, height: frame.height - 10))
            itemView.set(isLast: index == content.count - 1, isFirst: index == 0)
            itemView.button.addTarget(self, action:#selector(ProfileSegmentedControl.buttonAction(sender:)), for: .touchUpInside)
            itemView.itemImageStack.frame = .init(x: 0, y: 0, width: frame.width / 4, height: frame.height - 25)
            itemViews.append(itemView)
        }
    }
}


class ItemView: UIView {
    
    lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.distribution = .fillProportionally
        return stackView
    }()
    
    var bubbleImageView = UIImageView()
    var itemImageView = UIImageView()
    
    lazy var label: UILabel = {
        let label = UILabel()
        label.font = Constants.returnFontName(style: .italic, size: 10)
        return label
    }()
    
    lazy var button: UIButton = {
        let button = UIButton(type: .system)
        return button
    }()
    
    let bubbleStackView = UIStackView()
    let itemImageStack = UIStackView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        bubbleStackView.addArrangedSubview(bubbleImageView)
        
        stackView.addArrangedSubview(bubbleStackView)
        stackView.addArrangedSubview(label)
        
        addSubview(stackView)
        
        stackView.frame = bounds
        stackView.frame.origin.y = 4
        
        itemImageStack.addArrangedSubview(itemImageView)
        
        addSubview(itemImageStack)
        
        itemImageView.contentMode = .center
        
        
        addSubview(button)
        button.frame = bounds
        
        layer.borderWidth = 0.3
        layer.borderColor = #colorLiteral(red: 0.835205555, green: 0.8353466392, blue: 0.8351857662, alpha: 1)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func set(isLast: Bool , isFirst: Bool) {
         backgroundColor = .white
        if isLast {
            layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
                   layer.cornerRadius = 4
        }
       
        if isFirst {
            layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
            layer.cornerRadius = 4
        }
        
    }
    
    func update(title: String, bubbleImage: String, itemImage: String, isShadow: Bool) {
        label.text = title
               bubbleImageView.image = UIImage(named: bubbleImage)
               itemImageView.image = UIImage(named: itemImage)
             
               if isShadow {
                   layer.shadowColor = UIColor.black.cgColor
                   layer.shadowOffset = .zero
                   layer.shadowRadius = 1
                   layer.shadowOpacity = 0.3
               } else {
                   layer.shadowColor = UIColor.clear.cgColor
               }
               
    }
}
