//
//  DragableHorizontalSideView.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 02.12.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

class DraggableHorizontalSideView: UIView {
    enum State {
        case open
        case closed
    }
    
    enum Constants {
        static let transitionDuration: TimeInterval = 0.3
        static let initialState: State = .open
        static let animateOnTap = false
    }
    
    private var velocityFactor : CGFloat = 1000
    
    
    // MARK: - GestureRecognizer
    lazy var panRecognizer: InstantPanGestureRecognizer = {
        let recognizer = InstantPanGestureRecognizer()
        recognizer.addTarget(self, action: #selector(viewPanned))
        recognizer.delegate = self
        return recognizer
    }()
    
    // MARK: - Public
    var transitionAnimation: ((State) -> Void)?
    var transitionCompletion: ((State) -> Void)?
    var fractionCompletion: ((CGFloat) -> Void)?
    var didCloseView: (() -> Void)?
    private(set) var currentState: State = Constants.initialState {
        didSet {
            if currentState == .closed {
                didCloseView?()
            }
        }
    }
    
    // MARK: - Properties
    private var runningAnimators: [UIViewPropertyAnimator] = []
    private var animationProgress: [CGFloat] = []
    
    // MARK: - Init
    override func awakeFromNib() {
        super.awakeFromNib()
        addGestureRecognizer(panRecognizer)
    }
   
    // MARK: - Action
    @objc func viewPanned(recognizer: UIPanGestureRecognizer) {
        switch recognizer.state {
        case .began:
            break
        case .changed:
            if runningAnimators.isEmpty {
                animateTransitionIfNeeded(to: currentState.opposite, duration: Constants.transitionDuration)
                runningAnimators.forEach { $0.pauseAnimation() }
                animationProgress = runningAnimators.map { $0.fractionComplete }
            }
            let translation = recognizer.translation(in: self)
            let offset = State.open.width - State.closed.width
            var fraction: CGFloat = 0
            switch LocalizationManager.Language.language {
                case .english:
                fraction = translation.x / offset
                case .hebrew:
                  fraction = -translation.x / offset
            }
            fractionCompletion?(fraction)
            [currentState == .open, runningAnimators[0].isReversed].forEach {
                if $0 { fraction *= -1 }
            }
            runningAnimators.enumerated().forEach { index, animator in
                animator.fractionComplete = fraction + animationProgress[index]
            }
            
        case .ended:
            var translation = recognizer.translation(in: self)
            switch LocalizationManager.Language.language {
                case .english:
                    break
                case .hebrew:
                   translation.x *= -1.0
            }
            guard !(currentState == .open && translation.x > 0),
                !(currentState == .closed && translation.x < 0)
                else { return }
            var fractionCompleted = translation.x / (State.open.width - State.closed.width)
            var velocity = recognizer.velocity(in: self).x
            velocity = velocity > 0 ? velocity : -velocity
            fractionCompleted = fractionCompleted > 0 ? fractionCompleted : -fractionCompleted
            let modifiedFraction = velocity/velocityFactor + fractionCompleted
            ///
            let shouldClose = modifiedFraction > 0.5
            
            if velocity == 0 {
                if Constants.animateOnTap {
                    continueAllAnimations()
                }
                break
            }
            
            let isReversed = runningAnimators.first?.isReversed
            
            switch currentState {
            case .open:
                if shouldClose == isReversed {
                    toggleRunningAnimators()
                }
            case .closed:
                if shouldClose == isReversed {
                    toggleRunningAnimators()
                }
            }
            
            continueAllAnimations()
        default:
            break
        }
    }
    
    // MARK: - Private
    private func continueAllAnimations() {
        runningAnimators.forEach {
            $0.continueAnimation(withTimingParameters: nil, durationFactor: 0)
        }
    }
    
    private func toggleRunningAnimators() {
        runningAnimators.forEach { $0.isReversed.toggle() }
    }
    
    private func animateTransitionIfNeeded(to state: State, duration: TimeInterval) {
        guard runningAnimators.isEmpty else { return }
        let transitionAnimator = self.transitionAnimator(for: state, duration: duration)
        [transitionAnimator].forEach {
            $0.startAnimation()
            runningAnimators.append($0)
        }
    }
    
    // an animator for the transition
    private func transitionAnimator(for state: State, duration: TimeInterval) -> UIViewPropertyAnimator {
        let animator = UIViewPropertyAnimator(duration: duration, dampingRatio: 1) { [weak self] in
            self?.transitionAnimation?(state)
        }
        
        animator.addCompletion { [weak self] position in
            guard let self = self else { return }
            switch position {
            case .start:
                self.currentState = state.opposite
            case .end:
                self.currentState = state
            case .current:
                break
            @unknown default:
                break
            }
            
            self.transitionCompletion?(self.currentState)
            self.runningAnimators.removeAll()
        }
        return animator
    }
}

extension DraggableHorizontalSideView: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        true
    }
}

// MARK: - Status
private extension DraggableHorizontalSideView.State {
    var width: CGFloat {
        switch self {
        case .open: return 0
        case .closed: return -UIScreen.main.bounds.width
        }
    }
    
    var opposite: DraggableHorizontalSideView.State {
        switch self {
        case .open: return .closed
        case .closed: return .open
        }
    }
}

