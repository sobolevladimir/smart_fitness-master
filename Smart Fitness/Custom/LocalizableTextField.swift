//
//  LocalizableTextField.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 11.11.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class LocalizableTextField: UITextField {
    
    @IBInspectable var insetX: CGFloat = 0 {
          didSet {
            layoutIfNeeded()
          }
       }
       @IBInspectable var insetY: CGFloat = 0 {
          didSet {
            layoutIfNeeded()
          }
       }
    
    @IBInspectable var maxCharacters: Int = 999
    
       // placeholder position
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: insetX , dy: insetY)
       }

       // text position
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: insetX , dy: insetY)
       }
    
    // MARK: - Init
    override func awakeFromNib() {
        super.awakeFromNib()
        delegate = self
        switch LocalizationManager.Language.language {
            
            case .english:
                self.textAlignment = NSTextAlignment.left
                self.semanticContentAttribute = UISemanticContentAttribute.forceLeftToRight
            case .hebrew:
                self.textAlignment = NSTextAlignment.right
                self.semanticContentAttribute = UISemanticContentAttribute.forceRightToLeft
        }
    }
}

extension LocalizableTextField: UITextFieldDelegate {
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        let maxLength = maxCharacters
//        let currentString: NSString = textField.text! as NSString
//        let newString: NSString =
//            currentString.replacingCharacters(in: range, with: string) as NSString
//        return newString.length <= maxLength
//    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag == 404 {
            textField.resignFirstResponder()
        }
        return true
    }
}
