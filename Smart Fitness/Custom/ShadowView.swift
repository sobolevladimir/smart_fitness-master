//
//  ShadowView.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 10.05.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit.UIView

final class ShadowView: UIView {
    
    @IBInspectable var isShadow: Bool = false {
        didSet {
            if isShadow {
                layer.shadowColor = UIColor.black.cgColor
                layer.shadowOffset = .init(width: 0, height: 4)
                layer.shadowRadius = 4
                layer.shadowOpacity = 0.15
            }
        }
    }
    
    @IBInspectable var maskedCorners: Int = 5 {
        didSet {
            guard let cc = Corners.init(rawValue: maskedCorners) else { return }
            corners = cc
        }
    }
    var corners = Corners.all
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            switch corners {
            case .topLeft:
                layer.maskedCorners = [.layerMinXMinYCorner]
            case .topRight:
                layer.maskedCorners = [.layerMaxXMinYCorner]
            case .bottomLeft:
                layer.maskedCorners = [.layerMinXMaxYCorner]
            case .bottomRight:
                layer.maskedCorners = [.layerMaxXMaxYCorner]
            case .all:
                layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMaxYCorner]
            case .top:
                layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            case .bottom:
                layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
            case .none:
                layer.maskedCorners = []
            case .left:
                switch LocalizationManager.Language.language {
                    
                case .english:
                    layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
                case .hebrew:
                    layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
                }
            case .right:
                switch LocalizationManager.Language.language {
                case .english:
                    layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
                case .hebrew:
                    layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
                }
            }
            
            layer.cornerRadius = cornerRadius
        }
    }
}
