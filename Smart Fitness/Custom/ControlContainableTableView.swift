//
//  File.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 09.12.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class ControlContainableTableView: UITableView {
    
    override func touchesShouldCancel(in view: UIView) -> Bool {
        if view is UIControl
            && !(view is UITextInput)
            && !(view is UISlider)
            && !(view is UISwitch) {
            return true
        }
        
        return super.touchesShouldCancel(in: view)
    }
}

extension UICollectionView {
    override open func touchesShouldCancel(in view: UIView) -> Bool {
           if view is UIControl
               && !(view is UITextInput)
               && !(view is UISlider)
               && !(view is UISwitch) {
               return true
           }
           
           return super.touchesShouldCancel(in: view)
       }
}


