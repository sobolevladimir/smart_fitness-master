//
//  CircuitValueView.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 22.01.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class CircuitValueView: UIView {
    
    // MARK: - View
    var contentView: UIView?
    
    @IBOutlet weak private var titleLabel: UILabel!
    @IBOutlet weak private var button: LoginButton!
    @IBOutlet weak private var subtitleLabel: UILabel!
    
    
    // MARK: - Constraints
    
    // MARK: - Properties
 
    // MARK: - Closures
    var isExpandingPickerView: (() -> Void)?
    
    // MARK: - Init
    required init?(coder aDecoder: NSCoder) {
           super.init(coder: aDecoder)
           commonInit()
       }

       override init(frame: CGRect) {
           super.init(frame: frame)
           commonInit()
       }
    
    private func commonInit() {
        guard let view = loadViewFromNib() else { return }
        view.frame = self.bounds
        self.addSubview(view)
        contentView = view
        if let text = titleLabel.text {
            self.isHidden = text.count == 0
        }
    }

    private func loadViewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
    
        let nib = UINib(nibName: String(describing: Self.self), bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
    
    func setTitle(_ title: String, fontSize: CGFloat = 13, isRunningPace: Bool = false) {
        self.isHidden = title.count == 0
        titleLabel.text = title
        titleLabel.font = Constants.returnFontName(style: .regular, size: fontSize)
        if isRunningPace {
            button.imageBottom = nil
        } else {
            button.imageBottom = UIImage(named: "dropDownArrow")
        }
    }
    
    func setSubtitle(_ subtitle: String) {
        subtitleLabel.text = subtitle
    }
    
    func setButtonValue(_ value: String, font: UIFont = Constants.returnFontName(style: .regular, size: 17)) {
        button.setTitle(value, for: .normal)
        button.titleLabel?.font = font
    }
    
    func setViewMode() {
        button.imageBottom = nil
        button.contentEdgeInsets.bottom = 4
        button.contentEdgeInsets.top = 4
    }
    
    // MARK: - Action
    
    @IBAction private func buttonDidTap(_ sender: Any) {
       isExpandingPickerView?()
    }
    
    // MARK: - Private
}
