

import UIKit

extension ReorderController {
    static var isDragging: Bool = false
    static var sourceIndexPath: IndexPath = .init(item: 0, section: 0)

    @objc public func handleReorderGesture(_ gestureRecognizer: UIGestureRecognizer) {
        guard let superview = tableView?.superview else { return }
        let touchPosition = gestureRecognizer.location(in: superview)
        
        switch gestureRecognizer.state {
        case .began:
            UISelectionFeedbackGenerator().selectionChanged()
            
            let tableTouchPosition = superview.convert(touchPosition, to: tableView)
            
            guard let indexPath = tableView?.indexPathForRow(at: tableTouchPosition)
                else {
                    beginReorder(touchPosition: touchPosition)
                    return
            }

            ReorderController.sourceIndexPath = indexPath
            guard let cell = tableView?.cellForRow(at: indexPath) as? ExpandedCellProtocol else {
              guard let button = gestureRecognizer.view,
                let roundedView =  button.superview,
                let contentView = roundedView.superview,
                let supersetCell = contentView.superview as? ExerciseSupersetCell,
                supersetCell.tag == 666 else {
                    beginReorder(touchPosition: touchPosition)
                    return
                }
               
                if supersetCell.isExpanded {
                    supersetCell.didTapExpand?()
                     tableView?.layoutIfNeeded()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                        self.beginReorder(touchPosition: touchPosition)
                        return
                    }
                    
                }
                beginReorder(touchPosition: touchPosition)
                return
            }
            
            if cell.isExpanded {
                cell.didExpandClicked?()
                 tableView?.layoutIfNeeded()
            }
            beginReorder(touchPosition: touchPosition)
        case .changed:
            let tableTouchPosition = superview.convert(touchPosition, to: tableView)
            guard let indexPath = tableView?.indexPathForRow(at: tableTouchPosition)
                else { return }
            ReorderController.isDragging = true
            guard ReorderController.sourceIndexPath.section == indexPath.section else { return }
            updateReorder(touchPosition: touchPosition)
            tableView?.layoutIfNeeded()
            
        case .ended, .cancelled, .failed, .possible:
            endReorder()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                self.tableView?.reloadData()
                 ReorderController.isDragging = false
            }
           
        @unknown default: break
        }
    }
}

extension ReorderController: UIGestureRecognizerDelegate {
    
    public func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        guard let tableView = tableView else { return false }
        
        let gestureLocation = gestureRecognizer.location(in: tableView)
        guard let indexPath = tableView.indexPathForRow(at: gestureLocation) else { return false }
        
        return delegate?.tableView(tableView, canReorderRowAt: indexPath) ?? true
    }
    
}
