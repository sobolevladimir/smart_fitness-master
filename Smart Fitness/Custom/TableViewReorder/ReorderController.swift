
import UIKit

public enum ReorderSpacerCellStyle {
    case automatic
    case hidden
    case transparent
}


public protocol TableViewReorderDelegate: class {
    
    func tableView(_ tableView: UITableView, reorderRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath)
   
    func tableView(_ tableView: UITableView, canReorderRowAt indexPath: IndexPath) -> Bool
    
    func tableView(_ tableView: UITableView, targetIndexPathForReorderFromRowAt sourceIndexPath: IndexPath, to proposedDestinationIndexPath: IndexPath) -> IndexPath

    func tableViewDidBeginReordering(_ tableView: UITableView, at indexPath: IndexPath)
    
    func tableViewDidFinishReordering(_ tableView: UITableView, from initialSourceIndexPath: IndexPath, to finalDestinationIndexPath: IndexPath)
    
}

public extension TableViewReorderDelegate {
    
    func tableView(_ tableView: UITableView, canReorderRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, targetIndexPathForReorderFromRowAt sourceIndexPath: IndexPath, to proposedDestinationIndexPath: IndexPath) -> IndexPath {
        return proposedDestinationIndexPath
    }

    func tableViewDidBeginReordering(_ tableView: UITableView, at indexPath: IndexPath) {
    }
    
    func tableViewDidFinishReordering(_ tableView: UITableView, from initialSourceIndexPath: IndexPath, to finalDestinationIndexPath:IndexPath) {
    }
    
}

// MARK: - ReorderController
public class ReorderController: NSObject {
    
    // MARK: - Public interface
    public weak var delegate: TableViewReorderDelegate?
    public var isEnabled: Bool = true {
        didSet { reorderGestureRecognizer.isEnabled = isEnabled }
    }
    public var longPressDuration: TimeInterval = 0.3 {
        didSet {
            reorderGestureRecognizer.minimumPressDuration = longPressDuration
        }
    }
    public var cancelsTouchesInView: Bool = false {
        didSet {
            reorderGestureRecognizer.cancelsTouchesInView = cancelsTouchesInView
        }
    }
    
    public var tableGestureIsOn = false {
        didSet {
            if tableGestureIsOn {
                tableView?.addGestureRecognizer(reorderGestureRecognizer)
            }
        }
    }
    public var animationDuration: TimeInterval = 0.2
    public var cellOpacity: CGFloat = 1
    public var cellScale: CGFloat = 1
    public var shadowColor: UIColor = .black
    public var shadowOpacity: CGFloat = 0.3
    public var shadowRadius: CGFloat = 10
    public var shadowOffset = CGSize(width: 0, height: 3)
    public var spacerCellStyle: ReorderSpacerCellStyle = .automatic
    public var autoScrollEnabled = true

    public func spacerCell(for indexPath: IndexPath) -> UITableViewCell? {
        if case let .reordering(context) = reorderState, indexPath == context.destinationRow {
            return createSpacerCell()
        } else if case let .ready(snapshotRow) = reorderState, indexPath == snapshotRow {
            return createSpacerCell()
        }
        return nil
    }
    
    // MARK: - Internal state
    struct ReorderContext {
        var sourceRow: IndexPath
        var destinationRow: IndexPath
        var snapshotOffset: CGFloat
        var touchPosition: CGPoint
    }
    
    enum ReorderState {
        case ready(snapshotRow: IndexPath?)
        case reordering(context: ReorderContext)
    }
    
    weak var tableView: UITableView?
    
    var reorderState: ReorderState = .ready(snapshotRow: nil)
    var snapshotView: UIView? = nil
    
    var autoScrollDisplayLink: CADisplayLink?
    var lastAutoScrollTimeStamp: CFTimeInterval?
    
    lazy var reorderGestureRecognizer: UILongPressGestureRecognizer = {
        let gestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handleReorderGesture))
        gestureRecognizer.delegate = self
        gestureRecognizer.minimumPressDuration = self.longPressDuration
        return gestureRecognizer
    }()
    
    // MARK: - Lifecycle
    init(tableView: UITableView) {
        super.init()
        self.tableView = tableView
        reorderState = .ready(snapshotRow: nil)
    }
    
    // MARK: - Reordering
    func beginReorder(touchPosition: CGPoint) {
        guard case .ready = reorderState,
            let delegate = delegate,
            let tableView = tableView,
            let superview = tableView.superview
        else { return }
        
        let tableTouchPosition = superview.convert(touchPosition, to: tableView)
        
        guard let sourceRow = tableView.indexPathForRow(at: tableTouchPosition),
            delegate.tableView(tableView, canReorderRowAt: sourceRow)
        else { return }
        
        createSnapshotViewForCell(at: sourceRow)
        animateSnapshotViewIn()
        activateAutoScrollDisplayLink()
        
        tableView.reloadData()
        
        let snapshotOffset = (snapshotView?.center.y ?? 0) - touchPosition.y
        
        let context = ReorderContext(
            sourceRow: sourceRow,
            destinationRow: sourceRow,
            snapshotOffset: snapshotOffset,
            touchPosition: touchPosition
        )
        reorderState = .reordering(context: context)

        delegate.tableViewDidBeginReordering(tableView, at: sourceRow)
    }
    
    func updateReorder(touchPosition: CGPoint) {
        guard case .reordering(let context) = reorderState else { return }
        
        var newContext = context
        newContext.touchPosition = touchPosition
        reorderState = .reordering(context: newContext)
        
        updateSnapshotViewPosition()
        updateDestinationRow()
    }
    
    func endReorder() {
        guard case .reordering(let context) = reorderState,
            let tableView = tableView,
            let superview = tableView.superview
        else { return }
        
        var cellRectInTableView = tableView.rectForRow(at: context.destinationRow)
        if context.destinationRow.section != context.sourceRow.section {
             cellRectInTableView = tableView.rectForRow(at: context.sourceRow)
        }
        
        reorderState = .ready(snapshotRow: context.destinationRow)
        
       
        let cellRect = tableView.convert(cellRectInTableView, to: superview)
        let cellRectCenter = CGPoint(x: cellRect.midX, y: cellRect.midY)
        
        // If no values change inside a UIView animation block, the completion handler is called immediately.
        // This is a workaround for that case.
        if snapshotView?.center == cellRectCenter {
            snapshotView?.center.y += 0.1
        }
        
        UIView.animate(withDuration: animationDuration,
            animations: {
                self.snapshotView?.center = CGPoint(x: cellRect.midX, y: cellRect.midY)
            },
            completion: { _ in
                if case let .ready(snapshotRow) = self.reorderState, let row = snapshotRow {
                    self.reorderState = .ready(snapshotRow: nil)
                    UIView.performWithoutAnimation {
                        if context.destinationRow.section == context.sourceRow.section {
                             tableView.reloadRows(at: [row], with: .none)
                        }
                    }
                    self.removeSnapshotView()
                }
            }
        )
        animateSnapshotViewOut()
        clearAutoScrollDisplayLink()
        if context.destinationRow.section != context.sourceRow.section {
            delegate?.tableViewDidFinishReordering(tableView, from: context.sourceRow, to: context.sourceRow)
        } else {
            delegate?.tableViewDidFinishReordering(tableView, from: context.sourceRow, to: context.destinationRow)
        }
     
    }
    
    // MARK: - Spacer cell
    private func createSpacerCell() -> UITableViewCell? {
        guard let snapshotView = snapshotView else { return nil }
        
        let cell = UITableViewCell()
        let height = snapshotView.bounds.height
        
        let heightConstraint = NSLayoutConstraint(item: cell, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 0, constant: height)
        heightConstraint.priority = .fittingSizeLevel
        heightConstraint.isActive = true
        
        let hideCell: Bool
        switch spacerCellStyle {
        case .automatic: hideCell = tableView?.style == .grouped
        case .hidden: hideCell = true
        case .transparent: hideCell = false
        }
        
        if hideCell {
            cell.isHidden = true
        } else {
            cell.backgroundColor = .clear
        }
        
        return cell
    }
    
}
