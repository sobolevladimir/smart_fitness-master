//
//  InstantPanGestureRecognizer.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 02.12.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

/// A pan gesture that enters into the `began` state on touch down instead of waiting for a touches moved event.
class InstantPanGestureRecognizer: UIPanGestureRecognizer {
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent) {
//        guard state != .began else { return }
//        super.touchesBegan(touches, with: event)
//        state = .began
//    }
}
