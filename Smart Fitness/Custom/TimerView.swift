//
//  TimerView.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 08.01.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

class TimerView: UIView {
    
    // MARK: - Properties
    private var trackLayer: CAShapeLayer!
    private let progressStroke = CAShapeLayer()
    
    var progress: CGFloat = 1 {
        didSet {
            progressStroke.strokeEnd = progress
        }
    }
    
    private var startColor: UIColor = Constants.Colors.orangeLight
    private var endColor:   UIColor = Constants.Colors.orangeDark
    private var lineWidth:  CGFloat = 8
    
    private let gradientLayer: CAGradientLayer = {
        let gradientLayer = CAGradientLayer()
        if #available(iOS 12.0, *) {
            gradientLayer.type = .conic
        } else {
            // Fallback on earlier versions
        }
        gradientLayer.startPoint = CGPoint(x: 0.5, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0.5)
        return gradientLayer
    }()
    
    // MARK: - Init
    override func awakeFromNib() {
        super.awakeFromNib()
        setupCircleLayers()
        setupStart()
        configure()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = frame.height / 2
        setupCircleLayers()
        updateGradient()
    }
    
    // MARK: - Private
    private func createCircleShapeLayer(strokeColor: UIColor, lineWidth: CGFloat, endAngle: CGFloat) -> CAShapeLayer {
        let layer = CAShapeLayer()
        let radius = (min(bounds.width, bounds.height) - lineWidth) / 2
        let circularPath = UIBezierPath(arcCenter: .zero, radius: radius, startAngle: 0, endAngle: endAngle, clockwise: true)
        layer.path = circularPath.cgPath
        layer.strokeColor = strokeColor.cgColor
        layer.lineWidth = lineWidth
        layer.fillColor = UIColor.clear.cgColor
        layer.lineCap = CAShapeLayerLineCap.round
        layer.position = CGPoint(x: bounds.midX, y: bounds.midY)
        return layer
    }
    
    private func setupCircleLayers() {
        trackLayer = createCircleShapeLayer(strokeColor: Constants.Colors.graySuperLight, lineWidth: lineWidth, endAngle: 2 * CGFloat.pi)
    }
    
    private func animateLoading(layer: CAShapeLayer) {
        layer.strokeEnd = 1
    }
    
    private func setupStart() {
        layer.addSublayer(trackLayer)
    }
    
    func configure() {
        layer.addSublayer(gradientLayer)
    }
    
    func updateGradient() {
        gradientLayer.frame = bounds
        gradientLayer.colors = [startColor, endColor, startColor].map { $0.cgColor }
        
        progressStroke.fillColor = UIColor.clear.cgColor
        progressStroke.strokeColor = UIColor.white.cgColor
        progressStroke.lineWidth = lineWidth
        progressStroke.path = trackLayer.path
        progressStroke.lineCap = CAShapeLayerLineCap.round
        progressStroke.position = CGPoint(x: bounds.midX, y: bounds.midY)
        gradientLayer.transform = CATransform3DMakeRotation(-CGFloat.pi / 2, 0, 0, 1)
        gradientLayer.mask = progressStroke
        animateLoading(layer: progressStroke)
    }
}

