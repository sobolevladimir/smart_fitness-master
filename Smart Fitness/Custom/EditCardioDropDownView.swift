//
//  EditCardioDropDownView.swift
//  Smart Fitness
//
//  Created by Rusłan Chamski on 30/11/2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class EditCardioDropDownView: UIView {

    // MARK: - Properties

    // MARK: - Init
    override func layoutSubviews() {
        super.layoutSubviews()
        self.setNeedsDisplay()
    }

    override func draw(_ rect: CGRect) {
        let color = UIColor(red: 1.000, green: 1.000, blue: 1.000, alpha: 1.000)

        let bezier2Path = UIBezierPath()
        bezier2Path.move(to: CGPoint(x: 109.85, y: 10.14))
        bezier2Path.addCurve(to: CGPoint(x: 112.46, y: 10.39), controlPoint1: CGPoint(x: 111.19, y: 10.14), controlPoint2: CGPoint(x: 111.86, y: 10.14))
        bezier2Path.addLine(to: CGPoint(x: 112.58, y: 10.43))
        bezier2Path.addCurve(to: CGPoint(x: 114.27, y: 12.56), controlPoint1: CGPoint(x: 113.37, y: 10.79), controlPoint2: CGPoint(x: 113.99, y: 11.57))
        bezier2Path.addCurve(to: CGPoint(x: 114.5, y: 16.01), controlPoint1: CGPoint(x: 114.5, y: 13.47), controlPoint2: CGPoint(x: 114.5, y: 14.32))
        bezier2Path.addLine(to: CGPoint(x: 114.5, y: 43.63))
        bezier2Path.addCurve(to: CGPoint(x: 114.3, y: 46.93), controlPoint1: CGPoint(x: 114.5, y: 45.32), controlPoint2: CGPoint(x: 114.5, y: 46.17))
        bezier2Path.addLine(to: CGPoint(x: 114.27, y: 47.08))
        bezier2Path.addCurve(to: CGPoint(x: 112.58, y: 49.21), controlPoint1: CGPoint(x: 113.99, y: 48.07), controlPoint2: CGPoint(x: 113.37, y: 48.85))
        bezier2Path.addCurve(to: CGPoint(x: 109.85, y: 49.5), controlPoint1: CGPoint(x: 111.86, y: 49.5), controlPoint2: CGPoint(x: 111.19, y: 49.5))
        bezier2Path.addLine(to: CGPoint(x: 5.15, y: 49.5))
        bezier2Path.addCurve(to: CGPoint(x: 2.54, y: 49.25), controlPoint1: CGPoint(x: 3.81, y: 49.5), controlPoint2: CGPoint(x: 3.14, y: 49.5))
        bezier2Path.addLine(to: CGPoint(x: 2.42, y: 49.21))
        bezier2Path.addCurve(to: CGPoint(x: 0.73, y: 47.08), controlPoint1: CGPoint(x: 1.63, y: 48.85), controlPoint2: CGPoint(x: 1.01, y: 48.07))
        bezier2Path.addCurve(to: CGPoint(x: 0.5, y: 43.63), controlPoint1: CGPoint(x: 0.5, y: 46.17), controlPoint2: CGPoint(x: 0.5, y: 45.32))
        bezier2Path.addLine(to: CGPoint(x: 0.5, y: 16.01))
        bezier2Path.addCurve(to: CGPoint(x: 0.7, y: 12.71), controlPoint1: CGPoint(x: 0.5, y: 14.32), controlPoint2: CGPoint(x: 0.5, y: 13.47))
        bezier2Path.addLine(to: CGPoint(x: 0.73, y: 12.56))
        bezier2Path.addCurve(to: CGPoint(x: 2.42, y: 10.43), controlPoint1: CGPoint(x: 1.01, y: 11.57), controlPoint2: CGPoint(x: 1.63, y: 10.79))
        bezier2Path.addCurve(to: CGPoint(x: 5.15, y: 10.14), controlPoint1: CGPoint(x: 3.14, y: 10.14), controlPoint2: CGPoint(x: 3.81, y: 10.14))
        bezier2Path.addLine(to: CGPoint(x: 28.24, y: 10.14))
        bezier2Path.addLine(to: CGPoint(x: 33.21, y: 1.5))
        bezier2Path.addLine(to: CGPoint(x: 38.17, y: 10.14))
        bezier2Path.addLine(to: CGPoint(x: 109.85, y: 10.14))
        bezier2Path.close()
        color.setFill()
        bezier2Path.fill()
        Constants.Colors.grayLight.setStroke()
        bezier2Path.lineWidth = 1
        bezier2Path.stroke()
    }
}

