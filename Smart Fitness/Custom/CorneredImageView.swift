//
//  CorneredImageView.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 29.01.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

class CorneredImageView: UIImageView {
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
            layer.cornerRadius = cornerRadius
        }
    }
    @IBInspectable var circleImage: Bool = false
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if circleImage {
            layer.cornerRadius = bounds.height / 2
            layer.masksToBounds = true
        }
    }
}
