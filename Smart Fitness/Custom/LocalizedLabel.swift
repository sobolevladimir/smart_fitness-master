//
//  LocalizedLabel.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 13.04.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

class LocalizedLabel: UILabel {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        switch LocalizationManager.Language.language {
            
        case .english:
            self.textAlignment = .left
        case .hebrew:
            self.textAlignment = .right
        }
    }
}
