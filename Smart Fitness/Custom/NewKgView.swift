//
//  NewKgView.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 27.01.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import UIKit

class NewKgView: UIView {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.setNeedsDisplay()
    }
    
    override func draw(_ rect: CGRect) {
        //   let x = bounds.minY
        //    let y = bounds.minY
        //    let color = UIColor(red: 1.000, green: 1.000, blue: 1.000, alpha: 1.000)
        Constants.Colors.grayLight.setStroke()
        let rectanglePath = UIBezierPath(roundedRect: CGRect(x: 0, y: 7, width: bounds.maxX, height: bounds.maxY - 7), cornerRadius: 4)
        //     UIColor.gray.setFill()
        //    rectanglePath.fill()
        rectanglePath.lineWidth = 1
        rectanglePath.stroke()
        
        
        //// Bezier Drawing
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: 15, y: 7))
        bezierPath.addLine(to: CGPoint(x: 19, y: 0))
        bezierPath.addLine(to: CGPoint(x: 23, y: 7))
        
        //  UIColor.gray.setFill()
        //  bezierPath.fill()
        //// Bezier 3 Drawing
        let bezier3Path = UIBezierPath()
        bezier3Path.move(to: CGPoint(x: 16.88, y: 5))
        bezier3Path.addCurve(to: CGPoint(x: 15.02, y: 7), controlPoint1: CGPoint(x: 16.88, y: 5), controlPoint2: CGPoint(x: 16.05, y: 6.12))
        bezier3Path.addCurve(to: CGPoint(x: 11.5, y: 8), controlPoint1: CGPoint(x: 14, y: 7.88), controlPoint2: CGPoint(x: 11.5, y: 8))
        bezier3Path.addLine(to: CGPoint(x: 26, y: 8))
        bezier3Path.addCurve(to: CGPoint(x: 23, y: 7), controlPoint1: CGPoint(x: 26, y: 8), controlPoint2: CGPoint(x: 24, y: 8))
        bezier3Path.addCurve(to: CGPoint(x: 21, y: 5), controlPoint1: CGPoint(x: 22, y: 6), controlPoint2: CGPoint(x: 21, y: 5))
        bezier3Path.addLine(to: CGPoint(x: 16.88, y: 5))
        UIColor.white.setFill()
        bezier3Path.fill()
        //  color.setFill()
        // bezier2Path.fill()
        
        bezierPath.lineWidth = 1
        bezierPath.stroke()
    }
}
