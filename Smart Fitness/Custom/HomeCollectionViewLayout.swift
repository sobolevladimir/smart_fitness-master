//
//  HomeCollectionViewLayout.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 29.10.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

protocol HomeLayoutDelegate: AnyObject {
    func collectionView(
        _ collectionView: UICollectionView,
        heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat
}

final class HomeCollectionViewLayout: UICollectionViewLayout {
    
    enum Element: String {
        case sectionHeader
        case cell
    }
   
    weak var delegate: HomeLayoutDelegate?
   
    private let numberOfColumns = 2
    private let cellPadding: CGFloat = 10
   
    private var cache = [Element: [IndexPath: UICollectionViewLayoutAttributes]]()
  
    private var contentHeight: CGFloat = 0
    
    private var contentWidth: CGFloat {
        guard let collectionView = collectionView else {
            return 0
        }
        let insets = collectionView.contentInset
        return collectionView.bounds.width - (insets.left + insets.right)
    }
  
    override var collectionViewContentSize: CGSize {
        return CGSize(width: contentWidth, height: contentHeight)
    }
    
    override public func invalidateLayout() {
        cache.removeAll()
        contentHeight = 0
        
        super.invalidateLayout()
    }
    
    override func prepare() {
        prepareCache()
        guard  let collectionView = collectionView else { return }
        let columnWidth = contentWidth / CGFloat(numberOfColumns)
        var xOffset: [CGFloat] = []
        for column in 0..<numberOfColumns {
            xOffset.append(CGFloat(column) * columnWidth)
        }
        
        for section in 0..<collectionView.numberOfSections {
            let headerSize = CGSize(width: collectionView.bounds.width, height: 32)
            let headerX = (contentWidth - headerSize.width) / 2
            let headerFrame = CGRect(
                origin: CGPoint(
                    x: headerX,
                    y: contentHeight
                ),
                size: headerSize
            )
            let headerAttributes = UICollectionViewLayoutAttributes(
                forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                with: IndexPath(item: 0, section: section)
            )
            headerAttributes.frame = headerFrame
            cache[.sectionHeader]?[headerAttributes.indexPath] = headerAttributes
            
            contentHeight = headerFrame.maxY
            
            
            var yOffsets = [CGFloat](
                repeating: contentHeight,
                count: numberOfColumns
            )
            
            for item in 0..<collectionView.numberOfItems(inSection: section) {
                let indexPath = IndexPath(item: item, section: section)
                let photoHeight = delegate?.collectionView(
                    collectionView,
                    heightForPhotoAtIndexPath: indexPath) ?? 10
                let column = yOffsets.firstIndex(of: yOffsets.min() ?? 0) ?? 0
                let height = cellPadding * 2 + photoHeight
                let frame = CGRect(x: xOffset[column],
                                   y: yOffsets[column],
                                   width: columnWidth,
                                   height: height)
                let insetFrame = frame.insetBy(dx: cellPadding, dy: cellPadding)
                let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
                attributes.frame = insetFrame
                cache[.cell]?[attributes.indexPath] = attributes
                
                contentHeight = max(contentHeight, frame.maxY)
                yOffsets[column] = yOffsets[column] + height
                
            }
        }
    }
    
    private func prepareCache() {
        cache.removeAll(keepingCapacity: true)
        cache[.sectionHeader] = [IndexPath: UICollectionViewLayoutAttributes]()
        cache[.cell] = [IndexPath: UICollectionViewLayoutAttributes]()
    }
    
    override func layoutAttributesForElements(in rect: CGRect)
        -> [UICollectionViewLayoutAttributes]? {
            var visibleLayoutAttributes: [UICollectionViewLayoutAttributes] = []
            
            for (_, elementInfos) in cache {
                for (_, attributes) in elementInfos {
                    if attributes.frame.intersects(rect) {
                        visibleLayoutAttributes.append(attributes)
                    }
                }
            }
            return visibleLayoutAttributes
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return cache[.cell]?[indexPath]
    }
    
    override func layoutAttributesForSupplementaryView(ofKind elementKind: String, at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        switch elementKind {
        case UICollectionView.elementKindSectionHeader:
            return cache[.sectionHeader]?[indexPath]
        default:
            return nil
        }
    }
}
