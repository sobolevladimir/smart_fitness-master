//
//  LanguageDirectionButton.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 19.12.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class SpaceImageButton: UIButton {
    
    // MARK: - Init
    override func awakeFromNib() {
        super.awakeFromNib()
        switch LocalizationManager.Language.language {
        case .english:
            self.imageEdgeInsets.left = -16
            self.imageEdgeInsets.right = 0
        case .hebrew:
            self.imageEdgeInsets.right = -16
            self.imageEdgeInsets.left = 0
        }
    }
}
