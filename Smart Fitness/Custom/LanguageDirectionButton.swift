//
//  LanguageDirectionButton.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 19.12.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import UIKit

final class LanguageDirectionButton: UIButton {
    
    // MARK: - Init
    override func awakeFromNib() {
        super.awakeFromNib()
        switch LocalizationManager.Language.language {
        case .english:
            if self.imageView?.image?.imageOrientation != UIImage.Orientation.up {
                guard var image = self.imageView?.image else { return }
                guard var disabledImage = self.image(for: .disabled) else { return }
                image = UIImage(cgImage: image.cgImage!, scale: image.scale, orientation: UIImage.Orientation.up)
                disabledImage = UIImage(cgImage: disabledImage.cgImage!, scale: disabledImage.scale, orientation: UIImage.Orientation.up)
                self.setImage(image, for: .normal)
                self.setImage(disabledImage, for: .disabled)
                
            }
        case .hebrew:
            break
        }
        
    }
}
