//
//  ShortLink.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 15.03.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import Foundation

struct ShortLink: Decodable {
    let shortLink: String
}
