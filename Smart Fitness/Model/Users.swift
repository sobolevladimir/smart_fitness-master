//
//  Users.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 13.02.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import Foundation

class Users: Codable {
    var componentList: [User]
    var total: Int
}

