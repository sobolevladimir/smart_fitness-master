//
//  User.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 23.12.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import Foundation

struct Token: Codable {
    var token: String?
}

class User: NSObject, Codable {
    
    // MARK: - Types
    enum CodingKeys: String, CodingKey {
        case id
        case email
        case firstName
        case lastName
        case gender
        case imageUrl
        case username
        case level
        case height
        case weight
        case birthDay
        case hasSubscription
    }
    
    // MARK: - Properties
    private(set) var id: Int?
    private(set) var email: String?
    private(set) var firstName: String?
    private(set) var lastName: String?
    var gender: String?
    var level: String?
    var height: Int?
    var weight: Double?
    var birthDay: Double?
    private(set) var imageUrl: String?
    private(set) var username: String?
    private(set) var hasSubscription: Bool?
    
 

    // MARK: - Encoding
    override init() {
        super.init()
    }
    
    required init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        email = try container.decodeIfPresent(String.self, forKey: .email)
        id = try container.decodeIfPresent(Int.self, forKey: .id)
        firstName = try container.decodeIfPresent(String.self, forKey: .firstName)
        lastName = try container.decodeIfPresent(String.self, forKey: .lastName)
        gender = try container.decodeIfPresent(String.self, forKey: .gender)
        imageUrl = try container.decodeIfPresent(String.self, forKey: .imageUrl)
        username = try container.decodeIfPresent(String.self, forKey: .username)
        level = try container.decodeIfPresent(String.self, forKey: .level)
        height = try container.decodeIfPresent(Int.self, forKey: .height)
        weight = try container.decodeIfPresent(Double.self, forKey: .weight)
        birthDay = try container.decodeIfPresent(Double.self, forKey: .birthDay)
        hasSubscription = try container.decodeIfPresent(Bool.self, forKey: .hasSubscription)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(email, forKey: .email)
        try container.encode(firstName, forKey: .firstName)
        try container.encode(lastName, forKey: .lastName)
        try container.encode(gender, forKey: .gender)
        try container.encode(imageUrl, forKey: .imageUrl)
        try container.encode(username, forKey: .username)
        try container.encode(level, forKey: .level)
        try container.encode(height, forKey: .height)
        try container.encode(weight, forKey: .weight)
        try container.encode(birthDay, forKey: .birthDay)
    }
}

struct Stories: Codable {
    var id: Int
    var s3Urls: [Story]
}

struct Story: Codable {
    var id: Int
    var s3Urls: String
    var date: Int64
}

