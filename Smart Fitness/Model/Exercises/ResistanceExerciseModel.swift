//
//  ResistanceExerciseModel.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 13.12.2019.
//  Copyright © 2019 Ruslan Khamskyi. All rights reserved.
//

import Foundation

struct ResistanceExerciseModel: Codable {
    enum CodingKeys: String, CodingKey {
        case exercise
        case exercisesForSuperset
        case method
        case supersetMethodData
        case percentMethodData
        case dropSetMethodData
        case isExpanded
        case id
    }
    
    var exercise: Exercise
    var exercisesForSuperset: [Exercise] = []
    var method: DropSetMethodViewController.MethodType
    var supersetMethodData: [ExerciseSupersetSetCell.Content] = []
    var percentMethodData: ExercisePercentMainCell.Content?
    var dropSetMethodData: ExerciseDropSetMainCell.Content?
    var isExpanded: Bool = false
    var id: Int?
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(exercise, forKey: .exercise)
        try container.encode(exercisesForSuperset, forKey: .exercisesForSuperset)
        try container.encode(method, forKey: .method)
        try container.encode(supersetMethodData, forKey: .supersetMethodData)
        try container.encode(percentMethodData, forKey: .percentMethodData)
        try container.encode(dropSetMethodData, forKey: .dropSetMethodData)
        try container.encode(id, forKey: .id)
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
         id = try container.decodeIfPresent(Int.self, forKey: .id)
         let exercise = try container.decode(Exercise.self, forKey: .exercise)
        self.exercise = exercise
        
        if let exForSuperset = try container.decode([Exercise]?.self, forKey: .exercisesForSuperset) {
            exercisesForSuperset = exForSuperset
        } else {
            exercisesForSuperset = []
        }
        
        method = try container.decode(DropSetMethodViewController.MethodType.self, forKey: .method)
        
        if let supersetMData = try container.decode([ExerciseSupersetSetCell.Content]?.self, forKey: .supersetMethodData) {
            supersetMethodData = supersetMData
        } else {
            supersetMethodData = []
        }
        percentMethodData = try container.decode(ExercisePercentMainCell.Content?.self, forKey: .percentMethodData)
        dropSetMethodData = try container.decode(ExerciseDropSetMainCell.Content?.self, forKey: .dropSetMethodData)
        
        isExpanded = false
    }
    
    init(exercise: Exercise,
         exercisesForSuperset: [Exercise] = [],
         method: DropSetMethodViewController.MethodType,
         supersetMethodData: [ExerciseSupersetSetCell.Content] = [],
         percentMethodData: ExercisePercentMainCell.Content? = nil,
         dropSetMethodData: ExerciseDropSetMainCell.Content? = nil, id: Int? = nil) {
        self.exercise = exercise
        self.exercisesForSuperset = exercisesForSuperset
        self.method = method
        self.supersetMethodData = supersetMethodData
        self.percentMethodData = percentMethodData
        self.dropSetMethodData = dropSetMethodData
        self.id = id
    }
}
