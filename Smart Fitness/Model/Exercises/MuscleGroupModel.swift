//
//  MuscleGroupModel.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 17.01.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import Foundation

struct CategoryJSON: Decodable {
    var componentList: [MuscleGroupModel]
    var total: Int
}

struct MuscleGroupModel: Decodable {
    var category: String
    var subcategory: [Subcategory]
}

struct Subcategory: Decodable {
    var id: Int
    var muscleGroup: String
    var category: String
    var title: String
    var imageUrl: String?
    var exercises: [Exercise]
}

struct Exercises: Decodable {
    var componentList: [Exercise]
    var total: Int
}

struct Exercise {
    var id: Int
    var title: String
    var descriptions: [Description]
    var videoUrl: String?
    var mainImageUrl: String?
    var exerciseImages: [ImageContent]
    
    var favourite: Bool
    var isSelected: Bool
    
    var notes: String
    var isExpanded: Bool
    var category: Category
    var setsValue: Int
    var repsValue: Int
    var kgValue: [Double]
    var indexOfKg: Int
    var repsTime: String
    var restTime: String
    var machineNumber: Int
    var programExerciseId: Int?
    
    var privateId: String?
    
    init(id: Int,
         title: String,
         descriptions: [Description],
         videoUrl: String?,
         mainImageUrl: String?,
         exerciseImages: [ImageContent],
         favourite: Bool,
         category: Category,
         programExerciseId: Int?,
         setsValue: Int,
         repsValue: Int,
         kgValue: [Double],
         privateId: String,
         repsTime: String = "",
         indexOfKg: Int,
         notes: String,
         restTime: String,
         machineNumber: Int) {
        
        self.id = id
        self.title = title
        self.descriptions = descriptions
        self.videoUrl = videoUrl
        self.mainImageUrl = mainImageUrl
        self.exerciseImages = exerciseImages
        self.favourite = favourite
        self.category = category
        
        self.isSelected = false
        self.notes = notes
        self.isExpanded = false
        self.setsValue =  setsValue
        self.repsValue = repsValue
        self.kgValue = kgValue
        self.indexOfKg = indexOfKg
        self.repsTime = repsTime
        self.restTime = restTime
        self.machineNumber = machineNumber
        self.programExerciseId = programExerciseId
        self.privateId = privateId
    }
    
    
    struct ImageContent: Codable {
        var id: Int
        var url: String?
    }
    
    struct Description: Codable {
        var title: String?
        var description: String?
    }
}

extension Exercise: Codable {
    enum ExerciseKeys: String, CodingKey {
        case id = "id"
        case title = "title"
        case descriptions = "descriptions"
        case videoUrl = "videoUrl"
        case mainImageUrl = "mainImageUrl"
        case exerciseImages = "exerciseImages"
        
        case favourite = "favourite"
        case isSelected = "isSelected"
 
        case category = "category"
        
        case programExerciseId
        case setsValue
        case repsValue
        case kgValue
        
        case privateId
        case repsTime
        
        case indexOfKg
        
        case notes
        
        case restTime
        case machineNumber
  
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ExerciseKeys.self)
        let id: Int = try container.decode(Int.self, forKey: .id)
        let title: String = try container.decode(String.self, forKey: .title)
        let descriptions: [Description] = try container.decode([Description].self, forKey: .descriptions)
        let videoUrl: String? = try container.decode(String?.self, forKey: .videoUrl)
        let mainImageUrl: String? = try container.decode(String?.self, forKey: .mainImageUrl)
        let exerciseImages: [ImageContent] = try container.decode([ImageContent].self, forKey: .exerciseImages)
        let programExerciseId = try container.decodeIfPresent(Int.self, forKey: .programExerciseId)
        let favourite: Bool = try container.decode(Bool.self, forKey: .favourite)
   
        var category: Category = .machines
        if let categoryString: String = try container.decodeIfPresent(String.self, forKey: .category) {
            if let categoryFromServer = Category(rawValue: categoryString) {
                category = categoryFromServer
            }
        }
        
        var restTime: String = Constants.ResistanceExercises.Methods.restTimeDefault
        if let restTimeFromServer: String = try container.decodeIfPresent(String.self, forKey: .restTime) {
          restTime = restTimeFromServer
        }
        
        var machineNumber: Int = Constants.ResistanceExercises.Methods.defaultMachineNumber
        if let machineNumberFromServer: Int = try container.decodeIfPresent(Int.self, forKey: .machineNumber) {
            machineNumber = machineNumberFromServer
        }
        
        let setsValue: Int
        let repsValue: Int
        let kgValue: [Double]
        if let reps = try container.decodeIfPresent(Int.self, forKey: .repsValue) {
            repsValue = reps
        } else {
            repsValue = Constants.ResistanceExercises.Methods.supersetDefaultReps
        }
        
        if let sets = try container.decodeIfPresent(Int.self, forKey: .setsValue) {
            setsValue = sets
        } else {
            setsValue = Constants.ResistanceExercises.Methods.defaultAmountOfSets
        }
        
        if let kgs = try container.decodeIfPresent([Double].self, forKey: .kgValue) {
            kgValue = kgs
        } else {
            kgValue = [Constants.ResistanceExercises.Methods.supersetDefaultKg]
        }
        
        let privateId: String
        if let id = try container.decodeIfPresent(String.self, forKey: .privateId) {
                   privateId = id
               } else {
                   privateId = "1"
               }
        let repsTime: String
        if let time = try container.decodeIfPresent(String.self, forKey: .repsTime) {
                          repsTime = time
                      } else {
                          repsTime = ""
                      }
        let indexOfKg: Int
        if let  index = try container.decodeIfPresent(Int.self, forKey: .indexOfKg) {
            indexOfKg = index
        } else {
            indexOfKg = 0
        }
        
        let notes: String
       if let note = try container.decodeIfPresent(String.self, forKey: .notes) {
            notes = note
        } else {
            notes = ""
        }
        
        self.init(id: id, title: title, descriptions: descriptions, videoUrl: videoUrl, mainImageUrl: mainImageUrl, exerciseImages: exerciseImages, favourite: favourite, category: category, programExerciseId: programExerciseId, setsValue: setsValue, repsValue: repsValue, kgValue: kgValue, privateId: privateId, repsTime: repsTime, indexOfKg: indexOfKg, notes: notes, restTime: restTime, machineNumber: machineNumber)
        }
}

extension Exercise: Hashable, Equatable {
    
    func hash(into hasher: inout Hasher) { }
    
    static func == (lhs: Exercise, rhs: Exercise) -> Bool {
        return lhs.id == rhs.id && lhs.title == rhs.title && lhs.favourite == rhs.favourite
    }
}

extension Exercise {
    func isCustom() -> Bool {
        if self.descriptions.count == 0 && self.videoUrl == nil && self.exerciseImages.count == 0 {
            return true
        } else {
            return false
        }
    }
}
