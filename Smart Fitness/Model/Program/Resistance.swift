//
//  Resistance.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 14.02.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import Foundation

struct Resistance: Codable {
    
    enum CodingKeys: String, CodingKey {
           case id
           case group
           case exercisesData
          
       }
       
    var id: Int?
    var group: ResistanceGroup
    var exercisesData: [ResistanceExerciseModel]
    
    init(from decoder: Decoder) throws {
       let container = try decoder.container(keyedBy: CodingKeys.self)
        
        id = try container.decodeIfPresent(Int.self, forKey: .id)
        
        group = try container.decode(ResistanceGroup.self, forKey: .group)
        
        if let exercisesData = try container.decode([ResistanceExerciseModel]?.self, forKey: .exercisesData) {
            self.exercisesData = exercisesData
        } else {
            self.exercisesData = []
        }
    }
    
    init(group: ResistanceGroup, exercisesData: [ResistanceExerciseModel], id: Int?) {
        self.group = group
        self.exercisesData = exercisesData
        self.id = id
    }
}

struct ResistanceGroup: Codable {
    var key: GroupsData.MuscleGroupKey
    var title: String { key.localizedString }
    var imageUrl: String { key.imageName }
    var group: SplitDays
    var id: String
    var orderNumber: Int? = 0
}
