//
//  Cardio.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 07.02.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import Foundation

struct Cardio: Codable {
    
    enum CodingKeys: String, CodingKey {
        case state
        case cardioType
        case items
        case totalTime
        case totalDistance
    }
    
    var state: CardioActivity
    var cardioType: CardioCollectionViewCell.Content
    var items: [CardioTableViewCell.Content]
    var totalTime: String = "00:00"
    var totalDistance: String = "0.0"
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        state = try container.decode(CardioActivity.self, forKey: .state)
        items = try container.decode([CardioTableViewCell.Content].self, forKey: .items)
        cardioType = CardioCollectionViewCell.Content(title: state.localizeString, imageName: state.imageName, isAdded: true, isSelected: false)
        totalTime = calculateTotalTime()
        totalDistance = calculateTotalDistance()
    }
    
    init(state: CardioActivity, cardioType: CardioCollectionViewCell.Content, items: [CardioTableViewCell.Content]) {
        self.state = state
        self.cardioType = cardioType
        self.items = items
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(items, forKey: .items)
        try container.encode(state, forKey: .state)
    }
    
    private func calculateTotalTime() -> String {
        let timeString = items.map({ $0.timeValue })
        var minutes: Int = 0
        var seconds: Int = 0
        timeString.forEach {
            minutes += Int(String($0.prefix(2))) ?? 0
            seconds += Int(String($0.suffix(2))) ?? 0
        }
        minutes += seconds / 60
        seconds = seconds % 60
        let minutesString = minutes < 10 ? "0\(minutes)" : "\(minutes)"
        let secondsString = seconds < 10 ? "0\(seconds)" : "\(seconds)"
        return "\(minutesString):\(secondsString)"
    }
    
    private func calculateTotalDistance() -> String {
        var kilometres: Int = 0
        var meters: Int = 0
        let distanceString = items.map({ $0.distanceValue })
        
        distanceString.forEach {
            if let idx = $0.firstIndex(of: ".") {
                let index = $0.distance(from: $0.startIndex, to: idx)
                kilometres += Int(String($0.prefix(index))) ?? 0
                meters += Int(String($0.suffix($0.count - index - 1))) ?? 0
            }
            kilometres += meters / 100
            meters = meters % 100
        }
        return "\(kilometres).\(meters)"
    }
    
}
