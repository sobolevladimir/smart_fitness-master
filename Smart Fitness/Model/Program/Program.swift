//
//  Program.swift
//  Smart Fitness
//
//  Created by Ruslan Khamskyi on 07.02.2020.
//  Copyright © 2020 Ruslan Khamskyi. All rights reserved.
//

import Foundation

struct SharedPrograms: Codable {
    var componentList: [SharedProgram]
      var total: Int
}

struct SharedProgram: Codable {
    var username: String?
    var date: Int64?
    var program: Program
    var accept: Bool
    var imageUrl: String?
    var id: Int
}

struct Programs: Codable {
    var componentList: [Program]
    var total: Int
}

struct Program: Codable {
    enum CodingKeys: String, CodingKey {
        case id
        case cardioProgram
        case resistanceProgram
        case circuitProgram
        case programInfo
        case programType
    }
    
    var id: Int?
    var cardioProgram: [Cardio] = []
    var resistanceProgram: [Resistance] = []
    var circuitProgram: [CircuitTrainingViewController.Content] = []
    var programInfo: ProgramInfoViewController.Content
    var programType: ProgramType
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(cardioProgram, forKey: .cardioProgram)
        try container.encode(resistanceProgram, forKey: .resistanceProgram)
        try container.encode(circuitProgram, forKey: .circuitProgram)
        try container.encode(id, forKey: .id)
        try container.encode(programInfo, forKey: .programInfo)
        try container.encode(programType, forKey: .programType)
    }
    
    init(from decoder: Decoder) throws {
             
    let container = try decoder.container(keyedBy: CodingKeys.self)
        
        if let cardio = try container.decode([Cardio]?.self, forKey: .cardioProgram) {
            cardioProgram = cardio
        } else {
           cardioProgram = []
        }
     
        
        if let resistance = try container.decode([Resistance]?.self, forKey: .resistanceProgram) {
            resistanceProgram = resistance
        } else {
            resistanceProgram = []
        }
        
        if let circuit = try container.decode([CircuitTrainingViewController.Content]?.self, forKey: .circuitProgram) {
            circuitProgram = circuit
        } else {
             circuitProgram = []
        }
        id = try container.decodeIfPresent(Int.self, forKey: .id)
    
        programInfo = try container.decode(ProgramInfoViewController.Content.self, forKey: .programInfo)
        programType = try container.decode(ProgramType.self, forKey: .programType )
    }
    
    init(id: Int? = nil, cardioProgram: [Cardio] = [] ,resistanceProgram: [Resistance] = [], circuitProgram: [CircuitTrainingViewController.Content] = [], programInfo: ProgramInfoViewController.Content, programType: ProgramType) {
        self.cardioProgram = cardioProgram
        self.resistanceProgram = resistanceProgram
        self.circuitProgram = circuitProgram
        self.programInfo = programInfo
        self.programType = programType
    }
}
