//
//  Model_Exercise.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on May 10, 2019

import Foundation

struct Model_Exercises : Codable {

        let exerciseId : String?
        let exerciseName : String?

        enum CodingKeys: String, CodingKey {
                case exerciseId = "exercise_id"
                case exerciseName = "exercise_name"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                exerciseId = try values.decodeIfPresent(String.self, forKey: .exerciseId)
                exerciseName = try values.decodeIfPresent(String.self, forKey: .exerciseName)
        }

}
