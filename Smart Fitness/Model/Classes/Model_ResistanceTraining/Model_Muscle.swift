//
//  Model_Muscle.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on May 10, 2019

import Foundation

struct Model_Muscle : Codable {

        let isSelected : Int?
        let muscleIcon : String?
        let muscleId : String?
        let muscleName : String?
        let splitsDay : String?

        enum CodingKeys: String, CodingKey {
                case isSelected = "isSelected"
                case muscleIcon = "muscle_icon"
                case muscleId = "muscle_id"
                case muscleName = "muscle_name"
                case splitsDay = "splits_day"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                isSelected = try values.decodeIfPresent(Int.self, forKey: .isSelected)
                muscleIcon = try values.decodeIfPresent(String.self, forKey: .muscleIcon)
                muscleId = try values.decodeIfPresent(String.self, forKey: .muscleId)
                muscleName = try values.decodeIfPresent(String.self, forKey: .muscleName)
                splitsDay = try values.decodeIfPresent(String.self, forKey: .splitsDay)
        }

}
