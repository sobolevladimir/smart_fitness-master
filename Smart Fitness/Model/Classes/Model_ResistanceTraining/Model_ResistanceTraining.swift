//
//  Model_ResistanceTraining.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on May 10, 2019

import Foundation
struct Model_ResistanceTraining : Codable {
        let exerciseData : [Model_Exercise]?
        let muscle : [Model_Muscle]?
        enum CodingKeys: String, CodingKey {
                case exerciseData = "exerciseData"
                case muscle = "muscle"
        }
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                exerciseData = try values.decodeIfPresent([Model_Exercise].self, forKey: .exerciseData)
                muscle = try values.decodeIfPresent([Model_Muscle].self, forKey: .muscle)
        }
}
