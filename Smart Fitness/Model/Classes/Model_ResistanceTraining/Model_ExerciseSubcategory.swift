//
//  Model_ExerciseSubcategory.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on May 10, 2019

import Foundation

struct Model_ExerciseSubcategory : Codable {

        let exerciseSubcategoryIcon : String?
        let exerciseSubcategoryId : String?
        let exerciseSubcategoryName : String?
        let exercises : [Model_Exercise]?
        let isFavourites : Int?
        let isSelected : Int?

        enum CodingKeys: String, CodingKey {
                case exerciseSubcategoryIcon = "exercise_subcategory_icon"
                case exerciseSubcategoryId = "exercise_subcategory_id"
                case exerciseSubcategoryName = "exercise_subcategory_name"
                case exercises = "exercises"
                case isFavourites = "isFavourites"
                case isSelected = "isSelected"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                exerciseSubcategoryIcon = try values.decodeIfPresent(String.self, forKey: .exerciseSubcategoryIcon)
                exerciseSubcategoryId = try values.decodeIfPresent(String.self, forKey: .exerciseSubcategoryId)
                exerciseSubcategoryName = try values.decodeIfPresent(String.self, forKey: .exerciseSubcategoryName)
                exercises = try values.decodeIfPresent([Model_Exercise].self, forKey: .exercises)
                isFavourites = try values.decodeIfPresent(Int.self, forKey: .isFavourites)
                isSelected = try values.decodeIfPresent(Int.self, forKey: .isSelected)
        }

}
