//
//  MODEL_subcategory.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on June 13, 2019

import Foundation

struct MODEL_subcategory : Codable {

        let categoryId : String?
        let subcategoryIcon : String?
        let subcategoryId : String?
        let subcategoryName : String?

        enum CodingKeys: String, CodingKey {
                case categoryId = "category_id"
                case subcategoryIcon = "subcategory_icon"
                case subcategoryId = "subcategory_id"
                case subcategoryName = "subcategory_name"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                categoryId = try values.decodeIfPresent(String.self, forKey: .categoryId)
                subcategoryIcon = try values.decodeIfPresent(String.self, forKey: .subcategoryIcon)
                subcategoryId = try values.decodeIfPresent(String.self, forKey: .subcategoryId)
                subcategoryName = try values.decodeIfPresent(String.self, forKey: .subcategoryName)
        }

}
