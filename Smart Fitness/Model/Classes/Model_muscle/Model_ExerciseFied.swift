//
//  Model_ExerciseFied.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on May 17, 2019

import Foundation

struct Model_ExerciseFied : Codable {

        let exerciseCategoryIcon : String?
        let exerciseCategoryId : String?
        let exerciseCategoryName : String?
        var exerciseSubcategory : [Model_ExerciseSubcategory]?

        enum CodingKeys: String, CodingKey {
                case exerciseCategoryIcon = "exercise_category_icon"
                case exerciseCategoryId = "exercise_category_id"
                case exerciseCategoryName = "exercise_category_name"
                case exerciseSubcategory = "exercise_subcategory"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                exerciseCategoryIcon = try values.decodeIfPresent(String.self, forKey: .exerciseCategoryIcon)
                exerciseCategoryId = try values.decodeIfPresent(String.self, forKey: .exerciseCategoryId)
                exerciseCategoryName = try values.decodeIfPresent(String.self, forKey: .exerciseCategoryName)
                exerciseSubcategory = try values.decodeIfPresent([Model_ExerciseSubcategory].self, forKey: .exerciseSubcategory)
        }

}
