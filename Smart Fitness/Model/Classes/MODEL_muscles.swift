//
//  MODEL_muscles.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on June 13, 2019

import Foundation

struct MODEL_muscles : Codable {

        var displayIndex : String?
        var isSelected : Int?
        let muscleIcon : String?
        let muscleId : String?
        let muscleName : String?
        let musclePlaceholderIcon : String?
        let muscleSplitIcon : String?
        var splitsDay : String?
        var selectedExercises:[MODEL_exercise]?
    
    enum CodingKeys: String, CodingKey {
                case displayIndex = "display_index"
                case isSelected = "isSelected"
                case muscleIcon = "muscle_icon"
                case muscleId = "muscle_id"
                case muscleName = "muscle_name"
                case musclePlaceholderIcon = "muscle_placeholder_icon"
                case muscleSplitIcon = "muscle_split_icon"
                case splitsDay = "splits_day"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                displayIndex = try values.decodeIfPresent(String.self, forKey: .displayIndex)
                isSelected = try values.decodeIfPresent(Int.self, forKey: .isSelected)
                muscleIcon = try values.decodeIfPresent(String.self, forKey: .muscleIcon)
                muscleId = try values.decodeIfPresent(String.self, forKey: .muscleId)
                muscleName = try values.decodeIfPresent(String.self, forKey: .muscleName)
                musclePlaceholderIcon = try values.decodeIfPresent(String.self, forKey: .musclePlaceholderIcon)
                muscleSplitIcon = try values.decodeIfPresent(String.self, forKey: .muscleSplitIcon)
                splitsDay = try values.decodeIfPresent(String.self, forKey: .splitsDay)
        }

}
