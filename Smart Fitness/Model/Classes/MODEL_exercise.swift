//
//  MODEL_exercise.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on June 13, 2019

import Foundation

struct MODEL_exercise : Codable {

        let categoryId : String?
        let exerciseDesc : String?
        let exerciseIcon : String?
        let exerciseId : String?
        let exerciseName : String?
        var isSelected : String?
        var isFavourites : String?
        let subcategoryId : String?
    
        enum CodingKeys: String, CodingKey {
                case categoryId = "category_id"
                case exerciseDesc = "exercise_desc"
                case exerciseIcon = "exercise_icon"
                case exerciseId = "exercise_id"
                case exerciseName = "exercise_name"
                case isSelected = "isSelected"
                case subcategoryId = "subcategory_id"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                categoryId = try values.decodeIfPresent(String.self, forKey: .categoryId)
                exerciseDesc = try values.decodeIfPresent(String.self, forKey: .exerciseDesc)
                exerciseIcon = try values.decodeIfPresent(String.self, forKey: .exerciseIcon)
                exerciseId = try values.decodeIfPresent(String.self, forKey: .exerciseId)
                exerciseName = try values.decodeIfPresent(String.self, forKey: .exerciseName)
                isSelected = try values.decodeIfPresent(String.self, forKey: .isSelected)
                subcategoryId = try values.decodeIfPresent(String.self, forKey: .subcategoryId)
        }

}
